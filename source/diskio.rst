DISKIO (CCP4: Library)
======================

NAME
----

**diskio** - CCP4 subroutine library for input/output

DESCRIPTION
-----------

Since CCP4 version 5.0, diskio.f has been replaced by a set of C
functions. These functions are wrapped by a `Fortran
API <C_library/diskio_f_page.html>`__ which reproduces the behaviour of
the old library.

This document describes the library as seen from a Fortran programmer's
perspective. There are routines for opening and closing files, reading
and writing a variety of data items, seeking within a file, etc.

Contents
--------

-  `List of subroutines <#list_of_routines>`__
-  `List of arguments <#list_of_arguments>`__
-  `Descriptions of the subroutines <#descriptions>`__

.. _list_of_routines: 
 


List of subroutines
-------------------

The calls provided in diskio are given below. The type and meaning of
the arguments is given in the next section.

Call *routine (argument list)*

Purpose

CALL `QOPEN <#qopen>`__ (IUNIT,LOGNAM,ATBUTE)

Open file

*CALL* `QQOPEN <#qqopen>`__ *(IUNIT,LOGNAM,ISTAT)*

*Open file: use QOPEN*

CALL `QREADI <#qreadi>`__ (IUNIT,BUFFER,NITEMS,IER)

Read nitems into integer array

CALL `QREADR <#qreadr>`__ (IUNIT,BUFFER,NITEMS,IER)

Read nitems into real array

CALL `QREADQ <#qreadq>`__ (IUNIT,BUFFER,NITEMS,IER)

Read nitems into complex array

CALL `QWRITI <#qwriti>`__ (IUNIT,BUFFER,NITEMS)

Write nitems from integer array

CALL `QWRITR <#qwritr>`__ (IUNIT,BUFFER,NITEMS)

Write nitems from real array

CALL `QWRITQ <#qwritq>`__ (IUNIT,BUFFER,NITEMS)

Write nitems from complex array

CALL `QQINQ <#qqinq>`__ (IUNIT,LOGNAM,FILNAM,LENGTH)

Get filename and length

VAR = `QISNAN <#qisnan>`__ (VALUE)

check for magic number

Transfered to `library.c <./library.html>`__

call qclose (int *iunit)

Close file

call qmode (int *iunit,int *mode,int *nmcitm)

Change mode

call qread (int *iunit, *buffer,init *nitems, int *ier)

Read nitems

call qreadc (int *iunit,char *buffer, int *ier (,int *nitems))

Read bytes into character var.

call qwrite (int *iunit, *buffer, int *nitems)

Write nitems

call qwritc (int *iunit, char * buffer (,int *nitems))

Write character

call qseek (int *iunit,int * irec, int *iel, int *lrecl)

Move to irec,iel

call qskip (int *iunit, int *lrecl)

Skip 1 record

call qback (int *iunit, int *lrecl)

Backspace 1 record

call qlocate (int *iunit,int *locate)

Get position in file

call qrarch (int *iunit, int *ioffset, int *ier)

set up number conversion

call qwarch (int *iunit, int *ioffset)

write conversion info

call qnan (union float_uint_uchar *value)

return canonical magic number

**Notes**

The use of QREAD / QWRITE is deprecated - use QREAD*a* / QWRITE*a*
( *a*=I,R,Q) with a buffer of the correct type.
.. _list_of_arguments: 
 


List of arguments
-----------------

In the subroutines listed above the arguments have the following
meanings:

**IUNIT**
   Variable returned by (Q)QOPEN to identify a file stream
   Note: *IUNIT is NOT a Fortran Unit number, but an internal
   identifier*
**LOGNAM**
   logical name (or file name) for the stream
**ATBUTE**
   File status for opening file
   = 'UNKNOWN', 'SCRATCH', 'OLD', 'NEW', or 'READONLY'
**ISTAT**
   | File status on opening the file:
   | 1, 'UNKNOWN' open as 'OLD'/'NEW' check existence
   | 2, 'SCRATCH' open as 'OLD' and delete on closing
   | 3, 'OLD' file MUST exist or program halts
   | 4, 'NEW' create (overwrite) new file
   | 5, 'READONLY' self explanatory

   NOTE: When using QQOPEN or QOPEN with ATBUTE = 'NEW' [ISTAT = 4], a
   check is made on the environment variable CCP4_OPEN - if this is set
   to UNKNOWN then the file is opened with attribute UNKNOWN rather than
   NEW to allow overwriting files that already exist.

**MODE**
   Access mode
   = 0, BYTES
   = 1, SHORT INT
   = 2, (REAL) WORD
   = 3, SHORT COMPLEX
   = 4, COMPLEX
   = 6, INTEGER
**NMCITM**
   No. of machine items (eg bytes) per element
**BUFFER**
   Starting location for data storage in core

   NOTE: This should normally be an array of full-word fortran items
   (REAL or INTEGER) or double-word (COMPLEX) in the case that you want
   to transfer complex numbers (mode 4). If necessary, unpack bytes
   using the routines provided in the library (or new ones). In
   particular, DON'T try to use BYTE or INTEGER*2 arrays, as these will
   likely cause alignment errors on RISC architectures.

**CHAR**
   CHARACTER*n buffer for transfer
**NITEMS**
   Number of elements to transfer
**IER**
   Error flag (0 = no error) else number of words transferred
**IREC**
   Desired record number (starts at 1)
**IEL**
   Desired element number within record (word) (starts at 1)
**LRECL**
   Record length in elements

*Note:* the number of channels and buffer length in words are set in
**#DEFINE** statements. .. _descriptions: 
 


Description of the Subroutines
------------------------------

.. _qopen: 
 
 **QOPEN**
   Open a file unit

   Usage:

   CALL QOPEN

   (IUNIT, LOGNAME, ATBUTE)

   INTEGER

   IUNIT

   CHARACTER*( * )

   LOGNAME, ATBUTE

   Input:

   IUNIT

   unit number to assign to file

   LOGNAME

   Logical name of file to open

   ATBUTE

   File status = 'UNKNOWN', 'SCRATCH', 'OLD', 'NEW', or 'READONLY'

   Output:

   None.

   Calls QQOPEN

.. _qqopen: 
 
 **QQOPEN**
   Open a file unit

   NOTE: the routine QOPEN (which calls QQOPEN) is to be preferred to
   calling QQOPEN directly

   Usage:

   CALL QQOPEN

   (IUNIT, LOGNAME, ISTAT)

   INTEGER

   IUNIT, ISTAT

   CHARACTER*( * )

   LOGNAME

   Input:

   LOGNAME

   Logical name of file to open

   ISTAT

   File status: 1 (UNKNOWN), 2 (SCRATCH), 3 (OLD), 4 (NEW) or 5
   (READONLY)

   Output:

   IUNIT

   | Integer handle assigned to file.
   | If negative the following error conditions occurred:
   | -1 No more streams left
   | -2 Could not open the file

   Calls COPEN in `library.c <./library.html>`__

.. _qreadi: 
 
 **QREADI**
   Read from IUNIT into BUFFER, NITEMS items

   Usage:

   CALL QREADI

   (IUNIT,BUFFER,NITEMS,IER)

   INTEGER

   IUNIT, NITEMS, IER

   INTEGER

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   Output:

   IER

   | 0 (no error)
   | -1 (EOF)
   | or number of items read

   BUFFER

   holds the items read

   Calls QREAD in `library.c <./library.html>`__

.. _qreadr: 
 
 **QREADR**
   Read from IUNIT into BUFFER, NITEMS items

   Usage:

   CALL QREADR

   (IUNIT,BUFFER,NITEMS,IER)

   INTEGER

   IUNIT, NITEMS, IER

   REAL

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   Output:

   IER

   | 0 (no error)
   | -1 (EOF)
   | or number of items read

   BUFFER

   holds the items read

   Calls QREAD in `library.c <./library.html>`__

.. _qreadq: 
 
 **QREADQ**
   Read from IUNIT into BUFFER, NITEMS items

   Usage:

   CALL QREADQ

   (IUNIT,BUFFER,NITEMS,IER)

   INTEGER

   IUNIT, NITEMS, IER

   COMPLEX

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   Output:

   IER

   | 0 (no error)
   | -1 (EOF)
   | or number of items read

   BUFFER

   holds the items read

   Calls QREAD in `library.c <./library.html>`__

.. _qwriti: 
 
 **QWRITI**
   Write to IUNIT from BUFFER, NITEMS items

   Usage:

   CALL QWRITI

   (IUNIT,BUFFER,NITEMS)

   INTEGER

   IUNIT, NITEMS

   INTEGER

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   BUFFER

   holds the items to write

   Output:

   None.

   Calls QWRITE in `library.c <./library.html>`__

.. _qwritr: 
 
 **QWRITR**
   Write to IUNIT from BUFFER, NITEMS items

   Usage:

   CALL QWRITR

   (IUNIT,BUFFER,NITEMS)

   INTEGER

   IUNIT, NITEMS

   REAL

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   BUFFER

   holds the items to write

   Output:

   None.

   Calls QWRITE in `library.c <./library.html>`__

.. _qwritq: 
 
 **QWRITQ**
   Write to IUNIT from BUFFER, NITEMS items

   Usage:

   CALL QWRITQ

   (IUNIT,BUFFER,NITEMS)

   INTEGER

   IUNIT, NITEMS

   COMPLEX

   BUFFER

   Input:

   IUNIT

   unit number assigned to file

   NITEMS

   number of items (item size set by QMODE)

   BUFFER

   holds the items to write

   Output:

   None.

   Calls QWRITE in `library.c <./library.html>`__

.. _qqinq: 
 
 **QQINQ**
   | check file name and size.
   | Check IUNIT first, if no success then try LOGNAM, if this fails use
     LOGNAM as filename.

   Usage:

   CALL QQINQ

   (IUNIT,LOGNAM,FILNAM,LENGTH)

   INTEGER

   IUNIT,LENGTH

   CHARACTER*( * )

   LOGNAM,FILNAM

   Input:

   IUNIT

   handle to check (as returned by QOPEN)

   LOGNAM

   Logical name

   Output:

   FILNAM

   the full file name or "" if no file

   LENGTH

   file size or -1 if no file

   Calls CQINQ in `library.c <./library.html>`__

.. _qisnan: 
 
 **QISNAN**
   Logical Function, check for `magic number'

   Usage:

   RESULT = QISNAN

   (VALUE)

   Input:

   VALUE

   REAL value to test

   Returns:

   .TRUE.

   if VALUE is a `magic number' indicating the absence of data.

   .FALSE.

   if value is a number.

   In the current implementation, this is a NaN in IEEE or Rop on a VAX
   or Convex native. Any NaN (or Infinity) will return .true.

   Uses CISNAN in `library.c <./library.html>`__

Authors of original routines
----------------------------

| David Agard (Phil Evans and John Campbell)
| Modified: For Unix/F77 using words (and bytes if available) (John
  Campbell)
| Modified: For ccp ascii header system implemented (Jan Zelinka)

--------------
