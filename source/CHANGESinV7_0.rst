CCP4 v7.0 Program Changes
=========================

**New programs:**

-  CCP4I2: new ccp4 interface
-  DIALS: data processing and integration
-  SHELX suite: co distribution for academic users
-  ARCIMBOLDO-LITE: molecular replacement pipeline

**Program updates:**

-  XIA2 - support for DIALS
-  PHASER 2.6.1 - improved
-  REFMAC -
-  MONOMER LIBRARY - corrections and new monomers
-  AMPLE
-  COOT 0.8.2 -
-  AIMLESS
-  POINTLESS
-  CTRUNCATE
-  DIMPLE
-  BLEND
-  CCP4MG 2.10.4 -
-  ARP/wARP 7.6 (academic) -
