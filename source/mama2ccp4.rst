MAMA2CCP4 (CCP4: Supported Program)
===================================

NAME
----

**mama2ccp4** - Convert between `mama' and Cambridge/CCP4 map formats.

SYNOPSIS
--------

**mama2ccp4 maskin** *foo.mask* **maskout** *bar.mask* [ **spgrp**
*space_group_name* ]

.. _description: 
 
DESCRIPTION
---------------------------------

MAMA2CCP4 converts masks between the formats of Alwyn Jones' MAMA
program and CCP4 "mode 0" mask format, suitable for use in programs such
as `MAPMASK <mapmask.html>`__, `NCSMASK <ncsmask.html>`__,
`MAPROT <maprot.html>`__ and `DM <dm.html>`__. The input file format is
detected automatically.

If the input file is a MAMA mask, then it must be in one of the
following mama formats: **old** with no identifier, **new** identified
in the first line by "NEW_MASK", **compressed** identified in the first
line by "COMPRESSED_MASK" or **very new** identified in the first line
by ".MASK_INPUT". If the input file is a CCP4 mask, then a MAMA mask
will be output in **compressed** format.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

If **spgrp** is given, then an output CCP4 mask will have that
spacegroup, otherwise P1. This has no effect when converting from CCP4
format.

.. _examples: 
 
EXAMPLES
---------------------------

From `mama' to CCP4
~~~~~~~~~~~~~~~~~~~~

::

   mama2ccp4 maskin chmi.o maskout chmi.msk spgrp P41212

From CCP4 to `mama'
~~~~~~~~~~~~~~~~~~~~

::

   mama2ccp4 maskin chmi.msk maskout chmi.o

SEE ALSO
--------

`mapmask <mapmask.html>`__, `ncsmask <ncsmask.html>`__,
`maprot <maprot.html>`__, `dm <dm.html>`__
