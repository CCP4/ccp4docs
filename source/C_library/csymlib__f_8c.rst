`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

csymlib_f.c File Reference
===========================

Fortran API for symmetry information. `More... <#_details>`__

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include <math.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_general.h"``
| ``#include "ccp4_parser.h"``
| ``#include "csymlib.h"``
| ``#include "cmtzlib.h"``
| ``#include "cvecmat.h"``

| 

Defines
-------

 #define 

**CSYMLIB_DEBUG**(x)

 #define 

**MSPAC**   4

 #define 

**MAXSYM**   192

 #define 

**MAXSYMOPS**   20

 #define 

**MAXLENSYMOPSTR**   80

| 

Functions
---------

 void 

**ccp4spg_mem_tidy** (void)

  

**FORTRAN_SUBR** (INVSYM, invsym,(const float a[4][4], float
ai[4][4]),(const float a[4][4], float ai[4][4]),(const float a[4][4],
float ai[4][4]))

 int 

**symfr_driver** (const char *line, float rot[MAXSYMOPS][4][4])

  

**FORTRAN_SUBR** (SYMFR3, symfr3,(const fpstr icol, const int *i1, int
*nsym, float rot[MAXSYM][4][4], int *eflag, int icol_len),(const
fpstr icol, const int *i1, int *nsym, float rot[MAXSYM][4][4], int
*eflag),(const fpstr icol, int icol_len, const int *i1, int *nsym,
float rot[MAXSYM][4][4], int *eflag))

  

**FORTRAN_SUBR** (SYMFR2, symfr2,(fpstr symchs, int *icol, int *nsym,
float rot[MAXSYM][4][4], int symchs_len),(fpstr symchs, int *icol, int
*nsym, float rot[MAXSYM][4][4]),(fpstr symchs, int symchs_len, int
*icol, int *nsym, float rot[MAXSYM][4][4]))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a14>`__ (SYMTR3, symtr3,(const int
*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int *iprint,
int symchs_len),(const int *nsm, const float rsm[MAXSYM][4][4], fpstr
symchs, const int *iprint),(const int *nsm, const float
rsm[MAXSYM][4][4], fpstr symchs, int symchs_len, const int *iprint))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a15>`__ (SYMTR4, symtr4,(const int
*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs_len),(const int *nsm, const float rsm[MAXSYM][4][4], fpstr
symchs),(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs,
int symchs_len))

  

**FORTRAN_SUBR** (PGMDF, pgmdf,(int *jlass, int *jcentr, int
jscrew[3]),(int *jlass, int *jcentr, int jscrew[3]),(int *jlass, int
*jcentr, int jscrew[3]))

  

**FORTRAN_SUBR** (PGDEFN, pgdefn,(fpstr nampg, int *nsymp, const int
*nsym, float rsmt[192][4][4], const ftn_logical *lprint, int
nampg_len),(fpstr nampg, int *nsymp, const int *nsym, float
rsmt[192][4][4], const ftn_logical *lprint),(fpstr nampg, int
nampg_len, int *nsymp, const int *nsym, float rsmt[192][4][4], const
ftn_logical *lprint))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a18>`__ (PGNLAU, pgnlau,(const fpstr
nampg, int *nlaue, fpstr launam, int nampg_len, int
launam_len),(const fpstr nampg, int *nlaue, fpstr launam),(const fpstr
nampg, int nampg_len, int *nlaue, fpstr launam, int launam_len))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a19>`__ (CCP4SPG_F_GET_LAUE,
ccp4spg_f_get_laue,(const int *sindx, int *nlaue, fpstr launam, int
launam_len),(const int *sindx, int *nlaue, fpstr launam),(const int
*sindx, int *nlaue, fpstr launam, int launam_len))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a20>`__ (HKLRANGE, hklrange,(int
*ihrng0, int *ihrng1, int *ikrng0, int *ikrng1, int *ilrng0, int
*ilrng1),(int *ihrng0, int *ihrng1, int *ikrng0, int *ikrng1, int
*ilrng0, int *ilrng1),(int *ihrng0, int *ihrng1, int *ikrng0, int
*ikrng1, int *ilrng0, int *ilrng1))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a21>`__ (PATSGP, patsgp,(const fpstr
spgnam, const fpstr pgname, fpstr patnam, int *lpatsg, int spgnam_len,
int pgname_len, int patnam_len),(const fpstr spgnam, const fpstr
pgname, fpstr patnam, int *lpatsg),(const fpstr spgnam, int
spgnam_len, const fpstr pgname, int pgname_len, fpstr patnam, int
patnam_len, int *lpatsg))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a22>`__ (ASUSET, asuset,(fpstr
spgnam, int *numsgp, fpstr pgname, int *msym, float rrsym[192][4][4],
int *msymp, int *mlaue, ftn_logical *lprint, int spgnam_len, int
pgname_len),(fpstr spgnam, int *numsgp, fpstr pgname, int *msym,
float rrsym[192][4][4], int *msymp, int *mlaue, ftn_logical
*lprint),(fpstr spgnam, int spgnam_len, int *numsgp, fpstr pgname,
int pgname_len, int *msym, float rrsym[192][4][4], int *msymp, int
*mlaue, ftn_logical *lprint))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a23>`__ (ASUSYM, asusym,(float
rassym[384][4][4], float rinsym[384][4][4], int *nisym),(float
rassym[384][4][4], float rinsym[384][4][4], int *nisym),(float
rassym[384][4][4], float rinsym[384][4][4], int *nisym))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a24>`__ (ASUPUT, asuput,(const int
ihkl[3], int jhkl[3], int *isym),(const int ihkl[3], int jhkl[3], int
*isym),(const int ihkl[3], int jhkl[3], int *isym))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a25>`__ (ASUGET, asuget,(const int
ihkl[3], int jhkl[3], const int *isym),(const int ihkl[3], int jhkl[3],
const int *isym),(const int ihkl[3], int jhkl[3], const int *isym))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a26>`__ (ASUPHP, asuphp,(const int
jhkl[3], const int *lsym, const int *isign, const float *phasin,
float *phasout),(const int jhkl[3], const int *lsym, const int
*isign, const float *phasin, float *phasout),(const int jhkl[3],
const int *lsym, const int *isign, const float *phasin, float
*phasout))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a27>`__ (CCP4SPG_F_LOAD_BY_NAME,
ccp4spg_f_load_by_name,(const int *sindx, fpstr namspg, int
namspg_len),(const int *sindx, fpstr namspg),(const int *sindx, fpstr
namspg, int namspg_len))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a28>`__ (CCP4SPG_F_LOAD_BY_OPS,
ccp4spg_f_load_by_ops,(const int *sindx, int *msym, float
rrsym[192][4][4]),(const int *sindx, int *msym, float
rrsym[192][4][4]),(const int *sindx, int *msym, float
rrsym[192][4][4]))

 

`FORTRAN_FUN <csymlib__f_8c.html#a29>`__ (int,
CCP4SPG_F_EQUAL_OPS_ORDER, ccp4spg_f_equal_ops_order,(int
*msym1, float rrsym1[192][4][4], int *msym2, float
rrsym2[192][4][4]),(int *msym1, float rrsym1[192][4][4], int *msym2,
float rrsym2[192][4][4]),(int *msym1, float rrsym1[192][4][4], int
*msym2, float rrsym2[192][4][4]))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a30>`__ (CCP4SPG_F_ASUPUT,
ccp4spg_f_asuput,(const int *sindx, const int ihkl[3], int jhkl[3],
int *isym),(const int *sindx, const int ihkl[3], int jhkl[3], int
*isym),(const int *sindx, const int ihkl[3], int jhkl[3], int *isym))

 

`FORTRAN_FUN <csymlib__f_8c.html#a31>`__ (int, INASU, inasu,(const int
ihkl[3], const int *nlaue),(const int ihkl[3], const int
*nlaue),(const int ihkl[3], const int *nlaue))

 

`FORTRAN_FUN <csymlib__f_8c.html#a32>`__ (int, CCP4SPG_F_INASU,
ccp4spg_f_inasu,(const int *sindx, const int ihkl[3]),(const int
*sindx, const int ihkl[3]),(const int *sindx, const int ihkl[3]))

  

**FORTRAN_SUBR** (PRTRSM, prtrsm,(const fpstr pgname, const int
*nsymp, const float rsymiv[192][4][4], int pgname_len),(const fpstr
pgname, const int *nsymp, const float rsymiv[192][4][4]),(const fpstr
pgname, int pgname_len, const int *nsymp, const float
rsymiv[192][4][4]))

 void 

**ccp4spg_register_by_ccp4_num** (int numspg)

 void 

**ccp4spg_register_by_symops** (int nops, float rsm[][4][4])

 

`FORTRAN_SUBR <csymlib__f_8c.html#a36>`__ (MSYMLB3, msymlb3,(const int
*ist, int *lspgrp, fpstr namspg_cif, fpstr namspg_cifs, fpstr nampg,
int *nsymp, int *nsym, float rlsymmmatrx[192][4][4], int
namspg_cif_len, int namspg_cifs_len, int nampg_len),(const int
*ist, int *lspgrp, fpstr namspg_cif, fpstr namspg_cifs, fpstr nampg,
int *nsymp, int *nsym, float rlsymmmatrx[192][4][4]),(const int *ist,
int *lspgrp, fpstr namspg_cif, int namspg_cif_len, fpstr
namspg_cifs, int namspg_cifs_len, fpstr nampg, int nampg_len, int
*nsymp, int *nsym, float rlsymmmatrx[192][4][4]))

  

**FORTRAN_SUBR** (MSYMLB, msymlb,(const int *ist, int *lspgrp, fpstr
namspg_cif, fpstr nampg, int *nsymp, int *nsym, float
rlsymmmatrx[192][4][4], int namspg_cif_len, int nampg_len),(const int
*ist, int *lspgrp, fpstr namspg_cif, fpstr nampg, int *nsymp, int
*nsym, float rlsymmmatrx[192][4][4]),(const int *ist, int *lspgrp,
fpstr namspg_cif, int namspg_cif_len, fpstr nampg, int nampg_len,
int *nsymp, int *nsym, float rlsymmmatrx[192][4][4]))

  

**FORTRAN_SUBR** (MSYGET, msyget,(const int *ist, int *lspgrp, int
*nsym, float rlsymmmatrx[192][4][4]),(const int *ist, int *lspgrp,
int *nsym, float rlsymmmatrx[192][4][4]),(const int *ist, int
*lspgrp, int *nsym, float rlsymmmatrx[192][4][4]))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a39>`__ (EPSLN, epsln,(const int
*nsm, const int *nsmp, const float rsm[192][4][4], const int
*iprint),(const int *nsm, const int *nsmp, const float
rsm[192][4][4], const int *iprint),(const int *nsm, const int *nsmp,
const float rsm[192][4][4], const int *iprint))

  

**FORTRAN_SUBR** (EPSLON, epslon,(const int ih[3], float *epsi, int
*isysab),(const int ih[3], float *epsi, int *isysab),(const int
ih[3], float *epsi, int *isysab))

  

**FORTRAN_SUBR** (CCP4SPG_F_EPSLON, ccp4spg_f_epslon,(const int
*sindx, const int ih[3], float *epsi, int *isysab),(const int
*sindx, const int ih[3], float *epsi, int *isysab),(const int
*sindx, const int ih[3], float *epsi, int *isysab))

  

**FORTRAN_SUBR** (SYSAB, sysab,(const int in[3], int *isysab),(const
int in[3], int *isysab),(const int in[3], int *isysab))

  

**FORTRAN_SUBR** (CCP4SPG_F_IS_SYSABS, ccp4spg_f_is_sysabs,(const
int *sindx, const int in[3], int *isysab),(const int *sindx, const
int in[3], int *isysab),(const int *sindx, const int in[3], int
*isysab))

 

`FORTRAN_SUBR <csymlib__f_8c.html#a44>`__ (CENTRIC, centric,(const int
*nsm, const float rsm[192][4][4], const int *iprint),(const int *nsm,
const float rsm[192][4][4], const int *iprint),(const int *nsm, const
float rsm[192][4][4], const int *iprint))

  

**FORTRAN_SUBR** (CENTR, centr,(const int ih[3], int *ic),(const int
ih[3], int *ic),(const int ih[3], int *ic))

  

**FORTRAN_SUBR** (CCP4SPG_F_IS_CENTRIC,
ccp4spg_f_is_centric,(const int *sindx, const int ih[3], int
*ic),(const int *sindx, const int ih[3], int *ic),(const int *sindx,
const int ih[3], int *ic))

  

**FORTRAN_SUBR** (CENTPHASE, centphase,(const int ih[3], float
*cenphs),(const int ih[3], float *cenphs),(const int ih[3], float
*cenphs))

  

**FORTRAN_SUBR** (CCP4SPG_F_CENTPHASE, ccp4spg_f_centphase,(const
int *sindx, const int ih[3], float *cenphs),(const int *sindx, const
int ih[3], float *cenphs),(const int *sindx, const int ih[3], float
*cenphs))

  

**FORTRAN_SUBR** (SETLIM, setlim,(const int *lspgrp, float
xyzlim[3][2]),(const int *lspgrp, float xyzlim[3][2]),(const int
*lspgrp, float xyzlim[3][2]))

  

**FORTRAN_SUBR** (SETGRD, setgrd,(const int *nlaue, const float
*sample, const int *nxmin, const int *nymin, const int *nzmin, int
*nx, int *ny, int *nz),(const int *nlaue, const float *sample,
const int *nxmin, const int *nymin, const int *nzmin, int *nx, int
*ny, int *nz),(const int *nlaue, const float *sample, const int
*nxmin, const int *nymin, const int *nzmin, int *nx, int *ny, int
*nz))

  

**FORTRAN_SUBR** (FNDSMP, fndsmp,(const int *minsmp, const int *nmul,
const float *sample, int *nsampl),(const int *minsmp, const int
*nmul, const float *sample, int *nsampl),(const int *minsmp, const
int *nmul, const float *sample, int *nsampl))

  

**FORTRAN_SUBR** (CALC_ORIG_PS, calc_orig_ps,(fpstr namspg_cif,
int *nsym, float rsym[192][4][4], int *norig, float orig[96][3],
ftn_logical *lpaxisx, ftn_logical *lpaxisy, ftn_logical *lpaxisz,
int namspg_cif_len),(fpstr namspg_cif, int *nsym, float
rsym[192][4][4], int *norig, float orig[96][3], ftn_logical *lpaxisx,
ftn_logical *lpaxisy, ftn_logical *lpaxisz, int
namspg_cif_len),(fpstr namspg_cif, int namspg_cif_len, int *nsym,
float rsym[192][4][4], int *norig, float orig[96][3], ftn_logical
*lpaxisx, ftn_logical *lpaxisy, ftn_logical *lpaxisz))

  

**FORTRAN_SUBR** (SETRSL, setrsl,(const float *a, const float *b,
const float *c, const float *alpha, const float *beta, const float
*gamma),(const float *a, const float *b, const float *c, const float
*alpha, const float *beta, const float *gamma),(const float *a,
const float *b, const float *c, const float *alpha, const float
*beta, const float *gamma))

  

**FORTRAN_SUBR** (STHLSQ1, sthlsq1,(float *reso, const int *ih, const
int *ik, const int *il),(float *reso, const int *ih, const int *ik,
const int *il),(flaot *reso, const int *ih, const int *ik, const int
*il))

  

**FORTRAN_SUBR** (STS3R41, sts3r41,(float *reso, const int *ih, const
int *ik, const int *il),(float *reso, const int *ih, const int *ik,
const int *il),(float *reso, const int *ih, const int *ik, const int
*il))

  

**FORTRAN_SUBR** (HANDCHANGE, handchange,(const int *lspgrp, float
*cx, float *cy, float *cz),(const int *lspgrp, float *cx, float
*cy, float *cz),(const int *lspgrp, float *cx, float *cy, float
*cz))

--------------

Detailed Description
--------------------

Fortran API for symmetry information.

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

FORTRAN_FUN

( 

int 

 ,

CCP4SPG_F_INASU 

 ,

ccp4spg_f_inasu 

 ,

(const int *sindx, const int ihkl[3]) 

 ,

(const int *sindx, const int ihkl[3]) 

 ,

(const int *sindx, const int ihkl[3]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection or it's      |
|                                      | Friedel mate is in the asymmetric    |
|                                      | unit of the spacegroup on index      |
|                                      | "sindx".                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.   |                         |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |     | *ihkl*     | reflection indice |
|                                      | s.         |                         |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, -1 if -h -k -l is   |
|                                      |     in asu, 0 otherwise              |
+--------------------------------------+--------------------------------------+

FORTRAN_FUN

( 

int 

 ,

INASU 

 ,

inasu 

 ,

(const int ihkl[3], const int *nlaue) 

 ,

(const int ihkl[3], const int *nlaue) 

 ,

(const int ihkl[3], const int *nlaue) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection or it's      |
|                                      | Friedel mate is in asu. The argument |
|                                      | nlaue is checked against the value   |
|                                      | for the current spacegroup: if it    |
|                                      | differs then spacegroup->nlaue is    |
|                                      | updated temporarily.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |     | *ihkl*     | reflection indice |
|                                      | s.   |                               |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |     | *nlaue*    | Laue group number |
|                                      | .    |                               |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, -1 if -h -k -l is   |
|                                      |     in asu, 0 otherwise              |
+--------------------------------------+--------------------------------------+

FORTRAN_FUN

( 

int 

 ,

CCP4SPG_F_EQUAL_OPS_ORDER 

 ,

ccp4spg_f_equal_ops_order 

 ,

(int *msym1, float rrsym1[192][4][4], int *msym2, float
rrsym2[192][4][4]) 

 ,

(int *msym1, float rrsym1[192][4][4], int *msym2, float
rrsym2[192][4][4]) 

 ,

(int *msym1, float rrsym1[192][4][4], int *msym2, float
rrsym2[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Compare two sets of symmetry         |
|                                      | operators to see if they are in the  |
|                                      | same order. This is important for    |
|                                      | the consistent use of ISYM which     |
|                                      | encodes the operator position in the |
|                                      | list.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *msym1*     | number of symmet |
|                                      | ry matrices passed in first list.    |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rrsym1*    | first list of sy |
|                                      | mmetry matrices.                     |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *msym2*     | number of symmet |
|                                      | ry matrices passed in second list.   |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rrsym2*    | second list of s |
|                                      | ymmetry matrices.                    |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if operator lists are equal    |
|                                      |     and in the same order, 0         |
|                                      |     otherwise                        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

CENTRIC 

 ,

centric 

 ,

(const int *nsm, const float rsm[192][4][4], const int *iprint) 

 ,

(const int *nsm, const float rsm[192][4][4], const int *iprint) 

 ,

(const int *nsm, const float rsm[192][4][4], const int *iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set up centric zones based on        |
|                                      | symmetry operators. Convention:      |
|                                      | translations are in rsm[isym][3][*] |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.                  |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                                   |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*    | If iprint > 0 th |
|                                      | en a summary of centric zones is pri |
|                                      | nted.   |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

EPSLN 

 ,

epsln 

 ,

(const int *nsm, const int *nsmp, const float rsm[192][4][4], const
int *iprint) 

 ,

(const int *nsm, const int *nsmp, const float rsm[192][4][4], const
int *iprint) 

 ,

(const int *nsm, const int *nsmp, const float rsm[192][4][4], const
int *iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Epsilon zones currently set up in    |
|                                      | ccp4spg_load_spacegroup If these   |
|                                      | are not available, use lookup by     |
|                                      | symops.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry operators.                        |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsmp*      | number of primit |
|                                      | ive symmetry operators.              |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                                   |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*    | If iprint > 0 th |
|                                      | en a summary of epsilon zones is pri |
|                                      | nted.   |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

MSYMLB3 

 ,

msymlb3 

 ,

(const int *ist, int *lspgrp, fpstr namspg_cif, fpstr namspg_cifs,
fpstr nampg, int *nsymp, int *nsym, float rlsymmmatrx[192][4][4], int
namspg_cif_len, int namspg_cifs_len, int nampg_len) 

 ,

(const int *ist, int *lspgrp, fpstr namspg_cif, fpstr namspg_cifs,
fpstr nampg, int *nsymp, int *nsym, float rlsymmmatrx[192][4][4]) 

 ,

(const int *ist, int *lspgrp, fpstr namspg_cif, int namspg_cif_len,
fpstr namspg_cifs, int namspg_cifs_len, fpstr nampg, int nampg_len,
int *nsymp, int *nsym, float rlsymmmatrx[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for                  |
|                                      | ccp4spg_load_by_* functions.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *ist*             | Obsolete p |
|                                      | arameter.                            |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *lspgrp*          | Spacegroup |
|                                      |  number in CCP4 convention. If set o |
|                                      | n entry, used to search for spacegro |
|                                      | up. Returned value is that found.    |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *namspg_cif*     | Spacegroup |
|                                      |  name. If set on entry, used to sear |
|                                      | ch for spacegroup. Returned value is |
|                                      |  the full extended Hermann Mauguin s |
|                                      | ymbol, with one slight alteration. S |
|                                      | ymbols such as 'R 3 :H' are converte |
|                                      | d to 'H 3'. This is for backwards co |
|                                      | mpatibility.   |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *namspg_cifs*    | On output, |
|                                      |  contains the spacegroup name withou |
|                                      | t any spaces.                        |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nampg*           | On output, |
|                                      |  the point group name.               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nsymp*           | On output, |
|                                      |  the number of primitive symmetry op |
|                                      | erators.                             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nsym*            | On output, |
|                                      |  the total number of symmetry operat |
|                                      | ors.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *rlsymmmatrx*     | On output, |
|                                      |  the symmetry operators.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

CCP4SPG_F_ASUPUT 

 ,

ccp4spg_f_asuput 

 ,

(const int *sindx, const int ihkl[3], int jhkl[3], int *isym) 

 ,

(const int *sindx, const int ihkl[3], int jhkl[3], int *isym) 

 ,

(const int *sindx, const int ihkl[3], int jhkl[3], int *isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Put reflection in asymmetric unit of |
|                                      | spacegroup on index sindx.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.                   |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *ihkl*     | input indices.    |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *jhkl*     | output indices.   |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *isym*     | symmetry operatio |
|                                      | n applied (ISYM number).   |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

CCP4SPG_F_LOAD_BY_OPS 

 ,

ccp4spg_f_load_by_ops 

 ,

(const int *sindx, int *msym, float rrsym[192][4][4]) 

 ,

(const int *sindx, int *msym, float rrsym[192][4][4]) 

 ,

(const int *sindx, int *msym, float rrsym[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Loads a spacegroup onto index        |
|                                      | "sindx". The spacegroup is           |
|                                      | identified by the set of symmetry    |
|                                      | matrices.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.             |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *msym*     | number of symmetr |
|                                      | y matrices passed.   |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *rrsym*    | symmetry matrices |
|                                      | .                    |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

CCP4SPG_F_LOAD_BY_NAME 

 ,

ccp4spg_f_load_by_name 

 ,

(const int *sindx, fpstr namspg, int namspg_len) 

 ,

(const int *sindx, fpstr namspg) 

 ,

(const int *sindx, fpstr namspg, int namspg_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Loads a spacegroup onto index        |
|                                      | "sindx". The spacegroup is           |
|                                      | identified by the spacegroup name.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *namspg*    | spacegroup name. |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

ASUPHP 

 ,

asuphp 

 ,

(const int jhkl[3], const int *lsym, const int *isign, const float
*phasin, float *phasout) 

 ,

(const int jhkl[3], const int *lsym, const int *isign, const float
*phasin, float *phasout) 

 ,

(const int jhkl[3], const int *lsym, const int *isign, const float
*phasin, float *phasout) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Generate phase of symmetry           |
|                                      | equivalent JHKL from that of IHKL.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *jhkl*       | indices hkl gen |
|                                      | erated in ASUPUT       |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *lsym*       | symmetry number |
|                                      |  for generating JHKL   |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *isign*      | 1 for I+ , -1 f |
|                                      | or I-                  |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *phasin*     | phase for refle |
|                                      | ction IHKL             |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *phasout*    | phase for refle |
|                                      | ction JHKL             |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

ASUGET 

 ,

asuget 

 ,

(const int ihkl[3], int jhkl[3], const int *isym) 

 ,

(const int ihkl[3], int jhkl[3], const int *isym) 

 ,

(const int ihkl[3], int jhkl[3], const int *isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the original indices jkhl from   |
|                                      | input indices ihkl generated under   |
|                                      | symmetry operation isym.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *ihkl*    | input indices.     |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *jhkl*    | output indices (re |
|                                      | covered original indices).      |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *isym*    | symmetry operation |
|                                      |  to be applied (ISYM number).   |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

ASUPUT 

 ,

asuput 

 ,

(const int ihkl[3], int jhkl[3], int *isym) 

 ,

(const int ihkl[3], int jhkl[3], int *isym) 

 ,

(const int ihkl[3], int jhkl[3], int *isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Put reflection in asymmetric unit,   |
|                                      | as set up by ASUSET.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *ihkl*    | input indices.     |
|                                      |                           |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *jhkl*    | output indices.    |
|                                      |                           |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *isym*    | symmetry operation |
|                                      |  applied (ISYM number).   |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

ASUSYM 

 ,

asusym 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int *nisym) 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int *nisym) 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int *nisym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return symmetry operators and        |
|                                      | inverses, set up by ASUSET.          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *rassym*    | symmetry operato |
|                                      | rs.                      |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *rinsym*    | inverse symmetry |
|                                      |  operators.              |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *nisym*     | number of symmet |
|                                      | ry operators returned.   |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

ASUSET 

 ,

asuset 

 ,

(fpstr spgnam, int *numsgp, fpstr pgname, int *msym, float
rrsym[192][4][4], int *msymp, int *mlaue, ftn_logical *lprint, int
spgnam_len, int pgname_len) 

 ,

(fpstr spgnam, int *numsgp, fpstr pgname, int *msym, float
rrsym[192][4][4], int *msymp, int *mlaue, ftn_logical *lprint) 

 ,

(fpstr spgnam, int spgnam_len, int *numsgp, fpstr pgname, int
pgname_len, int *msym, float rrsym[192][4][4], int *msymp, int
*mlaue, ftn_logical *lprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set spacegroup for subsequent calls  |
|                                      | to ASUPUT, ASUGET, ASUSYM and        |
|                                      | ASUPHP.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *spgnam*    | spacegroup name  |
|                                      |                                      |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *numsgp*    | spacegroup numbe |
|                                      | r                                    |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *pgname*    | On return, point |
|                                      |  group name                          |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *msym*      | number of symmet |
|                                      | ry matrices passed.                  |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *rrsym*     | symmetry matrice |
|                                      | s (preferred method of identifying s |
|                                      | pacegroup).   |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *msymp*     | On return, numbe |
|                                      | r of primitive symmetry operators    |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *mlaue*     | On return, numbe |
|                                      | r of Laue group.                     |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *lprint*    | If true, print s |
|                                      | ymmetry information.                 |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

PATSGP 

 ,

patsgp 

 ,

(const fpstr spgnam, const fpstr pgname, fpstr patnam, int *lpatsg, int
spgnam_len, int pgname_len, int patnam_len) 

 ,

(const fpstr spgnam, const fpstr pgname, fpstr patnam, int *lpatsg) 

 ,

(const fpstr spgnam, int spgnam_len, const fpstr pgname, int
pgname_len, fpstr patnam, int patnam_len, int *lpatsg) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the Patterson group name and  |
|                                      | number corresponding to a spacegroup |
|                                      | identified by spacegroup name and    |
|                                      | point group name.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *spgnam*    | On input, spaceg |
|                                      | roup name.                |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *pgname*    | On input, point  |
|                                      | group name.               |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *patnam*    | On return, Patte |
|                                      | rson spacegroup name.     |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *lpatsg*    | On return, Patte |
|                                      | rson spacegroup number.   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

HKLRANGE 

 ,

hklrange 

 ,

(int *ihrng0, int *ihrng1, int *ikrng0, int *ikrng1, int *ilrng0,
int *ilrng1) 

 ,

(int *ihrng0, int *ihrng1, int *ikrng0, int *ikrng1, int *ilrng0,
int *ilrng1) 

 ,

(int *ihrng0, int *ihrng1, int *ikrng0, int *ikrng1, int *ilrng0,
int *ilrng1) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return ranges on H K L appropriate   |
|                                      | to spacegroup.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nlaue*     | Laue number      |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *launam*    | Laue name        |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

CCP4SPG_F_GET_LAUE 

 ,

ccp4spg_f_get_laue 

 ,

(const int *sindx, int *nlaue, fpstr launam, int launam_len) 

 ,

(const int *sindx, int *nlaue, fpstr launam) 

 ,

(const int *sindx, int *nlaue, fpstr launam, int launam_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return Laue number and name for a    |
|                                      | spacegroup onto index "sindx".       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nlaue*     | Laue number      |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *launam*    | Laue name        |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

PGNLAU 

 ,

pgnlau 

 ,

(const fpstr nampg, int *nlaue, fpstr launam, int nampg_len, int
launam_len) 

 ,

(const fpstr nampg, int *nlaue, fpstr launam) 

 ,

(const fpstr nampg, int nampg_len, int *nlaue, fpstr launam, int
launam_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return Laue number and name for      |
|                                      | current spacegroup.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nampg*     | Point group name |
|                                      |  (unused in this implementation)   | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nlaue*     | Laue number      |
|                                      |                                    | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *launam*    | Laue name        |
|                                      |                                    | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

SYMTR4 

 ,

symtr4 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs_len) 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs) 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for mat4_to_symop. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                    |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *symchs*    | symmetry strings |
|                                      |  returned.            |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

SYMTR3 

 ,

symtr3 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int
*iprint, int symchs_len) 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int
*iprint) 

 ,

(const int *nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs_len, const int *iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for mat4_to_symop. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                    |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *symchs*    | symmetry strings |
|                                      |  returned.            |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *iprint*    | print flag.      |
|                                      |                       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+
