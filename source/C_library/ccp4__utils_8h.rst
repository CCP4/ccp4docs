`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_utils.h File Reference
============================

Utility functions. `More... <#_details>`__

| ``#include <string.h>``
| ``#include "ccp4_types.h"``
| ``#include "library_file.h"``

`Go to the source code of this file. <ccp4__utils_8h-source.html>`__

| 

Functions
---------

size_t 

`ccp4_utils_flength <ccp4__utils_8h.html#a1>`__ (char *, int)

int 

`ccp4_utils_translate_mode_float <ccp4__utils_8h.html#a2>`__ (float
*, const void *, int, int)

 void 

**ccp4_utils_fatal** (const char * )

void 

`ccp4_utils_print <ccp4__utils_8h.html#a4>`__ (const char *message)

int 

`ccp4_utils_setenv <ccp4__utils_8h.html#a5>`__ (char * )

int 

`ccp4_utils_outbuf <ccp4__utils_8h.html#a6>`__ (void)

int 

`ccp4_utils_noinpbuf <ccp4__utils_8h.html#a7>`__ (void)

 float_uint_uchar 

**ccp4_nan** ()

int 

`ccp4_utils_isnan <ccp4__utils_8h.html#a9>`__ (const union
float_uint_uchar * )

 void 

**ccp4_utils_bml** (int, union float_uint_uchar * )

 void 

**ccp4_utils_wrg** (int, union float_uint_uchar *, float * )

void 

`ccp4_utils_hgetlimits <ccp4__utils_8h.html#a12>`__ (int *, float * )

int 

`ccp4_utils_mkdir <ccp4__utils_8h.html#a13>`__ (const char *, const
char * )

int 

`ccp4_utils_chmod <ccp4__utils_8h.html#a14>`__ (const char *, const
char * )

void * 

`ccp4_utils_malloc <ccp4__utils_8h.html#a15>`__ (size_t)

void * 

`ccp4_utils_realloc <ccp4__utils_8h.html#a16>`__ (void *, size_t)

void * 

`ccp4_utils_calloc <ccp4__utils_8h.html#a17>`__ (size_t, size_t)

 int 

**ccp4_file_size** (const char * )

char * 

`ccp4_utils_username <ccp4__utils_8h.html#a19>`__ (void)

char * 

`ccp4_utils_basename <ccp4__utils_8h.html#a20>`__ (const char
*filename)

char * 

`ccp4_utils_pathname <ccp4__utils_8h.html#a21>`__ (const char
*filename)

char * 

`ccp4_utils_extension <ccp4__utils_8h.html#a22>`__ (const char
*filename)

char * 

`ccp4_utils_joinfilenames <ccp4__utils_8h.html#a23>`__ (char *dir,
char *file)

 void 

**ccp4_utils_idate** (int * )

char * 

`ccp4_utils_date <ccp4__utils_8h.html#a25>`__ (char * )

 void 

**ccp4_utils_itime** (int * )

char * 

`ccp4_utils_time <ccp4__utils_8h.html#a27>`__ (char * )

 float 

**ccp4_utils_etime** (float * )

--------------

Detailed Description
--------------------

Utility functions.

 **Author:**
    Charles Ballard

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +--------------------------------+------+------------------+------------ |
| ----+------+----+                                                        |
| | char* ccp4_utils_basename   | (    | const char *    |   *filename |
| *   | )    |    |                                                        |
| +--------------------------------+------+------------------+------------ |
| ----+------+----+                                                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Extracts the basename from a full    |
|                                      | file name. Separators for            |
|                                      | directories and extensions are       |
|                                      | OS-specific.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |     | *filename*    | full file name |
|                                      |  string.   |                         |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to basename              |
+--------------------------------------+--------------------------------------+

void* ccp4_utils_calloc

( 

size_t 

  *nelem*,

size_t 

  *elsize*

) 

+--------------------------------------+--------------------------------------+
|                                      | This is a wrapper for the calloc     |
|                                      | function, which adds some error      |
|                                      | trapping.                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

int ccp4_utils_chmod

( 

const char * 

  *path*,

const char * 

  *cmode*

) 

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+------------+------------+------+-- |
| --+                                                                      |
| | char* ccp4_utils_date   | (    | char *    |   *date*   | )    |   |
|   |                                                                      |
| +----------------------------+------+------------+------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+------------------+----------- |
| -----+------+----+                                                       |
| | char* ccp4_utils_extension   | (    | const char *    |   *filenam |
| e*   | )    |    |                                                       |
| +---------------------------------+------+------------------+----------- |
| -----+------+----+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Extracts the extension from a full   |
|                                      | file name. Separators for            |
|                                      | directories and extensions are       |
|                                      | OS-specific.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |     | *filename*    | full file name |
|                                      |  string.   |                         |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to extension             |
+--------------------------------------+--------------------------------------+

size_t ccp4_utils_flength

( 

char * 

  *s*,

int 

  *len*

) 

+--------------------------------------+--------------------------------------+
|                                      | Gets the length of a Fortran string  |
|                                      | with trailing blanks removed.        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     length of string                 |
+--------------------------------------+--------------------------------------+

void ccp4_utils_hgetlimits

( 

int * 

  *IValueNotDet*,

float * 

  *ValueNotDet*

) 

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| -+---------------+------+----+                                           |
| | int ccp4_utils_isnan   | (    | const union float_uint_uchar *    |
|  |   *realnum*   | )    |    |                                           |
| +--------------------------+------+------------------------------------- |
| -+---------------+------+----+                                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

char* ccp4_utils_joinfilenames

( 

char * 

  *dir*,

char * 

  *file*

) 

+--------------------------------------+--------------------------------------+
|                                      | Joins a leading directory with a     |
|                                      | filename. Separators for directories |
|                                      | and extensions are OS-specific.      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --+                                  |
|                                      |     | *dir*     | directory path.    |
|                                      |   |                                  |
|                                      |     +-----------+------------------- |
|                                      | --+                                  |
|                                      |     | *file*    | file name string.  |
|                                      |   |                                  |
|                                      |     +-----------+------------------- |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to joined                |
|                                      |     directory-filename path.         |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+------------+------------+------+ |
| ----+                                                                    |
| | void* ccp4_utils_malloc   | (    | size_t    |   *size*   | )    | |
|     |                                                                    |
| +------------------------------+------+------------+------------+------+ |
| ----+                                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | This is a wrapper for the malloc     |
|                                      | function, which adds some error      |
|                                      | trapping.                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

int ccp4_utils_mkdir

( 

const char * 

  *path*,

const char * 

  *cmode*

) 

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------+-----+------+----+       |
| | int ccp4_utils_noinpbuf   | (    | void    |     | )    |    |       |
| +-----------------------------+------+---------+-----+------+----+       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+---------+-----+------+----+         |
| | int ccp4_utils_outbuf   | (    | void    |     | )    |    |         |
| +---------------------------+------+---------+-----+------+----+         |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+------+------------------+------------ |
| ----+------+----+                                                        |
| | char* ccp4_utils_pathname   | (    | const char *    |   *filename |
| *   | )    |    |                                                        |
| +--------------------------------+------+------------------+------------ |
| ----+------+----+                                                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Extracts the pathname from a full    |
|                                      | file name. Separators for            |
|                                      | directories and extensions are       |
|                                      | OS-specific.                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |     | *filename*    | full file name |
|                                      |  string.   |                         |
|                                      |     +---------------+--------------- |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to pathname with         |
|                                      |     trailing separator.              |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------+---------------+- |
| -----+----+                                                              |
| | void ccp4_utils_print   | (    | const char *    |   *message*   |  |
| )    |    |                                                              |
| +---------------------------+------+------------------+---------------+- |
| -----+----+                                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

void* ccp4_utils_realloc

( 

void * 

  *ptr*,

size_t 

  *size*

) 

+--------------------------------------+--------------------------------------+
|                                      | This is a wrapper for the realloc    |
|                                      | function, which adds some error      |
|                                      | trapping.                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------+-----------+------+---- |
| +                                                                        |
| | int ccp4_utils_setenv   | (    | char *    |   *str*   | )    |     |
| |                                                                        |
| +---------------------------+------+------------+-----------+------+---- |
| +                                                                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+------------+------------+------+-- |
| --+                                                                      |
| | char* ccp4_utils_time   | (    | char *    |   *time*   | )    |   |
|   |                                                                      |
| +----------------------------+------+------------+------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Alternative to ccp4_utils_itime    |
|                                      | with time as character string.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------+                  |
|                                      |     | *time*    | Character string o |
|                                      | f form HH:MM:SS   |                  |
|                                      |     +-----------+------------------- |
|                                      | ------------------+                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to character string.     |
+--------------------------------------+--------------------------------------+

int ccp4_utils_translate_mode_float

( 

float * 

  *out*,

const void * 

  *buffer*,

int 

  *dim*,

int 

  *mode*

) 

+--------------------------------------+--------------------------------------+
|                                      |  **Returns:**                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+------+---------+-----+------+----+    |
| | char* ccp4_utils_username   | (    | void    |     | )    |    |    |
| +--------------------------------+------+---------+-----+------+----+    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the user's login name.        |
|                                      | (MVisualStudio version in w32mvs.c)  |
|                                      | Note that getlogin only works for    |
|                                      | processes attached to a terminal     |
|                                      | (and hence won't work from the GUI). |
|                                      | In these instances use getpwuid      |
|                                      | instead.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to character string      |
|                                      |     containing login name.           |
+--------------------------------------+--------------------------------------+
