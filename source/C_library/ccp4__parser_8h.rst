`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_parser.h File Reference
=============================

Functions to read in and "parse" CCP4-style keyworded input.
`More... <#_details>`__

| ``#include <stdio.h>``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_spg.h"``

`Go to the source code of this file. <ccp4__parser_8h-source.html>`__

| 

Compounds
---------

struct  

**CCP4PARSERARRAY**

struct  

**CCP4PARSERTOKEN**

| 

Defines
-------

 #define 

**CCP4_PARSER_MAXLINELENGTH**   200

 #define 

**CCP4_PARSER_MAXFILENAMELENGTH**   200

| 

Functions
---------

CCP4PARSERARRAY * 

`ccp4_parse_start <ccp4__parser_8h.html#a3>`__ (const int maxtokens)

int 

`ccp4_parse_end <ccp4__parser_8h.html#a4>`__ (CCP4PARSERARRAY
*parsePtr)

 int 

**ccp4_parse_init_token** (const CCP4PARSERARRAY *parsePtr, const
int itok)

 int 

**ccp4_parse_delimiters** (CCP4PARSERARRAY *parsePtr, const char
*delim, const char *nulldelim)

 int 

**ccp4_parse_comments** (CCP4PARSERARRAY *parsePtr, const char
*comment_chars)

 int 

**ccp4_parse_maxmin** (CCP4PARSERARRAY *parsePtr, const double
max_exponent, const double min_exponent)

 int 

**ccp4_parse_reset** (CCP4PARSERARRAY *parsePtr)

 int 

**ccp4_parse** (const char *line, CCP4PARSERARRAY *parser)

int 

`ccp4_parser <ccp4__parser_8h.html#a11>`__ (char *line, const int n,
CCP4PARSERARRAY *parser, const int print)

int 

`ccp4_keymatch <ccp4__parser_8h.html#a12>`__ (const char *keyin1,
const char *keyin2)

char * 

`strtoupper <ccp4__parser_8h.html#a13>`__ (char *str1, const char
*str2)

char * 

`strtolower <ccp4__parser_8h.html#a14>`__ (char *str1, const char
*str2)

 int 

**strmatch** (const char *str1, const char *str2)

 int 

**charmatch** (const char character, const char *charlist)

 int 

**doublefromstr** (const char *str, const double max_exp, const double
min_exp, double *valuePtr, double *intvaluePtr, int *intdigitsPtr,
double *frcvaluePtr, int *frcdigitsPtr, double *expvaluePtr, int
*expdigitsPtr)

ccp4_symop 

`symop_to_rotandtrn <ccp4__parser_8h.html#a18>`__ (const char
*symchs_begin, const char *symchs_end)

const char * 

`symop_to_mat4 <ccp4__parser_8h.html#a19>`__ (const char
*symchs_begin, const char *symchs_end, float *rot)

 int 

**symop_to_mat4_err** (const char *symop)

 ccp4_symop 

**mat4_to_rotandtrn** (const float rsm[4][4])

 char * 

**rotandtrn_to_symop** (char *symchs_begin, char *symchs_end,
const ccp4_symop symop)

 void 

**rotandtrn_to_mat4** (float rsm[4][4], const ccp4_symop symop)

char * 

`mat4_to_symop <ccp4__parser_8h.html#a24>`__ (char *symchs_begin,
char *symchs_end, const float rsm[4][4])

char * 

`mat4_to_recip_symop <ccp4__parser_8h.html#a25>`__ (char
*symchs_begin, char *symchs_end, const float rsm[4][4])

--------------

Detailed Description
--------------------

Functions to read in and "parse" CCP4-style keyworded input.

 **Author:**
    Peter Briggs

 **Date:**
    April 2001

--------------

Function Documentation
----------------------

int ccp4_keymatch

( 

const char * 

  *keyin1*,

const char * 

  *keyin2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether two keywords are        |
|                                      | identical. Keywords are identical if |
|                                      | they are the same up to the first    |
|                                      | four characters, independent of      |
|                                      | case.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+--------------+   |
|                                      |     | *keyin1*    | keyword 1.   |   |
|                                      |     +-------------+--------------+   |
|                                      |     | *keyin2*    | keyword 2.   |   |
|                                      |     +-------------+--------------+   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if keywords keyin1 and keyin2  |
|                                      |     are "identical", 0 otherwise.    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+-----------------------+--------------- |
| -+------+----+                                                           |
| | int ccp4_parse_end   | (    | CCP4PARSERARRAY *    |   *parsePtr*   |
|  | )    |    |                                                           |
| +------------------------+------+-----------------------+--------------- |
| -+------+----+                                                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Cleans up a CCP4PARSEARRAY after     |
|                                      | being used by ccp4_parse/           |
|                                      | ccp4_parser functions.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ---------------------------+         |
|                                      |     | *parsePtr*    | pointer to a C |
|                                      | CP4PARSERARRAY structure   |         |
|                                      |     +---------------+--------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on completion                  |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------------------+------+--------------+-------- |
| ---------+------+----+                                                   |
| | CCP4PARSERARRAY* ccp4_parse_start   | (    | const int    |   *maxt |
| okens*   | )    |    |                                                   |
| +----------------------------------------+------+--------------+-------- |
| ---------+------+----+                                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Initialise a CCP4PARSERARRAY to be   |
|                                      | used in subsequent calls to          |
|                                      | ccp4_parser routines. The calling   |
|                                      | function must supply the maximum     |
|                                      | number of tokens on a line           |
|                                      | (including continuation lines).      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------+            |
|                                      |     | *maxtokens*    | maximum numbe |
|                                      | r of tokens on a line   |            |
|                                      |     +----------------+-------------- |
|                                      | ------------------------+            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to a new CCP4PARSERARRAY |
|                                      |     structure                        |
+--------------------------------------+--------------------------------------+

int ccp4_parser

( 

char * 

  *line*,

const int 

  *n*,

CCP4PARSERARRAY * 

  *parser*,

const int 

  *print*

) 

+--------------------------------------+--------------------------------------+
|                                      | The main function for parsing lines, |
|                                      | either supplied or read from stdin.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *line*      | pointer to a nul |
|                                      | l-terminated string of characters, f |
|                                      | orming the input to be processed. On |
|                                      |  input can either be an empty string |
|                                      |  ("") which forces reading from stdi |
|                                      | n, or contain characters to be proce |
|                                      | ssed. On output "line" will be overw |
|                                      | ritten with the actual input line.   |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *n*         | maximum number o |
|                                      | f characters that can be read into " |
|                                      | line" i.e. the size of "line" in mem |
|                                      | ory.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *parser*    | pointer to a CCP |
|                                      | 4PARSERARRAY structure which will be |
|                                      |  used to hold the results of process |
|                                      | ing the input line.                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *print*     | flag controlling |
|                                      |  echoing of input lines to stdout. p |
|                                      | rint=0: suppress echoing of lines to |
|                                      |  stdout. Otherwise echoing is turned |
|                                      |  on.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Number of tokens found.          |
+--------------------------------------+--------------------------------------+

char* mat4_to_recip_symop

( 

char * 

  *symchs_begin*,

char * 

  *symchs_end*,

const float 

  *rsm*[4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as matrix  |
|                                      | to string in reciprocal space        |
|                                      | notation. This is Charles' version   |
|                                      | of symtr. Note that translations are |
|                                      | held in elements [*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is *(symchs_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rsm*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to beginning of string   |
+--------------------------------------+--------------------------------------+

char* mat4_to_symop

( 

char * 

  *symchs_begin*,

char * 

  *symchs_end*,

const float 

  *rsm*[4][4]

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as matrix  |
|                                      | to string. This is Charles' version  |
|                                      | of symtr. Note that translations are |
|                                      | held in elements [*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is *(symchs_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rsm*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to beginning of string   |
+--------------------------------------+--------------------------------------+

char* strtolower

( 

char * 

  *str1*,

const char * 

  *str2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert string to lowercase.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str1*    | On exit str1 will  |
|                                      | contain lowercased copy of str2   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str2*    | Input string       |
|                                      |                                   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     str1                             |
+--------------------------------------+--------------------------------------+

char* strtoupper

( 

char * 

  *str1*,

const char * 

  *str2*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert string to uppercase.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str1*    | On exit str1 will  |
|                                      | contain uppercased copy of str2   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |     | *str2*    | Input string       |
|                                      |                                   |  |
|                                      |     +-----------+------------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     str1                             |
+--------------------------------------+--------------------------------------+

const char* symop_to_mat4

( 

const char * 

  *symchs_begin*,

const char * 

  *symchs_end*,

float * 

  *rot*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as string  |
|                                      | to matrix. This is Charles' version  |
|                                      | of symfr. Note that translations are |
|                                      | held in elements [*][3] and [3][3]  |
|                                      | is set to 1.0                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is *(symchs_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *rot*              | 4 x 4 mat |
|                                      | rix operator                         |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     NULL on error, final position    |
|                                      |     pointer on success               |
+--------------------------------------+--------------------------------------+

ccp4_symop symop_to_rotandtrn

( 

const char * 

  *symchs_begin*,

const char * 

  *symchs_end*

) 

+--------------------------------------+--------------------------------------+
|                                      | Convert symmetry operator as string  |
|                                      | to ccp4_symop struct.               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_begin*    | pointer t |
|                                      | o beginning of string                |
|                                      |                           |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *symchs_end*      | pointer t |
|                                      | o end of string (i.e. last character |
|                                      |  is *(symchs_end-1) )   |          |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to ccp4_symop struct    |
+--------------------------------------+--------------------------------------+
