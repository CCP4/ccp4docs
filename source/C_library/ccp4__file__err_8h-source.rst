`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_file_err.h
=================

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_file_err.h: header file with file handling error codes
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef _GUARD_FILE_ERR
    00011 #define _GUARD_FILE_ERR
    00012 
    00013 #define CCP4_ERRNO(y) (CCP4_ERR_FILE | (y))
    00014 #define CIO_Ok 0
    00015 #define CIO_BadMode 1
    00016 #define CIO_CantOpenFile 2
    00017 #define CIO_MaxFile 3
    00018 #define CIO_ReadFail 4
    00019 #define CIO_WriteFail 5
    00020 #define CIO_CloseFail 6
    00021 #define CIO_SeekFail 7
    00022 #define CIO_NullPtr 8
    00023 #define CIO_EOF 9
    00024 #define CIO_NoFile 10
    00025 #define CIO_NotOpen 11
    00026 #define CIO_UnlinkFail 12
    00027 
    00028 #endif
    00029 

.. raw:: html

   </div>
