`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library_file.h File Reference
==============================

| ``#include "ccp4_sysdep.h"``
| ``#include "ccp4_types.h"``

`Go to the source code of this file. <library__file_8h-source.html>`__

| 

Compounds
---------

struct  

**_CFileStruct**

| 

Typedefs
--------

typedef _CFileStruct 

`CCP4File <library__file_8h.html#a0>`__

| 

Functions
---------

`CCP4File <library__file_8h.html#a0>`__ * 

`ccp4_file_open <library__file_8h.html#a1>`__ (const char *, const
int)

`CCP4File <library__file_8h.html#a0>`__ * 

`ccp4_file_open_file <library__file_8h.html#a2>`__ (const FILE *,
const int)

`CCP4File <library__file_8h.html#a0>`__ * 

`ccp4_file_open_fd <library__file_8h.html#a3>`__ (const int, const
int)

int 

`ccp4_file_rarch <library__file_8h.html#a4>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_warch <library__file_8h.html#a5>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_close <library__file_8h.html#a6>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_mode <library__file_8h.html#a7>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_setmode <library__file_8h.html#a8>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const int)

int 

`ccp4_file_setstamp <library__file_8h.html#a9>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const size_t)

int 

`ccp4_file_itemsize <library__file_8h.html#a10>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_setbyte <library__file_8h.html#a11>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const int)

 int 

**ccp4_file_byteorder** (`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_is_write <library__file_8h.html#a13>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_is_read <library__file_8h.html#a14>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_is_append <library__file_8h.html#a15>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_is_scratch <library__file_8h.html#a16>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_is_buffered <library__file_8h.html#a17>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_status <library__file_8h.html#a18>`__ (const
`CCP4File <library__file_8h.html#a0>`__ * )

const char * 

`ccp4_file_name <library__file_8h.html#a19>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_read <library__file_8h.html#a20>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readcomp <library__file_8h.html#a21>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readshortcomp <library__file_8h.html#a22>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readfloat <library__file_8h.html#a23>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readint <library__file_8h.html#a24>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readshort <library__file_8h.html#a25>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_readchar <library__file_8h.html#a26>`__
(`CCP4File <library__file_8h.html#a0>`__ *, uint8 *, size_t)

int 

`ccp4_file_write <library__file_8h.html#a27>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writecomp <library__file_8h.html#a28>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writeshortcomp <library__file_8h.html#a29>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writefloat <library__file_8h.html#a30>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writeint <library__file_8h.html#a31>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writeshort <library__file_8h.html#a32>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_writechar <library__file_8h.html#a33>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const uint8 *, size_t)

int 

`ccp4_file_seek <library__file_8h.html#a34>`__
(`CCP4File <library__file_8h.html#a0>`__ *, long, int)

void 

`ccp4_file_rewind <library__file_8h.html#a35>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

void 

`ccp4_file_flush <library__file_8h.html#a36>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

long 

`ccp4_file_length <library__file_8h.html#a37>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

long 

`ccp4_file_tell <library__file_8h.html#a38>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

int 

`ccp4_file_feof <library__file_8h.html#a39>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

void 

`ccp4_file_clearerr <library__file_8h.html#a40>`__
(`CCP4File <library__file_8h.html#a0>`__ * )

void 

`ccp4_file_fatal <library__file_8h.html#a41>`__
(`CCP4File <library__file_8h.html#a0>`__ *, char * )

char * 

`ccp4_file_print <library__file_8h.html#a42>`__
(`CCP4File <library__file_8h.html#a0>`__ *, char *, char * )

int 

`ccp4_file_raw_seek <library__file_8h.html#a43>`__
(`CCP4File <library__file_8h.html#a0>`__ *, long, int)

int 

`ccp4_file_raw_read <library__file_8h.html#a44>`__
(`CCP4File <library__file_8h.html#a0>`__ *, char *, size_t)

int 

`ccp4_file_raw_write <library__file_8h.html#a45>`__
(`CCP4File <library__file_8h.html#a0>`__ *, const char *, size_t)

 int 

**ccp4_file_raw_setstamp** (`CCP4File <library__file_8h.html#a0>`__
*, const size_t)

--------------

Detailed Description
--------------------

Functions for file i/o. Charles Ballard

--------------

Typedef Documentation
---------------------

+--------------------------------------------------------------------------+
| +-----------------------------------------+                              |
| | typedef struct _CFileStruct CCP4File   |                              |
| +-----------------------------------------+                              |
+--------------------------------------------------------------------------+

+-----+----------------------+
|     | Generic CCP4 file.   |
+-----+----------------------+

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| -------------+-------------+------+----+                                 |
| | void ccp4_file_clearerr   | (    | `CCP4File <library__file_8h.html# |
| a0>`__ *    |   *cfile*   | )    |    |                                 |
| +-----------------------------+------+---------------------------------- |
| -------------+-------------+------+----+                                 |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_clearerr:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Clears error status of @cfile.       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4_file_close   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ *    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_close:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | close @cfile if owned, close         |
|                                      | (non-buffered) or fclose (buffered), |
|                                      | or fflush if stream not owned. Free  |
|                                      | resources.                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

void ccp4_file_fatal

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

char * 

  *message*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_fatal:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Die with error message based on      |
|                                      | @cfile error status.                 |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------------------------------------- |
| --------+-------------+------+----+                                      |
| | int ccp4_file_feof   | (    | `CCP4File <library__file_8h.html#a0>`_ |
| _ *    |   *cfile*   | )    |    |                                      |
| +------------------------+------+--------------------------------------- |
| --------+-------------+------+----+                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_feof:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     true if @cfile is at EoF.        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------+-------------+------+----+                                    |
| | void ccp4_file_flush   | (    | `CCP4File <library__file_8h.html#a0> |
| `__ *    |   *cfile*   | )    |    |                                    |
| +--------------------------+------+------------------------------------- |
| ----------+-------------+------+----+                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_flush:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | flush buffer contents of @cfile      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+--------------------------------- |
| --------------------+-------------+------+----+                          |
| | int ccp4_file_is_append   | (    | const `CCP4File <library__file_8 |
| h.html#a0>`__ *    |   *cfile*   | )    |    |                          |
| +------------------------------+------+--------------------------------- |
| --------------------+-------------+------+----+                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_is_append:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile in append mode         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+------+------------------------------- |
| ----------------------+-------------+------+----+                        |
| | int ccp4_file_is_buffered   | (    | const `CCP4File <library__file |
| _8h.html#a0>`__ *    |   *cfile*   | )    |    |                        |
| +--------------------------------+------+------------------------------- |
| ----------------------+-------------+------+----+                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_is_buffered:            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the file buffered                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
| | int ccp4_file_is_read   | (    | const `CCP4File <library__file_8h. |
| html#a0>`__ *    |   *cfile*   | )    |    |                            |
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_is_read:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile readable               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+-------------------------------- |
| ---------------------+-------------+------+----+                         |
| | int ccp4_file_is_scratch   | (    | const `CCP4File <library__file_ |
| 8h.html#a0>`__ *    |   *cfile*   | )    |    |                         |
| +-------------------------------+------+-------------------------------- |
| ---------------------+-------------+------+----+                         |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_is_scratch:             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is scratch file                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true.                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+------+---------------------------------- |
| -------------------+-------------+------+----+                           |
| | int ccp4_file_is_write   | (    | const `CCP4File <library__file_8h |
| .html#a0>`__ *    |   *cfile*   | )    |    |                           |
| +-----------------------------+------+---------------------------------- |
| -------------------+-------------+------+----+                           |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_is_write:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | is the @cfile writeable              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if true                        |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
| | int ccp4_file_itemsize   | (    | const `CCP4File <library__file_8h. |
| html#a0>`__ *    |   *cfile*   | )    |    |                            |
| +----------------------------+------+----------------------------------- |
| ------------------+-------------+------+----+                            |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_itemsize:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     itemsize of @cfile.              |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
| | long ccp4_file_length   | (    | `CCP4File <library__file_8h.html#a0 |
| >`__ *    |   *cfile*   | )    |    |                                   |
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_length:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Length of file on disk.              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     length of @cfile on success, EOF |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------------------------------------- |
| --------------+-------------+------+----+                                |
| | int ccp4_file_mode   | (    | const `CCP4File <library__file_8h.html |
| #a0>`__ *    |   *cfile*   | )    |    |                                |
| +------------------------+------+--------------------------------------- |
| --------------+-------------+------+----+                                |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_mode:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | get data mode of @cfile (BYTE =0,    |
|                                      | INT16 =1, INT32=6, FLOAT32 =2,       |
|                                      | COMP32 =3, COMP64 =4)                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     mode                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+------------------------------ |
| -----------------+-------------+------+----+                             |
| | const char* ccp4_file_name   | (    | `CCP4File <library__file_8h.h |
| tml#a0>`__ *    |   *cfile*   | )    |    |                             |
| +---------------------------------+------+------------------------------ |
| -----------------+-------------+------+----+                             |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_name:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | strdup @cfile->name                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     name of file as const char *    |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__* ccp4_file_open

( 

const char * 

  *filename*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_open:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *filename*    | (const char * |
|                                      | ) filename                           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |    |                                 |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *flag*        | (const int) i/ |
|                                      | o mode, possible values are O_RDONL |
|                                      | Y, O_WRONLY, O_RDWR, O_APPEND, O|
|                                      | _TMP, O_CREAT, O_TRUNC - see `ccp4 |
|                                      | _sysdep.h <ccp4__sysdep_8h.html>`__ |
|                                      |    |                                 |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |                                      |
|                                      | initialise CCP4File struct for file  |
|                                      | filename with mode @flag. If         |
|                                      | !buffered use open(), otherwise      |
|                                      | fopen() The struct stat is check to  |
|                                      | determine if file is a regular file, |
|                                      | if it is, and is not stdin, it is    |
|                                      | assumed to be direct access.         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File * ) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__* ccp4_file_open_fd

( 

const int 

  *fd*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_open_fd:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *fd*      | (const int) file d |
|                                      | escriptor                            |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *flag*    | (const int) io mod |
|                                      | e (O_RDONLY =0, O_WRONLY =1, O_RD |
|                                      | WR =2, O_TMP =, O_APPEND =)   |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      | initialise CCP4File struct with file |
|                                      | descriptor @fd and mode @flag The    |
|                                      | struct stat is check to determine if |
|                                      | file is a regular file, if it is,    |
|                                      | and is not stdin, it is assumed to   |
|                                      | be direct access.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File * ) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

`CCP4File <library__file_8h.html#a0>`__* ccp4_file_open_file

( 

const FILE * 

  *file*,

const int 

  *flag*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_open_file:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *file*    | (const FILE * ) FI |
|                                      | LE struct                            |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |     | *flag*    | (const int) io mod |
|                                      | e (O_RDONLY =0, O_WRONLY =1, O_RD |
|                                      | WR =2, O_TMP =, O_APPEND =)   |    |
|                                      |     +-----------+------------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------------+    |
|                                      |                                      |
|                                      | open @cfile with existing handle     |
|                                      | FILE struct file and mode @flag. The |
|                                      | struct stat is check to determine if |
|                                      | file is a regular file, if it is,    |
|                                      | and is not stdin, it is assumed to   |
|                                      | be direct access.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (CCP4File * ) on success, NULL   |
|                                      |     on failure                       |
+--------------------------------------+--------------------------------------+

char* ccp4_file_print

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

char * 

  *msg_start*,

char * 

  *msg_end*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_print:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     @cfile information in char array |
|                                      |     for printing.                    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4_file_rarch   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ *    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_rarch:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | read machine stamp from file         |
|                                      | @cfile->stream. The machine stamp is |
|                                      | at @cfile->stamp_loc items, set by  |
|                                      | `ccp4_file_setstamp <library__file |
|                                      | _8c.html#a24>`__ ()                 |
|                                      | (default 0). NB. these values may be |
|                                      | overrriden with the environmental    |
|                                      | variable CONVERT_FROM.              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     fileFT | (fileIT<<8)            |
+--------------------------------------+--------------------------------------+

int ccp4_file_raw_read

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

char * 

  *buffer*,

size_t 

  *n_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_raw_read:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *cfile*       | * (CCP4File |
|                                      | * )            |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *buffer*      | * (char * ) i |
|                                      | nput array    |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *n_items*    | (size_t) numb |
|                                      | er of items   |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      | reads block of n_items bytes from   |
|                                      | cfile to buffer via FILE struct      |
|                                      | cfile->stream(fread) or file desc    |
|                                      | cfile->fd read/_read). Increments   |
|                                      | location value cfile->loc. The       |
|                                      | cfile->iostat flag is set on         |
|                                      | failure.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes read.            |
+--------------------------------------+--------------------------------------+

int ccp4_file_raw_seek

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

long 

  *offset*,

int 

  *whence*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_raw_seek:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |                            |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *offset*    | (long) offset in |
|                                      |  bytes                     |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *whence*    | (int) SEEK_SET, |
|                                      |  SEEK_CUR, or SEEK_END   |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      | if the file is "seekable" (not       |
|                                      | stdin) the function seeks on @cfile  |
|                                      | by offset bytes using fseek/ftell    |
|                                      | (@cfile->stream) or lseek            |
|                                      | (@cfile->fd). SEEK_SET is relative  |
|                                      | to start of file, SEEK_CUR to       |
|                                      | current, SEEK_END to end.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     offset in bytes on success, -1   |
|                                      |     on failure.                      |
+--------------------------------------+--------------------------------------+

int ccp4_file_raw_write

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const char * 

  *buffer*,

size_t 

  *n_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_raw_write:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *cfile*       | (CCP4File * )  |
|                                      |               |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *buffer*      | (char * ) outp |
|                                      | ut array      |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |     | *n_items*    | (size_t) numb |
|                                      | er of items   |                      |
|                                      |     +---------------+--------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      | writes block of @n_items bytes from |
|                                      | @buffer to @cfile via FILE struct    |
|                                      | @cfile->stream(fwrite) or file desc  |
|                                      | @cfile->fd(write/_write).           |
|                                      | Increments @cfile->loc on success,   |
|                                      | or resets on failure, which is then  |
|                                      | used to determine the file length.   |
|                                      | On failure @cfile->iostat is set.    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes written.         |
+--------------------------------------+--------------------------------------+

int ccp4_file_read

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_read:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | mode dependent read function. Reads  |
|                                      | @nitems items from stream            |
|                                      | @cfile->stream to @buffer as         |
|                                      | determined by cfile->mode.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items read on success, |
|                                      |     EOF on failure                   |
+--------------------------------------+--------------------------------------+

int ccp4_file_readchar

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readchar:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | character read function. Reads       |
|                                      | @nitems characters from stream       |
|                                      | @cfile->stream to @buffer.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of characters read on     |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_readcomp

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readcomp:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float complex {float,float} read     |
|                                      | function. Reads @nitems complex from |
|                                      | stream @cfile->stream to @buffer.    |
|                                      | Allows short count when eof is       |
|                                      | detected ( buffered input only).     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex read on        |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_readfloat

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readfloat:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float read function. Reads @nitems   |
|                                      | floats from stream @cfile->stream to |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of floats read on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_readint

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readint:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | integer read function. Reads @nitems |
|                                      | int from stream @cfile->stream to    |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of int read on success,   |
|                                      |     EOF on failure                   |
+--------------------------------------+--------------------------------------+

int ccp4_file_readshort

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readshort:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short read function. Reads @nitems   |
|                                      | shorts from stream @cfile->stream to |
|                                      | @buffer.                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of shorts read on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_readshortcomp

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_readshortcomp:           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short complex {short,short} read     |
|                                      | function. Reads @nitems complex from |
|                                      | stream @cfile->stream to @buffer.    |
|                                      | Allows short count when eof is       |
|                                      | detected ( buffered input only).     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex read on        |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
| | void ccp4_file_rewind   | (    | `CCP4File <library__file_8h.html#a0 |
| >`__ *    |   *cfile*   | )    |    |                                   |
| +---------------------------+------+------------------------------------ |
| -----------+-------------+------+----+                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_rewind:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Seek to start of file. Clear error   |
|                                      | status.                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4_file_seek

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

long 

  *offset*,

int 

  *whence*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_seek:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |                            |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *offset*    | (long) offset in |
|                                      |  items                     |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |     | *whence*    | (int) SEEK_SET, |
|                                      |  SEEK_CUR, or SEEK_END   |         |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------+         |
|                                      |                                      |
|                                      | seeks on file by offset items.       |
|                                      | SEEK_SET is relative to start of    |
|                                      | file, SEEK_CUR to current,          |
|                                      | SEEK_END to end.                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, -1 on failure      |
+--------------------------------------+--------------------------------------+

int ccp4_file_setbyte

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const int 

  *byte_order*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_setbyte:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |     | *cfile*          | (CCP4File |
|                                      | * )   |                               |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |     | *byte_order*    | (int)       |
|                                      |      |                               |
|                                      |     +------------------+------------ |
|                                      | -----+                               |
|                                      |                                      |
|                                      | set byte ordering for file Return:   |
+--------------------------------------+--------------------------------------+

int ccp4_file_setmode

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const int 

  *mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_setmode:                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |     | *cfile*    | (CCP4File * )     |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |     | *mode*     | (int) io_mode    |
|                                      | |                                    |
|                                      |     +------------+------------------ |
|                                      | +                                    |
|                                      |                                      |
|                                      | set the data mode of cfile to mode   |
|                                      | (BYTE (8 bit) = 0, INT16 (16 bit) =  |
|                                      | 1, INT32 (32 bit) = 6, FLOAT32 (32   |
|                                      | bit) = 2, COMP32 (2*16 bit) = 3,    |
|                                      | COMP64 (2*32 bit) = 4).             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure.    |
+--------------------------------------+--------------------------------------+

int ccp4_file_setstamp

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const size_t 

  *offset*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_setstamp:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |     | *cfile*         | (CCP4File * |
|                                      | )               |                    |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |     | *stamp_loc*    | (size_t) of |
|                                      | fset in items   |                    |
|                                      |     +-----------------+------------- |
|                                      | ----------------+                    |
|                                      |                                      |
|                                      | set the machine stamp offset in CCP4 |
|                                      | items determined by the mode of      |
|                                      | @cfile. See                          |
|                                      | `ccp4_file_setmode <library__file_ |
|                                      | 8c.html#a25>`__ ().                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------------+-------------+------+----+                              |
| | int ccp4_file_status   | (    | const `CCP4File <library__file_8h.ht |
| ml#a0>`__ *    |   *cfile*   | )    |    |                              |
| +--------------------------+------+------------------------------------- |
| ----------------+-------------+------+----+                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_status:                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     @cfile error status              |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | long ccp4_file_tell   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ *    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_tell:                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | Current location in file, uses       |
|                                      | either ftell or lseek.               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     current offset of @cfile in      |
|                                      |     bytes.                           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
| | int ccp4_file_warch   | (    | `CCP4File <library__file_8h.html#a0>` |
| __ *    |   *cfile*   | )    |    |                                     |
| +-------------------------+------+-------------------------------------- |
| ---------+-------------+------+----+                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_warch:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+-----------------+ |
|                                      |     | *cfile*    | (CCP4File * )   | |
|                                      |     +------------+-----------------+ |
|                                      |                                      |
|                                      | write machine stamp to file          |
|                                      | @cfile->stream. The machine stamp is |
|                                      | placed at @cfile->stamp_loc items,  |
|                                      | set by                               |
|                                      | `ccp4_file_setstamp <library__file |
|                                      | _8c.html#a24>`__ ()                 |
|                                      | (defaults to 0).                     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4_file_write

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_write:                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | mode dependent write function. Write |
|                                      | @nitems items from @buffer to        |
|                                      | @cfile->stream as determined by      |
|                                      | cfile->mode.                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_writechar

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writechar:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | char write function. Write @nitems   |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_writecomp

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writecomp:               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | complex {float,float} write          |
|                                      | function. Write @nitems items from   |
|                                      | @buffer to @cfile->stream.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex items written  |
|                                      |     on success, EOF on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_file_writefloat

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writefloat:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | float write function. Write @nitems  |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      | Returns number of floats written on  |
|                                      | success, EOF on failure              |
+--------------------------------------+--------------------------------------+

int ccp4_file_writeint

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writeint:                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | int write function. Write @nitems    |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of int written on         |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_writeshort

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writeshort:              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short write function. Write @nitems  |
|                                      | items from @buffer to                |
|                                      | @cfile->stream.                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of short written on       |
|                                      |     success, EOF on failure          |
+--------------------------------------+--------------------------------------+

int ccp4_file_writeshortcomp

( 

`CCP4File <library__file_8h.html#a0>`__ * 

  *cfile*,

const uint8 * 

  *buffer*,

size_t 

  *nitems*

) 

+--------------------------------------+--------------------------------------+
|                                      | ccp4_file_writeshortcomp:          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *cfile*     | (CCP4File * )    |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *buffer*    | (uint8 * ) buffe |
|                                      | r           |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nitems*    | (size_t) number |
|                                      |  of items   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |                                      |
|                                      | short complex {short,short} write    |
|                                      | function. Write @nitems items from   |
|                                      | @buffer to @cfile->stream.           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of complex items written  |
|                                      |     on success, EOF on failure       |
+--------------------------------------+--------------------------------------+
