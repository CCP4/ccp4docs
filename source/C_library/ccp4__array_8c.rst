`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_array.c File Reference
============================

| ``#include "ccp4_array.h"``

| 

Functions
---------

`ccp4_ptr <ccp4__array_8h.html#a16>`__ 

`ccp4array_new_ <ccp4__array_8c.html#a1>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p)

`ccp4_ptr <ccp4__array_8h.html#a16>`__ 

`ccp4array_new_size_ <ccp4__array_8c.html#a2>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int size, const
size_t reclen)

void 

`ccp4array_resize_ <ccp4__array_8c.html#a3>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int size, const
size_t reclen)

void 

`ccp4array_reserve_ <ccp4__array_8c.html#a4>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int size, const
size_t reclen)

void 

`ccp4array_append_ <ccp4__array_8c.html#a5>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p,
`ccp4_constptr <ccp4__array_8h.html#a14>`__ data, const size_t reclen)

void 

`ccp4array_append_n_ <ccp4__array_8c.html#a6>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p,
`ccp4_constptr <ccp4__array_8h.html#a14>`__ data, const int n, const
size_t reclen)

void 

`ccp4array_append_list_ <ccp4__array_8c.html#a7>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p,
`ccp4_constptr <ccp4__array_8h.html#a14>`__ data, const int n, const
size_t reclen)

void 

`ccp4array_insert_ <ccp4__array_8c.html#a8>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int i,
`ccp4_constptr <ccp4__array_8h.html#a14>`__ data, const size_t reclen)

void 

`ccp4array_delete_ordered_ <ccp4__array_8c.html#a9>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int i, const size_t
reclen)

void 

`ccp4array_delete_ <ccp4__array_8c.html#a10>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const int i, const size_t
reclen)

void 

`ccp4array_delete_last_ <ccp4__array_8c.html#a11>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p, const size_t reclen)

int 

`ccp4array_size_ <ccp4__array_8c.html#a12>`__
(`ccp4_constptr <ccp4__array_8h.html#a14>`__ *p)

void 

`ccp4array_free_ <ccp4__array_8c.html#a13>`__
(`ccp4_ptr <ccp4__array_8h.html#a16>`__ *p)

--------------

Detailed Description
--------------------

implementation file for resizable array implementation. Kevin Cowtan

--------------

Function Documentation
----------------------

void ccp4array_append_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

`ccp4_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const size_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array_append   |
+-----+-------------------------------+

void ccp4array_append_list_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

`ccp4_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const int 

  *n*,

const size_t 

  *reclen*

) 

+-----+-------------------------------------+
|     | See macro ccp4array_append_list   |
+-----+-------------------------------------+

void ccp4array_append_n_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

`ccp4_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const int 

  *n*,

const size_t 

  *reclen*

) 

+-----+----------------------------------+
|     | See macro ccp4array_append_n   |
+-----+----------------------------------+

void ccp4array_delete_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *i*,

const size_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array_delete   |
+-----+-------------------------------+

void ccp4array_delete_last_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const size_t 

  *reclen*

) 

+-----+-------------------------------------+
|     | See macro ccp4array_delete_last   |
+-----+-------------------------------------+

void ccp4array_delete_ordered_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *i*,

const size_t 

  *reclen*

) 

+-----+----------------------------------------+
|     | See macro ccp4array_delete_ordered   |
+-----+----------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------------------------- |
| ----------+---------+------+----+                                        |
| | void ccp4array_free_   | (    | `ccp4_ptr <ccp4__array_8h.html#a16> |
| `__ *    |   *p*   | )    |    |                                        |
| +--------------------------+------+------------------------------------- |
| ----------+---------+------+----+                                        |
+--------------------------------------------------------------------------+

+-----+-----------------------------+
|     | See macro ccp4array_free   |
+-----+-----------------------------+

void ccp4array_insert_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *i*,

`ccp4_constptr <ccp4__array_8h.html#a14>`__ 

  *data*,

const size_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array_insert   |
+-----+-------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------------------------+------+--- |
| --------------------------------------------+---------+------+----+      |
| | `ccp4_ptr <ccp4__array_8h.html#a16>`__ ccp4array_new_   | (    | `c |
| cp4_ptr <ccp4__array_8h.html#a16>`__ *    |   *p*   | )    |    |      |
| +------------------------------------------------------------+------+--- |
| --------------------------------------------+---------+------+----+      |
+--------------------------------------------------------------------------+

+-----+----------------------------+
|     | See macro ccp4array_new   |
+-----+----------------------------+

`ccp4_ptr <ccp4__array_8h.html#a16>`__ ccp4array_new_size_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *size*,

const size_t 

  *reclen*

) 

+-----+----------------------------------+
|     | See macro ccp4array_new_size   |
+-----+----------------------------------+

void ccp4array_reserve_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *size*,

const size_t 

  *reclen*

) 

+-----+--------------------------------+
|     | See macro ccp4array_reserve   |
+-----+--------------------------------+

void ccp4array_resize_

( 

`ccp4_ptr <ccp4__array_8h.html#a16>`__ * 

  *p*,

const int 

  *size*,

const size_t 

  *reclen*

) 

+-----+-------------------------------+
|     | See macro ccp4array_resize   |
+-----+-------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+-------------------------------------- |
| --------------+---------+------+----+                                    |
| | int ccp4array_size_   | (    | `ccp4_constptr <ccp4__array_8h.html# |
| a14>`__ *    |   *p*   | )    |    |                                    |
| +-------------------------+------+-------------------------------------- |
| --------------+---------+------+----+                                    |
+--------------------------------------------------------------------------+

+-----+-----------------------------+
|     | See macro ccp4array_size   |
+-----+-----------------------------+
