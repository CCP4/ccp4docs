`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_general.c File Reference
==============================

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <math.h>``
| ``#include <stdarg.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_general.h"``
| ``#include "ccp4_program.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``
| ``#include "ccp4_errno.h"``

| 

Functions
---------

 int 

**ccperror** (int ierr, const char *message)

 int 

**ccperror_noexit** (int ierr, const char *message)

 int 

**ccp4printf** (int level, char *format,...)

 int 

**ccp4fyp** (int argc, char **argv)

 int 

**ccp4fyp_cleanup** (int ienv, char **envname, char **envtype, char
**envext, char *logical_name, char *file_name, char *file_type,
char *file_ext, char *env_file, char *def_file, char *dir,
CCP4PARSERARRAY *parser)

 int 

**ccp4setenv** (char *logical_name, char *value, char **envname,
char **envtype, char **envext, int *ienv, int no_overwrt)

 int 

**ccp4setenv_cleanup** (char *file_ext, char *file_root, char
*file_path, char *file_name)

 int 

**ccpexists** (char *filename)

 int 

**ccpputenv** (char *logical_name, char *file_name)

 void 

**ccp4_banner** (void)

--------------

Detailed Description
--------------------

General library functions and utilities. Peter Briggs et al
