`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CINCH File Members
==================

`a <#index_a>`__ | `c <#index_c>`__ | `d <#index_d>`__ | `f <#index_f>`__ | `g <#index_g>`__ | `h <#index_h>`__ | `i <#index_i>`__ | `m <#index_m>`__ | `n <#index_n>`__ | `o <#index_o>`__ | `r <#index_r>`__ | `s <#index_s>`__

Here is a list of all documented file members with links to the
documentation:

- a -
~~~~~

-  all_factors_le_19() : `csymlib.h <csymlib_8h.html#a61>`__
-  ASU_1b() : `csymlib.h <csymlib_8h.html#a14>`__
-  ASU_2_m() : `csymlib.h <csymlib_8h.html#a15>`__
-  ASU_3b() : `csymlib.h <csymlib_8h.html#a19>`__
-  ASU_3bm() : `csymlib.h <csymlib_8h.html#a20>`__
-  ASU_3bmx() : `csymlib.h <csymlib_8h.html#a21>`__
-  ASU_4_m() : `csymlib.h <csymlib_8h.html#a17>`__
-  ASU_4_mmm() : `csymlib.h <csymlib_8h.html#a18>`__
-  ASU_6_m() : `csymlib.h <csymlib_8h.html#a22>`__
-  ASU_6_mmm() : `csymlib.h <csymlib_8h.html#a23>`__
-  ASU_m3b() : `csymlib.h <csymlib_8h.html#a24>`__
-  ASU_m3bm() : `csymlib.h <csymlib_8h.html#a25>`__
-  ASU_mmm() : `csymlib.h <csymlib_8h.html#a16>`__

- c -
~~~~~

-  ccp4_byteptr : `ccp4_array.h <ccp4__array_8h.html#a15>`__
-  ccp4_cmap_close() : `cmaplib.h <cmaplib_8h.html#a7>`__
-  ccp4_cmap_closemode() : `cmaplib.h <cmaplib_8h.html#a8>`__
-  ccp4_cmap_get_cell() : `cmaplib.h <cmaplib_8h.html#a20>`__
-  ccp4_cmap_get_datamode() : `cmaplib.h <cmaplib_8h.html#a34>`__
-  ccp4_cmap_get_dim() : `cmaplib.h <cmaplib_8h.html#a24>`__
-  ccp4_cmap_get_grid() : `cmaplib.h <cmaplib_8h.html#a21>`__
-  ccp4_cmap_get_label() : `cmaplib.h <cmaplib_8h.html#a46>`__
-  ccp4_cmap_get_local_header() :
   `cmaplib.h <cmaplib_8h.html#a36>`__
-  ccp4_cmap_get_mapstats() : `cmaplib.h <cmaplib_8h.html#a26>`__
-  ccp4_cmap_get_mask() : `cmaplib.h <cmaplib_8h.html#a42>`__
-  ccp4_cmap_get_order() : `cmaplib.h <cmaplib_8h.html#a23>`__
-  ccp4_cmap_get_origin() : `cmaplib.h <cmaplib_8h.html#a22>`__
-  ccp4_cmap_get_spacegroup() : `cmaplib.h <cmaplib_8h.html#a25>`__
-  ccp4_cmap_get_symop() : `cmaplib.h <cmaplib_8h.html#a40>`__
-  ccp4_cmap_get_title() : `cmaplib.h <cmaplib_8h.html#a48>`__
-  ccp4_cmap_num_symop() : `cmaplib.h <cmaplib_8h.html#a38>`__
-  ccp4_cmap_number_label() : `cmaplib.h <cmaplib_8h.html#a44>`__
-  ccp4_cmap_open() : `cmaplib.h <cmaplib_8h.html#a6>`__,
   `cmap_open.c <cmap__open_8c.html#a3>`__
-  ccp4_cmap_read_data() : `cmaplib.h <cmaplib_8h.html#a14>`__
-  ccp4_cmap_read_row() : `cmaplib.h <cmaplib_8h.html#a13>`__
-  ccp4_cmap_read_section() : `cmaplib.h <cmaplib_8h.html#a12>`__
-  ccp4_cmap_read_section_header() :
   `cmaplib.h <cmaplib_8h.html#a18>`__
-  ccp4_cmap_seek_data() : `cmaplib.h <cmaplib_8h.html#a11>`__
-  ccp4_cmap_seek_row() : `cmaplib.h <cmaplib_8h.html#a10>`__
-  ccp4_cmap_seek_section() : `cmaplib.h <cmaplib_8h.html#a9>`__
-  ccp4_cmap_seek_symop() : `cmaplib.h <cmaplib_8h.html#a39>`__
-  ccp4_cmap_set_cell() : `cmaplib.h <cmaplib_8h.html#a27>`__
-  ccp4_cmap_set_datamode() : `cmaplib.h <cmaplib_8h.html#a35>`__
-  ccp4_cmap_set_dim() : `cmaplib.h <cmaplib_8h.html#a31>`__
-  ccp4_cmap_set_grid() : `cmaplib.h <cmaplib_8h.html#a28>`__
-  ccp4_cmap_set_label() : `cmaplib.h <cmaplib_8h.html#a45>`__
-  ccp4_cmap_set_local_header() :
   `cmaplib.h <cmaplib_8h.html#a37>`__
-  ccp4_cmap_set_mapstats() : `cmaplib.h <cmaplib_8h.html#a33>`__
-  ccp4_cmap_set_mask() : `cmaplib.h <cmaplib_8h.html#a43>`__
-  ccp4_cmap_set_order() : `cmaplib.h <cmaplib_8h.html#a30>`__
-  ccp4_cmap_set_origin() : `cmaplib.h <cmaplib_8h.html#a29>`__
-  ccp4_cmap_set_spacegroup() : `cmaplib.h <cmaplib_8h.html#a32>`__
-  ccp4_cmap_set_symop() : `cmaplib.h <cmaplib_8h.html#a41>`__
-  ccp4_cmap_set_title() : `cmaplib.h <cmaplib_8h.html#a47>`__
-  ccp4_cmap_write_data() : `cmaplib.h <cmaplib_8h.html#a17>`__
-  ccp4_cmap_write_row() : `cmaplib.h <cmaplib_8h.html#a16>`__
-  ccp4_cmap_write_section() : `cmaplib.h <cmaplib_8h.html#a15>`__
-  ccp4_cmap_write_section_header() :
   `cmaplib.h <cmaplib_8h.html#a19>`__
-  ccp4_constptr : `ccp4_array.h <ccp4__array_8h.html#a14>`__
-  ccp4_CtoFString() : `library_f.c <library__f_8c.html#a2>`__,
   `ccp4_fortran.h <ccp4__fortran_8h.html#a16>`__
-  ccp4_errno : `library_err.c <library__err_8c.html#a1>`__,
   `ccp4_errno.h <ccp4__errno_8h.html#a17>`__
-  ccp4_error() : `ccp4_errno.h <ccp4__errno_8h.html#a18>`__
-  ccp4_fatal() : `ccp4_errno.h <ccp4__errno_8h.html#a20>`__
-  ccp4_file_byte() : `library_file.c <library__file_8c.html#a30>`__
-  ccp4_file_clearerr() :
   `library_file.h <library__file_8h.html#a40>`__,
   `library_file.c <library__file_8c.html#a56>`__
-  ccp4_file_close() : `library_file.h <library__file_8h.html#a6>`__,
   `library_file.c <library__file_8c.html#a34>`__
-  ccp4_file_error() : `library_file.c <library__file_8c.html#a58>`__
-  ccp4_file_fatal() :
   `library_file.h <library__file_8h.html#a41>`__,
   `library_file.c <library__file_8c.html#a57>`__
-  ccp4_file_feof() : `library_file.h <library__file_8h.html#a39>`__,
   `library_file.c <library__file_8c.html#a55>`__
-  ccp4_file_flush() :
   `library_file.h <library__file_8h.html#a36>`__,
   `library_file.c <library__file_8c.html#a59>`__
-  ccp4_file_is_append() :
   `library_file.h <library__file_8h.html#a15>`__,
   `library_file.c <library__file_8c.html#a19>`__
-  ccp4_file_is_buffered() :
   `library_file.h <library__file_8h.html#a17>`__,
   `library_file.c <library__file_8c.html#a21>`__
-  ccp4_file_is_read() :
   `library_file.h <library__file_8h.html#a14>`__,
   `library_file.c <library__file_8c.html#a18>`__
-  ccp4_file_is_scratch() :
   `library_file.h <library__file_8h.html#a16>`__,
   `library_file.c <library__file_8c.html#a20>`__
-  ccp4_file_is_write() :
   `library_file.h <library__file_8h.html#a13>`__,
   `library_file.c <library__file_8c.html#a17>`__
-  ccp4_file_itemsize() :
   `library_file.h <library__file_8h.html#a10>`__,
   `library_file.c <library__file_8c.html#a27>`__
-  ccp4_file_length() :
   `library_file.h <library__file_8h.html#a37>`__,
   `library_file.c <library__file_8c.html#a53>`__
-  ccp4_file_mode() : `library_file.h <library__file_8h.html#a7>`__,
   `library_file.c <library__file_8c.html#a26>`__
-  ccp4_file_name() : `library_file.h <library__file_8h.html#a19>`__,
   `library_file.c <library__file_8c.html#a28>`__
-  ccp4_file_open() : `library_file.h <library__file_8h.html#a1>`__,
   `library_file.c <library__file_8c.html#a33>`__
-  ccp4_file_open_fd() :
   `library_file.h <library__file_8h.html#a3>`__,
   `library_file.c <library__file_8c.html#a32>`__
-  ccp4_file_open_file() :
   `library_file.h <library__file_8h.html#a2>`__,
   `library_file.c <library__file_8c.html#a31>`__
-  ccp4_file_print() :
   `library_file.h <library__file_8h.html#a42>`__,
   `library_file.c <library__file_8c.html#a60>`__
-  ccp4_file_rarch() : `library_file.h <library__file_8h.html#a4>`__,
   `library_file.c <library__file_8c.html#a35>`__
-  ccp4_file_raw_read() :
   `library_file.h <library__file_8h.html#a44>`__,
   `library_file.c <library__file_8c.html#a10>`__
-  ccp4_file_raw_seek() :
   `library_file.h <library__file_8h.html#a43>`__,
   `library_file.c <library__file_8c.html#a12>`__
-  ccp4_file_raw_write() :
   `library_file.h <library__file_8h.html#a45>`__,
   `library_file.c <library__file_8c.html#a11>`__
-  ccp4_file_read() : `library_file.h <library__file_8h.html#a20>`__,
   `library_file.c <library__file_8c.html#a37>`__
-  ccp4_file_readchar() :
   `library_file.h <library__file_8h.html#a26>`__,
   `library_file.c <library__file_8c.html#a43>`__
-  ccp4_file_readcomp() :
   `library_file.h <library__file_8h.html#a21>`__,
   `library_file.c <library__file_8c.html#a38>`__
-  ccp4_file_readfloat() :
   `library_file.h <library__file_8h.html#a23>`__,
   `library_file.c <library__file_8c.html#a40>`__
-  ccp4_file_readint() :
   `library_file.h <library__file_8h.html#a24>`__,
   `library_file.c <library__file_8c.html#a41>`__
-  ccp4_file_readshort() :
   `library_file.h <library__file_8h.html#a25>`__,
   `library_file.c <library__file_8c.html#a42>`__
-  ccp4_file_readshortcomp() :
   `library_file.h <library__file_8h.html#a22>`__,
   `library_file.c <library__file_8c.html#a39>`__
-  ccp4_file_rewind() :
   `library_file.h <library__file_8h.html#a35>`__,
   `library_file.c <library__file_8c.html#a52>`__
-  ccp4_file_seek() : `library_file.h <library__file_8h.html#a34>`__,
   `library_file.c <library__file_8c.html#a51>`__
-  ccp4_file_setbyte() :
   `library_file.h <library__file_8h.html#a11>`__,
   `library_file.c <library__file_8c.html#a29>`__
-  ccp4_file_setmode() :
   `library_file.h <library__file_8h.html#a8>`__,
   `library_file.c <library__file_8c.html#a25>`__
-  ccp4_file_setstamp() :
   `library_file.h <library__file_8h.html#a9>`__,
   `library_file.c <library__file_8c.html#a24>`__
-  ccp4_file_status() :
   `library_file.h <library__file_8h.html#a18>`__,
   `library_file.c <library__file_8c.html#a22>`__
-  ccp4_file_tell() : `library_file.h <library__file_8h.html#a38>`__,
   `library_file.c <library__file_8c.html#a54>`__
-  ccp4_file_warch() : `library_file.h <library__file_8h.html#a5>`__,
   `library_file.c <library__file_8c.html#a36>`__
-  ccp4_file_write() :
   `library_file.h <library__file_8h.html#a27>`__,
   `library_file.c <library__file_8c.html#a44>`__
-  ccp4_file_writechar() :
   `library_file.h <library__file_8h.html#a33>`__,
   `library_file.c <library__file_8c.html#a50>`__
-  ccp4_file_writecomp() :
   `library_file.h <library__file_8h.html#a28>`__,
   `library_file.c <library__file_8c.html#a45>`__
-  ccp4_file_writefloat() :
   `library_file.h <library__file_8h.html#a30>`__,
   `library_file.c <library__file_8c.html#a47>`__
-  ccp4_file_writeint() :
   `library_file.h <library__file_8h.html#a31>`__,
   `library_file.c <library__file_8c.html#a48>`__
-  ccp4_file_writeshort() :
   `library_file.h <library__file_8h.html#a32>`__,
   `library_file.c <library__file_8c.html#a49>`__
-  ccp4_file_writeshortcomp() :
   `library_file.h <library__file_8h.html#a29>`__,
   `library_file.c <library__file_8c.html#a46>`__
-  ccp4_FtoCString() : `library_f.c <library__f_8c.html#a1>`__,
   `ccp4_fortran.h <ccp4__fortran_8h.html#a15>`__
-  ccp4_int_compare() : `csymlib.h <csymlib_8h.html#a37>`__
-  ccp4_ismnf() : `cmtzlib.h <cmtzlib_8h.html#a79>`__
-  ccp4_keymatch() : `ccp4_parser.h <ccp4__parser_8h.html#a12>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a22>`__
-  ccp4_lhprt() : `cmtzlib.h <cmtzlib_8h.html#a80>`__
-  ccp4_lhprt_adv() : `cmtzlib.h <cmtzlib_8h.html#a81>`__
-  ccp4_liberr_verbosity() :
   `ccp4_errno.h <ccp4__errno_8h.html#a21>`__
-  ccp4_licence_exists() :
   `ccp4_program.h <ccp4__program_8h.html#a16>`__,
   `ccp4_program.c <ccp4__program_8c.html#a10>`__
-  ccp4_lrassn() : `cmtzlib.h <cmtzlib_8h.html#a75>`__
-  ccp4_lrbat() : `cmtzlib.h <cmtzlib_8h.html#a82>`__
-  ccp4_lrbats() : `cmtzlib.h <cmtzlib_8h.html#a70>`__
-  ccp4_lrcell() : `cmtzlib.h <cmtzlib_8h.html#a71>`__
-  ccp4_lrhist() : `cmtzlib.h <cmtzlib_8h.html#a68>`__
-  ccp4_lridx() : `cmtzlib.h <cmtzlib_8h.html#a76>`__
-  ccp4_lrreff() : `cmtzlib.h <cmtzlib_8h.html#a78>`__
-  ccp4_lrrefl() : `cmtzlib.h <cmtzlib_8h.html#a77>`__
-  ccp4_lrsort() : `cmtzlib.h <cmtzlib_8h.html#a69>`__
-  ccp4_lrsymi() : `cmtzlib.h <cmtzlib_8h.html#a72>`__
-  ccp4_lrsymm() : `cmtzlib.h <cmtzlib_8h.html#a73>`__
-  ccp4_lrtitl() : `cmtzlib.h <cmtzlib_8h.html#a67>`__
-  ccp4_lwbat() : `cmtzlib.h <cmtzlib_8h.html#a91>`__
-  ccp4_lwrefl() : `cmtzlib.h <cmtzlib_8h.html#a90>`__
-  ccp4_lwsymm() : `cmtzlib.h <cmtzlib_8h.html#a87>`__
-  ccp4_lwtitl() : `cmtzlib.h <cmtzlib_8h.html#a84>`__
-  ccp4_parse_end() : `ccp4_parser.h <ccp4__parser_8h.html#a4>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a14>`__
-  ccp4_parse_start() : `ccp4_parser.h <ccp4__parser_8h.html#a3>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a13>`__
-  ccp4_parser() : `ccp4_parser.h <ccp4__parser_8h.html#a11>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a21>`__
-  ccp4_prog_vers() : `ccp4_program.h <ccp4__program_8h.html#a7>`__,
   `ccp4_program.c <ccp4__program_8c.html#a1>`__
-  ccp4_ptr : `ccp4_array.h <ccp4__array_8h.html#a16>`__
-  ccp4_signal() : `ccp4_errno.h <ccp4__errno_8h.html#a22>`__
-  ccp4_spg_get_centering() : `csymlib.h <csymlib_8h.html#a12>`__
-  ccp4_spgrp_equal() : `csymlib.h <csymlib_8h.html#a34>`__
-  ccp4_spgrp_equal_order() : `csymlib.h <csymlib_8h.html#a35>`__
-  ccp4_spgrp_reverse_lookup() : `csymlib.h <csymlib_8h.html#a5>`__
-  ccp4_strerror() : `ccp4_errno.h <ccp4__errno_8h.html#a19>`__
-  ccp4_symop_code() : `csymlib.h <csymlib_8h.html#a36>`__
-  ccp4_symop_invert() : `csymlib.h <csymlib_8h.html#a27>`__
-  ccp4_utils_basename() :
   `library_utils.c <library__utils_8c.html#a21>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a20>`__
-  ccp4_utils_bml() :
   `library_utils.c <library__utils_8c.html#a12>`__
-  ccp4_utils_calloc() :
   `library_utils.c <library__utils_8c.html#a19>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a17>`__
-  ccp4_utils_chmod() :
   `library_utils.c <library__utils_8c.html#a16>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a14>`__
-  ccp4_utils_date() :
   `library_utils.c <library__utils_8c.html#a26>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a25>`__
-  ccp4_utils_etime() :
   `library_utils.c <library__utils_8c.html#a29>`__
-  ccp4_utils_extension() :
   `library_utils.c <library__utils_8c.html#a23>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a22>`__
-  ccp4_utils_flength() :
   `library_utils.c <library__utils_8c.html#a5>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a1>`__
-  ccp4_utils_hgetlimits() :
   `library_utils.c <library__utils_8c.html#a14>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a12>`__
-  ccp4_utils_idate() :
   `library_utils.c <library__utils_8c.html#a25>`__
-  ccp4_utils_isnan() :
   `library_utils.c <library__utils_8c.html#a11>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a9>`__
-  ccp4_utils_itime() :
   `library_utils.c <library__utils_8c.html#a27>`__
-  ccp4_utils_joinfilenames() :
   `library_utils.c <library__utils_8c.html#a24>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a23>`__
-  ccp4_utils_malloc() :
   `library_utils.c <library__utils_8c.html#a17>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a15>`__
-  ccp4_utils_mkdir() :
   `library_utils.c <library__utils_8c.html#a15>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a13>`__
-  ccp4_utils_noinpbuf() :
   `library_utils.c <library__utils_8c.html#a9>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a7>`__
-  ccp4_utils_outbuf() :
   `library_utils.c <library__utils_8c.html#a8>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a6>`__
-  ccp4_utils_pathname() :
   `library_utils.c <library__utils_8c.html#a22>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a21>`__
-  ccp4_utils_print() :
   `library_utils.c <library__utils_8c.html#a6>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a4>`__
-  ccp4_utils_realloc() :
   `library_utils.c <library__utils_8c.html#a18>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a16>`__
-  ccp4_utils_setenv() :
   `library_utils.c <library__utils_8c.html#a7>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a5>`__
-  ccp4_utils_time() :
   `library_utils.c <library__utils_8c.html#a28>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a27>`__
-  ccp4_utils_translate_mode_float() :
   `library_utils.c <library__utils_8c.html#a4>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a2>`__
-  ccp4_utils_username() :
   `library_utils.c <library__utils_8c.html#a20>`__,
   `ccp4_utils.h <ccp4__utils_8h.html#a19>`__
-  ccp4_utils_wrg() :
   `library_utils.c <library__utils_8c.html#a13>`__
-  ccp4array_append : `ccp4_array.h <ccp4__array_8h.html#a4>`__
-  ccp4array_append_() : `ccp4_array.h <ccp4__array_8h.html#a22>`__,
   `ccp4_array.c <ccp4__array_8c.html#a5>`__
-  ccp4array_append_list : `ccp4_array.h <ccp4__array_8h.html#a6>`__
-  ccp4array_append_list_() :
   `ccp4_array.h <ccp4__array_8h.html#a24>`__,
   `ccp4_array.c <ccp4__array_8c.html#a7>`__
-  ccp4array_append_n : `ccp4_array.h <ccp4__array_8h.html#a5>`__
-  ccp4array_append_n_() :
   `ccp4_array.h <ccp4__array_8h.html#a23>`__,
   `ccp4_array.c <ccp4__array_8c.html#a6>`__
-  ccp4array_base : `ccp4_array.h <ccp4__array_8h.html#a17>`__
-  ccp4array_delete : `ccp4_array.h <ccp4__array_8h.html#a9>`__
-  ccp4array_delete_() : `ccp4_array.h <ccp4__array_8h.html#a27>`__,
   `ccp4_array.c <ccp4__array_8c.html#a10>`__
-  ccp4array_delete_last : `ccp4_array.h <ccp4__array_8h.html#a10>`__
-  ccp4array_delete_last_() :
   `ccp4_array.h <ccp4__array_8h.html#a28>`__,
   `ccp4_array.c <ccp4__array_8c.html#a11>`__
-  ccp4array_delete_ordered :
   `ccp4_array.h <ccp4__array_8h.html#a8>`__
-  ccp4array_delete_ordered_() :
   `ccp4_array.h <ccp4__array_8h.html#a26>`__,
   `ccp4_array.c <ccp4__array_8c.html#a9>`__
-  ccp4array_free : `ccp4_array.h <ccp4__array_8h.html#a12>`__
-  ccp4array_free_() : `ccp4_array.h <ccp4__array_8h.html#a30>`__,
   `ccp4_array.c <ccp4__array_8c.html#a13>`__
-  ccp4array_insert : `ccp4_array.h <ccp4__array_8h.html#a7>`__
-  ccp4array_insert_() : `ccp4_array.h <ccp4__array_8h.html#a25>`__,
   `ccp4_array.c <ccp4__array_8c.html#a8>`__
-  ccp4array_new : `ccp4_array.h <ccp4__array_8h.html#a0>`__
-  ccp4array_new_() : `ccp4_array.h <ccp4__array_8h.html#a18>`__,
   `ccp4_array.c <ccp4__array_8c.html#a1>`__
-  ccp4array_new_size : `ccp4_array.h <ccp4__array_8h.html#a1>`__
-  ccp4array_new_size_() :
   `ccp4_array.h <ccp4__array_8h.html#a19>`__,
   `ccp4_array.c <ccp4__array_8c.html#a2>`__
-  ccp4array_reserve : `ccp4_array.h <ccp4__array_8h.html#a3>`__
-  ccp4array_reserve_() : `ccp4_array.h <ccp4__array_8h.html#a21>`__,
   `ccp4_array.c <ccp4__array_8c.html#a4>`__
-  ccp4array_resize : `ccp4_array.h <ccp4__array_8h.html#a2>`__
-  ccp4array_resize_() : `ccp4_array.h <ccp4__array_8h.html#a20>`__,
   `ccp4_array.c <ccp4__array_8c.html#a3>`__
-  ccp4array_size : `ccp4_array.h <ccp4__array_8h.html#a11>`__
-  ccp4array_size_() : `ccp4_array.h <ccp4__array_8h.html#a29>`__,
   `ccp4_array.c <ccp4__array_8c.html#a12>`__
-  ccp4Callback() : `ccp4_program.h <ccp4__program_8h.html#a12>`__,
   `ccp4_program.c <ccp4__program_8c.html#a6>`__
-  ccp4f_mem_tidy() :
   `ccp4_general_f.c <ccp4__general__f_8c.html#a2>`__
-  CCP4File : `library_file.h <library__file_8h.html#a0>`__
-  ccp4InvokeCallback() :
   `ccp4_program.h <ccp4__program_8h.html#a14>`__,
   `ccp4_program.c <ccp4__program_8c.html#a8>`__
-  CCP4IObj : `ccp4_diskio_f.c <ccp4__diskio__f_8c.html#a1>`__
-  ccp4NullCallback() : `ccp4_program.h <ccp4__program_8h.html#a15>`__,
   `ccp4_program.c <ccp4__program_8c.html#a9>`__
-  ccp4ProgramName() : `ccp4_program.h <ccp4__program_8h.html#a8>`__,
   `ccp4_program.c <ccp4__program_8c.html#a2>`__
-  ccp4ProgramTime() : `ccp4_program.h <ccp4__program_8h.html#a10>`__,
   `ccp4_program.c <ccp4__program_8c.html#a4>`__
-  ccp4RCSDate() : `ccp4_program.h <ccp4__program_8h.html#a9>`__,
   `ccp4_program.c <ccp4__program_8c.html#a3>`__
-  ccp4SetCallback() : `ccp4_program.h <ccp4__program_8h.html#a13>`__,
   `ccp4_program.c <ccp4__program_8c.html#a7>`__
-  ccp4spg_centric_phase() : `csymlib.h <csymlib_8h.html#a47>`__
-  ccp4spg_check_centric_zone() : `csymlib.h <csymlib_8h.html#a46>`__
-  ccp4spg_check_epsilon_zone() : `csymlib.h <csymlib_8h.html#a52>`__
-  ccp4spg_check_symm_cell() : `csymlib.h <csymlib_8h.html#a63>`__
-  ccp4spg_describe_centric_zone() :
   `csymlib.h <csymlib_8h.html#a49>`__
-  ccp4spg_describe_epsilon_zone() :
   `csymlib.h <csymlib_8h.html#a54>`__
-  ccp4spg_do_chb() : `csymlib.h <csymlib_8h.html#a43>`__
-  ccp4spg_free() : `csymlib.h <csymlib_8h.html#a9>`__
-  ccp4spg_generate_indices() : `csymlib.h <csymlib_8h.html#a41>`__
-  ccp4spg_generate_origins() : `csymlib.h <csymlib_8h.html#a56>`__
-  ccp4spg_get_multiplicity() : `csymlib.h <csymlib_8h.html#a51>`__
-  ccp4spg_is_centric() : `csymlib.h <csymlib_8h.html#a45>`__
-  ccp4spg_is_in_asu() : `csymlib.h <csymlib_8h.html#a39>`__
-  ccp4spg_is_in_pm_asu() : `csymlib.h <csymlib_8h.html#a38>`__
-  ccp4spg_is_sysabs() : `csymlib.h <csymlib_8h.html#a55>`__
-  ccp4spg_load_by_ccp4_num() : `csymlib.h <csymlib_8h.html#a2>`__
-  ccp4spg_load_by_ccp4_spgname() :
   `csymlib.h <csymlib_8h.html#a4>`__
-  ccp4spg_load_by_spgname() : `csymlib.h <csymlib_8h.html#a3>`__
-  ccp4spg_load_by_standard_num() :
   `csymlib.h <csymlib_8h.html#a1>`__
-  ccp4spg_load_laue() : `csymlib.h <csymlib_8h.html#a13>`__
-  ccp4spg_load_spacegroup() : `csymlib.h <csymlib_8h.html#a6>`__
-  ccp4spg_mem_tidy() : `csymlib.h <csymlib_8h.html#a7>`__
-  ccp4spg_name_de_colon() : `csymlib.h <csymlib_8h.html#a31>`__
-  ccp4spg_name_equal() : `csymlib.h <csymlib_8h.html#a28>`__
-  ccp4spg_name_equal_to_lib() : `csymlib.h <csymlib_8h.html#a29>`__
-  ccp4spg_norm_trans() : `csymlib.h <csymlib_8h.html#a33>`__
-  ccp4spg_pgname_equal() : `csymlib.h <csymlib_8h.html#a32>`__
-  ccp4spg_phase_shift() : `csymlib.h <csymlib_8h.html#a42>`__
-  ccp4spg_print_centric_zones() :
   `csymlib.h <csymlib_8h.html#a48>`__
-  ccp4spg_print_epsilon_zones() :
   `csymlib.h <csymlib_8h.html#a53>`__
-  ccp4spg_print_recip_ops() : `csymlib.h <csymlib_8h.html#a58>`__
-  ccp4spg_print_recip_spgrp() : `csymlib.h <csymlib_8h.html#a57>`__
-  ccp4spg_put_in_asu() : `csymlib.h <csymlib_8h.html#a40>`__
-  ccp4spg_register_by_ccp4_num() :
   `csymlib.h <csymlib_8h.html#a10>`__
-  ccp4spg_register_by_symops() : `csymlib.h <csymlib_8h.html#a11>`__
-  ccp4spg_set_centric_zones() : `csymlib.h <csymlib_8h.html#a44>`__
-  ccp4spg_set_epsilon_zones() : `csymlib.h <csymlib_8h.html#a50>`__
-  ccp4spg_symbol_Hall() : `csymlib.h <csymlib_8h.html#a26>`__
-  ccp4spg_to_shortname() : `csymlib.h <csymlib_8h.html#a30>`__
-  ccp4uc_calc_cell_volume() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a6>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a7>`__
-  ccp4uc_calc_rcell() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a1>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a2>`__
-  ccp4uc_cells_differ() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a7>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a8>`__
-  ccp4uc_frac_orth_mat() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a0>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a1>`__
-  ccp4uc_frac_to_orth() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a3>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a4>`__
-  ccp4uc_fracu_to_orthu() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a5>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a6>`__
-  ccp4uc_is_hexagonal() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a9>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a10>`__
-  ccp4uc_is_rhombohedral() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a8>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a9>`__
-  ccp4uc_orth_to_frac() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a2>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a3>`__
-  ccp4uc_orthu_to_fracu() :
   `ccp4_unitcell.h <ccp4__unitcell_8h.html#a4>`__,
   `ccp4_unitcell.c <ccp4__unitcell_8c.html#a5>`__
-  ccp4VerbosityLevel() :
   `ccp4_program.h <ccp4__program_8h.html#a11>`__,
   `ccp4_program.c <ccp4__program_8c.html#a5>`__

- d -
~~~~~

-  DEFMODE : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a4>`__
-  DFNT_CHAR : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a34>`__
-  DFNT_DOUBLE : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a36>`__
-  DFNT_FLOAT : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a35>`__
-  DFNT_INT : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a32>`__
-  DFNT_SINT : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a31>`__
-  DFNT_UCHAR : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a33>`__
-  DFNT_UINT : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a30>`__
-  DFNTF_BEIEEE : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a26>`__
-  DFNTF_CONVEXNATIVE : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a28>`__
-  DFNTF_LEIEEE : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a29>`__
-  DFNTF_VAX : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a27>`__
-  DFNTI_IBO : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a25>`__
-  DFNTI_MBO : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a24>`__

- f -
~~~~~

-  FORTRAN_CALL : `ccp4_fortran.h <ccp4__fortran_8h.html#a8>`__
-  FORTRAN_FUN : `csymlib_f.c <csymlib__f_8c.html#a32>`__,
   `cmaplib_f.c <cmaplib__f_8c.html#a46>`__,
   `ccp4_fortran.h <ccp4__fortran_8h.html#a9>`__
-  FORTRAN_SUBR : `csymlib_f.c <csymlib__f_8c.html#a44>`__,
   `cmtzlib_f.c <cmtzlib__f_8c.html#a85>`__,
   `cmaplib_f.c <cmaplib__f_8c.html#a53>`__,
   `ccp4_general_f.c <ccp4__general__f_8c.html#a19>`__,
   `ccp4_diskio_f.c <ccp4__diskio__f_8c.html#a23>`__,
   `ccp4_fortran.h <ccp4__fortran_8h.html#a7>`__

- g -
~~~~~

-  get_grid_sample() : `csymlib.h <csymlib_8h.html#a62>`__

- h -
~~~~~

-  html_log_output() :
   `ccp4_program.h <ccp4__program_8h.html#a17>`__,
   `ccp4_program.c <ccp4__program_8c.html#a11>`__

- i -
~~~~~

-  init_cmap_read() : `cmap_open.c <cmap__open_8c.html#a0>`__
-  init_cmap_write() : `cmap_open.c <cmap__open_8c.html#a1>`__
-  is_cmap() : `cmap_open.c <cmap__open_8c.html#a2>`__

- m -
~~~~~

-  mat4_to_recip_symop() :
   `ccp4_parser.h <ccp4__parser_8h.html#a25>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a35>`__
-  mat4_to_symop() : `ccp4_parser.h <ccp4__parser_8h.html#a24>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a34>`__
-  MAXFILES : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a3>`__
-  MAXFLEN : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a2>`__
-  MAXSPGNAMELENGTH : `mtzdata.h <mtzdata_8h.html#a5>`__
-  MCOLUMNS : `mtzdata.h <mtzdata_8h.html#a11>`__
-  MSETS : `mtzdata.h <mtzdata_8h.html#a10>`__
-  MTZ_MAJOR_VERSN : `mtzdata.h <mtzdata_8h.html#a1>`__
-  MTZ_MINOR_VERSN : `mtzdata.h <mtzdata_8h.html#a2>`__
-  MtzAddColumn() : `cmtzlib.h <cmtzlib_8h.html#a45>`__
-  MtzAddDataset() : `cmtzlib.h <cmtzlib_8h.html#a38>`__
-  MtzAddHistory() : `cmtzlib.h <cmtzlib_8h.html#a86>`__
-  MtzAddXtal() : `cmtzlib.h <cmtzlib_8h.html#a28>`__
-  MtzArrayToBatch() : `cmtzlib.h <cmtzlib_8h.html#a64>`__
-  MtzAssignColumn() : `cmtzlib.h <cmtzlib_8h.html#a47>`__
-  MtzAssignHKLtoBase() : `cmtzlib.h <cmtzlib_8h.html#a46>`__
-  MTZBAT : `mtzdata.h <mtzdata_8h.html#a12>`__
-  MtzBatchToArray() : `cmtzlib.h <cmtzlib_8h.html#a65>`__
-  MtzCallocHist() : `cmtzlib.h <cmtzlib_8h.html#a15>`__
-  MtzColLookup() : `cmtzlib.h <cmtzlib_8h.html#a56>`__
-  MtzColPath() : `cmtzlib.h <cmtzlib_8h.html#a53>`__
-  MtzColSet() : `cmtzlib.h <cmtzlib_8h.html#a49>`__
-  MtzColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a43>`__
-  MtzColType() : `cmtzlib.h <cmtzlib_8h.html#a57>`__
-  MtzDebugHierarchy() : `cmtzlib.h <cmtzlib_8h.html#a58>`__
-  MtzDeleteRefl() : `cmtzlib.h <cmtzlib_8h.html#a8>`__
-  MtzFindInd() : `cmtzlib.h <cmtzlib_8h.html#a61>`__
-  MtzFree() : `cmtzlib.h <cmtzlib_8h.html#a10>`__
-  MtzFreeBatch() : `cmtzlib.h <cmtzlib_8h.html#a14>`__
-  MtzFreeCol() : `cmtzlib.h <cmtzlib_8h.html#a12>`__
-  MtzFreeHist() : `cmtzlib.h <cmtzlib_8h.html#a16>`__
-  MtzGet() : `cmtzlib.h <cmtzlib_8h.html#a1>`__
-  MtzGetUserCellTolerance() : `cmtzlib.h <cmtzlib_8h.html#a2>`__
-  MtzHklcoeffs() : `cmtzlib.h <cmtzlib_8h.html#a63>`__
-  MtzIcolInSet() : `cmtzlib.h <cmtzlib_8h.html#a44>`__
-  MtzInd2reso() : `cmtzlib.h <cmtzlib_8h.html#a62>`__
-  MtzIsetInXtal() : `cmtzlib.h <cmtzlib_8h.html#a32>`__
-  MtzIxtal() : `cmtzlib.h <cmtzlib_8h.html#a25>`__
-  MtzListColumn() : `cmtzlib.h <cmtzlib_8h.html#a59>`__
-  MtzListInputColumn() : `cmtzlib.h <cmtzlib_8h.html#a60>`__
-  MtzMalloc() : `cmtzlib.h <cmtzlib_8h.html#a9>`__
-  MtzMallocBatch() : `cmtzlib.h <cmtzlib_8h.html#a13>`__
-  MtzMallocCol() : `cmtzlib.h <cmtzlib_8h.html#a11>`__
-  MtzMemTidy() : `cmtzlib.h <cmtzlib_8h.html#a17>`__
-  MtzNbat() : `cmtzlib.h <cmtzlib_8h.html#a18>`__
-  MtzNbatchesInSet() : `cmtzlib.h <cmtzlib_8h.html#a42>`__
-  MtzNcol() : `cmtzlib.h <cmtzlib_8h.html#a50>`__
-  MtzNcolsInSet() : `cmtzlib.h <cmtzlib_8h.html#a39>`__
-  MtzNref() : `cmtzlib.h <cmtzlib_8h.html#a19>`__
-  MtzNset() : `cmtzlib.h <cmtzlib_8h.html#a33>`__
-  MtzNsetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a29>`__
-  MtzNumActiveCol() : `cmtzlib.h <cmtzlib_8h.html#a51>`__
-  MtzNumActiveColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a40>`__
-  MtzNumActiveSet() : `cmtzlib.h <cmtzlib_8h.html#a34>`__
-  MtzNumActiveSetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a30>`__
-  MtzNumActiveXtal() : `cmtzlib.h <cmtzlib_8h.html#a23>`__
-  MtzNumSourceCol() : `cmtzlib.h <cmtzlib_8h.html#a52>`__
-  MtzNumSourceColsInSet() : `cmtzlib.h <cmtzlib_8h.html#a41>`__
-  MtzNxtal() : `cmtzlib.h <cmtzlib_8h.html#a22>`__
-  MtzOpenForWrite() : `cmtzlib.h <cmtzlib_8h.html#a5>`__
-  MtzParseLabin() : `cmtzlib.h <cmtzlib_8h.html#a74>`__
-  MtzPathMatch() : `cmtzlib.h <cmtzlib_8h.html#a55>`__
-  MtzPrintBatchHeader() : `cmtzlib.h <cmtzlib_8h.html#a83>`__
-  MtzPut() : `cmtzlib.h <cmtzlib_8h.html#a4>`__
-  MTZRECORDLENGTH : `mtzdata.h <mtzdata_8h.html#a4>`__
-  MtzResLimits() : `cmtzlib.h <cmtzlib_8h.html#a21>`__
-  MtzRJustPath() : `cmtzlib.h <cmtzlib_8h.html#a54>`__
-  MtzRrefl() : `cmtzlib.h <cmtzlib_8h.html#a3>`__
-  MtzSetLookup() : `cmtzlib.h <cmtzlib_8h.html#a37>`__
-  MtzSetPath() : `cmtzlib.h <cmtzlib_8h.html#a36>`__
-  MtzSetsInXtal() : `cmtzlib.h <cmtzlib_8h.html#a31>`__
-  MtzSetSortOrder() : `cmtzlib.h <cmtzlib_8h.html#a85>`__
-  MtzSetXtal() : `cmtzlib.h <cmtzlib_8h.html#a35>`__
-  MtzSpacegroupNumber() : `cmtzlib.h <cmtzlib_8h.html#a20>`__
-  MtzToggleColumn() : `cmtzlib.h <cmtzlib_8h.html#a48>`__
-  MTZVERSN : `mtzdata.h <mtzdata_8h.html#a0>`__
-  MtzWhdrLine() : `cmtzlib.h <cmtzlib_8h.html#a6>`__
-  MtzWrefl() : `cmtzlib.h <cmtzlib_8h.html#a7>`__
-  MtzXtalLookup() : `cmtzlib.h <cmtzlib_8h.html#a27>`__
-  MtzXtalPath() : `cmtzlib.h <cmtzlib_8h.html#a26>`__
-  MtzXtals() : `cmtzlib.h <cmtzlib_8h.html#a24>`__
-  MXTALS : `mtzdata.h <mtzdata_8h.html#a9>`__

- n -
~~~~~

-  NBATCHINTEGERS : `mtzdata.h <mtzdata_8h.html#a7>`__
-  NBATCHREALS : `mtzdata.h <mtzdata_8h.html#a8>`__
-  NBATCHWORDS : `mtzdata.h <mtzdata_8h.html#a6>`__

- o -
~~~~~

-  O_APPEND : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a14>`__
-  O_CREAT : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a15>`__
-  O_RDONLY : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a11>`__
-  O_RDWR : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a13>`__
-  O_TMP : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a17>`__
-  O_TRUNC : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a16>`__
-  O_WRONLY : `ccp4_sysdep.h <ccp4__sysdep_8h.html#a12>`__

- r -
~~~~~

-  range_to_limits() : `csymlib.h <csymlib_8h.html#a59>`__

- s -
~~~~~

-  set_fft_grid() : `csymlib.h <csymlib_8h.html#a60>`__
-  SIZE1 : `mtzdata.h <mtzdata_8h.html#a3>`__
-  sort_batches() : `cmtzlib.h <cmtzlib_8h.html#a66>`__
-  strtolower() : `ccp4_parser.h <ccp4__parser_8h.html#a14>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a24>`__
-  strtoupper() : `ccp4_parser.h <ccp4__parser_8h.html#a13>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a23>`__
-  summary_output() : `ccp4_program.h <ccp4__program_8h.html#a18>`__,
   `ccp4_program.c <ccp4__program_8c.html#a12>`__
-  symfr_driver() : `csymlib.h <csymlib_8h.html#a8>`__
-  symop_to_mat4() : `ccp4_parser.h <ccp4__parser_8h.html#a19>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a29>`__
-  symop_to_rotandtrn() :
   `ccp4_parser.h <ccp4__parser_8h.html#a18>`__,
   `ccp4_parser.c <ccp4__parser_8c.html#a28>`__
