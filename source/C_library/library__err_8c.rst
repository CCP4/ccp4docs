`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library_err.c File Reference
=============================

| ``#include <stdlib.h>``
| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include "ccp4_errno.h"``

| 

Compounds
---------

struct  

**error_system**

| 

Functions
---------

 const char * 

**ccp4_strerror** (int error)

 void 

**ccp4_error** (const char *msg)

 void 

**ccp4_fatal** (const char *message)

 int 

**CFile_Perror** (const char *msg)

 int 

**ccp4_liberr_verbosity** (int iverb)

 void 

**ccp4_signal** (const int code, const char *const msg,
void( *callback)())

| 

Variables
---------

int 

`ccp4_errno <library__err_8c.html#a1>`__ = 0

--------------

Detailed Description
--------------------

Error handling library. Charles Ballard

--------------

Variable Documentation
----------------------

+--------------------------------------------------------------------------+
| +-----------------------+                                                |
| | int ccp4_errno = 0   |                                                |
| +-----------------------+                                                |
+--------------------------------------------------------------------------+

+-----+---------------------------------------------+
|     | @global ccp4_errno: global to store data   |
+-----+---------------------------------------------+
