`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CMAP library
------------

.. raw:: html

   <div class="fragment">

::




       

.. raw:: html

   </div>

File list
---------

-  `cmaplib.h <cmaplib_8h.html>`__ - contains details of the C/C++ API
-  cmap_data.h
-  cmap_header.h
-  cmap_skew.h
-  cmap_errno.h
-  cmap_labels.h
-  cmap_stats.h

Overview
--------

Functions defining the C-level API for accessing CCP4 map files.
