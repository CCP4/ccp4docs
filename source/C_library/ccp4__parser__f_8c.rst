`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_parser_f.c File Reference
================================

Fortran API to `ccp4_parser.c <ccp4__parser_8c.html>`__.
`More... <#_details>`__

| ``#include "ccp4_sysdep.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_general.h"``

| 

Defines
-------

 #define 

**PARSER_DEBUG**(x)

| 

Functions
---------

 int 

**fparse_isblank** (const char *line, const int line_len)

 int 

**fparse_strncpypad** (char *fstr, const char *cstr, const int lfstr)

 int 

**fparse_populate_arrays** (CCP4PARSERARRAY *parser, int *ibeg, int
*iend, int *ityp, float *fvalue, fpstr cvalue, int cvalue_len, int
*idec)

 int 

**fparse_delimiters** (CCP4PARSERARRAY *parser, char
*new_delimiters, char *new_nulldelimiters)

  

**FORTRAN_SUBR** (PARSER, parser,(fpstr key, fpstr line, int *ibeg,
int *iend, int *ityp, float *fvalue, fpstr cvalue, int *idec, int
*ntok, ftn_logical *lend, const ftn_logical *print, int key_len,
int line_len, int cvalue_len),(fpstr key, fpstr line, int *ibeg, int
*iend, int *ityp, float *fvalue, fpstr cvalue, int *idec, int
*ntok, ftn_logical *lend, const ftn_logical *print),(fpstr key, int
key_len, fpstr line, int line_len, int *ibeg, int *iend, int *ityp,
float *fvalue, fpstr cvalue, int cvalue_len, int *idec, int *ntok,
ftn_logical *lend, const ftn_logical *print))

  

**FORTRAN_SUBR** (PARSE, parse,(fpstr line, int *ibeg, int *iend, int
*ityp, float *fvalue, fpstr cvalue, int *idec, int *n, int
line_len, int cvalue_len),(fpstr line, int *ibeg, int *iend, int
*ityp, float *fvalue, fpstr cvalue, int *idec, int *n),(fpstr line,
int line_len, int *ibeg, int *iend, int *ityp, float *fvalue, fpstr
cvalue, int cvalue_len, int *idec, int *n))

  

**FORTRAN_SUBR** (PARSDL, parsdl,(fpstr newdlm, int *nnewdl, int
*nspecd, int newdlm_len),(fpstr newdlm, int *nnewdl, int
*nspecd),(fpstr newdlm, int newdlm_len, int *nnewdl, int *nspecd))

--------------

Detailed Description
--------------------

Fortran API to `ccp4_parser.c <ccp4__parser_8c.html>`__.

 **Author:**
    Peter Briggs
