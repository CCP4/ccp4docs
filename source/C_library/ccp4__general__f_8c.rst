`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_general_f.c File Reference
=================================

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include <math.h>``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_parser.h"``
| ``#include "ccp4_program.h"``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_general.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``

| 

Defines
-------

 #define 

**TMP_LENGTH**   128

| 

Functions
---------

void 

`ccp4f_mem_tidy <ccp4__general__f_8c.html#a2>`__ (void)

  

**FORTRAN_SUBR** (CCPFYP, ccpfyp,(),(),())

  

**FORTRAN_SUBR** (CCP4H_INIT_CLIB, ccp4h_init_clib,(int *ihtml,
int *isumm),(int *ihtml, int *isumm),(int *ihtml, int *isumm))

  

**FORTRAN_SUBR** (CCPUPC, ccpupc,(fpstr string, int string_len),(fpstr
string),(fpstr string, int string_len))

  

**FORTRAN_SUBR** (CCPERR, ccperr,(const int *istat, const fpstr
errstr, int errstr_len),(const int *istat, const fpstr errstr),(const
int *istat, const fpstr errstr, int errstr_len))

  

**FORTRAN_SUBR** (QPRINT, qprint,(const int *iflag, const fpstr msg,
int msg_len),(const int *iflag, const fpstr msg),(const int *iflag,
const fpstr msg, int msg_len))

 

`FORTRAN_SUBR <ccp4__general__f_8c.html#a8>`__ (UIDATE, uidate,(int
*imonth, int *iday, int *iyear),(int *imonth, int *iday, int
*iyear),(int *imonth, int *iday, int *iyear))

 

`FORTRAN_SUBR <ccp4__general__f_8c.html#a9>`__ (CCPDAT, ccpdat,(fpstr
caldat, int caldat_len),(fpstr caldat),(fpstr caldat, int caldat_len))

  

**FORTRAN_SUBR** (CCPTIM, ccptim,(int *iflag, float *cpu, float
*elaps),(int *iflag, float *cpu, float *elaps),(int *iflag, float
*cpu, float *elaps))

  

**FORTRAN_SUBR** (UTIME, utime,(fpstr ctime, int ctime_len),(fpstr
ctime),(fpstr ctime, int ctime_len))

  

**FORTRAN_SUBR** (UCPUTM, ucputm,(float *sec),(float *sec),(float
*sec))

  

**FORTRAN_SUBR** (CCP4_VERSION, ccp4_version,(const fpstr version,
int version_len),(const fpstr version),(const fpstr version, int
version_len))

  

**FORTRAN_SUBR** (CCP4_PROG_VERSION, ccp4_prog_version,(const fpstr
version, int *iflag, int version_len),(const fpstr version, int
*iflag),(const fpstr version, int version_len, int *iflag))

  

**FORTRAN_SUBR** (CCPVRS, ccpvrs,(const int *ilp, const fpstr prog,
const fpstr vdate, int prog_len, int vdate_len),(const int *ilp,
const fpstr prog, const fpstr vdate),(const int *ilp, const fpstr prog,
int prog_len, const fpstr vdate, int vdate_len))

  

**FORTRAN_SUBR** (CCPRCS, ccprcs,(const int *ilp, const fpstr prog,
const fpstr rcsdat, int prog_len, int rcsdat_len),(const int *ilp,
const fpstr prog, const fpstr rcsdat),(const int *ilp, const fpstr
prog, int prog_len, const fpstr rcsdat, int rcsdat_len))

  

**FORTRAN_SUBR** (CCPPNM, ccppnm,(const fpstr pnm, int pnm_len),(const
fpstr pnm, int pnm_len),(const fpstr pnm, int pnm_len))

  

**FORTRAN_FUN** (ftn_logical, CCPEXS, ccpexs,(const fpstr name, int
name_len),(const fpstr name),(const fpstr name, int name_len))

 

`FORTRAN_SUBR <ccp4__general__f_8c.html#a19>`__ (GETELAPSED,
getelapsed,(void),(void),(void))

--------------

Detailed Description
--------------------

Fortran API to `ccp4_general.c <ccp4__general_8c.html>`__. Created Oct.
2001 by Martyn Winn

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-------------------------+------+---------+-----+------+----+           |
| | void ccp4f_mem_tidy   | (    | void    |     | )    |    |           |
| +-------------------------+------+---------+-----+------+----+           |
+--------------------------------------------------------------------------+

+-----+-----------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Free all memory malloc'd from static pointers in Fortran interface. To be called before program exit. The function can be registered with atexit.   |
+-----+-----------------------------------------------------------------------------------------------------------------------------------------------------+

FORTRAN_SUBR

( 

GETELAPSED 

 ,

getelapsed 

 ,

(void) 

 ,

(void) 

 ,

(void) 

 

) 

+-----+------------------------------------------------------------+
|     | Print timing information to stdout wraps ccp4ProgramTime   |
+-----+------------------------------------------------------------+

FORTRAN_SUBR

( 

CCPDAT 

 ,

ccpdat 

 ,

(fpstr caldat, int caldat_len) 

 ,

(fpstr caldat) 

 ,

(fpstr caldat, int caldat_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to string data       |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------+                  |
|                                      |     | *caldat*    | Date string in f |
|                                      | ormat dd/mm/yy.   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------+                  |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

UIDATE 

 ,

uidate 

 ,

(int *imonth, int *iday, int *iyear) 

 ,

(int *imonth, int *iday, int *iyear) 

 ,

(int *imonth, int *iday, int *iyear) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to integer data      |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *imonth*    | Month (1-12).    |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *iday*      | Day (1-31).      |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
|                                      |     | *iyear*     | Year (4 digit).  |
|                                      |   |                                  |
|                                      |     +-------------+----------------- |
|                                      | --+                                  |
+--------------------------------------+--------------------------------------+
