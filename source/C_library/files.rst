`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CINCH File List
===============

Here is a list of all documented files with brief descriptions:

+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **binsort.h** `[code] <binsort_8h-source.html>`__                                          |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_array.c <ccp4__array_8c.html>`__                                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_array.h <ccp4__array_8h.html>`__ `[code] <ccp4__array_8h-source.html>`__            |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_diskio_f.c <ccp4__diskio__f_8c.html>`__                                            |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_errno.h <ccp4__errno_8h.html>`__ `[code] <ccp4__errno_8h-source.html>`__            |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **ccp4_file_err.h** `[code] <ccp4__file__err_8h-source.html>`__                          |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_fortran.h <ccp4__fortran_8h.html>`__ `[code] <ccp4__fortran_8h-source.html>`__      |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_general.c <ccp4__general_8c.html>`__                                                |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **ccp4_general.h** `[code] <ccp4__general_8h-source.html>`__                              |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_general_f.c <ccp4__general__f_8c.html>`__                                          |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_parser.c <ccp4__parser_8c.html>`__                                                  | Functions to read in and "parse" CCP4-style keyworded input                            |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_parser.h <ccp4__parser_8h.html>`__ `[code] <ccp4__parser_8h-source.html>`__         | Functions to read in and "parse" CCP4-style keyworded input                            |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_parser_f.c <ccp4__parser__f_8c.html>`__                                            | Fortran API to `ccp4_parser.c <ccp4__parser_8c.html>`__                               |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_program.c <ccp4__program_8c.html>`__                                                |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_program.h <ccp4__program_8h.html>`__ `[code] <ccp4__program_8h-source.html>`__      |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_spg.h <ccp4__spg_8h.html>`__ `[code] <ccp4__spg_8h-source.html>`__                  | Data structure for symmetry information                                                |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_sysdep.h <ccp4__sysdep_8h.html>`__ `[code] <ccp4__sysdep_8h-source.html>`__         | System-dependent definitions                                                           |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **ccp4_types.h** `[code] <ccp4__types_8h-source.html>`__                                  |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_unitcell.c <ccp4__unitcell_8c.html>`__                                              |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_unitcell.h <ccp4__unitcell_8h.html>`__ `[code] <ccp4__unitcell_8h-source.html>`__   |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_unitcell_f.c <ccp4__unitcell__f_8c.html>`__                                        |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `ccp4_utils.h <ccp4__utils_8h.html>`__ `[code] <ccp4__utils_8h-source.html>`__            | Utility functions                                                                      |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **ccp4_vars.h** `[code] <ccp4__vars_8h-source.html>`__                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_data.h** `[code] <cmap__data_8h-source.html>`__                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_errno.h** `[code] <cmap__errno_8h-source.html>`__                                  |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_header.h** `[code] <cmap__header_8h-source.html>`__                                |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_labels.h** `[code] <cmap__labels_8h-source.html>`__                                |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cmap_open.c <cmap__open_8c.html>`__                                                      | Opening CCP4-format map files                                                          |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_skew.h** `[code] <cmap__skew_8h-source.html>`__                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmap_stats.h** `[code] <cmap__stats_8h-source.html>`__                                  |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cmaplib.h <cmaplib_8h.html>`__ `[code] <cmaplib_8h-source.html>`__                        | Ccp4 map i/o user-level library header file                                            |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cmaplib_f.c <cmaplib__f_8c.html>`__                                                      | Fortran API for input, output and manipulation of CCP4 map files                       |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cmaplib_f.h** `[code] <cmaplib__f_8h-source.html>`__                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cmtzlib.h <cmtzlib_8h.html>`__ `[code] <cmtzlib_8h-source.html>`__                        | C-level library for input, output and manipulation of `MTZ <structMTZ.html>`__ files   |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cmtzlib_f.c <cmtzlib__f_8c.html>`__                                                      | Fortran API for input, output and manipulation of `MTZ <structMTZ.html>`__ files       |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `csymlib.h <csymlib_8h.html>`__ `[code] <csymlib_8h-source.html>`__                        | C-level library for symmetry information                                               |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `csymlib_f.c <csymlib__f_8c.html>`__                                                      | Fortran API for symmetry information                                                   |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `cvecmat.c <cvecmat_8c.html>`__                                                            |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **cvecmat.h** `[code] <cvecmat_8h-source.html>`__                                          |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `library_err.c <library__err_8c.html>`__                                                  |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `library_f.c <library__f_8c.html>`__                                                      |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **library_f.h** `[code] <library__f_8h-source.html>`__                                    |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `library_file.c <library__file_8c.html>`__                                                |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `library_file.h <library__file_8h.html>`__ `[code] <library__file_8h-source.html>`__      |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `library_utils.c <library__utils_8c.html>`__                                              | Utility functions                                                                      |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| `mtzdata.h <mtzdata_8h.html>`__ `[code] <mtzdata_8h-source.html>`__                        | Definition of `MTZ <structMTZ.html>`__ data structure                                  |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **overview.h** `[code] <overview_8h-source.html>`__                                        |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **pack_c.h** `[code] <pack__c_8h-source.html>`__                                          |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| **w32mvs.h** `[code] <w32mvs_8h-source.html>`__                                            |                                                                                        |
+--------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
