`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_errno.h File Reference
============================

| ``#include <errno.h>``

`Go to the source code of this file. <ccp4__errno_8h-source.html>`__

| 

Defines
-------

 #define 

**CCP4_ERRSYSTEM**(x)   (((x)&0xfff)<<24)

 #define 

**CCP4_ERRLEVEL**(x)   (((x)&0xf)<<16)

 #define 

**CCP4_ERRSETLEVEL**(y, x)   ((y) & (~CCP4_ERRLEVEL(0xf)) |
CCP4_ERRLEVEL(x)))

 #define 

**CCP4_ERRGETSYS**(x)   (((x)>>24)&0xfff)

 #define 

**CCP4_ERRGETLEVEL**(x)   (((x)>>16)&0xf)

 #define 

**CCP4_ERRGETCODE**(x)   ((x)&0xffff)

 #define 

**CCP4_ERR_SYS**   CCP4_ERRSYSTEM(0x0)

 #define 

**CCP4_ERR_FILE**   CCP4_ERRSYSTEM(0x1)

 #define 

**CCP4_ERR_COORD**   CCP4_ERRSYSTEM(0x2)

 #define 

**CCP4_ERR_MTZ**   CCP4_ERRSYSTEM(0x3)

 #define 

**CCP4_ERR_MAP**   CCP4_ERRSYSTEM(0x4)

 #define 

**CCP4_ERR_UTILS**   CCP4_ERRSYSTEM(0x5)

 #define 

**CCP4_ERR_PARS**   CCP4_ERRSYSTEM(0x6)

 #define 

**CCP4_ERR_SYM**   CCP4_ERRSYSTEM(0x7)

 #define 

**CCP4_ERR_GEN**   CCP4_ERRSYSTEM(0x8)

 #define 

**CCP4_COUNT**(x)   sizeof(x)/sizeof(x[0])

| 

Functions
---------

void 

`ccp4_error <ccp4__errno_8h.html#a18>`__ (const char * )

const char * 

`ccp4_strerror <ccp4__errno_8h.html#a19>`__ (int)

void 

`ccp4_fatal <ccp4__errno_8h.html#a20>`__ (const char * )

int 

`ccp4_liberr_verbosity <ccp4__errno_8h.html#a21>`__ (int iverb)

void 

`ccp4_signal <ccp4__errno_8h.html#a22>`__ (const int, const char
*const, void( * )())

 int 

**cfile_perror** (const char * )

| 

Variables
---------

int 

`ccp4_errno <ccp4__errno_8h.html#a17>`__

--------------

Detailed Description
--------------------

Header file for error handling routines base error codes on system
errors.

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +--------------------+------+------------------+-----------+------+----+ |
| | void ccp4_error   | (    | const char *    |   *msg*   | )    |    | |
| +--------------------+------+------------------+-----------+------+----+ |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print out passed message and         |
|                                      | internal message based upon          |
|                                      | ccp4_errno "message : error message |
|                                      | "                                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *message*    | (const char * ) |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------+------+------------------+---------------+------+- |
| ---+                                                                     |
| | void ccp4_fatal   | (    | const char *    |   *message*   | )    |  |
|    |                                                                     |
| +--------------------+------+------------------+---------------+------+- |
| ---+                                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Wrapper for ccp4_error which also   |
|                                      | calls exit(1)                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *message*    | (const char * ) |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
| | int ccp4_liberr_verbosity   | (    | int    |   *iverb*   | )    |   |
|   |                                                                      |
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to set verbosity level for  |
|                                      | messages from ccp4_signal.          |
|                                      | Currently just off (0) and on (1).   |
|                                      | It should be generalised to be able  |
|                                      | to switch individual components on   |
|                                      | and off, i.e. replace 1 by a mask.   |
|                                      | cf. ccp4VerbosityLevel which sets    |
|                                      | the verbosity level for ccp4printf   |
|                                      | These are separate as they may be    |
|                                      | used differently.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *iverb*    | If >= 0 then set  |
|                                      | the verbosity level to the value of  |
|                                      | iverb. If < 0 (by convention -1) the |
|                                      | n report current level.   |          |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     current verbosity level          |
+--------------------------------------+--------------------------------------+

void ccp4_signal

( 

const int 

  *code*,

const char *const 

  *msg*,

void( * 

  *callback* )()

) 

+--------------------------------------+--------------------------------------+
|                                      | Routine to set ccp4_errno and print |
|                                      | out message for error tracing. This  |
|                                      | should be the only way in which      |
|                                      | ccp4_errno is set. See error codes  |
|                                      | above for levels and systems. A      |
|                                      | callback with prototype void         |
|                                      | function(void) may also be passed to |
|                                      | the routine. Note: FATAL calls       |
|                                      | exit(1). If ccp4_liberr_verbosity  |
|                                      | returns 0, then ccp4_signal sets    |
|                                      | ccp4_errno and returns without      |
|                                      | doing anything else.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *error*       | code (int)     |
|                                      |                         |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *message*     | (const char * |
|                                      |  const)                 |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *callback*    | (point to rout |
|                                      | ine void ( * )(void) )   |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
| | const char* ccp4_strerror   | (    | int    |   *error*   | )    |   |
|   |                                                                      |
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Obtain character string based upon   |
|                                      | error code. Typical use              |
|                                      | ccp4_strerror(ccp4_errno) The      |
|                                      | returned string is statically        |
|                                      | allocated in the                     |
|                                      | `library_err.c <library__err_8c.htm |
|                                      | l>`__                                |
|                                      | file and should not be freed.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+--------------+    |
|                                      |     | *error*    | code (int)   |    |
|                                      |     +------------+--------------+    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     const pointer to error message   |
|                                      |     (const char * )                  |
+--------------------------------------+--------------------------------------+

--------------

Variable Documentation
----------------------

+--------------------------------------------------------------------------+
| +-------------------+                                                    |
| | int ccp4_errno   |                                                    |
| +-------------------+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | @global ccp4_errno: global variable |
|                                      | that stores the error last error     |
|                                      | code from the ccp4 libraries | 12   |
|                                      | bits - library | 4 bits - level |  |
|                                      | 16 bits - code |                    |
|                                      |                                      |
|                                      | associated macros CCP4_ERR_SYS 0   |
|                                      | OS error CCP4_ERR_FILE 1 io        |
|                                      | library CCP4_ERR_COORD 2 mmdb      |
|                                      | CCP4_ERR_MTZ 3 cmtz CCP4_ERR_MAP |
|                                      | 4 map io CCP4_ERR_UTILS 5 utility  |
|                                      | routines CCP4_ERR_PARS 6 parser    |
|                                      | routines CCP4_ERR_SYM 7 csymlib    |
|                                      |                                      |
|                                      | and bit manipulation CCP4_ERRSYSTEM |
|                                      | system mask for setting              |
|                                      | CCP4_ERRLEVEL error level mask      |
|                                      | CCP4_ERRSETLEVEL error level mask   |
|                                      | for setting error level              |
|                                      | CCP4_ERRGETSYS mask for returning   |
|                                      | system CCP4_ERRGETLEVEL mask for    |
|                                      | returning level CCP4_ERRGETCODE     |
|                                      | mask for returning the code          |
|                                      |                                      |
|                                      | error levels 0 Success 1             |
|                                      | Informational 2 Warning 3 Error 4    |
|                                      | Fatal                                |
+--------------------------------------+--------------------------------------+
