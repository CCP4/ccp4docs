`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_array.h
=============

`Go to the documentation of this file. <ccp4__array_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_array.h: header file for resizable array implementation. 
    00003      Copyright (C) 2002  Kevin Cowtan
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00016 /*
    00017 CCP4 resizable array implementation.
    00018 
    00019 This defines an object and methods which looks just like a simple C
    00020 array, but can be resized at will without incurring excessive
    00021 overheads.
    00022 
    00023 A pointer to the desired type is created. Array elements are accessed
    00024 from this pointer as normal. Other operations depend on macros which
    00025 extract the stored type from the type of the pointer.
    00026 
    00027 The array is managed with a header, which is positioned before the
    00028 beginning of the array. The malloc'ed memory area starts at the
    00029 beginning of this header. However the pointer to the header is not
    00030 stored, but rather derived from the array pointer whenever it is
    00031 required.
    00032 
    00033 Arrays have a size and a capacity. The size is the number of elements
    00034 in use, and the capacity is the number of elements available before a
    00035 new memory allocation is required. When new memory is required, an
    00036 excess is reqested to allow the array to grow further before
    00037 performing another malloc.
    00038 
    00039 If the precise amount of memory is known, the capacity can be
    00040 controlled directly using the 'reserve' macro.
    00041 
    00042 Example: to handle an array of type mytype:
    00043 
    00044 \code
    00045   int i;
    00046   mytype x,y;
    00047   mytype *array;
    00048 
    00049   ccp4array_new(array);
    00050 
    00051   ccp4array_append_n(array, x, 3);
    00052 
    00053   for ( i = 0; i < 3; i++ ) y = array[i];
    00054 
    00055   ccp4array_free(array);
    00056 \endcode
    00057 */
    00058 
    00059 #ifndef __CCP4_ARRAY_INC
    00060 #define __CCP4_ARRAY_INC
    00061 
    00062 #ifdef  __cplusplus
    00063 extern "C" {
    00064 #endif
    00065 
    00066 #include <stdlib.h>
    00067 #include <string.h>
    00068 static char rcsidha[] = "$Id$";
    00069 
    00071 typedef const void *ccp4_constptr;
    00073 typedef char *ccp4_byteptr;
    00075 typedef void *ccp4_ptr;
    00076 
    00078 typedef struct ccp4array_base_ {
    00079   int size, capacity;
    00080 } ccp4array_base;
    00081 
    00087 #define ccp4array_new(v) ccp4array_new_((ccp4_ptr* )(&v))
    00088 
    00095 #define ccp4array_new_size(v,s) ccp4array_new_size_((ccp4_ptr* )(&v),s,sizeof( *v))
    00096 
    00105 #define ccp4array_resize(v,s) ccp4array_resize_((ccp4_ptr* )(&v),s,sizeof( *v))
    00106 
    00115 #define ccp4array_reserve(v,s) ccp4array_reserve_((ccp4_ptr* )(&v),s,sizeof( *v))
    00116 
    00123 #define ccp4array_append(v,d) ccp4array_append_((ccp4_ptr* )(&v),(ccp4_constptr)(&d),sizeof( *v))
    00124 
    00132 #define ccp4array_append_n(v,d,n) ccp4array_append_n_((ccp4_ptr* )(&v),(ccp4_constptr)(&d),n,sizeof( *v))
    00133 
    00141 #define ccp4array_append_list(v,l,n) ccp4array_append_list_((ccp4_ptr* )(&v),(ccp4_constptr)l,n,sizeof( *v))
    00142 
    00150 #define ccp4array_insert(v,i,d) ccp4array_insert_((ccp4_ptr* )(&v),i,(ccp4_constptr)(&d),sizeof( *v))
    00151 
    00158 #define ccp4array_delete_ordered(v,i) ccp4array_delete_ordered_((ccp4_ptr* )(&v),i,sizeof( *v))
    00159 
    00165 #define ccp4array_delete(v,i) ccp4array_delete_((ccp4_ptr* )(&v),i,sizeof( *v))
    00166 
    00171 #define ccp4array_delete_last(v) ccp4array_delete_last_((ccp4_ptr* )(&v),sizeof( *v))
    00172 
    00177 #define ccp4array_size(v) ccp4array_size_((ccp4_constptr* )(&v))
    00178 
    00183 #define ccp4array_free(v) ccp4array_free_((ccp4_ptr* )(&v))
    00184 
    00188 ccp4_ptr ccp4array_new_(ccp4_ptr *p);
    00192 ccp4_ptr ccp4array_new_size_(ccp4_ptr *p, const int size, const size_t reclen);
    00196 void ccp4array_resize_(ccp4_ptr *p, const int size, const size_t reclen);
    00200 void ccp4array_reserve_(ccp4_ptr *p, const int size, const size_t reclen);
    00204 void ccp4array_append_(ccp4_ptr *p, ccp4_constptr data, const size_t reclen);
    00208 void ccp4array_append_n_(ccp4_ptr *p, ccp4_constptr data, const int n, const size_t reclen);
    00212 void ccp4array_append_list_(ccp4_ptr *p, ccp4_constptr data, const int n, const size_t reclen);
    00216 void ccp4array_insert_(ccp4_ptr *p, const int i, ccp4_constptr data, const size_t reclen);
    00220 void ccp4array_delete_ordered_(ccp4_ptr *p, const int i, const size_t reclen);
    00224 void ccp4array_delete_(ccp4_ptr *p, const int i, const size_t reclen);
    00228 void ccp4array_delete_last_(ccp4_ptr *p, const size_t reclen);
    00232 int ccp4array_size_(ccp4_constptr *p);
    00236 void ccp4array_free_(ccp4_ptr *p);
    00237 
    00238 #ifdef __cplusplus
    00239 } 
    00240 #endif
    00241 
    00242 #endif /* __CCP4_ARRAY_INC */

.. raw:: html

   </div>
