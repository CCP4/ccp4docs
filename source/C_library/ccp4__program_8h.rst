`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_program.h File Reference
==============================

`Go to the source code of this file. <ccp4__program_8h-source.html>`__

| 

Defines
-------

 #define 

**CCP4_VERSION_NO**   "6.3"

 #define 

**CCP4_PATCH_LEVEL**   "6.3.0"

 #define 

**MAXLEN_PROGNAME**   80

 #define 

**MAXLEN_PROGVERSION**   80

 #define 

**MAXLEN_RCSDATE**   80

| 

Typedefs
--------

 typedef int( * 

**CCP4INTFUNCPTR** )(int, char * )

| 

Functions
---------

char * 

`ccp4_prog_vers <ccp4__program_8h.html#a7>`__ (char *progvers)

char * 

`ccp4ProgramName <ccp4__program_8h.html#a8>`__ (const char *progname)

char * 

`ccp4RCSDate <ccp4__program_8h.html#a9>`__ (const char *rcs_string)

void 

`ccp4ProgramTime <ccp4__program_8h.html#a10>`__ (int init)

int 

`ccp4VerbosityLevel <ccp4__program_8h.html#a11>`__ (int level)

int 

`ccp4Callback <ccp4__program_8h.html#a12>`__ (CCP4INTFUNCPTR mycallback,
char *mode, int ierr, char *message)

int 

`ccp4SetCallback <ccp4__program_8h.html#a13>`__ (CCP4INTFUNCPTR
mycallback)

int 

`ccp4InvokeCallback <ccp4__program_8h.html#a14>`__ (int ierr, char
*message)

int 

`ccp4NullCallback <ccp4__program_8h.html#a15>`__ (int level, char
*message)

int 

`ccp4_licence_exists <ccp4__program_8h.html#a16>`__ (const char
*name)

int 

`html_log_output <ccp4__program_8h.html#a17>`__ (int ihtml_in)

int 

`summary_output <ccp4__program_8h.html#a18>`__ (int isumm_in)

--------------

Detailed Description
--------------------

Utilies to set and fetch program information. Peter Briggs CCP4 May 2001

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +-----------------------------+------+------------------+------------+-- |
| ----+----+                                                               |
| | int ccp4_licence_exists   | (    | const char *    |   *name*   | ) |
|     |    |                                                               |
| +-----------------------------+------+------------------+------------+-- |
| ----+----+                                                               |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Check existence of licence agreement |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |     | *name*    | Name of licence, e |
|                                      | .g. "CCP4".   |                      |
|                                      |     +-----------+------------------- |
|                                      | --------------+                      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 for licence exists, else 0.    |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------+------+------------+----------------+------ |
| +----+                                                                   |
| | char* ccp4_prog_vers   | (    | char *    |   *progvers*   | )     |
| |    |                                                                   |
| +---------------------------+------+------------+----------------+------ |
| +----+                                                                   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Register or query program version.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *progvers*    | Program versio |
|                                      | n string, or NULL to query existing  |
|                                      | value.   |                           |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Program version string.          |
+--------------------------------------+--------------------------------------+

int ccp4Callback

( 

CCP4INTFUNCPTR 

  *mycallback*,

char * 

  *mode*,

int 

  *ierr*,

char * 

  *message*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set or invoke a user-defined         |
|                                      | callback function The callback must  |
|                                      | be of the form "function(const int,  |
|                                      | const char * )" This is essentially  |
|                                      | an internal function which operates  |
|                                      | in one of two modes - in "set" mode  |
|                                      | the named function is stored and the |
|                                      | remaining arguments are discarded;   |
|                                      | in "invoke" mode the stored function |
|                                      | is executed with the supplied values |
|                                      | (the supplied name is discarded).    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *mycallback*    | Callback fun |
|                                      | ction (discarded in "invoke" mode)   |
|                                      |        |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *mode*          | Either "set" |
|                                      |  or "invoke"                         |
|                                      |        |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *ierr*          | An error lev |
|                                      | el equivalent to that used in ccperr |
|                                      | or     |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *message*       | A message st |
|                                      | ring equivalent to that used in ccpe |
|                                      | rror   |                             |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Result of the executed function  |
|                                      |     (invoke mode)                    |
+--------------------------------------+--------------------------------------+

int ccp4InvokeCallback

( 

int 

  *ierr*,

char * 

  *message*

) 

+--------------------------------------+--------------------------------------+
|                                      | Invoke the user-defined callback     |
|                                      | function This is a wrapper to        |
|                                      | ccp4Callback - it executes the       |
|                                      | user-defined callback function       |
|                                      | previously stored.                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *ierr*       | An error level  |
|                                      | equivalent to that used in ccperror  |
|                                      |     |                                |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *message*    | A message strin |
|                                      | g equivalent to that used in ccperro |
|                                      | r   |                                |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Result of the executed function  |
+--------------------------------------+--------------------------------------+

int ccp4NullCallback

( 

int 

  *level*,

char * 

  *message*

) 

+--------------------------------------+--------------------------------------+
|                                      | A dummy callback function used by    |
|                                      | default in ccp4CallOnExit Internal   |
|                                      | function. This function does         |
|                                      | nothing.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -------------------------+           |
|                                      |     | *level*      | Severity level  |
|                                      | supplied from ccperror   |           |
|                                      |     +--------------+---------------- |
|                                      | -------------------------+           |
|                                      |     | *message*    | Message text su |
|                                      | pplied from ccperror     |           |
|                                      |     +--------------+---------------- |
|                                      | -------------------------+           |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Always returns 1                 |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+------------------+----------------+- |
| -----+----+                                                              |
| | char* ccp4ProgramName   | (    | const char *    |   *progname*   |  |
| )    |    |                                                              |
| +--------------------------+------+------------------+----------------+- |
| -----+----+                                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set or return program name.          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -----------------------------------+ |
|                                      |     | *progname*    | Program name,  |
|                                      | or NULL to query existing value.   | |
|                                      |     +---------------+--------------- |
|                                      | -----------------------------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Program name                     |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+--------+------------+------+----+      |
| | void ccp4ProgramTime   | (    | int    |   *init*   | )    |    |      |
| +------------------------+------+--------+------------+------+----+      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set or print program time            |
|                                      | information                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+----+               |
|                                      |     | *init*    |    |               |
|                                      |     +-----------+----+               |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------+------+------------------+-------------------+-- |
| ----+----+                                                               |
| | char* ccp4RCSDate   | (    | const char *    |   *rcs_string*   | ) |
|     |    |                                                               |
| +----------------------+------+------------------+-------------------+-- |
| ----+----+                                                               |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set or return program RCS date       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rcs_string*    | Date string |
|                                      | , or NULL to query existing value.   |
|                                      |  |                                   |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Date string                      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+-------------------+------------------+- |
| -----+----+                                                              |
| | int ccp4SetCallback   | (    | CCP4INTFUNCPTR    |   *mycallback*   |  |
| )    |    |                                                              |
| +-----------------------+------+-------------------+------------------+- |
| -----+----+                                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set a user-defined callback function |
|                                      | This is a wrapper to ccp4Callback -  |
|                                      | it stores a user-defined callback    |
|                                      | function which must be of the form   |
|                                      | "function(const int, const char * )" |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | --------+                            |
|                                      |     | *mycallback*    | Callback fun |
|                                      | ction   |                            |
|                                      |     +-----------------+------------- |
|                                      | --------+                            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 (if the function is stored), 0 |
|                                      |     (if it is not)                   |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------+------+--------+-------------+------+----+   |
| | int ccp4VerbosityLevel   | (    | int    |   *level*   | )    |    |   |
| +--------------------------+------+--------+-------------+------+----+   |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Set or return the reference          |
|                                      | verbosity level Always return the    |
|                                      | verbosity level - if verboselevel is |
|                                      | between 0 and 9 then reset the       |
|                                      | verbosity level to verboselevel      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------------------+   |
|                                      |     | *level*    | Verbosity level,  |
|                                      | or -1 to query existing value.   |   |
|                                      |     +------------+------------------ |
|                                      | ---------------------------------+   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     Verbosity level                  |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+--------+-----------------+------+---- |
| +                                                                        |
| | int html_log_output   | (    | int    |   *ihtml_in*   | )    |     |
| |                                                                        |
| +-------------------------+------+--------+-----------------+------+---- |
| +                                                                        |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Register or query html output level. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------+      |
|                                      |     | *ihtml_in*    | 0 = turn off  |
|                                      | html output, 1 = turn on html output |
|                                      | , -1 = query existing value   |      |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------+      |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 = no html output, 1 = html     |
|                                      |     output                           |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------+------+--------+-----------------+------+----+  |
| | int summary_output   | (    | int    |   *isumm_in*   | )    |    |  |
| +-----------------------+------+--------+-----------------+------+----+  |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Register or query summary output     |
|                                      | level.                               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |     | *isumm_in*    | 0 = turn off  |
|                                      | summary output, 1 = turn on summary  |
|                                      | output, -1 = query existing value    |
|                                      | |                                    |
|                                      |     +----------------+-------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | +                                    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 = no summary output, 1 =       |
|                                      |     summary output                   |
+--------------------------------------+--------------------------------------+
