`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_spg.h File Reference
==========================

Data structure for symmetry information. `More... <#_details>`__

`Go to the source code of this file. <ccp4__spg_8h-source.html>`__

| 

Compounds
---------

struct  

**ccp4_spacegroup_**

struct  

**ccp4_symop_**

| 

Typedefs
--------

 typedef ccp4_symop_ 

**ccp4_symop**

 typedef ccp4_spacegroup_ 

**CCP4SPG**

--------------

Detailed Description
--------------------

Data structure for symmetry information.

A data structure for spacegroup information and related quantities. Some
items are loaded from syminfo.lib while others are calculated on the
fly.

 **Author:**
    Martyn Winn
