`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_unitcell_f.c File Reference
==================================

| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_unitcell.h"``

| 

Functions
---------

  

**FORTRAN_SUBR** (CCP4UC_F_FRAC_ORTH_MAT,
ccp4uc_f_frac_orth_mat,(const float cell[6], const int *ncode,
float ro[3][3], float rf[3][3], float *volume),(const float cell[6],
const int *ncode, float ro[3][3], float rf[3][3], float
*volume),(const float cell[6], const int *ncode, float ro[3][3], float
rf[3][3], float *volume))

  

**FORTRAN_SUBR** (CCP4UC_F_CALC_RCELL, ccp4uc_f_calc_rcell,(const
float cell[6], float rcell[6], float *rvolume),(const float cell[6],
float rcell[6], float *rvolume),(const float cell[6], float rcell[6],
float *rvolume))

  

**FORTRAN_SUBR** (CCP4UC_F_ORTH_TO_FRAC,
ccp4uc_f_orth_to_frac,(const float rf[3][3], const float xo[3],
float xf[3]),(const float rf[3][3], const float xo[3], float
xf[3]),(const float rf[3][3], const float xo[3], float xf[3]))

  

**FORTRAN_SUBR** (CCP4UC_F_FRAC_TO_ORTH,
ccp4uc_f_frac_to_orth,(const float ro[3][3], const float xf[3],
float xo[3]),(const float ro[3][3], const float xf[3], float
xo[3]),(const float ro[3][3], const float xf[3], float xo[3]))

  

**FORTRAN_SUBR** (CCP4UC_F_ORTHU_TO_FRACU,
ccp4uc_f_orthu_to_fracu,(const float rf[3][3], const float uo[3],
float uf[3]),(const float rf[3][3], const float uo[3], float
uf[3]),(const float rf[3][3], const float uo[3], float uf[3]))

  

**FORTRAN_SUBR** (CCP4UC_F_FRACU_TO_ORTHU,
ccp4uc_f_fracu_to_orthu,(const float ro[3][3], const float uf[3],
float uo[3]),(const float ro[3][3], const float uf[3], float
uo[3]),(const float ro[3][3], const float uf[3], float uo[3]))

  

**FORTRAN_SUBR** (CELLCHK, cellchk,(const float cell1[6], const float
cell2[6], const float *errfrc, int *ierr),(const float cell1[6], const
float cell2[6], const float *errfrc, int *ierr),(const float cell1[6],
const float cell2[6], const float *errfrc, int *ierr))

--------------

Detailed Description
--------------------

Fortran API to `ccp4_unitcell.c <ccp4__unitcell_8c.html>`__. Martyn
Winn
