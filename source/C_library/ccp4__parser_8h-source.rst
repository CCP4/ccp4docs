`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_parser.h
==============

`Go to the documentation of this file. <ccp4__parser_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_parser.h: Headers for functions to read in and "parse" CCP4 keyworded input.
    00003      Copyright (C) 2001  CCLRC, Peter Briggs
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00090 /*------------------------------------------------------------------*/
    00091 
    00092 /* Macro definitions */
    00093 
    00094 /*------------------------------------------------------------------*/
    00095 
    00096 #ifndef __CCP4_Parser__
    00097 #define __CCP4_Parser__
    00098 
    00099 static char rcsidhhh[] = "$Id$";
    00100 
    00101 /* note order: these must be outside CCP4 namespace */
    00102 #include <stdio.h>
    00103 #include"ccp4_utils.h"
    00104 #include"ccp4_spg.h"
    00105 
    00106 /* Macro to make C functions callable from C++ */
    00107 #ifdef  __cplusplus
    00108 namespace CCP4 {
    00109 extern "C" {
    00110 typedef CSym::ccp4_symop ccp4_symop;
    00111 #endif
    00112 
    00113 /*------------------------------------------------------------------*/
    00114 
    00115 /* Parser String Sizes */
    00116 
    00117 /*------------------------------------------------------------------*/
    00118 
    00119 #define CCP4_PARSER_MAXLINELENGTH     200
    00120 #define CCP4_PARSER_MAXFILENAMELENGTH 200
    00121 
    00122 /*------------------------------------------------------------------*/
    00123 
    00124 /* Structures and typedefs */
    00125 
    00126 /*------------------------------------------------------------------*/
    00127 
    00128 /* CCP4 Parser token
    00129    Construct to hold the information about a single token */
    00130 
    00131 typedef struct {
    00132   char   *fullstring;   /* Full string containing all of token */
    00133   char   word[5];       /* First four characters of token */
    00134   double value;         /* Equivalent numerical value */
    00135   int    isstring;      /* Flag: true if token is character string */
    00136   int    strlength;     /* Number of characters in whole token (strings only) */
    00137   int    isnumber;      /* Flag: true if token is number */
    00138   int    intdigits;     /* Number of 'digits' preceeding the decimal point
    00139                            (numbers only) */
    00140   int    frcdigits;     /* Number of 'digits' after the decimal point (numbers
    00141                            only) */
    00142   int    isquoted;      /* Flag: true if token is contained in quotes */
    00143   int    isnull;        /* Flag: true if token is null field */
    00144   int    ibeg,iend;     /* Begin and end character positions of token
    00145                            in input line */
    00146 } CCP4PARSERTOKEN;
    00147 
    00148 /* CCP4 Parser array
    00149    Construct to hold the information about a parsed line */
    00150 
    00151 typedef struct {
    00152   /* "Public" members */
    00153   char   keyword[5];      /* Keyword (=token[1].token, uppercased) */
    00154   int    ntokens;         /* Number of tokens */
    00155   CCP4PARSERTOKEN *token; /* Array of tokens */
    00156   /* "Private" members */
    00157   FILE   *fp;             /* Pointer to an external command file */
    00158   int    maxtokens;       /* Maximum number of tokens allowed */
    00159   char   *delim;          /* List of delimiter characters */
    00160   char   *nulldelim;      /* List of null delimiter characters */
    00161   char   *comment;        /* List of comment characters */
    00162   double max_exponent;    /* Largest allowed exponent for numerical tokens */
    00163   double min_exponent;    /* Smallest allowed exponent for numerical tokens */
    00164 } CCP4PARSERARRAY;     
    00165 
    00166 /*------------------------------------------------------------------*/
    00167 
    00168 /* Function Prototypes */
    00169 
    00170 /*------------------------------------------------------------------*/
    00171 
    00172 /* Core cparser functions */
    00173 
    00180 CCP4PARSERARRAY* ccp4_parse_start(const int maxtokens);
    00181 
    00187 int ccp4_parse_end(CCP4PARSERARRAY *parsePtr);
    00188 
    00189 int ccp4_parse_init_token(const CCP4PARSERARRAY *parsePtr, const int itok);
    00190 
    00191 int ccp4_parse_delimiters(CCP4PARSERARRAY *parsePtr, const char *delim,
    00192                                   const char *nulldelim);
    00193 
    00194 int ccp4_parse_comments(CCP4PARSERARRAY *parsePtr, const char *comment_chars);
    00195 
    00196 int ccp4_parse_maxmin(CCP4PARSERARRAY *parsePtr, const double max_exponent,
    00197                               const double min_exponent);
    00198 
    00199 int ccp4_parse_reset(CCP4PARSERARRAY *parsePtr);
    00200 
    00201 int ccp4_parse(const char *line, CCP4PARSERARRAY *parser);
    00202 
    00219 int ccp4_parser(char *line, const int n, CCP4PARSERARRAY *parser,
    00220                         const int print);
    00221 
    00222 /* External utility functions */
    00223 
    00230 int ccp4_keymatch(const char *keyin1, const char *keyin2);
    00231 
    00232 /* Internal utility functions */
    00233 
    00239 char *strtoupper (char *str1, const char *str2);
    00240 
    00246 char *strtolower (char *str1, const char *str2);
    00247 
    00248 int strmatch (const char *str1, const char *str2);
    00249 
    00250 int charmatch(const char character, const char *charlist);
    00251 
    00252 int doublefromstr(const char *str, const double max_exp, const double min_exp,
    00253                           double *valuePtr, double *intvaluePtr, int *intdigitsPtr,
    00254                           double *frcvaluePtr, int *frcdigitsPtr,
    00255                           double *expvaluePtr, int *expdigitsPtr);
    00256 
    00263 ccp4_symop symop_to_rotandtrn(const char *symchs_begin, const char *symchs_end);
    00264 
    00274 const char * symop_to_mat4(const char *symchs_begin, const char *symchs_end, float *rot);
    00275 int symop_to_mat4_err(const char *symop);
    00276 ccp4_symop mat4_to_rotandtrn(const float rsm[4][4]);
    00277 /* This is Charles' version of symtr */
    00278 char *rotandtrn_to_symop(char *symchs_begin, char *symchs_end, const ccp4_symop symop);
    00279 void rotandtrn_to_mat4(float rsm[4][4], const ccp4_symop symop);
    00280 
    00290 char *mat4_to_symop(char *symchs_begin, char *symchs_end, const float rsm[4][4]);
    00291 
    00301 char *mat4_to_recip_symop(char *symchs_begin, char *symchs_end, const float rsm[4][4]);
    00302 
    00303 #ifdef __cplusplus
    00304 }
    00305 }
    00306 #endif
    00307 
    00308 #endif  /* __CCP4_Parser__ */

.. raw:: html

   </div>
