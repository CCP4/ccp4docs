`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmtzlib_f.c File Reference
===========================

Fortran API for input, output and manipulation of
`MTZ <structMTZ.html>`__ files. `More... <#_details>`__

| ``#include <math.h>``
| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_utils.h"``
| ``#include "cmtzlib.h"``
| ``#include "csymlib.h"``
| ``#include "ccp4_program.h"``
| ``#include "ccp4_general.h"``

| 

Defines
-------

 #define 

**CMTZLIB_DEBUG**(x)

 #define 

**MFILES**   4

 #define 

**MAXSYM**   192

| 

Functions
---------

 void 

**MtzMemTidy** (void)

 int 

**MtzCheckSubInput** (const int mindx, const char *subname, const int
rwmode)

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a23>`__ (MTZINI, mtzini,(),(),())

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a24>`__ (LROPEN, lropen,(int *mindx,
fpstr filename, int *iprint, int *ifail, int filename_len),(int
*mindx, fpstr filename, int *iprint, int *ifail),(int *mindx, fpstr
filename, int filename_len, int *iprint, int *ifail))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a25>`__ (LRTITL, lrtitl,(int *mindx,
fpstr ftitle, int *len, int ftitle_len),(int *mindx, fpstr ftitle,
int *len),(int *mindx, fpstr ftitle, int ftitle_len, int *len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a26>`__ (LRHIST, lrhist,(int *mindx,
fpstr hstrng, int *nlines, int hstrng_len),(int *mindx, fpstr hstrng,
int *nlines),(int *mindx, fpstr hstrng, int hstrng_len, int
*nlines))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a27>`__ (LRINFO, lrinfo,(int *mindx,
fpstr versnx, int *ncolx, int *nreflx, float *ranges, int
versnx_len),(int *mindx, fpstr versnx, int *ncolx, int *nreflx,
float *ranges),(int *mindx, fpstr versnx, int versnx_len, int
*ncolx, int *nreflx, float *ranges))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a28>`__ (LRNCOL, lrncol,(int *mindx,
int *ncolx),(int *mindx, int *ncolx),(int *mindx, int *ncolx))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a29>`__ (LRNREF, lrnref,(int *mindx,
int *nreflx),(int *mindx, int *nreflx),(int *mindx, int *nreflx))

  

**FORTRAN_SUBR** (LRSORT, lrsort,(int *mindx, int sortx[5]),(int
*mindx, int sortx[5]),(int *mindx, int sortx[5]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a31>`__ (LRBATS, lrbats,(int *mindx,
int *nbatx, int batchx[]),(int *mindx, int *nbatx, int batchx[]),(int
*mindx, int *nbatx, int batchx[]))

  

**FORTRAN_SUBR** (LRCLAB, lrclab,(int *mindx, fpstr clabs, fpstr
ctyps, int *ncol, int clabs_len, int ctyps_len),(int *mindx, fpstr
clabs, fpstr ctyps, int *ncol),(int *mindx, fpstr clabs, int
clabs_len, fpstr ctyps, int ctyps_len, int *ncol))

  

**FORTRAN_SUBR** (LRCLID, lrclid,(int *mindx, int csetid[], int
*ncol),(int *mindx, int csetid[], int *ncol),(int *mindx, int
csetid[], int *ncol))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a34>`__ (LRCELL, lrcell,(int *mindx,
float cell[]),(int *mindx, float cell[]),(int *mindx, float cell[]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a35>`__ (LRRSOL, lrrsol,(int *mindx,
float *minres, float *maxres),(int *mindx, float *minres, float
*maxres),(int *mindx, float *minres, float *maxres))

  

**FORTRAN_SUBR** (LRSYMI, lrsymi,(int *mindx, int *nsympx, fpstr
ltypex, int *nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex_len, int
spgrnx_len, int pgnamx_len),(int *mindx, int *nsympx, fpstr ltypex,
int *nspgrx, fpstr spgrnx, fpstr pgnamx),(int *mindx, int *nsympx,
fpstr ltypex, int ltypex_len, int *nspgrx, fpstr spgrnx, int
spgrnx_len, fpstr pgnamx, int pgnamx_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a37>`__ (LRSYMM, lrsymm,(int *mindx,
int *nsymx, float rsymx[MAXSYM][4][4]),(int *mindx, int *nsymx, float
rsymx[MAXSYM][4][4]),(int *mindx, int *nsymx, float
rsymx[MAXSYM][4][4]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a38>`__ (LKYIN, lkyin,(const int
*mindx, const fpstr lsprgi, const int *nlprgi, const int *ntok, const
fpstr labin_line, const int ibeg[], const int iend[], int lsprgi_len,
int labin_line_len),(const int *mindx, const fpstr lsprgi, const int
*nlprgi, const int *ntok, const fpstr labin_line, const int ibeg[],
const int iend[]),(const int *mindx, const fpstr lsprgi, int
lsprgi_len, const int *nlprgi, const int *ntok, const fpstr
labin_line, int labin_line_len, const int ibeg[], const int iend[]))

  

**FORTRAN_SUBR** (LKYOUT, lkyout,(const int *mindx, const fpstr
lsprgo, const int *nlprgo, const int *ntok, const fpstr labin_line,
const int ibeg[], const int iend[], int lsprgo_len, int
labin_line_len),(const int *mindx, const fpstr lsprgo, const int
*nlprgo, const int *ntok, const fpstr labin_line, const int ibeg[],
const int iend[]),(const int *mindx, const fpstr lsprgo, int
lsprgo_len, const int *nlprgo, const int *ntok, const fpstr
labin_line, int labin_line_len, const int ibeg[], const int iend[]))

  

**FORTRAN_SUBR** (LKYSET, lkyset,(const fpstr lsprgi, const int
*nlprgi, fpstr lsusrj, int kpoint[], const int *itok, const int
*ntok, const fpstr labin_line, const int ibeg[], const int iend[], int
lsprgi_len, int lsusrj_len, int labin_line_len),(const fpstr lsprgi,
const int *nlprgi, fpstr lsusrj, int kpoint[], const int *itok, const
int *ntok, const fpstr labin_line, const int ibeg[], const int
iend[]),(const fpstr lsprgi, int lsprgi_len, const int *nlprgi, fpstr
lsusrj, int lsusrj_len, int kpoint[], const int *itok, const int
*ntok, const fpstr labin_line, int labin_line_len, const int ibeg[],
const int iend[]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a41>`__ (LRASSN, lrassn,(const int
*mindx, fpstr lsprgi, int *nlprgi, int lookup[], fpstr ctprgi, int
lsprgi_len, int ctprgi_len),(const int *mindx, fpstr lsprgi, int
*nlprgi, int lookup[], fpstr ctprgi),(const int *mindx, fpstr lsprgi,
int lsprgi_len, int *nlprgi, int lookup[], fpstr ctprgi, int
ctprgi_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a42>`__ (LRIDX, lridx,(const int
*mindx, fpstr project_name, fpstr crystal_name, fpstr dataset_name,
int *isets, float *datcell, float *datwave, int *ndatasets, int
project_name_len, int crystal_name_len, int
dataset_name_len),(const int *mindx, fpstr project_name, fpstr
crystal_name, fpstr dataset_name, int *isets, float *datcell, float
*datwave, int *ndatasets),(const int *mindx, fpstr project_name, int
project_name_len, fpstr crystal_name, int crystal_name_len, fpstr
dataset_name, int dataset_name_len, int *isets, float *datcell,
float *datwave, int *ndatasets))

  

**FORTRAN_SUBR** (LRCELX, lrcelx,(const int *mindx, const int *iset,
float *mtzcell),(const int *mindx, const int *iset, float
*mtzcell),(const int *mindx, const int *iset, float *mtzcell))

  

**FORTRAN_SUBR** (LRIDC, lridc,(const int *mindx, fpstr project_name,
fpstr dataset_name, int *isets, float *datcell, float *datwave, int
*ndatasets, int project_name_len, int dataset_name_len),(const int
*mindx, fpstr project_name, fpstr dataset_name, int *isets, float
*datcell, float *datwave, int *ndatasets),(const int *mindx, fpstr
project_name, int project_name_len, fpstr dataset_name, int
dataset_name_len, int *isets, float *datcell, float *datwave, int
*ndatasets))

  

**FORTRAN_SUBR** (LRID, lrid,(const int *mindx, fpstr project_name,
fpstr dataset_name, int *isets, int *ndatasets, int
project_name_len, int dataset_name_len),(const int *mindx, fpstr
project_name, fpstr dataset_name, int *isets, int *ndatasets),(const
int *mindx, fpstr project_name, int project_name_len, fpstr
dataset_name, int dataset_name_len, int *isets, int *ndatasets))

  

**FORTRAN_SUBR** (LRSEEK, lrseek,(const int *mindx, int
*nrefl),(const int *mindx, int *nrefl),(const int *mindx, int
*nrefl))

  

**FORTRAN_SUBR** (LRREFL, lrrefl,(const int *mindx, float *resol,
float adata[], ftn_logical *eof),(const int *mindx, float *resol,
float adata[], ftn_logical *eof),(const int *mindx, float *resol,
float adata[], ftn_logical *eof))

  

**FORTRAN_SUBR** (LRREFM, lrrefm,(const int *mindx, ftn_logical
logmiss[]),(const int *mindx, ftn_logical logmiss[]),(const int
*mindx, ftn_logical logmiss[]))

  

**FORTRAN_SUBR** (MTZ_CHECK_FOR_MNF, mtz_check_for_mnf,(const int
*mindx, const int *ndata, float adata[], ftn_logical
logmiss[]),(const int *mindx, const int *ndata, float adata[],
ftn_logical logmiss[]),(const int *mindx, const int *ndata, float
adata[], ftn_logical logmiss[]))

  

**FORTRAN_SUBR** (LHPRT, lhprt,(const int *mindx, const int
*iprint),(const int *mindx, const int *iprint),(const int *mindx,
const int *iprint))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a51>`__ (LRBAT, lrbat,(const int
*mindx, int *batno, float rbatch[], fpstr cbatch, const int *iprint,
int cbatch_len),(const int *mindx, int *batno, float rbatch[], fpstr
cbatch, const int *iprint),(const int *mindx, int *batno, float
rbatch[], fpstr cbatch, int cbatch_len, const int *iprint))

  

**FORTRAN_SUBR** (LBPRT, lbprt,(const int *ibatch, const int *iprint,
float rbatch[], fpstr cbatch, int cbatch_len),(const int *ibatch,
const int *iprint, float rbatch[], fpstr cbatch),(const int *ibatch,
const int *iprint, float rbatch[], fpstr cbatch, int cbatch_len))

  

**FORTRAN_SUBR** (LRBRES, lrbres,(const int *mindx, const int
*batno),(const int *mindx, const int *batno),(const int *mindx,
const int *batno))

  

**FORTRAN_SUBR** (LRBTIT, lrbtit,(const int *mindx, const int *batno,
fpstr tbatch, const int *iprint, int tbatch_len),(const int *mindx,
const int *batno, fpstr tbatch, const int *iprint),(const int *mindx,
const int *batno, fpstr tbatch, int tbatch_len, const int *iprint))

  

**FORTRAN_SUBR** (LRBSCL, lrbscl,(const int *mindx, const int *batno,
float batscl[], int *nbatsc),(const int *mindx, const int *batno,
float batscl[], int *nbatsc),(const int *mindx, const int *batno,
float batscl[], int *nbatsc))

  

**FORTRAN_SUBR** (LRBSETID, lrbsetid,(const int *mindx, const int
*batno, int *bsetid),(const int *mindx, const int *batno, int
*bsetid),(const int *mindx, const int *batno, int *bsetid))

  

**FORTRAN_SUBR** (LRREWD, lrrewd,(const int *mindx),(const int
*mindx),(const int *mindx))

  

**FORTRAN_SUBR** (LSTRSL, lstrsl,(const int *mindx, const float *a,
const float *b, const float *c, const float *alpha, const float
*beta, const float *gamma),(const int *mindx, const float *a, const
float *b, const float *c, const float *alpha, const float *beta,
const float *gamma),(const int *mindx, const float *a, const float
*b, const float *c, const float *alpha, const float *beta, const
float *gamma))

  

**FORTRAN_SUBR** (LSTLSQ1, lstlsq1,(float *reso, const int *mindx,
const int *ih, const int *ik, const int *il),(float *reso, const int
*mindx, const int *ih, const int *ik, const int *il),(float *reso,
const int *mindx, const int *ih, const int *ik, const int *il))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a60>`__ (LWOPEN_NOEXIT,
lwopen_noexit,(const int *mindx, fpstr filename, int *ifail, int
filename_len),(const int *mindx, fpstr filename, int *ifail),(const
int *mindx, fpstr filename, int filename_len, int *ifail))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a61>`__ (LWOPEN, lwopen,(const int
*mindx, fpstr filename, int filename_len),(const int *mindx, fpstr
filename),(const int *mindx, fpstr filename, int filename_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a62>`__ (LWTITL, lwtitl,(const int
*mindx, const fpstr ftitle, const int *flag, int ftitle_len),(const
int *mindx, const fpstr ftitle, const int *flag),(const int *mindx,
const fpstr ftitle, int ftitle_len, const int *flag))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a63>`__ (LWSORT, lwsort,(const int
*mindx, int sortx[5]),(const int *mindx, int sortx[5]),(const int
*mindx, int sortx[5]))

  

**FORTRAN_SUBR** (LWHSTL, lwhstl,(int *mindx, const fpstr hstrng, int
hstrng_len),(int *mindx, const fpstr hstrng),(int *mindx, const fpstr
hstrng, int hstrng_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a65>`__ (LWID, lwid,(const int
*mindx, const fpstr project_name, const fpstr dataset_name, int
project_name_len, int dataset_name_len),(const int *mindx, const
fpstr project_name, const fpstr dataset_name),(const int *mindx,
const fpstr project_name, int project_name_len, const fpstr
dataset_name, int dataset_name_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a66>`__ (LWIDC, lwidc,(const int
*mindx, const fpstr project_name, const fpstr dataset_name, float
datcell[6], float *datwave, int project_name_len, int
dataset_name_len),(const int *mindx, const fpstr project_name, const
fpstr dataset_name, float datcell[6], float *datwave),(const int
*mindx, const fpstr project_name, int project_name_len, const fpstr
dataset_name, int dataset_name_len, float datcell[6], float
*datwave))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a67>`__ (LWIDX, lwidx,(const int
*mindx, const fpstr project_name, const fpstr crystal_name, const
fpstr dataset_name, float datcell[6], float *datwave, int
project_name_len, int crystal_name_len, int
dataset_name_len),(const int *mindx, const fpstr project_name, const
fpstr crystal_name, const fpstr dataset_name, float datcell[6], float
*datwave),(const int *mindx, const fpstr project_name, int
project_name_len, const fpstr crystal_name, int crystal_name_len,
const fpstr dataset_name, int dataset_name_len, float datcell[6],
float *datwave))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a68>`__ (LWCELL, lwcell,(const int
*mindx, float cell[6]),(const int *mindx, float cell[6]),(const int
*mindx, float cell[6]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a69>`__ (LWIDAS, lwidas,(const int
*mindx, int *nlprgo, fpstr pname, fpstr dname, int *iappnd, int
pname_len, int dname_len),(const int *mindx, int *nlprgo, fpstr
pname, fpstr dname, int *iappnd),(const int *mindx, int *nlprgo,
fpstr pname, int pname_len, fpstr dname, int dname_len, int *iappnd))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a70>`__ (LWIDASX, lwidasx,(const int
*mindx, int *nlprgo, fpstr xname, fpstr dname, int *iappnd, int
xname_len, int dname_len),(const int *mindx, int *nlprgo, fpstr
xname, fpstr dname, int *iappnd),(const int *mindx, int *nlprgo,
fpstr xname, int xname_len, fpstr dname, int dname_len, int *iappnd))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a71>`__ (LWIDALL, lwidall,(const int
*mindx, fpstr xname, fpstr dname, int xname_len, int
dname_len),(const int *mindx, fpstr xname, fpstr dname),(const int
*mindx, fpstr xname, int xname_len, fpstr dname, int dname_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a72>`__ (LWSYMM, lwsymm,(int *mindx,
int *nsymx, int *nsympx, float rsymx[MAXSYM][4][4], fpstr ltypex, int
*nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex_len, int spgrnx_len,
int pgnamx_len),(int *mindx, int *nsymx, int *nsympx, float
rsymx[MAXSYM][4][4], fpstr ltypex, int *nspgrx, fpstr spgrnx, fpstr
pgnamx),(int *mindx, int *nsymx, int *nsympx, float
rsymx[MAXSYM][4][4], fpstr ltypex, int ltypex_len, int *nspgrx, fpstr
spgrnx, int spgrnx_len, fpstr pgnamx, int pgnamx_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a73>`__ (LWASSN, lwassn,(const int
*mindx, fpstr lsprgo, const int *nlprgo, fpstr ctprgo, int *iappnd,
int lsprgo_len, int ctprgo_len),(const int *mindx, fpstr lsprgo,
const int *nlprgo, fpstr ctprgo, int *iappnd),(const int *mindx,
fpstr lsprgo, int lsprgo_len, const int *nlprgo, fpstr ctprgo, int
ctprgo_len, int *iappnd))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a74>`__ (LWBAT, lwbat,(const int
*mindx, int *batno, float rbatch[], fpstr cbatch, int
cbatch_len),(const int *mindx, int *batno, float rbatch[], fpstr
cbatch),(const int *mindx, int *batno, float rbatch[], fpstr cbatch,
int cbatch_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a75>`__ (LWBTIT, lwbtit,(const int
*mindx, int *batno, fpstr tbatch, int tbatch_len),(const int *mindx,
int *batno, fpstr tbatch),(const int *mindx, int *batno, fpstr
tbatch, int tbatch_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a76>`__ (LWBSCL, lwbscl,(const int
*mindx, int *batno, float batscl[], int *nbatsc),(const int *mindx,
int *batno, float batscl[], int *nbatsc),(const int *mindx, int
*batno, float batscl[], int *nbatsc))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a77>`__ (LWBSETID, lwbsetid,(const
int *mindx, const int *batno, const fpstr project_name, const fpstr
dataset_name, int project_name_len, int dataset_name_len),(const
int *mindx, const int *batno, const fpstr project_name, const fpstr
dataset_name),(const int *mindx, const int *batno, const fpstr
project_name, int project_name_len, const fpstr dataset_name, int
dataset_name_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a78>`__ (LWBSETIDX, lwbsetidx,(const
int *mindx, const int *batno, const fpstr crystal_name, const fpstr
dataset_name, int crystal_name_len, int dataset_name_len),(const
int *mindx, const int *batno, const fpstr crystal_name, const fpstr
dataset_name),(const int *mindx, const int *batno, const fpstr
crystal_name, int crystal_name_len, const fpstr dataset_name, int
dataset_name_len))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a79>`__ (EQUAL_MAGIC,
equal_magic,(const int *mindx, float adata[], const int *ncol),(const
int *mindx, float adata[], const int *ncol),(const int *mindx, float
adata[], const int *ncol))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a80>`__ (SET_MAGIC,
set_magic,(const int *mindx, float *val_magic, ftn_logical
*setval),(const int *mindx, float *val_magic, ftn_logical
*setval),(const int *mindx, float *val_magic, ftn_logical
*setval))

  

**FORTRAN_SUBR** (RESET_MAGIC, reset_magic,(const int *mindx, const
float adata[], float bdata[], const int *ncol, const float
*val_magica, const float *val_magicb),(const int *mindx, const
float adata[], float bdata[], const int *ncol, const float
*val_magica, const float *val_magicb),(const int *mindx, const
float adata[], float bdata[], const int *ncol, const float
*val_magica, const float *val_magicb))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a82>`__ (LWREFL_NOEXIT,
lwrefl_noexit,(const int *mindx, const float adata[], int
*ifail),(const int *mindx, const float adata[], int *ifail),(const
int *mindx, const float adata[], int *ifail))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a83>`__ (LWREFL, lwrefl,(const int
*mindx, const float adata[]),(const int *mindx, const float
adata[]),(const int *mindx, const float adata[]))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a84>`__ (LWCLOS_NOEXIT,
lwclos_noexit,(const int *mindx, int *iprint, int *ifail),(const int
*mindx, int *iprint, int *ifail),(const int *mindx, int *iprint,
int *ifail))

 

`FORTRAN_SUBR <cmtzlib__f_8c.html#a85>`__ (LWCLOS, lwclos,(const int
*mindx, int *iprint),(const int *mindx, int *iprint),(const int
*mindx, int *iprint))

  

**FORTRAN_SUBR** (RBATHD, rbathd,(),(),())

  

**FORTRAN_SUBR** (WBATHD, wbathd,(),(),())

  

**FORTRAN_SUBR** (LRHDRL, lrhdrl,(),(),())

  

**FORTRAN_SUBR** (SORTUP, sortup,(),(),())

  

**FORTRAN_SUBR** (ADDLIN, addlin,(),(),())

  

**FORTRAN_FUN** (int, NEXTLN, nextln,(),(),())

  

**FORTRAN_SUBR** (IS_MAGIC, is_magic,(const float *val_magic, const
float *valtst, ftn_logical *lvalms),(const float *val_magic, const
float *valtst, ftn_logical *lvalms),(const float *val_magic, const
float *valtst, ftn_logical *lvalms))

--------------

Detailed Description
--------------------

Fortran API for input, output and manipulation of
`MTZ <structMTZ.html>`__ files.

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

FORTRAN_SUBR

( 

LWCLOS 

 ,

lwclos 

 ,

(const int *mindx, int *iprint) 

 ,

(const int *mindx, int *iprint) 

 ,

(const int *mindx, int *iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write `MTZ <structMTZ.html>`__ file  |
|                                      | header and close output file.        |
|                                      | Wrapper for MtzPut.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iprint*    | (I) Specify whet |
|                                      | her to write output file header to l |
|                                      | og.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWCLOS_NOEXIT 

 ,

lwclos_noexit 

 ,

(const int *mindx, int *iprint, int *ifail) 

 ,

(const int *mindx, int *iprint, int *ifail) 

 ,

(const int *mindx, int *iprint, int *ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write `MTZ <structMTZ.html>`__ file  |
|                                      | header and close output file.        |
|                                      | Wrapper for MtzPut.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *iprint*    | (I) Specify whet |
|                                      | her to write output file header to l |
|                                      | og.   |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
|                                      |     | *ifail*     | (O) Returns 0 if |
|                                      |  successful, non-zero otherwise.     |
|                                      |       |                              |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------+                              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWREFL 

 ,

lwrefl 

 ,

(const int *mindx, const float adata[]) 

 ,

(const int *mindx, const float adata[]) 

 ,

(const int *mindx, const float adata[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write a one array of reflection data |
|                                      | to output file. This is a wrapper    |
|                                      | for ccp4_lwrefl. This routine exits |
|                                      | upon failure.                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index   |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *adata*    | (I) Array of refl |
|                                      | ection data to write.    |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWREFL_NOEXIT 

 ,

lwrefl_noexit 

 ,

(const int *mindx, const float adata[], int *ifail) 

 ,

(const int *mindx, const float adata[], int *ifail) 

 ,

(const int *mindx, const float adata[], int *ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write a one array of reflection data |
|                                      | to output file. This is a wrapper    |
|                                      | for ccp4_lwrefl.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index            |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *adata*    | (I) Array of refl |
|                                      | ection data to write.             |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *ifail*    | (O) Returns 0 if  |
|                                      | successful, non-zero otherwise.   |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

SET_MAGIC 

 ,

set_magic 

 ,

(const int *mindx, float *val_magic, ftn_logical *setval) 

 ,

(const int *mindx, float *val_magic, ftn_logical *setval) 

 ,

(const int *mindx, float *val_magic, ftn_logical *setval) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set or get MNF of file.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*         | `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |                                      |
|                                      |                                      |
|                                      |                       |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *val_magic*    | Value of MNF |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                       |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
|                                      |     | *setval*        | If true, set |
|                                      |  the MNF with the value in val_magi |
|                                      | c. If false, return value of MNF in  |
|                                      | val_magic. Returned as true, unless |
|                                      |  there is an error.   |              |
|                                      |     +-----------------+------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

EQUAL_MAGIC 

 ,

equal_magic 

 ,

(const int *mindx, float adata[], const int *ncol) 

 ,

(const int *mindx, float adata[], const int *ncol) 

 ,

(const int *mindx, float adata[], const int *ncol) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set whole array to MNF. The value of |
|                                      | the MNF is taken from the            |
|                                      | `MTZ <structMTZ.html>`__ struct on   |
|                                      | unit mindx.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index                 | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *adata*    | Array of reflecti |
|                                      | on data to be initialised.         | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
|                                      |     | *ncol*     | Number of columns |
|                                      |  in the array to be initialised.   | |
|                                      |     +------------+------------------ |
|                                      | -----------------------------------+ |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWBSETIDX 

 ,

lwbsetidx 

 ,

(const int *mindx, const int *batno, const fpstr crystal_name, const
fpstr dataset_name, int crystal_name_len, int dataset_name_len) 

 ,

(const int *mindx, const int *batno, const fpstr crystal_name, const
fpstr dataset_name) 

 ,

(const int *mindx, const int *batno, const fpstr crystal_name, int
crystal_name_len, const fpstr dataset_name, int dataset_name_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign a batch to a particular       |
|                                      | dataset, identified by crystal name  |
|                                      | and dataset name.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index   |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *batno*            | Serial nu |
|                                      | mber of batch.               |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *crystal_name*    | Crystal N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *dataset_name*    | Dataset N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWBSETID 

 ,

lwbsetid 

 ,

(const int *mindx, const int *batno, const fpstr project_name, const
fpstr dataset_name, int project_name_len, int dataset_name_len) 

 ,

(const int *mindx, const int *batno, const fpstr project_name, const
fpstr dataset_name) 

 ,

(const int *mindx, const int *batno, const fpstr project_name, int
project_name_len, const fpstr dataset_name, int dataset_name_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Obsolete. Use LWBSETIDX              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index   |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *batno*            | Serial nu |
|                                      | mber of batch.               |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *project_name*    | Project N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
|                                      |     | *dataset_name*    | Dataset N |
|                                      | ame                          |       |
|                                      |     +--------------------+---------- |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWBSCL 

 ,

lwbscl 

 ,

(const int *mindx, int *batno, float batscl[], int *nbatsc) 

 ,

(const int *mindx, int *batno, float batscl[], int *nbatsc) 

 ,

(const int *mindx, int *batno, float batscl[], int *nbatsc) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno. New batch scales are set.     |
|                                      | batno must correspond to             |
|                                      | pre-existing batch.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batscl*    | Array of batch s |
|                                      | cales.                |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nbatsc*    | Number of batch  |
|                                      | scales.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWBTIT 

 ,

lwbtit 

 ,

(const int *mindx, int *batno, fpstr tbatch, int tbatch_len) 

 ,

(const int *mindx, int *batno, fpstr tbatch) 

 ,

(const int *mindx, int *batno, fpstr tbatch, int tbatch_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno. Only the batch title is       |
|                                      | provided, so dummy header is         |
|                                      | written.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *tbatch*    | Batch title.     |
|                                      |                       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWBAT 

 ,

lwbat 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch, int
cbatch_len) 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch) 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch, int
cbatch_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write batch header for batch number  |
|                                      | batno.                               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batno*     | Serial number of |
|                                      |  batch.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rbatch*    | Real/integer bat |
|                                      | ch information.       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *cbatch*    | Character batch  |
|                                      | information.          |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWASSN 

 ,

lwassn 

 ,

(const int *mindx, fpstr lsprgo, const int *nlprgo, fpstr ctprgo, int
*iappnd, int lsprgo_len, int ctprgo_len) 

 ,

(const int *mindx, fpstr lsprgo, const int *nlprgo, fpstr ctprgo, int
*iappnd) 

 ,

(const int *mindx, fpstr lsprgo, int lsprgo_len, const int *nlprgo,
fpstr ctprgo, int ctprgo_len, int *iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to assign columns of |
|                                      | output `MTZ <structMTZ.html>`__      |
|                                      | file. First this updates labels from |
|                                      | user_label_out if set by lkyout,   |
|                                      | then sets collookup_out array of    |
|                                      | pointers to columns.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *lsprgo*    | array of output  |
|                                      | labels                               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *nlprgo*    | number of output |
|                                      |  labels                              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *ctprgo*    | array of output  |
|                                      | column types                         |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |           |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *iappnd*    | if = 0 replace a |
|                                      | ll existing columns, else if = 1 "ap |
|                                      | pend" to existing columns. Note that |
|                                      |  columns are appended to the relevan |
|                                      | t datasets and are not therefore nec |
|                                      | essarily at the end of the list of c |
|                                      | olumns.   |                          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWSYMM 

 ,

lwsymm 

 ,

(int *mindx, int *nsymx, int *nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int *nspgrx, fpstr spgrnx, fpstr pgnamx, int ltypex_len,
int spgrnx_len, int pgnamx_len) 

 ,

(int *mindx, int *nsymx, int *nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int *nspgrx, fpstr spgrnx, fpstr pgnamx) 

 ,

(int *mindx, int *nsymx, int *nsympx, float rsymx[MAXSYM][4][4],
fpstr ltypex, int ltypex_len, int *nspgrx, fpstr spgrnx, int
spgrnx_len, fpstr pgnamx, int pgnamx_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Write or update symmetry information |
|                                      | for `MTZ <structMTZ.html>`__ header. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsymx*     | (I) number of sy |
|                                      | mmetry operators                     |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nsympx*    | (I) number of pr |
|                                      | imitive symmetry operators           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *rsymx*     | (I) Array of sym |
|                                      | metry operators as 4 x 4 matrices. E |
|                                      | ach matrix is input with translation |
|                                      | s in elements [3][*] (i.e. reversed |
|                                      |  with respect to the way the Fortran |
|                                      |  application sees it). This function |
|                                      |  reverses the order before passing t |
|                                      | o ccp4_lwsymm.   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *ltypex*    | (I) lattice type |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *nspgrx*    | (I) spacegroup n |
|                                      | umber                                |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *spgrnx*    | (I) spacegroup n |
|                                      | ame                                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *pgnamx*    | (I) point group  |
|                                      | name                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                   |                  |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWIDALL 

 ,

lwidall 

 ,

(const int *mindx, fpstr xname, fpstr dname, int xname_len, int
dname_len) 

 ,

(const int *mindx, fpstr xname, fpstr dname) 

 ,

(const int *mindx, fpstr xname, int xname_len, fpstr dname, int
dname_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign output columns to             |
|                                      | crystal/datasets. This is a simpler  |
|                                      | version of LWIDASX to assign all     |
|                                      | columns to one dataset (except for   |
|                                      | HKL which are assigned to base       |
|                                      | dataset).                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *xname*    | Crystal name for  |
|                                      | all columns.          |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *dname*    | Dataset name for  |
|                                      | all columns.          |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWIDASX 

 ,

lwidasx 

 ,

(const int *mindx, int *nlprgo, fpstr xname, fpstr dname, int
*iappnd, int xname_len, int dname_len) 

 ,

(const int *mindx, int *nlprgo, fpstr xname, fpstr dname, int
*iappnd) 

 ,

(const int *mindx, int *nlprgo, fpstr xname, int xname_len, fpstr
dname, int dname_len, int *iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Assign output columns to             |
|                                      | crystal/datasets.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nlprgo*    | Number of output |
|                                      |  columns.                            |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *xname*     | Array of crystal |
|                                      |  names for columns.                  |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *dname*     | Array of dataset |
|                                      |  names for columns.                  |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *iappnd*    | If 0 then assign |
|                                      |  all columns, if 1 then assign appen |
|                                      | ded columns.   |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWIDAS 

 ,

lwidas 

 ,

(const int *mindx, int *nlprgo, fpstr pname, fpstr dname, int
*iappnd, int pname_len, int dname_len) 

 ,

(const int *mindx, int *nlprgo, fpstr pname, fpstr dname, int
*iappnd) 

 ,

(const int *mindx, int *nlprgo, fpstr pname, int pname_len, fpstr
dname, int dname_len, int *iappnd) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Obsolete - use LWIDASX.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nlprgo*    | Number of output |
|                                      |  columns.                            |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *pname*     | Array of project |
|                                      |  names.                              |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *dname*     | Array of dataset |
|                                      |  names.                              |
|                                      |                |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *iappnd*    | If 0 then assign |
|                                      |  all columns, if 1 then assign appen |
|                                      | ded columns.   |                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWCELL 

 ,

lwcell 

 ,

(const int *mindx, float cell[6]) 

 ,

(const int *mindx, float cell[6]) 

 ,

(const int *mindx, float cell[6]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to update cell of    |
|                                      | output `MTZ <structMTZ.html>`__      |
|                                      | file. Overall cell is obsolete - we  |
|                                      | only store crystal cell dimensions.  |
|                                      | Therefore this simply writes the     |
|                                      | cell dimensions for any crystal      |
|                                      | which has not yet been set. Crystal  |
|                                      | cell dimensions should be set        |
|                                      | directly with lwidc.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *cell*     | Output cell dimen |
|                                      | sions.                |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWIDX 

 ,

lwidx 

 ,

(const int *mindx, const fpstr project_name, const fpstr
crystal_name, const fpstr dataset_name, float datcell[6], float
*datwave, int project_name_len, int crystal_name_len, int
dataset_name_len) 

 ,

(const int *mindx, const fpstr project_name, const fpstr
crystal_name, const fpstr dataset_name, float datcell[6], float
*datwave) 

 ,

(const int *mindx, const fpstr project_name, int project_name_len,
const fpstr crystal_name, int crystal_name_len, const fpstr
dataset_name, int dataset_name_len, float datcell[6], float
*datwave) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4_lwidx for   |
|                                      | writing dataset header information.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *crystal_name*    | Name of p |
|                                      | arent crystal.                       |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datcell*          | Cell dime |
|                                      | nsions of parent crystal.            |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datwave*          | Wavelengt |
|                                      | h of dataset.                        |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWIDC 

 ,

lwidc 

 ,

(const int *mindx, const fpstr project_name, const fpstr
dataset_name, float datcell[6], float *datwave, int
project_name_len, int dataset_name_len) 

 ,

(const int *mindx, const fpstr project_name, const fpstr
dataset_name, float datcell[6], float *datwave) 

 ,

(const int *mindx, const fpstr project_name, int project_name_len,
const fpstr dataset_name, int dataset_name_len, float datcell[6],
float *datwave) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4_lwidx for   |
|                                      | writing dataset header information.  |
|                                      | As for LWIDX except crystal name is  |
|                                      | not provided, and defaults to        |
|                                      | supplied project name. This exists   |
|                                      | for backwards-compatibility - use    |
|                                      | LWIDX instead.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datcell*          | Cell dime |
|                                      | nsions of parent crystal.            |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *datwave*          | Wavelengt |
|                                      | h of dataset.                        |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWID 

 ,

lwid 

 ,

(const int *mindx, const fpstr project_name, const fpstr
dataset_name, int project_name_len, int dataset_name_len) 

 ,

(const int *mindx, const fpstr project_name, const fpstr
dataset_name) 

 ,

(const int *mindx, const fpstr project_name, int project_name_len,
const fpstr dataset_name, int dataset_name_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4_lwidx for   |
|                                      | writing dataset header information.  |
|                                      | As for LWIDX except crystal name is  |
|                                      | not provided, and defaults to        |
|                                      | supplied project name. Also cell and |
|                                      | wavelength are not provided and      |
|                                      | default to zero. This exists for     |
|                                      | backwards-compatibility - use LWIDX  |
|                                      | instead.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index.          |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *project_name*    | Name of p |
|                                      | roject that parent crystal belongs t |
|                                      | o.   |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
|                                      |     | *dataset_name*    | Name of d |
|                                      | ataset.                              |
|                                      |      |                               |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | -----+                               |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWSORT 

 ,

lwsort 

 ,

(const int *mindx, int sortx[5]) 

 ,

(const int *mindx, int sortx[5]) 

 ,

(const int *mindx, int sortx[5]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set sort order for output file. The  |
|                                      | integer array is stored as static.   |
|                                      | Try to set sort order now, but may   |
|                                      | not be possible if LWCLAB/LWASSN not |
|                                      | yet called.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index.   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *sortx*    | Sort order as int |
|                                      | eger array.           |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWTITL 

 ,

lwtitl 

 ,

(const int *mindx, const fpstr ftitle, const int *flag, int
ftitle_len) 

 ,

(const int *mindx, const fpstr ftitle, const int *flag) 

 ,

(const int *mindx, const fpstr ftitle, int ftitle_len, const int
*flag) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set title for output file.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index.                 |
|                                      |                              |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *ftitle*    | Title to be adde |
|                                      | d to output `MTZ <structMTZ.html>`__ |
|                                      |  file.                       |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
|                                      |     | *flag*      | =0 replace old t |
|                                      | itle with new one, or =1 append new  |
|                                      | one to old, with one space   |       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -----------------------------+       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWOPEN 

 ,

lwopen 

 ,

(const int *mindx, fpstr filename, int filename_len) 

 ,

(const int *mindx, fpstr filename) 

 ,

(const int *mindx, fpstr filename, int filename_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to open output       |
|                                      | `MTZ <structMTZ.html>`__ file. In    |
|                                      | fact, if reflection data is being    |
|                                      | held in memory, defer opening until  |
|                                      | MtzPut call. But if reflections are  |
|                                      | written immediately to file, need to |
|                                      | open now.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
|                                      |     | *mindx*       | `MTZ <structMT |
|                                      | Z.html>`__ file index.   |           |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
|                                      |     | *filename*    | Output file na |
|                                      | me.                      |           |
|                                      |     +---------------+--------------- |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LWOPEN_NOEXIT 

 ,

lwopen_noexit 

 ,

(const int *mindx, fpstr filename, int *ifail, int filename_len) 

 ,

(const int *mindx, fpstr filename, int *ifail) 

 ,

(const int *mindx, fpstr filename, int filename_len, int *ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to open output       |
|                                      | `MTZ <structMTZ.html>`__ file. In    |
|                                      | fact, if reflection data is being    |
|                                      | held in memory, defer opening until  |
|                                      | MtzPut call. But if reflections are  |
|                                      | written immediately to file, need to |
|                                      | open now.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *mindx*       | `MTZ <structMT |
|                                      | Z.html>`__ file index.               |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *filename*    | Output file na |
|                                      | me.                                  |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *ifail*       | (O) Returns 0  |
|                                      | if successful, non-zero otherwise.   |
|                                      |  |                                   |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRBAT 

 ,

lrbat 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch, const int
*iprint, int cbatch_len) 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch, const int
*iprint) 

 ,

(const int *mindx, int *batno, float rbatch[], fpstr cbatch, int
cbatch_len, const int *iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4_lrbat.     |
|                                      | Returns the header info for the next |
|                                      | batch from the multi-record          |
|                                      | `MTZ <structMTZ.html>`__ file open   |
|                                      | on index MINDX, as the two arrays    |
|                                      | RBATCH (for numbers) and CBATCH (for |
|                                      | characters).                         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *batno*     | On return, batch |
|                                      |  number                              |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *rbatch*    | On return, real  |
|                                      | and integer batch data.              |
|                                      |          |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *cbatch*    | On return, chara |
|                                      | cter batch data (title and axes name |
|                                      | s).      |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
|                                      |     | *iprint*    | =0 no printing,  |
|                                      | =1 print title only, >1 print full h |
|                                      | eader.   |                           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ---------+                           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRIDX 

 ,

lridx 

 ,

(const int *mindx, fpstr project_name, fpstr crystal_name, fpstr
dataset_name, int *isets, float *datcell, float *datwave, int
*ndatasets, int project_name_len, int crystal_name_len, int
dataset_name_len) 

 ,

(const int *mindx, fpstr project_name, fpstr crystal_name, fpstr
dataset_name, int *isets, float *datcell, float *datwave, int
*ndatasets) 

 ,

(const int *mindx, fpstr project_name, int project_name_len, fpstr
crystal_name, int crystal_name_len, fpstr dataset_name, int
dataset_name_len, int *isets, float *datcell, float *datwave, int
*ndatasets) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4_lridx.     |
|                                      | Return dataset information. Note     |
|                                      | requirement to input how much memory |
|                                      | allocated in calling routine.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *mindx*            | `MTZ <str |
|                                      | uctMTZ.html>`__ file index           |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *project_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *crystal_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *dataset_name*    |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *isets*            |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *datcell*          |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *datwave*          |           |
|                                      |                                      |
|                                      |                                      |
|                                      |         |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *ndatasets*        | On input: |
|                                      |  space reserved for dataset informat |
|                                      | ion. On output: number of datasets f |
|                                      | ound.   |                            |
|                                      |     +--------------------+---------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRASSN 

 ,

lrassn 

 ,

(const int *mindx, fpstr lsprgi, int *nlprgi, int lookup[], fpstr
ctprgi, int lsprgi_len, int ctprgi_len) 

 ,

(const int *mindx, fpstr lsprgi, int *nlprgi, int lookup[], fpstr
ctprgi) 

 ,

(const int *mindx, fpstr lsprgi, int lsprgi_len, int *nlprgi, int
lookup[], fpstr ctprgi, int ctprgi_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to ccp4_lrassn.     |
|                                      | First this updates labels from       |
|                                      | user_label_in if set by lkyin,     |
|                                      | then sets collookup array of         |
|                                      | pointers to columns.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *lsprgi*    | (I) Array of pro |
|                                      | gram column labels. These are the co |
|                                      | lumn labels used by the program, as  |
|                                      | opposed to the column labels in the  |
|                                      | file.                                |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *nlprgi*    | (I) Number of in |
|                                      | put program labels.                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *lookup*    | (I/O) On input,  |
|                                      | indicates whether a column is compul |
|                                      | sory or not (-1 = hard compulsory -  |
|                                      | program will fail if column not foun |
|                                      | d, 1 = soft compulsory - program wil |
|                                      | l attempt to find column even if not |
|                                      |  assigned on LABIN, 0 = optional). O |
|                                      | n output, gives the index of the col |
|                                      | umn in the input file.   |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
|                                      |     | *ctprgi*    | (I) Array of col |
|                                      | umn types.                           |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                          |           |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LKYIN 

 ,

lkyin 

 ,

(const int *mindx, const fpstr lsprgi, const int *nlprgi, const int
*ntok, const fpstr labin_line, const int ibeg[], const int iend[], int
lsprgi_len, int labin_line_len) 

 ,

(const int *mindx, const fpstr lsprgi, const int *nlprgi, const int
*ntok, const fpstr labin_line, const int ibeg[], const int iend[]) 

 ,

(const int *mindx, const fpstr lsprgi, int lsprgi_len, const int
*nlprgi, const int *ntok, const fpstr labin_line, int
labin_line_len, const int ibeg[], const int iend[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to MtzParseLabin.    |
|                                      | This matches tokens from the LABIN   |
|                                      | line against the program labels      |
|                                      | supplied by the program.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *mindx*          | (I) `MTZ <s |
|                                      | tructMTZ.html>`__ file index.        |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *lsprgi*         | (I) Array o |
|                                      | f program column labels. These are t |
|                                      | he column labels used by the program |
|                                      | , as opposed to the column labels in |
|                                      |  the file.   |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *nlprgi*         | (I) Number  |
|                                      | of input program labels.             |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *ntok*           | (I) From Pa |
|                                      | rser: number of tokens on line       |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *labin_line*    | (I) From Pa |
|                                      | rser: input line                     |
|                                      |                                      |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *ibeg*           | (I) From Pa |
|                                      | rser: array of starting delimiters f |
|                                      | or each token                        |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *iend*           | (I) From Pa |
|                                      | rser: array of ending delimiters for |
|                                      |  each token                          |
|                                      |                                      |
|                                      |              |                       |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRSYMM 

 ,

lrsymm 

 ,

(int *mindx, int *nsymx, float rsymx[MAXSYM][4][4]) 

 ,

(int *mindx, int *nsymx, float rsymx[MAXSYM][4][4]) 

 ,

(int *mindx, int *nsymx, float rsymx[MAXSYM][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get symmetry matrices from           |
|                                      | `MTZ <structMTZ.html>`__ structure.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index.              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *nsymx*    | (O) Number of sym |
|                                      | metry operators held in `MTZ <struct |
|                                      | MTZ.html>`__ header.                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *rsymx*    | (O) Symmetry oper |
|                                      | ators as 4 x 4 matrices, in the orde |
|                                      | r they are held in the `MTZ <structM |
|                                      | TZ.html>`__ header. Each matrix has  |
|                                      | translations in elements [3][*]. No |
|                                      | te that a Fortran application will r |
|                                      | everse the order of indices.   |     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRRSOL 

 ,

lrrsol 

 ,

(int *mindx, float *minres, float *maxres) 

 ,

(int *mindx, float *minres, float *maxres) 

 ,

(int *mindx, float *minres, float *maxres) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the overall resolution limits |
|                                      | of the `MTZ <structMTZ.html>`__      |
|                                      | structure. These are the widest      |
|                                      | limits over all crystals present.    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *minres*    | (O) minimum reso |
|                                      | lution                    |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *maxres*    | (O) maximum reso |
|                                      | lution                    |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRCELL 

 ,

lrcell 

 ,

(int *mindx, float cell[]) 

 ,

(int *mindx, float cell[]) 

 ,

(int *mindx, float cell[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the nominal cell dimensions   |
|                                      | of the `MTZ <structMTZ.html>`__      |
|                                      | structure. In fact, these are the    |
|                                      | cell dimensions of the 1st crystal   |
|                                      | in the `MTZ <structMTZ.html>`__      |
|                                      | structure. It is better to use the   |
|                                      | cell dimensions of the correct       |
|                                      | crystal, as obtained from LRIDX.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *mindx*    | (I) `MTZ <structM |
|                                      | TZ.html>`__ file index   |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
|                                      |     | *cell*     | (O) Cell dimensio |
|                                      | ns.                      |           |
|                                      |     +------------+------------------ |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRBATS 

 ,

lrbats 

 ,

(int *mindx, int *nbatx, int batchx[]) 

 ,

(int *mindx, int *nbatx, int batchx[]) 

 ,

(int *mindx, int *nbatx, int batchx[]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for ccp4_lrbats.    |
|                                      | May be called for non-multirecord    |
|                                      | file, just to check there are no     |
|                                      | batches.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nbatx*     | Number of batche |
|                                      | s found.              |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *batchx*    | Array of batch n |
|                                      | umbers.               |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRNREF 

 ,

lrnref 

 ,

(int *mindx, int *nreflx) 

 ,

(int *mindx, int *nreflx) 

 ,

(int *mindx, int *nreflx) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Returns the current reflection       |
|                                      | number from an                       |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read. Files are normally read    |
|                                      | sequentially, the number returned is |
|                                      | the number of the *NEXT*           |
|                                      | reflection record to be read. If you |
|                                      | are going to jump about the file     |
|                                      | with LRSEEK then use this to record  |
|                                      | the current position before you      |
|                                      | start, so that it can be restored    |
|                                      | afterwards, if required.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |              |                       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
|                                      |     | *nreflx*    | the reflection r |
|                                      | ecord number of the next reflection  |
|                                      | to be read   |                       |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -------------+                       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRNCOL 

 ,

lrncol 

 ,

(int *mindx, int *ncolx) 

 ,

(int *mindx, int *ncolx) 

 ,

(int *mindx, int *ncolx) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to function          |
|                                      | returning number of columns read in  |
|                                      | from input file.                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *mindx*    | `MTZ <structMTZ.h |
|                                      | tml>`__ file index   |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *ncolx*    | Number of columns |
|                                      | .                    |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRINFO 

 ,

lrinfo 

 ,

(int *mindx, fpstr versnx, int *ncolx, int *nreflx, float *ranges,
int versnx_len) 

 ,

(int *mindx, fpstr versnx, int *ncolx, int *nreflx, float *ranges) 

 ,

(int *mindx, fpstr versnx, int versnx_len, int *ncolx, int *nreflx,
float *ranges) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper to functions         |
|                                      | returning number of reflections,     |
|                                      | columns and ranges. In fact, it      |
|                                      | returns current values on MINDX      |
|                                      | rather than those of the input file. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | `MTZ <structMTZ. |
|                                      | html>`__ file index                  |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *versnx*    | (O) `MTZ <struct |
|                                      | MTZ.html>`__ version. This is the ve |
|                                      | rsion of the current library rather  |
|                                      | than that of the input file. If thes |
|                                      | e differ, a warning will have been i |
|                                      | ssued by LROPEN/MtzGet.   |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *ncolx*     | Number of column |
|                                      | s.                                   |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *nreflx*    | Number of reflec |
|                                      | tions.                               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *ranges*    | Array of column  |
|                                      | ranges.                              |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRHIST 

 ,

lrhist 

 ,

(int *mindx, fpstr hstrng, int *nlines, int hstrng_len) 

 ,

(int *mindx, fpstr hstrng, int *nlines) 

 ,

(int *mindx, fpstr hstrng, int hstrng_len, int *nlines) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get history lines from               |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index              |
|                                      |                                      |
|                                      |                                      |
|                                      |                         |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *hstrng*    | (O) Array of his |
|                                      | tory lines.                          |
|                                      |                                      |
|                                      |                                      |
|                                      |                         |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
|                                      |     | *nlines*    | (I/O) On input,  |
|                                      | dimension of hstrng, i.e. the maximu |
|                                      | m number of history lines to be retu |
|                                      | rned. On output, actual number of hi |
|                                      | story lines returned.   |            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------+            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LRTITL 

 ,

lrtitl 

 ,

(int *mindx, fpstr ftitle, int *len, int ftitle_len) 

 ,

(int *mindx, fpstr ftitle, int *len) 

 ,

(int *mindx, fpstr ftitle, int ftitle_len, int *len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get title from                       |
|                                      | `MTZ <structMTZ.html>`__ file opened |
|                                      | for read.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *mindx*     | (I) `MTZ <struct |
|                                      | MTZ.html>`__ file index   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *ftitle*    | (O) Title.       |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *len*       | (O) Length of re |
|                                      | turned title.             |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

LROPEN 

 ,

lropen 

 ,

(int *mindx, fpstr filename, int *iprint, int *ifail, int
filename_len) 

 ,

(int *mindx, fpstr filename, int *iprint, int *ifail) 

 ,

(int *mindx, fpstr filename, int filename_len, int *iprint, int
*ifail) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Open an `MTZ <structMTZ.html>`__     |
|                                      | file for reading. This is a wrapper  |
|                                      | to MtzGet.                           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *mindx*       | (I) `MTZ <stru |
|                                      | ctMTZ.html>`__ file index            |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *filename*    | (I) Filename t |
|                                      | o open (real or logical)             |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*      | (I) Specifies  |
|                                      | how much header information to print |
|                                      |  out.   |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *ifail*       | (O) Returns 0  |
|                                      | if successful, non-zero otherwise.   |
|                                      |         |                            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

MTZINI 

 ,

mtzini 

 ,

() 

 ,

() 

 ,

() 

 

) 

+-----+------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Mainly for backwards compatibility. The only thing it does now is initialise the html/summary stuff, which is usually done by CCPFYP anyway.   |
+-----+------------------------------------------------------------------------------------------------------------------------------------------------+
