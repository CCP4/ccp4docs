`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library_f.h
============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      library_f.h: header file for library_f.c
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00011 #ifndef __CCP4_LIBRARY_F
    00012 #define __CCP4_LIBRARY_F
    00013 
    00014 /****************************************************************************
    00015  * Prototype subroutines                                                    *
    00016  ****************************************************************************/
    00017 
    00018 size_t flength (char *s, int len);
    00019 
    00020 #endif  /* __CCP4_LIBRARY_F */
    00021 

.. raw:: html

   </div>
