`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

mtzdata.h File Reference
========================

Definition of `MTZ <structMTZ.html>`__ data structure.
`More... <#_details>`__

`Go to the source code of this file. <mtzdata_8h-source.html>`__

| 

Compounds
---------

struct  

`bathead <structbathead.html>`__

union  

**MNF**

struct  

`MTZ <structMTZ.html>`__

struct  

`MTZCOL <structMTZCOL.html>`__

struct  

`MTZSET <structMTZSET.html>`__

struct  

`MTZXTAL <structMTZXTAL.html>`__

struct  

`SYMGRP <structSYMGRP.html>`__

| 

Defines
-------

#define 

`MTZVERSN <mtzdata_8h.html#a0>`__   "MTZ:V1.1"

#define 

`MTZ_MAJOR_VERSN <mtzdata_8h.html#a1>`__   1

#define 

`MTZ_MINOR_VERSN <mtzdata_8h.html#a2>`__   1

#define 

`SIZE1 <mtzdata_8h.html#a3>`__   20

#define 

`MTZRECORDLENGTH <mtzdata_8h.html#a4>`__   80

#define 

`MAXSPGNAMELENGTH <mtzdata_8h.html#a5>`__   20

#define 

`NBATCHWORDS <mtzdata_8h.html#a6>`__   185

#define 

`NBATCHINTEGERS <mtzdata_8h.html#a7>`__   29

#define 

`NBATCHREALS <mtzdata_8h.html#a8>`__   156

#define 

`MXTALS <mtzdata_8h.html#a9>`__   100

#define 

`MSETS <mtzdata_8h.html#a10>`__   1000

#define 

`MCOLUMNS <mtzdata_8h.html#a11>`__   10000

| 

Typedefs
--------

typedef `bathead <structbathead.html>`__ 

`MTZBAT <mtzdata_8h.html#a12>`__

--------------

Detailed Description
--------------------

Definition of `MTZ <structMTZ.html>`__ data structure.

The file defines a hierarchy of structs which hold the
`MTZ <structMTZ.html>`__ data structure.

 **Author:**
    Martyn Winn

--------------

Define Documentation
--------------------

+--------------------------------------------------------------------------+
| +---------------------------------+                                      |
| | #define MAXSPGNAMELENGTH   20   |                                      |
| +---------------------------------+                                      |
+--------------------------------------------------------------------------+

+-----+-----------------------------------+
|     | max length of a spacegroup name   |
+-----+-----------------------------------+

+--------------------------------------------------------------------------+
| +----------------------------+                                           |
| | #define MCOLUMNS   10000   |                                           |
| +----------------------------+                                           |
+--------------------------------------------------------------------------+

+-----+-----------------------------------------------------------------+
|     | maximum number of columns (for a few arrays - to be removed!)   |
+-----+-----------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+                                               |
| | #define MSETS   1000   |                                               |
| +------------------------+                                               |
+--------------------------------------------------------------------------+

+-----+------------------------------------------------------------------+
|     | maximum number of datasets (for a few arrays - to be removed!)   |
+-----+------------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+                                      |
| | #define MTZ_MAJOR_VERSN   1   |                                      |
| +---------------------------------+                                      |
+--------------------------------------------------------------------------+

+-----+----------------------------------------------------------------------+
|     | `MTZ <structMTZ.html>`__ file major version - keep to single digit   |
+-----+----------------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+                                      |
| | #define MTZ_MINOR_VERSN   1   |                                      |
| +---------------------------------+                                      |
+--------------------------------------------------------------------------+

+-----+----------------------------------------------------------------------+
|     | `MTZ <structMTZ.html>`__ file minor version - keep to single digit   |
+-----+----------------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------------------+                                       |
| | #define MTZRECORDLENGTH   80   |                                       |
| +--------------------------------+                                       |
+--------------------------------------------------------------------------+

+-----+---------------------+
|     | length of records   |
+-----+---------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+                                      |
| | #define MTZVERSN   "MTZ:V1.1"   |                                      |
| +---------------------------------+                                      |
+--------------------------------------------------------------------------+

+-----+-------------------------------+
|     | traditional version number!   |
+-----+-------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+                                               |
| | #define MXTALS   100   |                                               |
| +------------------------+                                               |
+--------------------------------------------------------------------------+

+-----+------------------------------------------------------------------+
|     | maximum number of crystals (for a few arrays - to be removed!)   |
+-----+------------------------------------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+                                        |
| | #define NBATCHINTEGERS   29   |                                        |
| +-------------------------------+                                        |
+--------------------------------------------------------------------------+

+-----+--------------------------------------------------+
|     | size of integer section of batch header buffer   |
+-----+--------------------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+                                          |
| | #define NBATCHREALS   156   |                                          |
| +-----------------------------+                                          |
+--------------------------------------------------------------------------+

+-----+------------------------------------------------+
|     | size of float section of batch header buffer   |
+-----+------------------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------+                                          |
| | #define NBATCHWORDS   185   |                                          |
| +-----------------------------+                                          |
+--------------------------------------------------------------------------+

+-----+-------------------------------------+
|     | total size of batch header buffer   |
+-----+-------------------------------------+

+--------------------------------------------------------------------------+
| +----------------------+                                                 |
| | #define SIZE1   20   |                                                 |
| +----------------------+                                                 |
+--------------------------------------------------------------------------+

+-----+--------------------------------+
|     | size of pre-reflection block   |
+-----+--------------------------------+

--------------

Typedef Documentation
---------------------

+--------------------------------------------------------------------------+
| +----------------------------------------------------------+             |
| | typedef struct `bathead <structbathead.html>`__ MTZBAT   |             |
| +----------------------------------------------------------+             |
+--------------------------------------------------------------------------+

+-----+------------------------------------------+
|     | `MTZ <structMTZ.html>`__ batch struct.   |
+-----+------------------------------------------+
