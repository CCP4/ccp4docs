`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library_f.c File Reference
===========================

| ``#include "ccp4_utils.h"``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_fortran.h"``

| 

Functions
---------

char * 

`ccp4_FtoCString <library__f_8c.html#a1>`__ (fpstr str1, int str1_len)

void 

`ccp4_CtoFString <library__f_8c.html#a2>`__ (fpstr str1, int str1_len,
const char *cstring)

  

**FORTRAN_SUBR** (USTENV, ustenv,(fpstr str, int *result, int
str_len),(fpstr str, int *result),(fpstr str, int str_len, int
*result))

  

**FORTRAN_SUBR** (USTIME, ustime,(int *isec),(int *isec),(int
*isec))

  

**FORTRAN_SUBR** (OUTBUF, outbuf,(),(),())

  

**FORTRAN_SUBR** (CUNLINK, cunlink,(fpstr filename, int
filename_len),(fpstr filename),(fpstr filename, int filename_len))

  

**FORTRAN_SUBR** (CCPAL1, ccpal1,(void( *routne)(), int *n, int
type[], int length[]),(void( *routne)(), int *n, int type[], int
length[]),(void( *routne)(), int *n, int type[], int length[]))

  

**FORTRAN_SUBR** (QNAN, qnan,(union float_uint_uchar
*realnum),(union float_uint_uchar *realnum),(union
float_uint_uchar *realnum))

  

**FORTRAN_FUN** (int, QISNAN, qisnan,(union float_uint_uchar
*realnum),(union float_uint_uchar *realnum),(union
float_uint_uchar *realnum))

  

**FORTRAN_SUBR** (CCPBML, ccpbml,(int *ncols, union float_uint_uchar
cols[]),(int *ncols, union float_uint_uchar cols[]),(int *ncols,
union float_uint_uchar cols[]))

  

**FORTRAN_SUBR** (CCPWRG, ccpwrg,(int *ncols, union float_uint_uchar
cols[], float wminmax[]),(int *ncols, union float_uint_uchar cols[],
float wminmax[]),(int *ncols, union float_uint_uchar cols[], float
wminmax[]))

  

**FORTRAN_SUBR** (HGETLIMITS, hgetlimits,(int *IValueNotDet, float
*ValueNotDet),(int *IValueNotDet, float *ValueNotDet),(int
*IValueNotDet, float *ValueNotDet))

  

**FORTRAN_SUBR** (CMKDIR, cmkdir,(const fpstr path, const fpstr cmode,
int *result, int path_len, int cmode_len),(const fpstr path, const
fpstr cmode, int *result),(const fpstr path, int path_len, const fpstr
cmode, int cmode_len, int *result))

--------------

Detailed Description
--------------------

FORTRAN API for library.c. Charles Ballard

--------------

Function Documentation
----------------------

void ccp4_CtoFString

( 

fpstr 

  *str1*,

int 

  *str1_len*,

const char * 

  *cstring*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a Fortran string from an     |
|                                      | input C string for passing back to   |
|                                      | Fortran call. Characters after       |
|                                      | null-terminator may be junk, so pad  |
|                                      | with spaces. If input cstring is     |
|                                      | NULL, return blank string.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1*         | pointer Fortr |
|                                      | an to string   |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1_len*    | Fortran lengt |
|                                      | h of string    |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *cstring*      | input C strin |
|                                      | g              |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

char* ccp4_FtoCString

( 

fpstr 

  *str1*,

int 

  *str1_len*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a null-terminated C string   |
|                                      | from an input string obtained from a |
|                                      | Fortran call. Trailing blanks are    |
|                                      | removed. If input string is blank    |
|                                      | then return string "\\0". Memory     |
|                                      | assigned by malloc, so can be freed. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1*         | pointer to st |
|                                      | ring          |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1_len*    | Fortran lengt |
|                                      | h of string   |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+
