`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_diskio_f.c File Reference
================================

| ``#include <string.h>``
| ``#include "ccp4_utils.h"``
| ``#include "ccp4_errno.h"``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_file_err.h"``

| 

Compounds
---------

struct  

**_CCP4IObj**

| 

Typedefs
--------

typedef _CCP4IObj 

`CCP4IObj <ccp4__diskio__f_8c.html#a1>`__

| 

Enumerations
------------

enum  

**FILE_KLASS** { **NONE**, **CCP4_FILE**, **CCP4_MAP** }

| 

Functions
---------

 int 

**_get_channel** ()

 `CCP4IObj <ccp4__diskio__f_8c.html#a1>`__ * 

**_iobj_init** ()

  

**FORTRAN_SUBR** (QOPEN, qopen,(int *iunit, fpstr lognam, fpstr
atbuta, int lognam_len, int atbuta_len),(int *iunit, fpstr lognam,
fpstr atbuta),(int *iunit, fpstr lognam, int lognam_len, fpstr atbuta,
int atbuta_len))

  

**FORTRAN_SUBR** (QQOPEN, qqopen,(int *iunit, fpstr lognam, const int
*istat, int lognam_len),(int *iunit, fpstr lognam, const int
*istat),(int *iunit, fpstr lognam, int lognam_len, const int
*istat))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a11>`__ (COPEN, copen,(int
*iunit, fpstr filename, int *istat, int filename_len),(int *iunit,
fpstr filename, int *istat),(int *iunit, fpstr filename, int
filename_len, int *istat))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a12>`__ (QRARCH, qrarch,(int
*iunit, int *ipos, int *ireslt),(int *iunit, int *ipos, int
*ireslt),(int *iunit, int *ipos, int *ireslt))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a13>`__ (QWARCH, qwarch,(int
*iunit, int *ipos),(int *iunit, int *ipos),(int *iunit, int
*ipos))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a14>`__ (QCLOSE, qclose,(int
*iunit),(int *iunit),(int *iunit))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a15>`__ (QMODE, qmode,(int
*iunit, int *mode, int *size),(int *iunit, int *mode, int
*size),(int *iunit, int *mode, int *size))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a16>`__ (QREAD, qread,(int
*iunit, uint8 *buffer, int *nitems, int *result),(int *iunit, uint8
*buffer, int *nitems, int *result),(int *iunit, uint8 *buffer, int
*nitems, int *result))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a17>`__ (QREADC, qreadc,(int
*iunit, fpstr buffer, int *result, int buffer_len),(int *iunit,
fpstr buffer, int *result),(int *iunit, fpstr buffer, int buffer_len,
int *result))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a18>`__ (QWRITE, qwrite,(int
*iunit, uint8 *buffer, int *nitems),(int *iunit, uint8 *buffer, int
*nitems),(int *iunit, uint8 *buffer, int *nitems))

  

**FORTRAN_SUBR** (QWRITC, qwritc,(int *iunit, fpstr buffer, int
buffer_len),(int *iunit, fpstr buffer),(int *iunit, fpstr buffer, int
buffer_len))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a20>`__ (QSEEK, qseek,(int
*iunit, int *irec, int *iel, int *lrecl),(int *iunit, int *irec,
int *iel, int *lrecl),(int *iunit, int *irec, int *iel, int
*lrecl))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a21>`__ (QBACK, qback,(int
*iunit, int *lrecl),(int *iunit, int *lrecl),(int *iunit, int
*lrecl))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a22>`__ (QQINQ, qqinq,(int
*istrm, fpstr logname, fpstr filename, int *length, int logname_len,
int filename_len),(int *istrm, fpstr logname, fpstr filename, int
*length),(int *istrm, fpstr logname, int logname_len, fpstr filename,
int filename_len, int *length))

 

`FORTRAN_SUBR <ccp4__diskio__f_8c.html#a23>`__ (QLOCATE, qlocate,(int
*iunit, int *locate),(int *iunit, int *locate),(int *iunit, int
*locate))

--------------

Detailed Description
--------------------

FORTRAN API for file i/o. Charles Ballard and Martyn Winn

--------------

Typedef Documentation
---------------------

+--------------------------------------------------------------------------+
| +--------------------------------------+                                 |
| | typedef struct _CCP4IObj CCP4IObj   |                                 |
| +--------------------------------------+                                 |
+--------------------------------------------------------------------------+

+-----+-----------------------------------------+
|     | _ioChannels: structure to hold files   |
+-----+-----------------------------------------+

--------------

Function Documentation
----------------------

FORTRAN_SUBR

( 

QLOCATE 

 ,

qlocate 

 ,

(int *iunit, int *locate) 

 ,

(int *iunit, int *locate) 

 ,

(int *iunit, int *locate) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qlocate: @iunit: @locate:            |
|                                      |                                      |
|                                      | Returns the current position         |
|                                      | \\meta{locate} in the diskio stream  |
|                                      | @iunit.                              |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QQINQ 

 ,

qqinq 

 ,

(int *istrm, fpstr logname, fpstr filename, int *length, int
logname_len, int filename_len) 

 ,

(int *istrm, fpstr logname, fpstr filename, int *length) 

 ,

(int *istrm, fpstr logname, int logname_len, fpstr filename, int
filename_len, int *length) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qqinq: @istrm: @filnam: @length:     |
|                                      |                                      |
|                                      | Returns the name @filnam and @length |
|                                      | of the file (if any) open on diskio  |
|                                      | stream @istrm.                       |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QBACK 

 ,

qback 

 ,

(int *iunit, int *lrecl) 

 ,

(int *iunit, int *lrecl) 

 ,

(int *iunit, int *lrecl) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qback: @iunit: @lrecl:               |
|                                      |                                      |
|                                      | Backspaces one record, of length     |
|                                      | @lrecl on diskio stream @iunit.      |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QSEEK 

 ,

qseek 

 ,

(int *iunit, int *irec, int *iel, int *lrecl) 

 ,

(int *iunit, int *irec, int *iel, int *lrecl) 

 ,

(int *iunit, int *irec, int *iel, int *lrecl) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qseek: @iunit: @irec: @iel: @lrecl:  |
|                                      |                                      |
|                                      | Seeks to element @iel in record      |
|                                      | @irec in diskio stream @iunit whose  |
|                                      | record length is @lrecl.             |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QWRITE 

 ,

qwrite 

 ,

(int *iunit, uint8 *buffer, int *nitems) 

 ,

(int *iunit, uint8 *buffer, int *nitems) 

 ,

(int *iunit, uint8 *buffer, int *nitems) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qwrite: @iunit: @buffer: @meta:      |
|                                      |                                      |
|                                      | This write @nitems items from        |
|                                      | @buffer to qopen() stream            |
|                                      | \\meta{iunit} using the current      |
|                                      | mode.                                |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QREADC 

 ,

qreadc 

 ,

(int *iunit, fpstr buffer, int *result, int buffer_len) 

 ,

(int *iunit, fpstr buffer, int *result) 

 ,

(int *iunit, fpstr buffer, int buffer_len, int *result) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qreadc:: @iunit: @buffer: @result:   |
|                                      |                                      |
|                                      | Fills CHARACTER buffer in byte mode  |
|                                      | from diskio stream @iunit previously |
|                                      | opened by qopen() and returns        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     which is 0 on success or -1 on   |
|                                      |     EOF. It aborts on an i/o         |
|                                      |     failure. Call it with a          |
|                                      |     character substring if necessary |
|                                      |     to control the number of bytes   |
|                                      |     read.                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QREAD 

 ,

qread 

 ,

(int *iunit, uint8 *buffer, int *nitems, int *result) 

 ,

(int *iunit, uint8 *buffer, int *nitems, int *result) 

 ,

(int *iunit, uint8 *buffer, int *nitems, int *result) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qread: @iunit: io channel @buffer:   |
|                                      | @nitems: number of items @result:    |
|                                      | return value                         |
|                                      |                                      |
|                                      | Reads @nitems in the current mode    |
|                                      | qmode() from diskio stream @iunit    |
|                                      | previously opened by qopen() and     |
|                                      | returns                              |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     which is %0 on success or %-1 at |
|                                      |     EOF. It aborts on an i/o error.  |
|                                      |     Numbers written in a foreign     |
|                                      |     format will be translated if     |
|                                      |     necessary if the stream is       |
|                                      |     connected to an                  |
|                                      |     `MTZ <structMTZ.html>`__ or map  |
|                                      |     file.                            |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QMODE 

 ,

qmode 

 ,

(int *iunit, int *mode, int *size) 

 ,

(int *iunit, int *mode, int *size) 

 ,

(int *iunit, int *mode, int *size) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qmode: @iunit: io channel @mode:     |
|                                      | access mode @size: item size         |
|                                      |                                      |
|                                      | Changes the diskio access mode for   |
|                                      | stream @iunit to @mode. The          |
|                                      | resulting size in bytes of items for |
|                                      | transfer is returned as @size        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QCLOSE 

 ,

qclose 

 ,

(int *iunit) 

 ,

(int *iunit) 

 ,

(int *iunit) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qclose: @iunit: io channel           |
|                                      |                                      |
|                                      | Closes the file open on diskio       |
|                                      | stream iunit                         |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QWARCH 

 ,

qwarch 

 ,

(int *iunit, int *ipos) 

 ,

(int *iunit, int *ipos) 

 ,

(int *iunit, int *ipos) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qwarch @iunit: io channel @ipos:     |
|                                      | position                             |
|                                      |                                      |
|                                      | This is the complement of qrarch,    |
|                                      | writing the native machine           |
|                                      | architecture information machine     |
|                                      | stamp to diskio stream iunit at word |
|                                      | ipos. Currently called from mtzlib   |
|                                      | and maplib.                          |
|                                      |                                      |
|                                      | The machine stamp in mtstring is     |
|                                      | four nibbles in order, indicating    |
|                                      | complex and real format (must both   |
|                                      | be the same), integer format and     |
|                                      | character format (currently          |
|                                      | irrelevant). The last two bytes of   |
|                                      | mtstring are currently unused and    |
|                                      | always zero.                         |
|                                      |                                      |
|                                      | N.B.: leaves the stream positioned   |
|                                      | just after the machine stamp.        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

QRARCH 

 ,

qrarch 

 ,

(int *iunit, int *ipos, int *ireslt) 

 ,

(int *iunit, int *ipos, int *ireslt) 

 ,

(int *iunit, int *ipos, int *ireslt) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | qrarch: @iunit: iochannel @ipos:     |
|                                      | position in file @ireslt: return     |
|                                      | value                                |
|                                      |                                      |
|                                      | For binary files with a              |
|                                      | well-determined structure in terms   |
|                                      | of [[float]]s and [[int]]s we may    |
|                                      | want to set up the connected stream  |
|                                      | to do transparent reading of files   |
|                                      | written on a machine with a          |
|                                      | different architecture. This is      |
|                                      | currently the case for map files and |
|                                      | `MTZ <structMTZ.html>`__ files and   |
|                                      | this routine is called from mtzlib   |
|                                      | and maplib.                          |
|                                      |                                      |
|                                      | qrarch reads the machine stamp at    |
|                                      | word ipos for the diskio file on     |
|                                      | stream iunit and sets up the         |
|                                      | appropriate bit-twiddling for        |
|                                      | subsequent qreads on that stream.    |
|                                      | The information read from the file   |
|                                      | is returned in \\meta{ireslt} in the |
|                                      | form fileFT+16fileIT. If the stamp   |
|                                      | is zero (as it would be for files    |
|                                      | written with a previous version of   |
|                                      | the library) we assume the file is   |
|                                      | in native format and needs no        |
|                                      | conversion in qread; in this case    |
|                                      | ireslt will be zero and the caller   |
|                                      | can issue a warning. Iconvert and    |
|                                      | Fconvert are used by qread to        |
|                                      | determine the type of conversion (if |
|                                      | any) to be applied to integers and   |
|                                      | reals.                               |
|                                      |                                      |
|                                      | Fudge:fudge Ian Tickle reports old   |
|                                      | VAX files which have a machine stamp |
|                                      | which is byte-flipped from the       |
|                                      | correct VAX value,although it should |
|                                      | always have been zero as far as I    |
|                                      | can see. To accommodate this, set    |
|                                      | the logical NATIVEMTZ and the        |
|                                      | machine stamp won't be read for any  |
|                                      | input files for which qrarch is      |
|                                      | called.                              |
|                                      |                                      |
|                                      | Extra feature: logical/environment   |
|                                      | variable CONVERT_FROM may be set to |
|                                      | one of BEIEEE, LEIEEE, VAX or        |
|                                      | CONVEXNATIVE to avoid reading the    |
|                                      | machine stamp and assume the file is |
|                                      | from the stipulated archictecture    |
|                                      | for all input                        |
|                                      | `MTZ <structMTZ.html>`__ and map     |
|                                      | files for which qrarch is called.    |
|                                      |                                      |
|                                      | N.B.: leaves the stream positioned   |
|                                      | just after the machine stamp.        |
+--------------------------------------+--------------------------------------+

FORTRAN_SUBR

( 

COPEN 

 ,

copen 

 ,

(int *iunit, fpstr filename, int *istat, int filename_len) 

 ,

(int *iunit, fpstr filename, int *istat) 

 ,

(int *iunit, fpstr filename, int filename_len, int *istat) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Opens filename on io stream iunit.   |
|                                      | istat corresponds to the open mode.  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ----------------------------+        |
|                                      |     | *iunit*       | iochannel numb |
|                                      | er                          |        |
|                                      |     +---------------+--------------- |
|                                      | ----------------------------+        |
|                                      |     | *filename*    | fortran charac |
|                                      | ter array giving filename   |        |
|                                      |     +---------------+--------------- |
|                                      | ----------------------------+        |
|                                      |     | *istat*       | file mode      |
|                                      |                             |        |
|                                      |     +---------------+--------------- |
|                                      | ----------------------------+        |
+--------------------------------------+--------------------------------------+
