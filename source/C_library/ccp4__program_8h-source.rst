`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4_program.h
===============

`Go to the documentation of this file. <ccp4__program_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_program.h: Headers to utilies to set and fetch program information.
    00003      Copyright (C) 2001  CCLRC, Peter Briggs
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00011 
    00017 /*------------------------------------------------------------------*/
    00018 
    00019 /* Macro definitions */
    00020 
    00021 /*------------------------------------------------------------------*/
    00022 
    00023 #ifndef __CCP4Program__
    00024 #define __CCP4Program__
    00025 
    00026 static char rcsidhp[] = "$Id$";
    00027 
    00028 #ifdef  __cplusplus
    00029 namespace CCP4 {
    00030 extern "C" {
    00031 #endif
    00032 
    00033 #define CCP4_VERSION_NO "5.99"
    00034 #define CCP4_PATCH_LEVEL "5.99.5"
    00035 
    00036 /* Maximum lengths of strings holding program names and versions */
    00037 #define MAXLEN_PROGNAME    80
    00038 #define MAXLEN_PROGVERSION 80
    00039 #define MAXLEN_RCSDATE     80
    00040 
    00041 /*------------------------------------------------------------------*/
    00042 
    00043 /* Type Definitions */
    00044 
    00045 /*------------------------------------------------------------------*/
    00046 
    00047 /* Define a type which is a pointer to a function taking an integer
    00048    and a pointer to character, and returning an integer */
    00049 typedef int ( *CCP4INTFUNCPTR)(int, char * );
    00050 
    00051 /*------------------------------------------------------------------*/
    00052 
    00053 /* Function Prototypes */
    00054 
    00055 /*------------------------------------------------------------------*/
    00056 
    00061 char *ccp4_prog_vers(char *progvers);
    00062 
    00067 char *ccp4ProgramName(const char *progname);
    00068 
    00073 char *ccp4RCSDate(const char *rcs_string);
    00074 
    00078 void ccp4ProgramTime(int init);
    00079 
    00087 int ccp4VerbosityLevel(int level);
    00088 
    00101 int ccp4Callback(CCP4INTFUNCPTR mycallback, char *mode, int ierr, char *message);
    00102 
    00110 int ccp4SetCallback(CCP4INTFUNCPTR mycallback);
    00111 
    00119 int ccp4InvokeCallback(int ierr, char *message);
    00120 
    00127 int ccp4NullCallback(int level, char *message);
    00128 
    00133 int ccp4_licence_exists(const char *name);
    00134 
    00139 int html_log_output(int ihtml_in);
    00140 
    00145 int summary_output(int isumm_in);
    00146 
    00147 #ifdef __cplusplus
    00148 } 
    00149 } 
    00150 #endif
    00151 
    00152 #endif   /* __CCP4Program__ */

.. raw:: html

   </div>
