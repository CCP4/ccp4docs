`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmaplib.h File Reference
========================

ccp4 map i/o user-level library header file. `More... <#_details>`__

| ``#include "ccp4_utils.h"``

`Go to the source code of this file. <cmaplib_8h-source.html>`__

| 

Compounds
---------

struct  

**_CMMFile**

struct  

**_CMMFile_Data**

struct  

**_CMMFile_Labels**

struct  

**_CMMFile_Skew**

struct  

**_CMMFile_Stats**

struct  

**_CMMFile_Symop**

| 

Typedefs
--------

 typedef _CMMFile_Skew 

**CMMFile_Skew**

 typedef _CMMFile_Labels 

**CMMFile_Labels**

 typedef _CMMFile_Symop 

**CMMFile_Symop**

 typedef _CMMFile_Data 

**CMMFile_Data**

 typedef _CMMFile_Stats 

**CMMFile_Stats**

 typedef _CMMFile 

**CMMFile**

| 

Functions
---------

void * 

`ccp4_cmap_open <cmaplib_8h.html#a6>`__ (const char *filename, int
mode)

void 

`ccp4_cmap_close <cmaplib_8h.html#a7>`__ (CMMFile *mfile)

void 

`ccp4_cmap_closemode <cmaplib_8h.html#a8>`__ (CMMFile *mfile,
unsigned int closemode)

int 

`ccp4_cmap_seek_section <cmaplib_8h.html#a9>`__ (CMMFile *mfile, int
offset, unsigned int seek_mode)

int 

`ccp4_cmap_seek_row <cmaplib_8h.html#a10>`__ (CMMFile *, int offset,
unsigned int seek_mode)

int 

`ccp4_cmap_seek_data <cmaplib_8h.html#a11>`__ (CMMFile *, int
offset, unsigned int seek_mode)

int 

`ccp4_cmap_read_section <cmaplib_8h.html#a12>`__ (CMMFile *mfile,
void *section)

int 

`ccp4_cmap_read_row <cmaplib_8h.html#a13>`__ (CMMFile *mfile, void
*row)

int 

`ccp4_cmap_read_data <cmaplib_8h.html#a14>`__ (const CMMFile *mfile,
void *items, int n_items)

int 

`ccp4_cmap_write_section <cmaplib_8h.html#a15>`__ (CMMFile *mfile,
const void *section)

int 

`ccp4_cmap_write_row <cmaplib_8h.html#a16>`__ (CMMFile *mfile, const
void *row)

int 

`ccp4_cmap_write_data <cmaplib_8h.html#a17>`__ (CMMFile *mfile,
const void *items, int n_items)

int 

`ccp4_cmap_read_section_header <cmaplib_8h.html#a18>`__ (const
CMMFile *mfile, char *header)

int 

`ccp4_cmap_write_section_header <cmaplib_8h.html#a19>`__ (CMMFile
*mfile, const char *header)

void 

`ccp4_cmap_get_cell <cmaplib_8h.html#a20>`__ (const CMMFile *mfile,
float *cell)

void 

`ccp4_cmap_get_grid <cmaplib_8h.html#a21>`__ (const CMMFile *mfile,
int *grid)

void 

`ccp4_cmap_get_origin <cmaplib_8h.html#a22>`__ (const CMMFile
*mfile, int *origin)

void 

`ccp4_cmap_get_order <cmaplib_8h.html#a23>`__ (const CMMFile *mfile,
int *axes_order)

void 

`ccp4_cmap_get_dim <cmaplib_8h.html#a24>`__ (const CMMFile *mfile,
int *map_dim)

int 

`ccp4_cmap_get_spacegroup <cmaplib_8h.html#a25>`__ (const CMMFile
*mfile)

void 

`ccp4_cmap_get_mapstats <cmaplib_8h.html#a26>`__ (const CMMFile
*mfile, float *min, float *max, double *mean, double *rms)

void 

`ccp4_cmap_set_cell <cmaplib_8h.html#a27>`__ (CMMFile *mfile, const
float *cell)

void 

`ccp4_cmap_set_grid <cmaplib_8h.html#a28>`__ (CMMFile *mfile, const
int *grid)

void 

`ccp4_cmap_set_origin <cmaplib_8h.html#a29>`__ (CMMFile *mfile,
const int *origin)

void 

`ccp4_cmap_set_order <cmaplib_8h.html#a30>`__ (CMMFile *mfile, const
int *axes_order)

void 

`ccp4_cmap_set_dim <cmaplib_8h.html#a31>`__ (CMMFile *mfile, const
int *map_dim)

void 

`ccp4_cmap_set_spacegroup <cmaplib_8h.html#a32>`__ (CMMFile *mfile,
int spacegroup)

void 

`ccp4_cmap_set_mapstats <cmaplib_8h.html#a33>`__ (CMMFile *mfile,
const float min, const float max, const double mean, const double rms)

unsigned int 

`ccp4_cmap_get_datamode <cmaplib_8h.html#a34>`__ (const CMMFile
*mfile)

void 

`ccp4_cmap_set_datamode <cmaplib_8h.html#a35>`__ (CMMFile *mfile,
unsigned int datamode)

size_t 

`ccp4_cmap_get_local_header <cmaplib_8h.html#a36>`__ (CMMFile
*mfile)

void 

`ccp4_cmap_set_local_header <cmaplib_8h.html#a37>`__ (CMMFile
*mfile, size_t size)

int 

`ccp4_cmap_num_symop <cmaplib_8h.html#a38>`__ (const CMMFile *mfile)

int 

`ccp4_cmap_seek_symop <cmaplib_8h.html#a39>`__ (CMMFile *mfile, int
isymop, unsigned int whence)

int 

`ccp4_cmap_get_symop <cmaplib_8h.html#a40>`__ (CMMFile *mfile, char
*buffer)

int 

`ccp4_cmap_set_symop <cmaplib_8h.html#a41>`__ (CMMFile *mfile, const
char *buffer)

int 

`ccp4_cmap_get_mask <cmaplib_8h.html#a42>`__ (const CMMFile *mfile,
float *skew_mat, float *skew_trans)

int 

`ccp4_cmap_set_mask <cmaplib_8h.html#a43>`__ (CMMFile *mfile, const
float *skew_mat, const float *skew_trans)

int 

`ccp4_cmap_number_label <cmaplib_8h.html#a44>`__ (const CMMFile
*mfile)

int 

`ccp4_cmap_set_label <cmaplib_8h.html#a45>`__ (CMMFile *mfile, const
char *label, int posn)

char * 

`ccp4_cmap_get_label <cmaplib_8h.html#a46>`__ (const CMMFile *mfile,
int posn)

int 

`ccp4_cmap_set_title <cmaplib_8h.html#a47>`__ (CMMFile *mfile, const
char *label)

char * 

`ccp4_cmap_get_title <cmaplib_8h.html#a48>`__ (const CMMFile *mfile)

--------------

Detailed Description
--------------------

ccp4 map i/o user-level library header file.

Functions defining the C-level API for accessing CCP4 map files.

 **Author:**
    Charles Ballard

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +--------------------------+------+---------------+-------------+------+ |
| ----+                                                                    |
| | void ccp4_cmap_close   | (    | CMMFile *    |   *mfile*   | )    | |
|     |                                                                    |
| +--------------------------+------+---------------+-------------+------+ |
| ----+                                                                    |
+--------------------------------------------------------------------------+

+-----+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Close the file. In write mode the header is output, along with the machine stamp. In read mode the file is just closed. Write mode supports ways of updating the map statistics ( only active for FLOAT32). /param mfile (CMMFile * ) /return void   |
+-----+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

void ccp4_cmap_closemode

( 

CMMFile * 

  *mfile*,

unsigned int 

  *closemode*

) 

+-----+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     | Set the close mode: 0: calculate based on stored values (default) 1: just dump the current values /param mfile (CMMFile * ) /param mask (unsigned int) close mode /return void   |
+-----+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

void ccp4_cmap_get_cell

( 

const CMMFile * 

  *mfile*,

float * 

  *cell*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the cell parameters              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )                                    |
|                                      |     |                                |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *cell*     | (float * ) contai |
|                                      | ns the cell parameter on exit (dim 6 |
|                                      | )   |                                |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------+------+--------------------- |
| +-------------+------+----+                                              |
| | unsigned int ccp4_cmap_get_datamode   | (    | const CMMFile *     |
| |   *mfile*   | )    |    |                                              |
| +------------------------------------------+------+--------------------- |
| +-------------+------+----+                                              |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the datamode                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )   |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     datamode                         |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_get_dim

( 

const CMMFile * 

  *mfile*,

int * 

  *map_dim*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the stored map dimension         |
|                                      | (rows,sections,columns)              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*       | (const CMMFile |
|                                      |  * )                                 |
|                                      |     |                                |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *map_dim*    | (int * ) conta |
|                                      | ins the map dimension on exit (dim 3 |
|                                      | )   |                                |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_get_grid

( 

const CMMFile * 

  *mfile*,

int * 

  *grid*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the grid for the complete cell   |
|                                      | (X,Y,Z) ordering                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )                                    |
|                                      |   |                                  |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
|                                      |     | *grid*     | (int * ) contains |
|                                      |  the grid dimension on exit (dim 3)  |
|                                      |   |                                  |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
+--------------------------------------+--------------------------------------+

char* ccp4_cmap_get_label

( 

const CMMFile * 

  *mfile*,

int 

  *posn*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the label corresponding to       |
|                                      | position posn                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------+                        |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )           |                        |
|                                      |     +------------+------------------ |
|                                      | ------------+                        |
|                                      |     | *posn*     | (int) desired lab |
|                                      | el number   |                        |
|                                      |     +------------+------------------ |
|                                      | ------------+                        |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to label posn            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------------------+------+---------------+----- |
| --------+------+----+                                                    |
| | size_t ccp4_cmap_get_local_header   | (    | CMMFile *    |   *m |
| file*   | )    |    |                                                    |
| +------------------------------------------+------+---------------+----- |
| --------+------+----+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the local header size         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----------------+  |
|                                      |     | *mfile*    | (CMMFile * )   |  |
|                                      |     +------------+----------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     header size associated with each |
|                                      |     section (in bytes)               |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_get_mapstats

( 

const CMMFile * 

  *mfile*,

float * 

  *min*,

float * 

  *max*,

double * 

  *mean*,

double * 

  *rms*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the map statistics, including    |
|                                      | maximum, minimum, mean and standard  |
|                                      | deviation. This is only meaningful   |
|                                      | for datamode FLOAT32.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )   |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *min*      | (float * )        |
|                                      |     |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *max*      | (float * )        |
|                                      |     |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *mean*     | (double * )       |
|                                      |     |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *rms*      | (double * )       |
|                                      |     |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_get_mask

( 

const CMMFile * 

  *mfile*,

float * 

  *skew_mat*,

float * 

  *skew_trans*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the values of the translation    |
|                                      | and rotation elements of the skew    |
|                                      | matrix. Note: the stored file is in  |
|                                      | FORTRAN order mat[fastest][slowest], |
|                                      | the returned values are in C         |
|                                      | mat[slowest][fastest] ordering       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *mfile*          | (CMMFile * |
|                                      | )                                    |
|                                      |           |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *skew_mat*      | (const floa |
|                                      | t * ) the skew translation vestor    |
|                                      |           |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *skew_trans*    | (const floa |
|                                      | t * ) the skew rotation matrix (C or |
|                                      | dering)   |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if mask is set                 |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_get_order

( 

const CMMFile * 

  *mfile*,

int * 

  *axes_order*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the stored map axes order        |
|                                      | (rows,sections,columns) where 1=X,   |
|                                      | 2=Y, 3=Z                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*          | (const CMMF |
|                                      | ile * )                              |
|                                      |     |                                |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *axes_order*    | (float * )  |
|                                      | contains the ordering on exit (dim 3 |
|                                      | )   |                                |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----+                                |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_get_origin

( 

const CMMFile * 

  *mfile*,

int * 

  *origin*

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the stored map origin            |
|                                      | (rows,sections,columns)              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -------------------------------+     |
|                                      |     | *mfile*     | (const CMMFile |
|                                      | * )                             |     |
|                                      |     +-------------+----------------- |
|                                      | -------------------------------+     |
|                                      |     | *origin*    | (int * ) contain |
|                                      | s the origin on exit (dim 3)   |     |
|                                      |     +-------------+----------------- |
|                                      | -------------------------------+     |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-----------------------------------+------+---------------------+------ |
| -------+------+----+                                                     |
| | int ccp4_cmap_get_spacegroup   | (    | const CMMFile *    |   *mf |
| ile*   | )    |    |                                                     |
| +-----------------------------------+------+---------------------+------ |
| -------+------+----+                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the spacegroup listed in the  |
|                                      | map header. This is overriden by the |
|                                      | symops.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----------------+  |
|                                      |     | *mfile*    | (CMMFile * )   |  |
|                                      |     +------------+----------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     spacegroup number                |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_get_symop

( 

CMMFile * 

  *mfile*,

char * 

  *buffer*

) 

+--------------------------------------+--------------------------------------+
|                                      | get a symop string of 80 characters  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------+                |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                     |                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------+                |
|                                      |     | *buffer*    | (char * ) array  |
|                                      | of bytes which will contain the symo |
|                                      | p string. This must be at least 81 c |
|                                      | haracters long (including space for  |
|                                      | null terminator).   |                |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------+                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 if no symops,    |
|                                      |     EOF on failure                   |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+---------------------+-------- |
| -----+------+----+                                                       |
| | char* ccp4_cmap_get_title   | (    | const CMMFile *    |   *mfil |
| e*   | )    |    |                                                       |
| +---------------------------------+------+---------------------+-------- |
| -----+------+----+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Get the label corresponding to the   |
|                                      | title wrapping                       |
|                                      | ccp4_cmap_get_label.              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )   |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     pointer to label 0, or NULL      |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+---------------------+----------- |
| --+------+----+                                                          |
| | int ccp4_cmap_num_symop   | (    | const CMMFile *    |   *mfile*  |
|   | )    |    |                                                          |
| +------------------------------+------+---------------------+----------- |
| --+------+----+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the number of symops          |
|                                      | (estimated as the size/80)           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*    | (const CMMFile * |
|                                      | )   |                                |
|                                      |     +------------+------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of symops                 |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +---------------------------------+------+---------------------+-------- |
| -----+------+----+                                                       |
| | int ccp4_cmap_number_label   | (    | const CMMFile *    |   *mfil |
| e*   | )    |    |                                                       |
| +---------------------------------+------+---------------------+-------- |
| -----+------+----+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Return the number of labels.         |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----------------+  |
|                                      |     | *mfile*    | (CMMFile * )   |  |
|                                      |     +------------+----------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     the number of labels             |
+--------------------------------------+--------------------------------------+

void* ccp4_cmap_open

( 

const char * 

  *filename*,

int 

  *mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | The file is opened.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
|                                      |     | *filename*    | (char * ) the  |
|                                      | filename                             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |   |                                  |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
|                                      |     | *mode*        | (int) the i/o  |
|                                      | mode , possible values are O_RDONLY |
|                                      | , O_WRONLY, O_RDWR, O_APPEND, O_ |
|                                      | TMP, O_CREAT, O_TRUNC - see `ccp4|
|                                      | _sysdep.h <ccp4__sysdep_8h.html>`__  |
|                                      |   |                                  |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --+                                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     (void * ) CMMFile structure      |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_read_data

( 

const CMMFile * 

  *mfile*,

void * 

  *items*,

int 

  *n_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | raw read of nelements items from     |
|                                      | file according to the datamode at    |
|                                      | current location                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *mfile*       | (const CMMFile |
|                                      |  * )                                 |
|                                      |                                |     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *items*       | (void * ) valu |
|                                      | es read to here, so should have enou |
|                                      | gh space for nelements items   |     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |     | *n_items*    | (int) number o |
|                                      | f items to be read                   |
|                                      |                                |     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | -------------------------------+     |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items read or EOF      |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_read_row

( 

CMMFile * 

  *mfile*,

void * 

  *row*

) 

+--------------------------------------+--------------------------------------+
|                                      | read current map section from file   |
|                                      | to section. Some checking is         |
|                                      | performed to ensure we are at the    |
|                                      | start of a legitimate map row.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                                   |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |     | *row*      | (void * ) array l |
|                                      | arge enough to hold the map row   |  |
|                                      |     +------------+------------------ |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_read_section

( 

CMMFile * 

  *mfile*,

void * 

  *section*

) 

+--------------------------------------+--------------------------------------+
|                                      | read current map section from file   |
|                                      | to section. Some checking is         |
|                                      | performed to ensure we are at the    |
|                                      | start of a legitimate map section.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *mfile*      | (CMMFile * )    |
|                                      |                                      |
|                                      |     |                                |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |     | *section*    | (void * ) array |
|                                      |  large enough to hold the map sectio |
|                                      | n   |                                |
|                                      |     +--------------+---------------- |
|                                      | ------------------------------------ |
|                                      | ----+                                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_read_section_header

( 

const CMMFile * 

  *mfile*,

char * 

  *header*

) 

+--------------------------------------+--------------------------------------+
|                                      | read current section header          |
|                                      | (character array) After reading we   |
|                                      | are at the end of the local header   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                      |
|                                      |                                      |
|                                      |    |                                 |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |     | *header*    | (char * ) charac |
|                                      | ter array large enough to hold the l |
|                                      | ocal header (raw read so not string) |
|                                      |    |                                 |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_seek_data

( 

CMMFile * 

  *mfile*,

int 

  *offset*,

unsigned int 

  *seek_mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | raw seek in items                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------------------+  |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                   |  |
|                                      |     +-------------+----------------- |
|                                      | ----------------------------------+  |
|                                      |     | *offset*    | (int) number of  |
|                                      | items                             |  |
|                                      |     +-------------+----------------- |
|                                      | ----------------------------------+  |
|                                      |     | *whence*    | (unsigned int) S |
|                                      | EEK_SET, SEEK_CUR, SEEK_END;   |  |
|                                      |     +-------------+----------------- |
|                                      | ----------------------------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_seek_row

( 

CMMFile * 

  *mfile*,

int 

  *offset*,

unsigned int 

  *seek_mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | seek a row within a map section      |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------------+   |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                  |   |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------------+   |
|                                      |     | *row*       | (int)            |
|                                      |                                  |   |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------------+   |
|                                      |     | *whence*    | (unsigned int) S |
|                                      | EEK_SET, SEEK_END, SEEK_CUR   |   |
|                                      |     +-------------+----------------- |
|                                      | ---------------------------------+   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     offset in file or EOF            |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_seek_section

( 

CMMFile * 

  *mfile*,

int 

  *offset*,

unsigned int 

  *seek_mode*

) 

+--------------------------------------+--------------------------------------+
|                                      | seek among the map sections. The     |
|                                      | units are of size block_size.       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                    | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *sec*       | (int) section nu |
|                                      | mber                               | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *whence*    | (unsigned int) S |
|                                      | EEK_SET, SEEK_CUR or SEEK_END   | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     offset in file, or EOF           |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_seek_symop

( 

CMMFile * 

  *mfile*,

int 

  *isymop*,

unsigned int 

  *whence*

) 

+--------------------------------------+--------------------------------------+
|                                      | navigate around the symops, seeking  |
|                                      | in 80 byte units The result must lie |
|                                      | within the symop strings in the      |
|                                      | file.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                      |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *isymop*    | (int) the number |
|                                      |  of the symop "string" of interest   |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *whence*    | (unsigned int) m |
|                                      | ode of seek                          |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     symop string number or EOF       |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_cell

( 

CMMFile * 

  *mfile*,

const float * 

  *cell*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the cell parameters. Only        |
|                                      | allowed when file is opened in write |
|                                      | mode.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                       |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
|                                      |     | *cell*     | (const float * )  |
|                                      | the cell parameters   |              |
|                                      |     +------------+------------------ |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_datamode

( 

CMMFile * 

  *mfile*,

unsigned int 

  *datamode*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the datamode. This is only       |
|                                      | allowed if the file is opened in     |
|                                      | write mode, and no data has been     |
|                                      | written.                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
|                                      |     | *mfile*       | (CMMFile * )   |
|                                      |                      |               |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
|                                      |     | *datamode*    | (unsigned int) |
|                                      |  major mode of map   |               |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_dim

( 

CMMFile * 

  *mfile*,

const int * 

  *map_dim*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the stored map dimension         |
|                                      | (rows,sections,columns) Only allowed |
|                                      | when file is opened in write mode    |
|                                      | before any data is written. Note:    |
|                                      | the row dimension will be overridden |
|                                      | during writing                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
|                                      |     | *mfile*       | (CMMFile * )   |
|                                      |                      |               |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
|                                      |     | *map_dim*    | (const int * ) |
|                                      |  the map dimension   |               |
|                                      |     +---------------+--------------- |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_grid

( 

CMMFile * 

  *mfile*,

const int * 

  *grid*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the cell grid dimension. Only    |
|                                      | allowed when file is opened in write |
|                                      | mode.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | --------------------------------+    |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                                 |    |
|                                      |     +------------+------------------ |
|                                      | --------------------------------+    |
|                                      |     | *grid*     | (const int * ) th |
|                                      | e cell grid dimension (X,Y,Z)   |    |
|                                      |     +------------+------------------ |
|                                      | --------------------------------+    |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_set_label

( 

CMMFile * 

  *mfile*,

const char * 

  *label*,

int 

  *posn*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the label in the map header.     |
|                                      | Headers are 80 characters long. The  |
|                                      | labels are written to the file when  |
|                                      | it is closed. Therefore, the file    |
|                                      | must be in write mode. If label ==   |
|                                      | NULL the element corresponding to    |
|                                      | posn is removed. The number of       |
|                                      | labels is recalculated on each call. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                              |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *label*    | (const char * ) t |
|                                      | he C-style character array   |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |     | *posn*     | (int) the label n |
|                                      | umber (C-style, 0 -> 9)      |       |
|                                      |     +------------+------------------ |
|                                      | -----------------------------+       |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of label effected, or EOF |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_local_header

( 

CMMFile * 

  *mfile*,

size_t 

  *size*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the local header size (in bytes) |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -----------+                         |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                                      |
|                                      |            |                         |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -----------+                         |
|                                      |     | *size*     | (size_t) header  |
|                                      | size associated with each section (i |
|                                      | n bytes)   |                         |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | -----------+                         |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_mapstats

( 

CMMFile * 

  *mfile*,

const float 

  *min*,

const float 

  *max*,

const double 

  *mean*,

const double 

  *rms*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the map statistics, including    |
|                                      | maximum, minimum, mean and standard  |
|                                      | deviation. This is only meaningful   |
|                                      | for datamode FLOAT32 and the file    |
|                                      | open in write mode.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----------------+  |
|                                      |     | *mfile*    | (CMMFile * )   |  |
|                                      |     +------------+----------------+  |
|                                      |     | *min*      | (float)        |  |
|                                      |     +------------+----------------+  |
|                                      |     | *max*      | (float)        |  |
|                                      |     +------------+----------------+  |
|                                      |     | *mean*     | (double)       |  |
|                                      |     +------------+----------------+  |
|                                      |     | *rms*      | (double)       |  |
|                                      |     +------------+----------------+  |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_set_mask

( 

CMMFile * 

  *mfile*,

const float * 

  *skew_mat*,

const float * 

  *skew_trans*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the values of the translation    |
|                                      | and rotation elements of the skew    |
|                                      | matrix. Note: the stored file is in  |
|                                      | FORTRAN order mat[fastest][slowest]  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *mfile*          | (CMMFile * |
|                                      | )                                    |
|                                      |           |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *skew_mat*      | (const floa |
|                                      | t * ) the skew translation vestor    |
|                                      |           |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |     | *skew_trans*    | (const floa |
|                                      | t * ) the skew rotation matrix (C or |
|                                      | dering)   |                          |
|                                      |     +------------------+------------ |
|                                      | ------------------------------------ |
|                                      | ----------+                          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if either skew_trans or       |
|                                      |     skew_mat is non-NULL            |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_order

( 

CMMFile * 

  *mfile*,

const int * 

  *axes_order*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the stored map axes order        |
|                                      | (rows,sections,columns) where 1=X,   |
|                                      | 2=Y, 3=Z. Only allowed when file is  |
|                                      | opened in write mode.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | --------------------------+          |
|                                      |     | *mfile*          | (CMMFile * |
|                                      | )                         |          |
|                                      |     +------------------+------------ |
|                                      | --------------------------+          |
|                                      |     | *axes_order*    | (const floa |
|                                      | t * ) the axes ordering   |          |
|                                      |     +------------------+------------ |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_origin

( 

CMMFile * 

  *mfile*,

const int * 

  *origin*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the stored map origin            |
|                                      | (rows,sections,columns) Only allowed |
|                                      | when file is opened in write mode.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *origin*    | (const int * ) t |
|                                      | he origin   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

void ccp4_cmap_set_spacegroup

( 

CMMFile * 

  *mfile*,

int 

  *spacegroup*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the spacegroup listed in the map |
|                                      | header. Only allowed when file is    |
|                                      | opened in write mode.                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------------+------------- |
|                                      | --------------+                      |
|                                      |     | *mfile*         | (CMMFile * ) |
|                                      |               |                      |
|                                      |     +-----------------+------------- |
|                                      | --------------+                      |
|                                      |     | *spacegroup*    | (int) spaceg |
|                                      | roup number   |                      |
|                                      |     +-----------------+------------- |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_set_symop

( 

CMMFile * 

  *mfile*,

const char * 

  *buffer*

) 

+--------------------------------------+--------------------------------------+
|                                      | write symops to file. This wraps a   |
|                                      | raw write. It is up to the calling   |
|                                      | program to ensure the positioning    |
|                                      | (effectively assume appends).        |
|                                      | Writing is blocked if data has       |
|                                      | alread been written to the file. 80  |
|                                      | bytes of continuous memory is        |
|                                      | written to the file.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                                      |
|                                      |                                      |
|                                      |        |                             |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |     | *symop*    | (const char * ) c |
|                                      | haracter array containing the symop  |
|                                      | string (at least 80 characters in le |
|                                      | ngth   |                             |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | -------+                             |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, EOF on failure     |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_set_title

( 

CMMFile * 

  *mfile*,

const char * 

  *label*

) 

+--------------------------------------+--------------------------------------+
|                                      | Set the label corresponding to the   |
|                                      | title, wrapping                      |
|                                      | ccp4_cmap_set_label               |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+----------------+  |
|                                      |     | *mfile*    | (CMMFile * )   |  |
|                                      |     +------------+----------------+  |
|                                      |     | *label*    |                |  |
|                                      |     +------------+----------------+  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     0 or EOF on failure              |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_write_data

( 

CMMFile * 

  *mfile*,

const void * 

  *items*,

int 

  *n_items*

) 

+--------------------------------------+--------------------------------------+
|                                      | raw write of nelements items to      |
|                                      | file, according to the datamode, at  |
|                                      | current location                     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *mfile*       | (const CMMFile |
|                                      |  * )                                 |
|                                      |                   |                  |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *section*     | (void * ) valu |
|                                      | es written, should contain at least  |
|                                      | nelements items   |                  |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |     | *n_items*    | (int) number o |
|                                      | f items to be written                |
|                                      |                   |                  |
|                                      |     +---------------+--------------- |
|                                      | ------------------------------------ |
|                                      | ------------------+                  |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of items written or EOF   |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_write_row

( 

CMMFile * 

  *mfile*,

const void * 

  *row*

) 

+--------------------------------------+--------------------------------------+
|                                      | write map row to file. Note: this    |
|                                      | wraps a raw write, with no location  |
|                                      | checking. It is therefore the        |
|                                      | responsibility of the calling        |
|                                      | program to ensure that everything is |
|                                      | correct. Effectively assume          |
|                                      | appending to file.                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | --------------------+                |
|                                      |     | *mfile*    | (CMMFile * )      |
|                                      |                     |                |
|                                      |     +------------+------------------ |
|                                      | --------------------+                |
|                                      |     | *row*      | (const void * ) d |
|                                      | ata to be written   |                |
|                                      |     +------------+------------------ |
|                                      | --------------------+                |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_write_section

( 

CMMFile * 

  *mfile*,

const void * 

  *section*

) 

+--------------------------------------+--------------------------------------+
|                                      | write map section to file. Note:     |
|                                      | this wraps a raw write, with no      |
|                                      | location checking. It is therefore   |
|                                      | the responsibility of the calling    |
|                                      | program to ensure that everything is |
|                                      | correct. Effectively assume          |
|                                      | appending to file.                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *mfile*      | (CMMFile * )    |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *section*    | (const void * ) |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 on success, 0 on failure       |
+--------------------------------------+--------------------------------------+

int ccp4_cmap_write_section_header

( 

CMMFile * 

  *mfile*,

const char * 

  *header*

) 

+--------------------------------------+--------------------------------------+
|                                      | write the local section header to    |
|                                      | the file. This must be of size       |
|                                      | mfile->data.header.size. Note: no    |
|                                      | checking is done so it is up to the  |
|                                      | calling program to ensure that the   |
|                                      | file is in the correct location. As  |
|                                      | seeking is turned off, this assumes  |
|                                      | we are appending to the file.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *mfile*     | (CMMFile * )     |
|                                      |                                      |
|                                      |                           |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *header*    | (const char * )  |
|                                      | the local header character array (no |
|                                      | t necessarily a string)   |          |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     number of bytes written or EOF   |
+--------------------------------------+--------------------------------------+
