SOLOMON (CCP4: Supported Program)
=================================

NAME
----

**solomon** - density modification (phase improvement) by solvent
flipping.

SYNOPSIS
--------

| **solomon MAPIN** *foo_in.map* [ **MAPOUT** *foo_out.map* ] [
  **RMSMAP** *foo_out2.map* ]
| `[Keyworded input] <#keywords>`__

.. _description: 
 
DESCRIPTION
---------------------------------

Solomon is a program which modifies the electron density maps by
averaging, solvent flipping and protein truncation
[`1 <#reference1>`__]. It can also remove overlapped parts of a mask
between itself and its symmetry equivalents. Please note that this
program has various non CCP4 standard features.

.. _usage: 
 
USAGE
---------------------

Several stages are required for phase refinement with Solomon. The
stages described here assume that the phase probability distributions
were determined experimentally ( *i.e.* SIR, MIR, MAD).

#. Determine phase probability distributions, described by
   Hendrickson-Lattman coefficients. This is done automatically by
   `MLPHARE <mlphare.html>`__.
#. Calculate the map to be flattened. Initially, you will have a map
   calculated from experimental phases. It is advised that you start
   from a resolution which has got significant phase information and do
   not yet extend the resolution!
#. Have a look at the map and compare it with the original FOM weighted
   map. If there is no non-crystallographic symmetry, you might get some
   additional improvement by playing around with the solvent
   multiplication factor and the truncation level (see the keywords
   `TRUNC <#trunc>`__ and `MULSOLV <#mulsolv>`__). If the crystals
   contain a lot of solvent (70 to 80%) you might try phase extension,
   but make sure the map actually improves by doing so. If you have got
   very weak phase information at higher resolution, try including it
   from the start, but keep the `RADIUS <#radius>`__ at the lower
   resolution. (If you have got reasonable phase information to 3.7 Å,
   and very weak information to 3.2 Å, use a radius of 3.7 Å, and a high
   resolution cutoff of 3.2 Å; don't extend to 3.2, but use all
   information from the first cycle).
#. Identify and refine non-crystallographic symmetry if present. The
   Uppsala programmes "O", "mama" and "imp" are ideally suited for this
   purpose. You must remove overlap between symmetry related masks. The
   mask can be either in "O" or "CCP4" format.
#. The mask will encompass a monomer. Each mask has a set of associated
   symmetry operators which describe how the density within this mask is
   related to other density within the asymmetric unit. This is a bit
   different from the way things are done in the "rave" package and has
   some advantages.
#. Include the non-crystallographic restraints in "Solomon" and run the
   script again. You might need to reduce the level of truncation a bit
   and the solvent multiplier should probably be a bit less negative.

.. _files: 
 
INPUT AND OUTPUT FILES
--------------------------------------

.. _input_files: 
 
Input
~~~~~~~~~~~~~~~~~~~~~~~~~~~

MAPIN
   The input map must cover the asymmetric unit exactly. The input map
   (CCP4 format) is associated with the logical name MAPIN.
others
   Input masks can either be in CCP4 or "O" style formats. The program
   can automatically determine which type of format it is. These masks
   are not associated with logical names but are input via the keyword
   `MSKIN <#mskin>`__; this is not a standard CCP4 procedure.
   Another possible input file is governed by the keyword
   `CSYM <#csym>`__.

.. _outputfiles: 
 
Output
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MAPOUT / RMSMAP
   MAPOUT and RMSMAP are both output maps and are in CCP4 format. They
   are not output by default, so if output is required this must be
   indicated by the keywords `MAPOUT <#mapout>`__ and
   `RMSMAP <#rmsmap>`__.
others
   Other output files are governed by the keywords
   `SLVMASK <#slvmask>`__ and `ASMASK <#asmask>`__. All output masks
   will be in CCP4 format.

Conversion between formats can be done with
`MAMA2CCP4 <mama2ccp4.html>`__, `XDLMAPMAN <xdlmapman.html>`__ or
MAPMAN.

.. _note-on-solomon-masks: 
 
Note on SOLOMON masks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two ``features'' of SOLOMON masks which users should be aware
of:

#. Although SOLOMON uses the standard CCP4 map format for its input and
   output masks, it does not use the same convention as other programs
   *e.g.* 'dm'. In SOLOMON, mask grid points are set to 0.0 for protein
   and 1.0 for solvent. If you intend to import/export maps from or to
   other CCP4 programs then use `MAPMASK <mapmask.html>`__ to change the
   mask convention.
#. Masks output by SOLOMON may suffer from artificial features, namely
   lines of set grid points along the mask edges away from the origin.
   For the time being this can be fixed by giving SOLOMON a map covering
   the whole unit cell plus the edges, and then extracting the whole
   unit cell afterwards (thus discarding the problematic sections).
   *E.g.* run MAPMASK before and after SOLOMON, using the
   `XYZLIM <mapmask.html#xyzlim>`__ keyword the first time to extend the
   mask by an extra grid point, and the second to restore the limits to
   the original values.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

Keywords can be upper or lower case and only the first four letters are
significant. Solomon echoes keywords as it reads them, allowing you to
check the input. Keywords are only recognised if they occur at the start
of a line, but they can be preceded by spaces or tabs. Keywords which
are not found in the input are flagged to say that defaults will be
used, misspelt keywords are ignored but not flagged. After reading the
keyword and its specifier(s), the rest of the line is treated as a
comment and ignored. Lines which do not start with a keyword are treated
as comments too. In most cases, the order of the keywords is irrelevant,
and you will probably find that Solomon does not read them in the order
you specified anyway. However, there are a few exceptions (see
`MSKIN <#mskin>`__)!

Available keywords are:

   `ASMASK <#asmask>`__, `CSYM <#csym>`__, `EXTRUDE <#extrude>`__,
   `MAPOUT <#mapout>`__, `MSKIN <#mskin>`__, `MSKOUT <#mskout>`__,
   `MULSOLV <#mulsolv>`__, `PTOS <#ptos>`__, `RADIUS <#radius>`__,
   `RMSMAP <#rmsmap>`__, `SLVDENS <#slvdens>`__, `SLVFRC <#slvfrc>`__,
   `SLVMASK <#slvmask>`__, `TRANS <#trans>`__, `TRUNC <#trunc>`__,
   `VERBOSE <#verbose>`__

.. _verbose: 
 
**VERBOSE**

| (default: not verbose)
| This option produces more information in the log file.

.. _mapout: 
 
**MAPOUT**

| (default: no mapout)
| This keyword only has a meaning when Solomon is modifying the electron
  density. The modified map will be written to the file associated with
  the logical name MAPOUT.

.. _slvfrc: 
 
**SLVFRC** <value>

| (default: 0)
| Defines the fraction of the map to be treated as solvent. The protein
  mask is assumed to be a connected volume without small islands.
  Sometimes (especially during the first cycles) a larger fraction of
  the map than actually specified will be treated as solvent. This is
  because small islands with a high local standard variation ( *i.e.*
  look like a protein region) are included in the solvent region.

.. _radius: 
 
**RADIUS** <value>

| (default: 3.5)
| The solvent region is determined from the local standard deviation of
  the map within a sphere of a radius specified in Angstroms. The
  standard deviation is calculated relative to the mean solvent density,
  which by default is assumed to be the same as the whole map, but which
  can be changed with the keyword `SLVDENS <#slvdens>`__. Solomon also
  produces a histogram of the local standard deviation allowing you to
  make a judgement of the separation between solvent and protein. This
  histogram is printed if you specify `VERBOSE <#verbose>`__. The mask
  can be inspected with the `RMSMAP <#rmsmap>`__ or the
  `SLVMASK <#slvmask>`__ commands. Tests suggest that the optimum size
  of the radius is about the resolution limit to which significant phase
  information is available. Do not set it too high! The algorithm for
  determining the solvent mask is different from the Wang-algorithm,
  which needs larger radii!

.. _rmsmap: 
 
**RMSMAP**

| (default: not output)
| A map containing the local standard deviation at each grid point is
  written out (associated logical name is RMSMAP). This map can be
  manipulated with CCP4 programs and inspected within "O". Solomon will
  inform you about the appropriate contour level to choose to see the
  solvent boundary.

.. _slvmask: 
 
**SLVMASK** <filename>

After determining the solvent mask, it will be written to disk as
specified. See `INPUT AND OUTPUT <#files>`__ section for information on
mask format.

.. _slvdens: 
 
**SLVDENS** <value>

| (default: 0)
| The mean density of the solvent after subtracting the F000 term. The
  solvent mask is determined from the local standard deviation from the
  mean density of the solvent, which by default is assumed to be the
  same as the whole map. However, inclusion of (possibly reconstructed)
  very low resolution terms can change the mean solvent density
  significantly. This keyword allows one to correct for this.

.. _mulsolv: 
 
**MULSOLV** [AUTO] <value>

| (default: -1.00 with AUTO or -0.75 without)
| Defines the multiplier used to modify the solvent density with respect
  to its mean, the flipping factor. For conventional solvent flattening,
  set this value to 0. For solvent flipping, set this value to -1. Tests
  on crystals with a solvent content of around 50% suggest that there is
  a clear relationship between the optimal flipping factor and the level
  of truncation. Therefore it is advisable to use the "AUTO" option,
  which scales the solvent multiplier by the ratio between the variance
  of the protein after and before truncation.

Tests also indicate that when the solvent content is higher than 50%,
the solvent multiplier needs to be made less negative than with lower
solvent content. In a test case with significant data to 3 Å and a
solvent content of 33%, the best results were obtained with "MULSOLV
AUTO -1.6".

.. _trunc: 
 
**TRUNC** <minvalue> <maxvalue>

| (default: 0.35 1)
| Defines truncation levels.

::

           trunc 0.05 0.95

will truncate the lowest and highest 5 percent of the protein area of
the map. Tests strongly suggest that by truncating the lower 30 to 40%
of the protein, major improvements of the phases can be obtained. It is
likely that there is a relationship between the optimum level of
truncation and the maximum resolution of the data. In test cases of
crystals diffraction beyond 3 Å, the optimum lies between 30 and 40%.

.. _ptos: 
 
**PTOS** <value>

| (default: 1.31)
| Ratio between the mean protein density and the mean solvent density.
  This is used in conjunction with `EXTRUDE <#extrude>`__.

.. _extrude: 
 
**EXTRUDE**

| (default: not extrude)
| By specifying "EXTRUDE", very low resolution structure factors will be
  reconstructed. If low resolution terms are missing or are not well
  phased, the mean protein density will be almost equal to the mean
  solvent density. If the density modification procedure is used to also
  reconstruct missing terms (by using the program
  `SIGMAA <sigmaa.html>`__ or `FFT <fft.html>`__ to substitute Fc), a
  constant value (which can be negative) will be added to the grid
  points in the solvent, adjusting the mean ratio between protein and
  solvent to be as given by `PTOS <#ptos>`__. It is assumed that the
  lowest density found within the protein region is zero after adding in
  an F000 term. The main function of this keyword is to prevent the
  inflation of the mean protein density during iterative density
  modification caused by truncating protein density on every cycle.

.. _csym: 
 
**CSYM** <filename>

| (default: operators from mapfile)
| "O"-style crystallographic symmetry operators. If this keyword is
  omitted, the symmetry operators from the input map are used.

.. _asmask: 
 
**ASMASK** <filename>

| (default: none)
| This keyword only has a meaning when Solomon is removing overlap and
  generating new masks. If this keyword is given, a mask will be written
  to disk which defines the area of the asymmetric unit covered by all
  the other specified masks. See `INPUT AND OUTPUT <files>`__ section
  for information on mask format.

.. _mskin: 
 
**MSKIN** <filename>

.. _trans: 
 
**TRANS** <filename>

.. _mskout: 
 
[MSKOUT <filename>]

A "mskin" keyword, defining the filename of an "O"-style mask or a CCP4
style mask. The "O" masks are similar in format and are ASCII files.
This keyword is followed by the transformations of the mask producing
the symmetry related copies ("trans" keyword). The transformation
matrices are read from an ASCII file (see `example
below <#example-transformation>`__) and are in the 'O' style. However,
Solomon can read these files with or without the normal 'O' data block
header. Like in 'dm' the translation part is in Angstrom units. Do not
forget to provide the unit symmetry operator too!!

If a "trans" keyword is followed by the "mskout" keyword, averaging will
not be undertaken, but instead the masks will be checked for overlap,
and where overlap occurs, this will be removed by dividing the common
volume between the overlapping masks. The trimmed masks will be written
to disk as specified by the filename following the "mskout" keyword.
CCP4 masks will not need to be trimmed if generated from
`MAPMASK <mapmask.html>`__ or `NCSMASK <ncsmask.html>`__. The output
mask will have the same format as the input mask.

.. _example-transformation: 
 
An example:

::

           
           mskin r1.msk
           trans unit.ncs
           mskout r1trim.msk
           trans r1tor3.msk
           mskout r3trim.msk

No averaging will be undertaken (at least one "mskout" keyword was
present in the command file).

Any overlap between the mask and any of its symmetry related copies
(either crystallographic, non-crystallographic, or a combination of
both) will be removed, and the resulting symmetry related masks will be
written to disk with the specified filenames.

In addition, "O"-style non-crystallographic symmetry operators will be
written to disk. Their filenames are constructed by replacing the
extension of the new mask by "ncs%d", where "%d" is an integer,
indicating which symmetry is described. The integer indicates the mask
which will be generated by the symmetry operation:

::

           
           r1trim.ncs0 : unity operator
           r1trim.ncs1 : generates r3trim.msk from r1trim.msk
           r3trim.ncs0 : generates r1trim.msk from r3trim.msk
           r3trim.ncs1 : unity operator

Another example:

::

           
           mskin r1trim.msk
           trans r1trim.ncs0
           trans r1trim.ncs1
           mskin r3trim.msk
           trans r3trim.ncs0
           trans r3trim.ncs1

The density in "r1trim.msk" will be averaged with the density as defined
by the symmetry operators "r1trim.ncs0" and "r1trim.ncs1". Likewise, the
density within "r3trim.msk" will be averaged. The maps produced by
averaging contain just the density local to the specified maps, and are
on the same scale. Their filenames are constructed by replacing the
extension ".msk" by ".map". So, the files "r1trim.map" and "r3trim.map"
are produced.

.. _references: 
 
REFERENCES
-------------------------------

.. _reference1: 
 
Abrahams J. P. and Leslie A. G. W., *Acta
   Cryst.* **D52**, 30-42 (1996)

AUTHOR
------

Jan-Pieter Abrahams, MRC Laboratory of Molecular Biology, Cambridge. I/O
subroutines modified by: Kevin Cowtan, University of York.

SEE ALSO
--------

`MAMA2CCP4 <mama2ccp4.html>`__, `XDLMAPMAN <xdlmapman.html>`__,
`DM <dm.html>`__
