DiffractionImage Library (CCP4: Library)
========================================

NAME
----

**diffImage** - CCP4 Diffraction Image Handling Library 

DESCRIPTION
-----------

**diffImage** contains a set of C++ classes to handle different
diffraction image format. The library is organised in a generic classe
"DiffractionImage" that internally handle all the formats by using
specific methods dedicated to each format. 

Contents of this file
---------------------

-  `General information <#general>`__
-  `Supported Image Formats <#supported>`__
-  `Using the DiffractionImage Object <#using>`__
-  `Using the DiffractionImage Tcl-Tk, Java and Python
   Modules <#usingPythonTcl>`__
-  `Classes Description <#classes>`__
-  `Examples of programs <#printheader>`__

General Information
-------------------

.. _general: 
 


The Diffraction Image was originally part of XIA-DPA automation project
and is still used by it. However, it has been decided to include this
library with the other CCP4 core libraries because of a wider possible
interest among the comunity.

Even if the library contains several different files implementing
similar methods for different types of images it has been designed so
that you only need to use a single object. This object will perform
internal checks and will load the data according to the image types.
Then accessing any information, is done in a single way on this general
object therefore hiding the unnecessary complexity of the internal
loading.

The classes comes with a Tcl, Java and Python modules available so that
the library can be used with these languages as well.

The library also contains the a PeakList object that can be populated
with the peaks found on particular images.

Supported Detectors
-------------------

.. _supported: 
 


Currently the following detector/image format are implemented in this
library:

-  ADSC
-  MAR/MARCCD
-  RIGAKU
-  CBF/MINI-CBF
-  BRUKER
-  OXFORD-DIFFRACTION

Using the Diffraction Image Object
----------------------------------

.. _using: 
 


Firstly, below is the description of the classes from the Diffraction
Image library, there are only two classes / objects availables which
make things a lot simple to use.

The are different ways for initialising a diffraction Image Object.
Which one is more appropriate mainly depends on what you want to do.

-  You can create an empty object and load an image in the object via
   the

   ::

      load

   method.

-  You can create an object giving the filename as a parameter to have
   everything already loaded (i.e. header and image).

-  You can create an object and only load the header via the

   ::

      loadHeader

   method. This is useful if you just need to access information from
   the header.

Using the Diffraction Image Tcl-Tk, Java and Python Modules
-----------------------------------------------------------

.. _usingPythonTcl: 
 


Firstly, you need to load or import the module. In tcl, this is done
with

::

          load DiffractionImage.[ext]
           

Where [ext] is so, dll, or dylib depending on your platform. In Python,
you need to import the class definition that is in the Diffraction.py
file with the command.

::

          from DiffractionImage import *
           

In Java, just as in tcl you need to import the shared library with the
command

::

          System.loadLibrary("DiffractionImaJ");
               

Note that you do not need to add the dll/so/dylib extension, nor do you
need to add the "lib" prefix that is part of the shared library
filename. N.B.: Make sure that the tcl module is not at the same place
as DiffractionImage.py file otherwise python will try to use the shared
library instead of the python file.

Next you need to create an "object" to work with. In tcl this is done
with one the following syntax

::

          set mydiffractionimage [new_DiffractionImage]
              or
           set mydiffractionimage [new_DiffractionImage filename]
           

where *mydiffractionimage* is the name of the tcl variable you want to
use as a pointer and where filename is the name of the diffraction image
file. Any function call on the diffraction image can then be done with
the following syntax

::

          set returnval [$mydiffractionimage functionname functionargs]
           

where *returnval* is the possible return value of the function,
*functionname* the name of the function and *functionargs* a list of all
its arguments.

In Python the creation of the object is done with one of the following
syntax

::

          mydiffractionimage = DiffractionImage()
              or
           mydiffractionimage = DiffractionImage(filename)
           

Any other function call is then done with the following syntax.

::

          returnval=mydiffractionimage.functionname(arg1,...,argn)
           

Where *arg1* to *argn* are the arguments of the function *functionname*
.

In Java the creation and use of function is very similar to the C++ one.

::

          mydiffractionimage = new DiffractionImage();
              or
           mydiffractionimage = new DiffractionImage(filename);
           

The functions calls are identical to the way you do them in C++. To
finish here are three examples of how to display the diffraction image
objects variables in python, java and tcl.

::

   if {$tcl_platform(platform) == "windows"} {
       load DiffractionImage.dll
   } else {
       load libDiffractionImage.so
       }

   set diff [new_DiffractionImage]
   $diff loadHeader "[lindex $argv 1]"
   set format [$diff getFormat]
   set epoch [$diff getDate]
   set exposure [$diff getExposureTime]
   set SN [$diff getSerialNo]
   set wl [$diff getWavelength]
   set BeamX [$diff getBeamX]
   set BeamY [$diff getBeamY]
   set dist [$diff getDistance]
   set width [$diff getWidth]
   set height [$diff getHeight]
   set pixX [$diff getPixelX]
   set pixY [$diff getPixelY]
   set oscS [$diff getOscStart]
   set oscE [$diff getOscEnd]
   set oscAx [$diff getOscAxis]
   set twoTheta [$diff getTwoTheta]

   puts "Image type: $format nExposure Epoch: $epoch"
   puts "Exposure Time: $exposure nDetector S/N: $SN"
   puts "Wavelength: $wl nBeam Center: ($BeamX, $BeamY)"
   puts "Distance to detector: $dist"
   puts "Image size: ($width px, $height px)"
   puts "Pixel size: ($pixX mm, $pixY mm)"
   puts "Oscillation($oscAx): $oscS -> $oscE"
   if {$twoTheta > 0} {
       puts "Two theta value: $twoTheta\n"
   } else {
       puts "Two theta value (not in header): 0.0\n"
       }

::

   from DiffractionImage import *
   import sys
   diff=DiffractionImage()
   diff.loadHeader(sys.argv[1])
   print "Image type: ",diff.getFormat()
   print "Exposure Epoch: ",diff.getDate()
   print "Exposure Time: ",diff.getExposureTime()
   print "Detector S/N: ",diff.getSerialNo()
   print "Wavelength: ",diff.getWavelength()
   print "Beam Center: (",diff.getBeamX(),", ",diff.getBeamY(),")"
   print "Distance to Detector: ",diff.getDistance()
   print "Image size : (",diff.getWidth()," px, ",diff.getHeight()," px)"
   print "Pixel size : (",diff.getPixelX()," mm, ",diff.getPixelY()," mm)"
   print "Oscillation(",diff.getOscAxis(),") : ",diff.getOscStart()," -> ",diff.getOscEnd()
   if diff.getTwoTheta() > 0 :
       print "Two theta value: ", diff.getTwoTheta()
   else :
       print "Two theta value (not in header) : 0.0"

::

   public class diffdump
       {
       public static void main(String[] args) 
           {
           System.loadLibrary("DiffractionImaJ");
           DiffractionImage diff=new DiffractionImage();
           diff.loadHeader(args[0]);
           System.out.println("Format : "+diff.getFormat());
           System.out.println("Manufacturer : "+diff.getManufacturer());
           System.out.println("Collection date: "+diff.getDate());
           System.out.println("Exposure Time: "+Float.toString(diff.getExposureTime()));
           System.out.println("Detector S/N: "+diff.getSerialNo());
           System.out.println("Wavelength: "+Float.toString(diff.getWavelength());
           System.out.println("Beam Center: ("+Float.toString(diff.getBeamX())+
                              ", "+Float.toString(diff.getBeamY())+")");
           System.out.println("Distance to Detector: "+Float.toString(diff.getDistance()));
           System.out.println("Image size : ("+Integer.toString(diff.getWidth())+
                              " px, "+Integer.toString(diff.getHeight())+" px)");
           System.out.println("Pixel size : ("+Float.toString(diff.getPixelX())+
                              " mm, "+Float.toString(diff.getPixelY())+" mm)");
           System.out.println("Oscillation ("+diff.getOscAxis()+") :"+
                              +Float.toString(diff.getOscStart())+" -> "+
                              +Float.toString(diff.getOscEnd()));
           if(diff.getTwoTheta() >= 0)
               System.out.println("Two theta value: "+Float.toString(diff.getTwoTheta()));
           else
               System.out.println("Two theta value (not in header) : 0.0");
           }
       }
                   
                   

Classes Descriptions
--------------------

.. _classes: 
 


-  DiffractionImage

   -  

   -  

   -  

   -  

   -  

   -  

   -  

   -  

   -  

   -  

   -  

-  PeakList

   -  

   -  

   -  

   -  

   -  

-  Peak is a structure having the following 7 fields,

   -  **float** x : x coordinate of the peak on the image
   -  **float** y : y coordinate of the peak on the image
   -  **float** theta : Theta? what is theta on this context? 
   -  **float** phi : phi? what is phi on this context?
   -  **float** kappa : kappa? what is kappa on this context?
   -  **float**[3] p : Array containing the reciprocal spaces
      coordinates of the Peak
   -  **float** intensity : Intensity of the Peak

Programs
--------

.. _printheader: 
 


Currently the library provides also four simple programs called
"diffdump", "printpeaks", automask and diff2jpeg. These are respectively
used to display all the "standard" information of a Diffraction Image,
printing the list of peaks found on the image, wrapping the Automask
function from the library and converting the image data to a jpeg file.
These are also simple code examples of how to use the object from the
library.

printpeaks usage:       

::

   "printpeaks [-th <intensity_threshold>] <filename>"

-th is an optional keyword to specify the intensity threshold for peak
searching.

diffdump usage:       

::

   "diffdump [-gain] <filename>"

 -gain is an optional keyword to ask for a detector gain estimation.

automask usage:       

::

   "automask [-beam <x> <y>] <filename>"

 -beam is an optional keyword to speficy the start point (in pixels
coordinates) for the algorithm.

diff2jpeg usage:       

::

   "diff2jpeg [-thumbnail] <filename>"

 -thumbnail is an optional keyword to ask for the creation of a
thumbnail version of the image in addition to the real image The
thumbnail image is created to be as near as 400x400 as possible.
