MOLREP (CCP4: Supported Program)
================================

NAME
----

**molrep** - automated program for molecular replacement

SYNOPSIS
--------

| **molrep** [**HKLIN** *in.mtz*] [**MAPIN** *EM_map.ccp4*] [**MODEL**
  *in.pdb ( or EM_mod_map.ccp4)*] [**MODEL2** *in2.pdb*] [**PATH_OUT**
  *path_out*] [**PATH_SCR** *path_scr*]
| `[Keyworded input] <#keywords>`__

.. _description: 
 
DESCRIPTION
--------------------------------------------------

Version 11.0 /22.07.2010/
~~~~~~~~~~~~~~~~~~~~~~~~~

`Features <#features>`__

`References <#references>`__

`Installation and Downloads (source code,
binaries,tutorials) <#installation>`__

`New style to use <#new_style>`__

`News and New keywords <#new_keywords>`__

`Input/output files <#input/output>`__

`What MOLREP can do <#what>`__

`How to use MOLREP <#how>`__

`Dialogue and keywords <#keywords>`__

`Command (Batch) file <#batch>`__

`MOLREP version to read MTZ file <#CCP4>`__

`Testing super_program MOLREP <#testing>`__

`Molecular replacement method. Some theory. <#theory>`__

`Input file examples. <#input_examples>`__

`Command (batch) file examples <#batch_examples>`__

`How to define NCS <#howNCS>`__

`How to redirect output and scratch files <#redirection>`__

`Convention for rotation and coord. system <#convention>`__

--------------

.. _features: 
 
FEATURES
-------------------------------------------

-  `standard molecular replacement
   method <#standard_molecular_replacement_method>`__ by
   cross rotation function (`RF <#RF>`__),
   full-symmetry translation function (`TF <#TF>`__),
   packing function (`PF <#PF>`__)
   also
   `Self Rotation Function <#self_rotation_function>`__ with PostScript
   plots
   Spherically averaged phased translation function (`SAPTF <#SAPTF>`__)
   Phased Rotation function(`PRF <#PRF>`__)
   Phased Translation function (`PTF <#PTF>`__)
   Locked cross rotation function (`LRF <#LRF>`__)
-  allows input of `a priori knowledge <#priori_knowledge>`__ of
   similarity of the model.
-  `scaling by Patterson <#scaling_by_Patterson>`__ origin peak
   `soft low resolution cut-off <#soft_low_resolution_cut-off>`__
   `anisotropic correction and scaling <#aniso_scaling>`__
   can use `modified stucture factors <#modified_structure_factors>`__
   instead of Fobs for RF
-  `rigid body refinement <#RB>`__
-  can use second fixed model
-  can check and manage `pseudo-translation <#pseudo-translation>`__
-  can use `MTZ <#CCP4>`__ file
-  `fitting two atomic models <#fitting_two_models>`__
-  can `just rotate and position <#just_rotate_and_position>`__ the
   model and compute R-factor, CC
-  `search model <#search_model>`__ in electron density or EM map
-  `multi-copy searh <#dyad_search>`__
-  can choose from symmetry-related models closest to which was found
   before
-  can `improve <#model_correction>`__ the model before to use
   model correction by sequence `alignment <#sequence_alignment>`__
-  can use `NMR <#NMR>`__ model
-  can use `EM <#EM>`__ or electron density map as model or use it
   instead of Fobs
   for searching a atomic model in EM map
-  `search model orientation <#search_orientation>`__ in electron
   density map for particular position by phased RF
-  `find HA <#find%20HA>`__ positions by MR solution
-  `heavy atom search <#heavy%20atom%20search>`__

--------------

.. _references:  
 
REFERENCES
-----------------------------------------------

   ::

            
         Author:  A.A.Vagin
                      email: alexei@ysbl.york.ac.uk

         References:     A.A.Vagin, New translation and packing functions.,
                         Newsletter on protein crystallography., Daresbury
                         Laboratory, (1989) 24, pp 117-121.

                         Alexei Vagin and Alexei Teplyakov.
                         An approach to multi-copy search in molecular replacement., 
                         Acta Cryst.D,(2000) 56, pp 1622-1624 

                         A.A.Vagin and M.N.Isupov
                         Spherically averaged phased translation function and 
                         its application to the search for molecules and fragments 
                         in electron-density maps
                         Acta Cryst.D,(2001) 57, pp 1451-1456


                  main:  A.Vagin,A.Teplyakov, MOLREP: an automated program for
                         molecular replacement.,
                         J. Appl. Cryst. (1997) 30, 1022-1025.

.. _installation:
 
INSTALLATION
-----------------------------------------------------

Copy file `molrep.tar.gz <downloads/molrep.tar.gz>`__

and uncompress it (`gunzip molrep.tar.gz')

After untaring `molrep.tar' (command: tar xvf molrep.tar) you will get
a molrep directory, with src, doc, data, molrep_check and bin
subdirectories and README file. To build the executable, go to src.

**1. setenv BLANC_FORT**
   define compiler with options,for example:
   for linux and mac:
   setenv BLANC_FORT "f90 -fno-globals -fno-automatic -O1 -w"
   for linux intel compiler:
   setenv BLANC_FORT "ifort -static -O1"
   for mac ibm compiler:
   setenv BLANC_FORT " xlf -qextname -qarch=auto -qtune=auto -qstrict
   -O3"
   else
   setenv BLANC_FORT "f90 -O1"
**2. csh molrep.setup**
   the executable (molrep) will finish up in the bin directory;
   providing the full pathname (.../molrep/bin/molrep) one can execute
   it from anywhere without having to define an environmental variable.
   CCP4 version (which can read MTZ file ) will be prepared automaticly
   if ccp4 is installed

Also you can download binaries (executable files):

| `molrep_linux.gz <downloads/molrep_linux.gz>`__
| `molrep_macintel.gz <downloads/molrep_macintel.gz>`__

Tutorials:

| `tutorial_MR <downloads/tutorial_MR.tar.gz>`__

| `tutorial_EM <downloads/tutorial_EM.tar.gz>`__

also you can use `Testing_program_MOLREP <#testing>`__ as tutorial

.. _new_style:
 
New style to use
--------------------------------------------------

::


   You can use this version as previous one:

   1. by command (batch) file
   2. interactively
   3. by ccp4i

   New style to use:

        You can use program by command string with options:
     
      molrep -f file_sf_or_map -m  model_crd_or_map
             -mx fixed model   -m2 model_2
             -po path_out      -ps path_scrath
             -s file_sequence  -s2  file_seq_for_m2
             -k file_keywords  -doc y_or_a_or_n
             -h   -i  -r
     
              h = only keyword and mtz label information, clean
              i = interactive mode
              r = rest some special files
              file_keywords = simple text file with keywords
                              (one line - one keyword)
     
          Examples:

          Without any keywords:
     
          Usual MR: RF + TF
      molrep -f file.mtz -m model.pdb
     
          Usual MR with fixed model
      molrep -f file.mtz -m model.pdb -mx mfix.pdb
     
          Usual MR with sequence and redirect output and scratch files
      molrep -f file.mtz -m model.pdb -s file_seq -po out/ -ps scr/
    
          Self rotation funtion
      molrep -f file.mtz
     
          multi-copy search, one model (DYAD M)
      molrep -f file.mtz -m model.pdb -m2 model.pdb
     
          multi-copy search, two different models (DYAD M)
      molrep -f file.mtz -m model1.pdb -m2 model2.pdb
     
          Fitting two atomic models
      molrep -m model1.pdb -mx model2.pdb
     
          Rigid body refinement
      molrep -f file.mtz  -mx model.pdb

          Get information about keywords and mtz labels, and clean
      molrep -h -f file.mtz

          If you like to play with keywords:
     
          Using keywords from file
      molrep -f fobs.pdb -m model.pdb -k file_keywords
     
          Using keywords interactivly
      molrep -f file.mtz  -m model.pdb -i "CR"
      sim .4 "CR"
      sg all "CR"
      "CR"

          Script example:
    
      --------------------------------
      molrep -f fobs.mtz -m model.pdb -i  <<stop
      fun t
      file_t tab
      stick n
      stop
      --------------------------------
    
     

.. _new_keywords:
 
News and New keywords
--------------------------------------------------------

Use "**molrep -h**" to get short manual of MOLREP.

You can stop program safely if you create in current directory or in
PATH_OUT (if option -po is used) file:

*molrep_stop_signal.xml*

(contents does not matter)

`DYAD <#dyad>`__ < M >
~~~~~~~~~~~~~~~~~~~~~~

+---+-----------------------------------------------------------------+
|   | new value **M** means "multi-monomer search", new algorithm of  |
|   | `Multi-copy search <#dyad_search>`__ . Instead to search two    |
|   | copies and then next one program constructs maximal possible    |
|   | complexes of monomers (3mres,4mers,5mers,...) and checks its by |
|   | TF.                                                             |
+---+-----------------------------------------------------------------+

`SCORE <#score>`__ < Y | N | C >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------------------+-----------------------------------+
|                                   | **N** means do not stop if        |
|                                   | *contrast* is good and do not     |
|                                   | assess solution. Scoring system   |
|                                   | is working well if expected       |
|                                   | number of models and proper       |
|                                   | symmetry of model are             |
|                                   | correct.Proper symmetry of model  |
|                                   | program defines by model Self     |
|                                   | Rotation Funtion when Cross       |
|                                   | Rotation function is computed.    |
|                                   | If you like or if you use keyword |
|                                   | **FUN = T** you can define Proper |
|                                   | symmetry of model by keyword      |
|                                   | **NCSM**.                         |
+-----------------------------------+-----------------------------------+
|                                   | **C** use Correlation Coeffitient |
|                                   | instead of *score* and do not     |
|                                   | stop.                             |
|                                   | *score* is product Correlation    |
|                                   | Coeffitient, value of Packing     |
|                                   | function and maximal value of     |
|                                   | Packing index.                    |
|                                   | *contrast* means:                 |
|                                   | >3.0 - definitly solution         |
|                                   | <3.0 and > 2.0 - solution         |
|                                   | <2.0 and > 1.5 - maybe solution   |
|                                   | <1.5 and > 1.3 - maybe not        |
|                                   | solution, but program accepts it  |
|                                   | <1.3 - probably not solution.     |
+-----------------------------------+-----------------------------------+

**NCSM** <auto>
~~~~~~~~~~~~~~~

== ==================================================
 number of subunits in the model (only for scoring)
== ==================================================

`PST <#pst>`__ <A>
~~~~~~~~~~~~~~~~~~

+---+-----------------------------------------------------------------+
|   | new value **A** means to check all pseudo-translation vectors   |
|   | automatically (default now).                                    |
+---+-----------------------------------------------------------------+

`DIFF <#diff>`__ <M>
~~~~~~~~~~~~~~~~~~~~

+---+-----------------------------------------------------------------+
|   | new value **M** means to remove fixed model from map            |
|   | (Fobs,PHobs) by mask for RF or PRF.                             |
+---+-----------------------------------------------------------------+

`SG <#sg>`__ <name>
~~~~~~~~~~~~~~~~~~~

+-----------------------------------+-----------------------------------+
|                                   | space group name. You can use     |
|                                   | this keyword instead **NOSG**     |
|                                   | **ALL** means to check all        |
|                                   | possible space groups             |
|                                   | automatically.                    |
+-----------------------------------+-----------------------------------+

`RESMIN <#resmin>`__ <auto>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

== =================================================
 soft resolution cut_off, use it instead **COMPL**
== =================================================

**SEQ** < D | Y | N >
~~~~~~~~~~~~~~~~~~~~~~~

+-------+-------------------------------------------------------------+
| **D** | default: to use identity as **SIM** and to use corrected    |
|       | model only if identity > 20%                                |
+-------+-------------------------------------------------------------+
| **N** | means to use identity as **SIM** only (without model        |
|       | correction)                                                 |
+-------+-------------------------------------------------------------+
| **Y** | means to use identity as **SIM** and to use corrected model |
+-------+-------------------------------------------------------------+

`SURF <#surf>`__ <C >
~~~~~~~~~~~~~~~~~~~~~

== ===================================================================
 as **Y** but new B only for Packing function, not change original B
== ===================================================================

**SURFX** < Y | C | N >
~~~~~~~~~~~~~~~~~~~~~~~~~

+---+-------------------------------------------------------------------------+
|   | **Y** is default. `SURF <#surf>`__ for fixed model (no shift to origin) |
+---+-------------------------------------------------------------------------+

`SELF <#self>`__ < N | A >
~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-------+-------------------------------------------------------------+
| **A** | after RF program computes Self_RF and use from RF only      |
|       | related peaks for TF.                                       |
+-------+-------------------------------------------------------------+

.. _input/output: 
 
INPUT/OUTPUT FILES
-----------------------------------------------------------

.. _input_file_formats: 
 
 Input file formats
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Format of input file of models

   A. for atomic model - PDB or CIF or BLANC.
   B. for EM or electron density map - CCP4 or BLANC.
      CCP4 file must have extension "ccp4" or "map"

#. Input file of structure factors and phases.

   A. Input formatted file of structure factors (CIF).
      This file must be CIF file which contains indices, structure
      factors (and phases if you need them): *h,k,l,|F|,sig(F),Phi* or
      *h,k,l,|F|,sig(F),Phi,Fom*
   B. Input PDB file of structure factors.
      This file contains indices and structure factors or intensities.
      (also simple formatted file with *h,k,l,|F|,sig(F)* or
      *h,k,l,|F| * and without titles is acceptable)
   C. Alternative input file of structure factors or phases is
      unformatted file of
      `BLANC <http://www.ysbl.york.ac.uk/~alexei/blanc.html>`__ suite.
   D. `MTZ <#CCP4>`__ file, which must have extension "mtz".
   E. `EM <#EM>`__ or electron density file (CCP4 file must have
      extension "ccp4" or "map").
      This file will be converted to files of !F! and phases.

You can find some examples in `Input file examples. <#Input_examples>`__

Space group and unit cell parameters of the unknown structure will be
taken from the file of structure factors. You can change the space group
of the structure factor file by using keywords `NOSG <#nosg>`__ or
**SG** .

.. _output_files: 
 
Output files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*molrep.crd*
   new coordinates of model (plus model_2) corresponding to the best
   solution of Cross Rotation and Translation Function.(BLANC format)
*molrep.pdb*
   new PDB coordinate file with molrep solution (this file will be
   created if you start from PDB file).
*molrep_fcalc.dat*
*molrep_phcalc.dat*
   !F! and phases of molrep solution (plus from model_2). These files
   will be created if your model is EM or density. (BLANC format)
*molrep_fcalc.cif*
   formatted CIFile of molrep solution (plus from model_2) with Fobs,
   Fcalc, Phcalc (this files will be created if your model is EM or
   density in CCP4 format).
*molrep.doc*
   full protocol of calculation (text format).
*molrep.btc*
   command (batch) file. You can repeat calculation using this file: *cp
   molrep.btc bat* and *sh bat*

.. _molrep_rf_tab:
 
 *molrep_rf.tab*
   List of peaks of rotation function, created as soon as the program
   calculates a Rotation Function.
   *molrep_rf.tab* is default name, you can use another name using
   keyword `FILE_T <#file_t>`__.
   You can edit this file and use it for subsequent calculations without
   computing the rotation function again (keyword `FUN=T <#fun_t>`__).
   The program then reads (in free format!): "Sol_", peak number and
   Polar angles (theta,phi,chi), *e.g.*:

      ::

         "Sol_  23  10.0    22.2 40.0"

   In Phased Rotation Function calculations, the program reads "Sol_",
   peak number, Polar angles and shift (sx,sy,sz), *e.g.*:

      ::

         "Sol_  23  10.0    22.2 40.0  .564 .443 .032"

   In Rotation and Position the model (keyword `FUN=S <#fun_t>`__), the
   program reads "Sol_", peak number,Polar angles and shift (sx,sy,sz)
   *e.g.*:

      ::

         "Sol_  23  10.0    22.2 40.0  .564 .443 .032"

   If you like to use Eiler angles use "Sol_A" instead "Sol_"

   In 'Search model orientation by PRF' (keyword `PRF=P <#PRF>`__), the
   program reads "Sol_", peak number,Polar angles and shift (sx,sy,sz)
   *e.g.*:

      ::

         "Sol_  23  10.0    22.2 40.0  .564 .443 .032"

   But program will use only the shift (sx,sy,sz).

*molrep_srf.tab*
   List of peaks of Self Rotation Function.
   *molrep_srf.tab* is default name, you can use another name using
   keyword `FILE_TSRF <#file_tsr>`__.
*molrep_rf.ps*
   PostScript file of Self Rotation Function

Some output files will not be deleted if option "-r" in command string
was used. They have the internal format of the
`BLANC <http://www.ysbl.york.ac.uk/~alexei/blanc.html>`__ program suite
and can be used by programs of this suite.

*crossrot_alo.dat*
   spherical coefficients of F_observed
*crossrot_alm.dat*
   spherical coefficients of F_model
*crossrot_dns.dat*
   rotation function map
*molrep_fob.dat*
   F_observed

also (if you started from MTZ file):

*molrep_mtz.cif*
   formatted CIF file of F_observed

also (if you used keyword `FILE_S <#file_s>`__):

*align.pdb*
   corrected by sequence alignment input model

See also `How to redirect output and scratch files <#redirection>`__

.. _what: 
 
WHAT MOLREP CAN DO
-------------------------------------------

::


                             +-- Self RF  (FUN=R, without any model)
                             !
            +-- Standard MR -+-- Cross RF (FUN=A or FUN=R )
            !                !
            !                +-- Locked Cross RF ( FUN=A or R and LOCK=Y )
            !                !
            !                +-- TF       (FUN=A or FUN=T )
            !
            !
            !                                       +- identical models
            !                                       !
            !                      +--Multi-monomer-+
            !                      !     search     !
            !                      !   (DYAD=M)     +- complex with pairs of
            !                      !                   different models 
            !                      !                    
            !                      !                +- two identical models
            !                      !                !
            +-- Multi-copy search -+-- Dyad search -+
            !     for MR           !   (DYAD=D)     !
            !                      !                +- two different
            !                      !                   models 
            !                      !
   MOLREP --+                      +-- Multy-copy for one model
            !                          (DYAD=Y)
            !                        
            !                        
            !                       +-- RF and PTF
            !                       !   (PRF=N)
            !                       !
            +-- Fitting two models -+-- SAPTF, RF and PTF
            !                       !   (PRF=S)
            !                       !
            !                       +-- SAPTF, PRF and PTF
            !                           (PRF=Y)
            !
            !
            !                        +-- RF and PTF
            !                        !   (PRF=N)
            !                        !
            +-- Searching in ED map -+-- SAPTF, RF and PTF 
            !                        !   (PRF=S)
            !                        !
            !                        +-- SAPTF, PRF and PTF
            !                            (PRF=Y)
            !
            +-- Rotate and position the model (FUN=S FILE_T)
            !
            !
            !
            +-- Search model orientation in electron density map 
            !   for particular position by phased RF (PRF=P FILE_T2)
            !
            !
            !                +-- find HA positions by MR solution 
            !                !   (FUN=D, model_2)
            !                !
            +-- HA search ---+-- HA search for SIR or SAD   
            !                !   (DIFF=H, FUN=T, without any model)
            !                !
            !                +-- Self RF for HA position 
            !                    (DIFF=H, FUN=R, without any model)
            !
            +-- pure RB refinement (phased or unphased, FUN=B, model_2)
                


      where: FUN, DYAD, PRF, LOCK, DIFF - keyword
             MR    - Molecular Replacement
             RF    - Rotation function
             TF    - Translation function     
             PRF   - Phased Rotation function
             PTF   - Phased Translation function
             SAPTF - Spherically Averaged Phased Translation function
             ED    - Electron density
             HA    - heavy atom
             RB    - rigid body

-  `Standard molecular replacement
   method <#standard_molecular_replacement_method>`__
-  `Self Rotation Function only <#self_rotation_function>`__
-  `Search model in electron density or EM map <#search_model>`__
-  `Search model orientation <#search_orientation>`__ by PRF
-  `Fitting two atomic models <#fitting_two_models>`__
-  `Just rotate and position the model <#just_rotate_and_position>`__
-  `Multi-copy search <#dyad_search>`__
-  `Model correction <#model_correction>`__
-  `Use sequence alignment <#sequence_alignment>`__
-  `NMR model <#NMR>`__
-  `EM or electron density model <#EM>`__
-  `Locked Cross Rotation Function <#LRF>`__
-  `rigid body refinement <#RB>`__
-  `find HA positions by MR solution <#find%20HA>`__
-  `heavy atom search <#heavy%20atom%20search>`__

.. _standard_molecular_replacement_method: 


Standard molecular replacement method
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The program performs molecular replacement in two steps:

#. 

   Rotation function (`RF <#RF>`__)
      search orientation of model

#. 

   Cross Translation function (`TF <#TF>`__) and Packing function (`PF <#RF>`__)
      search position of oriented model

The result of the Rotation function depends on the radius of a spherical
domain in the centre of the Patterson function (the so-called cut-off
radius). This radius must be chosen so as to maximize the ratio between
the number of inter- and intramolecular vectors. The program chooses the
value of this radius as twice the radius of gyration, but can also use
an input value (keyword RAD).

Instead of computing RF, the program can use a list of orientations from
a Rotation function (keyword FILE_T) which was prepared before.
Anisotropic correction of data before computing RF can be useful for
data with high anisotropy (keyword ANISO).

.. _modified_structure_factors: 
 

With a second fixed model (MODEL_2), the use of modified stucture
factors instead of |Fobs| for RF (keywords
`DIFF <#diff>`__, `P2 <#p2>`__) may make RF clearer. The modified
stucture factor is:

   sqrt(|Iobs-Imod2*(P2/100)|)

where P2 is the percentage of model_2 in the whole structure.

The Translation function can check several peaks of the rotation
function (NP) by computing a correlation coefficient for each peak and
sorting the result. For scaling observed and calculated structure
factors, the program uses the scaling by the origin peak of Patterson,
but for data with high anisotropy the program can use anisotropic
scaling (ANISO). The Translation function can take into account the
second fixed model (MODEL_2) and also, if the number of monomers is
known, MOLREP can position the input number of monomers in a simple run
(keyword NMON). Also in this case the possibility to choose from
symmetry-related models closest to which was found before is useful
(keyword STICK).

The program can detect and use pseudo-translation vectors. In this case
the pseudo-translation related copy will be added to the final model
(keyword PST).

The Packing function is very important in removing wrong solutions which
correspond to overlapping symmetry-related or different models (keyword
PACK).

Be careful, with keyword SURF='Y' program use to calculate Packing
Function only atoms which lying inside of molecule.So, for nonglobular
model (for example, only CA atoms) you cannot use keyword SURF='Y' with
PACK='Y'.

Use keywords:

   `DIFF <#diff>`__, `FUN <#fun>`__, `MODEL_2 <#model_2>`__,
   `NMON <#nmon>`__, `NP <#np>`__, `NPT <#npt>`__, `P2 <#p2>`__,
   `PACK <#pack>`__, `PST <#pst>`__, `RAD <#rad>`__,
   `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `SIM <#sim>`__,
   `STICK <#stick>`__, `SURF <#surf>`__, `VPST <#vpst>`__,
   `FILE_T <#file_t>`__, `FILE_TSRF <#file_tsr>`__,
   `NSRF <#dyad_nsrf>`__

.. _self_rotation_function:
 
 * Self Rotation Function only
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you define only a file of structure factors (Fobs), the program will
compute a Self Rotation function with default value of cut-off radius.
Use keyword `RAD <#rad>`__ if you want another value. Other useful
keywords: `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `SIM <#sim>`__.

Resulting output:

*molrep_srf.tab*
   List of peaks of Self Rotation Function.
   *molrep_srf.tab* is default name, you can use another name using
   keyword `FILE_TSRF <#file_tsr>`__.
*molrep_rf.ps*
   PostScript file of Self Rotation Function which contains four plots
   RF (theta,phi,chi) for
   chi = 180, 90, 120, 60.
   You can change the fourth value of chi (60) by keyword `CHI <#chi>`__
   You can change the scale of these plots of RF by keyword
   `SCALE <#scale>`__
   This picture can help understand stereographic projection
   |stereoproj|

.. _search_model: 
 
 * Search model in electron density or EM map
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases it is difficult to solve an X-ray structure by molecular
replacement even when a structure for a homologous molecule is khown. If
prior phase information either from SIR/MAD or from a partial structure
is known, this could be used in a six-dimensional search. The program
divides the six-dimensional search with phases into three steps:

#. a spherically averaged phased translation function
   (`SAPTF <#SAPTF>`__) is used to locate the position of the molecule
   or its fragment. It compares locally spherically avaraged
   experimental electron density with that calculated from the model and
   tabulates highest scoring positions.
#. then for each such position a local phased rotation function
   (`PRF <#PRF>`__) is used to find the orientation of the molecule.
   Another possibility is to use usual rotation function (RF) for
   modified map, i.e program sets 0 the density outside of sphere with
   radius = twice radius of model and with the centre in current point.
#. the phased translation function (`PTF <#PTF>`__) for found
   orientation which checks and refines the found position.

You need to have the phases in a CIF file of structure factors or to use
corresponding keywords for MTZ file or use EM map as input instead of
Fobs file. In EM case map will be converted into !F! and phases.

For input map use keywords:

   `DSCALE <#dscale>`__, `INVER <#inver>`__, `DLIM <#dlim>`__



   -  with keyword **PRF** = 'N' (default value):
      usual Rotation function and Phased Translation function will be
      used.
   -  with keyword **PRF** = 'Y':
      SAPTF (Spherically averaged phased translation function), Phased
      Rotation function and Phased Translation functions will be used.
   -  with keyword **PRF** = 'S':
      1.SAPTF (Spherically averaged phased translation function). 2.For
      current point of SAPTF solution input map is modified, i.e program
      sets 0 the density outside of sphere with radius = twice radius of
      model and with the centre in current point. 3.usual Rotation
      function for this modified map. 4.Phased Translation functions

`SFCHECK <sfcheck.html>`__ can convert input map to scaled and/or
inverted map.

If you structure contains several molecules which forms some point group
you can use this NCS. Program will generate complete model and use it
for stage PTF. Also result (molrep.pdb) will be complete model.

First of all you must define parameters of NCS in PDB file or using
keywords NCS, ANGLES, CENTRE. See `How to define NCS <#howNCS>`__

Other useful keywords:

   `NMON <#nmon>`__, `NP <#np>`__, `NPT <#npt>`__, `RAD <#rad>`__,
   `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `SIM <#sim>`__,
   `SURF <#surf>`__, `INVER <#inver>`__, `NCS <#ncs>`__,
   `ANGLES <#angles>`__, `CENTRE <#centre>`__

Also you can refine solution by `Pure Rigid Body
Refinement <#pure%20RB>`__

.. _search_orientation: 
 
* Search model orientation in electron density map for particular position by PRF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can use this possibility (keywords `PRF <#PRF>`__=P and
`FUN <#fun>`__=R or A) if you want to find the model orientation in ED
map by rotating model around the defined point in ED map. Program puts
the origin of model coordinate sysytem to the defined point and performs
phased rotation function (PRF). Use keyword `RAD <#rad>`__ to define the
radius of sphere for PRF.

You must define the list of defined points of ED map using file
`FILE_T2 <#file_t>`__ , wich must contain lines with "Sol_", peak
number,Polar angles and shift (sx,sy,sz) *e.g.*:

   ::

      "Sol_  23  10.0    22.2 40.0  .564 .443 .032"

But program will use only the shift (sx,sy,sz).

Model is rotated around the origin of model coordinate sysytem. If
keyword `SURF <#surf>`__= Y,A,2,O program puts the centre of model to
the origin of model coordinate sysytem automatically. If you want, for
example, to rotate the model around some atom, shift the origin to this
atom and use `SURF <#surf>`__=N

Other useful keywords:

   `NMON <#nmon>`__, `NP <#np>`__, `NPT <#npt>`__, `RESMAX <#resmax>`__,
   `RESMIN <#resmin>`__, `SIM <#sim>`__, `INVER <#inver>`__

.. _fitting_two_models:
 
* Fitting two atomic models (`FM <#FM>`__)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The idea is to fit the electron densities instead of the atomic models,
trying to find the best overlap. Advantages are:

-  can be used for cases with very low homology;
-  can be used when amino acid sequence is absent;
-  no need to use the list of equivalent atoms.

If you define only two files of models (searching model and model_2),
without a file of structure factors (Fobs), the program will fit the
search model (keyword `FILE_M <#file_m>`__) to the second model (keyword
`MODEL_2 <#model_2>`__). The search model must be smaller or equal to
the second model.

   -  with keyword **PRF** = 'N' (default value):
      usual Rotation function (RF) to search the orientation and Phased
      Translation function (PTF) to search position will be used.
   -  with keyword **PRF** = 'Y':
      Spherically averaged phased translation function (SAPTF) gives the
      expected position for model. Phased Rotation function (PRF) for
      expected position gives orientation. Phased Translation function
      (PTF) checks and refines the translation vector.
   -  with keyword **PRF** = 'S':
      1.SAPTF (Spherically averaged phased translation function). 2.For
      current point of SAPTF solution input map is modified, i.e program
      sets 0 the density outside of sphere with radius = twice radius of
      model and with the centre in current point. 3.usual Rotation
      function for this modified map. 4.Phased Translation functions

Other useful keywords:

   `NP <#np>`__, `NPT <#npt>`__, `RAD <#rad>`__, `RESMAX <#resmax>`__,
   `RESMIN <#resmin>`__, `SIM <#sim>`__, `SURF <#surf>`__

The result is file *molrep.crd* (or *molrep.pdb* ) - model fitted to
second model.

.. _just_rotate_and_position:
 
 * Just rotate and position the model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This possibility may be useful if you want to place the model to a
particular orientation and position, or to compare several solutions.

Use keyword `FUN=S <#fun_s>`__ and define three files: a model (keyword
`FILE_M <#file_m>`__), a file of structure factors (keyword
`FILE_F <#file_f>`__) and file with polar angles and shifts (keyword
`FILE_T <#file_t>`__). The program will shift the model to the origin,
rotate (by polar angles) and the position it (in fractional unis). The
new model will be written to an output coordinate file. Also the program
will compute an R-factor and a Correlation Coefficient.

If you like to use Eiler angles use "Sol_A" instead "Sol_" see
`molrep_rf.tab <#molrep_rf_tab>`__

If you like to rotate around some atom, you have to shift coordinate
system origin to this atom by hand and use keyword SURF = N.

Other useful keywords:

   `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `SIM <#sim>`__

.. _dyad_search: 
 
* Multi-copy search
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are three modes: "dyad_search", "Multi-copy search"and
"Multi-monomer search".

Dyad_search - Search two copies of a model simultaneously (keyword
`DYAD=D <#dyad>`__).

Sometimes you can not find a solution starting with one molecule if you
have several copies of the molecule in the asymmetrical part of the unit
cell. In this case a search with two independent molecules may give a
solution. The central point of method is the construction of a
multi-copy search model from properly oriented monomers using a special
TF (`STF <#STF>`__), which gives the intermolrecular verctor between
properly oriented monomers (dyad). This dyad can then be used for a
positional search with a conventional TF.

#. the program checks all pairs of `NP <#np>`__ peaks of the Rotation
   Function (RF). For each pair the program uses the first rotation to
   prepare model-1. Model-2 will be prepared by using the second
   rotation and one rotation from the crystallographical symmetry
   operators. The total number of pairs to be checked is
   ((NP+1)*NP*Nsym)/2
#. next, for model-1 and model-2: the program computes the Special
   Translation Function ( `STF <#STF>`__) to find the inter-molecular
   vector of the dyad.
#. for `NPT <#dyad_npt>`__ peaks ( *i.e.* inter-molecular vectors) of the
   STF, the program computes the standard Translation Function (TF)
   using the current dyad as a model, and it calculates a Correlation
   Coefficient for the first`NPTD <#dyad_nptd>`__ peaks of the TF.

Solution and output file: *molrep.pdb* will be the dyad with the best
Correlation Coefficient (or several dyads if keyword `NMON <#nmon>`__ >
1).

WARNING: the procedure takes quite some time, because the total number
of Translation Functions to be calculated is
NMON*NPT*((NP+1)*NP*Nsym)/2.

In the output .log (.doc) file you can find the following information:

   ::


      Sol_      R1  R2  Rs Rslf STF TF        Shift_1     PFmax PFmin    Rfac   Corr
      Sol_       1   1   1   0   2   1  0.059 0.000 0.201  1.01  0.99   0.569  0.379

      and

      Sol_best   1   1   1   0   2   1  0.059 0.000 0.201  1.01  0.99   0.569  0.379
      Sol_best         Rot1-->2               Dyad_vector       dist d_ort d_par
      Sol_best    0.0    0.0    0.0    -0.210  0.000 -0.487    39.2  19.6  33.9

These lines means:

   R1
      peak number of rotation for model-1
   R2
      peak number of rotation for model-2
   Rs
      CS operator number which applyed before rotation for model-2
   Rslf
      peak number of self rotation function
   STF
      peak number of special translation function
   TF
      peak number of translation function
   Shift_1
      position of model-1
   PFmax PFmin
      min, max values of Packing function
   Rfac Corr
      R-factor and Correlation Coefficient
   Rot1->2
      polar angles of rotation from model-1 to model-2.
   Dyad_vector
      vector (in fractional) from model-1 to model-2.
   dist d_ort d_part
      first number - distance between models (in Angstrom)
      second number - distance orthogonal to rotation 1->2
      third number - distance parallel to rotation, *i.e.* for pure
      dimer this is 0.

Also you can find additional information:

   ::

      Sol_              angles_1             angles_2        shift_2
      Sol_      90.63   98.70  118.12   90.63   98.70  118.12  0.189  0.256 -0.415
       
             +---------------------------------------------------------+ 
             !                                                         !
             !                                                         !
             !                                                         !
             !                                                         !
             !                                                         !
             !      -----------------            -----------------     !
             !     /                           /                     !
             !    /                           /                      !
             !    ! rotated (angles_1) !       ! rotated (angles_2) !  !
             !    !     monomer_1      !       !     monomer_2      !  !
             !    !                    ! dyad  !                    !  !
             !    !         +----------!-----------------+          !  !
             !    !        /           ! vector!       '            !  !
             !    !       /           /           '               /   !
             !          /           /                           /    ! 
             !         /           /          '                /     !
             !      ---/------------        '      --------------      !
             !        /shift_1           ' shift_2                     !
             !       /                '                                !
             !      /              '                                   !
             !     /            '                                      !
             !    /          '                                         !
             !   /        '                                            !
             !  /      '                                               !
             ! /    '                                                  !
             !/  '                                                     !
             +---------------------------------------------------------+
               origin

If you believe the Self-RF, you can try to find a dyad which has the
rotation between monomers corresponding to the rotation of the Self-RF
(use keywords `NSRF <#dyad_nsrf>`__, `FILE_TSRF <#file_tsr>`__).

Model-2 can be different from model-1. Use keywords
`FILE_M2 <#dyad_filem2>`__ to define file of searching model-2,
`FILE_T2 <#dyad_filet2>`__ with list of peaks rotation function for this
model (this RF have to be computed before) and `NP2 <#dyad_np2>`__
number of peacks which will be used.

Multi-copy search - Search many copies of a model (not only dyad)
(keyword `DYAD=Y <#dyad>`__).

Program starts to search a single monomer, after that produces the dyad
search, repeates dyad search for next dyad with the first being fixed
and ,finaly, tryes add a single monomer.

Multi-monomer search - construct complexes of monomers and check.
(keyword `DYAD=M <#dyad>`__).

Instead to search two copies and then next one program constructs
maximal possible complexes of monomers (3mres,4mers,5mers,...) and
checks its by TF. This algorithm is better and faster then DYAD=D or
DYAD=Y. But also it takes a time (about 30min,1h,2h ...). It depends on
resolution, size of cell, space group.

Use keywords:

   `DYAD <#dyad>`__, `DIST <#dyad_dist>`__, `NP <#np>`__,
   `NSRF <#dyad_nsrf>`__, `NPT <#dyad_npt>`__, `NPTD <#dyad_nptd>`__,
   `NP2 <#dyad_np2>`__, `AXIS <#dyad_axis>`__,
   `FILE_M2 <#dyad_filem2>`__, `FILE_T2 <#dyad_filet2>`__,
   `FILE_T <#file_t>`__, `FILE_TSRF <#file_tsr>`__, `NMON <#nmon>`__,
   `ALL <#all>`__, `PACK <#pack>`__

For DYAD M:

   `DYAD <#dyad>`__, `NP <#np>`__, `NPTD <#dyad_nptd>`__,
   `NP2 <#dyad_np2>`__, `FILE_M2 <#dyad_filem2>`__,
   `FILE_T2 <#dyad_filet2>`__, `FILE_T <#file_t>`__, `NMON <#nmon>`__,
   `ALL <#all>`__, `PACK <#pack>`__ `NML <#nml>`__

and also:

   `SIM <#sim>`__, `RESMAX <#resmax>`__, `RESMIN <#resmin>`__,
   `SURF <#surf>`__, `STICK <#stick>`__

.. _model_correction:  
 


* Model correction
~~~~~~~~~~~~~~~~~~~

You can improve your model beforehand by using keyword `SURF <#surf>`__.

**N**

do not perform any model correction.For FUN=S (just_rotate_and_position)
program changes N to O

**O**

only shift to the origin

**A**

make the protein into a polyalanine model ( *i.e.* remove from the model:
water molecules, H atoms, atoms with alternative conformation (except
the first), atoms with occupacy = 0), make all B = 20, and shift to the
origin

**Y**

remove various atoms from the model (water molecules, H atoms, atoms
with alternative conformation (except the first), atoms atoms with
occupacy = 0), shift to the origin, compute atomic accessible surface
area and replace atomic B with B = 15.0 + SURFACE_AREA*10.0

**2**

set all B = 20 and shift to the origin

**C**

as Y but new B only for Packing function (not change original B) and
shift to the origin

.. _sequence_alignment:
 


* Using sequence alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Another way to improve your model is to use the sequence of the unknown
structure.

Use keyword `FILE_S <#file_s>`__ to define a file containing a sequence.
This sequence file must be ASCII:

   ::

       
      !> title
      !# sequence 
      !SVIGSDDRTRVTNTTAYPYRAIVHISSSIGSCTGWMIGPKTVATAGHCIY
      !# this is comment
      !    DTSSG--SFAGTATVSP   GRNGTSYPYG
      !NRGTRITKEVFDNLTNWKNSAQ
      !

If the first symbol in the line is "#", it means the line contains
comments. Blancs are ignored.

The program will perform sequence alignment and create a new corrected
model with the atoms corresponding to the alignment. The output file
with the corrected model is *align.pdb*. Without an Fobs file, the
program only performs `model correction <#model_correction>`__.

.. _nmr:  
 


* NMR Model
~~~~~~~~~~~~

You can use PDB file with NMR models or pseudo-NMR file with several
homologous structures which were superimposed before. Algorithm is
equivalent to sum RF or/and TF for individual structures. Program can
find the best model in NMR file or use all models (see keyword
`NMR <#nmr>`__) .

In the PDB file different models must be separated by MODEL record. For
example:

   ::

      HEADER    HYDROLASE (ENDORIBONUCLEASE)         
      CRYST1   64.900   78.320   38.790  90.00  90.00 ...
      MODEL        1 
      ATOM      1  N   ASP A   1      45.161  12.836 ... 
      ATOM      2  CA  ASP A   1      45.220  12.435 ...   
       ... 
      ATOM    745  SG  CYS A  96      58.398   6.673 ... 
      ATOM    746  OXT CYS A  96      62.238   7.178 ...  
      ENDMDL                                        
      MODEL        2   
      ATOM      1  N   ASP B   1      44.487  11.386 ...  
      ATOM      2  CA  ASP B   1      44.559  11.129 ... 
       ...

Use keyword `NMR <#keyNMR>`__

.. _em: 
 


* EM or electron density model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Searching model can be Electron Microscopic model (EM) or electron
density map. Only values higher the limit (if keyword `ROLIM <#rolim>`__
is defined) will be used. Map must have space group P1 and contains
whole model. Vector ORIGIN defines the centre of model and the rotation
will be performed around this point. If parameter DRAD (radius of model)
is defined program will use the density only inside the sphere with
radius = DRAD and with centre in vector ORIGIN.

::


           +--------------------------------+ nz
      !    !                                !
      !    .                                .
      !
      !    !                                !
      !    .                                .
      !
      !    !                                !
      !    +--------------------------------+ izmax
      !    !                                !
      !    !                                !
      !    !                                !
      !    !      ----------------          !
      !    !     /                         !
      !    !    /                          !
      !    !   /                           !
    C_cell !  /                            !
      !    !  !                       !     !
      !    !  !   DRAD                !     !
      !    !  !---------- +           !     !
      !    !  !          / centre     !     !
      !    !  !         /             /     !
      !    !          /             /      !
      !    !         /             /       !
      !    !        /             /        !
      !    !       /             /         !
      !    !      -/--------------          !
      !    !      /                         !
      !    !     /                          !
      !    !    / ORIGIN                    !
      !    !   /                            !
      !    !  /                             !
      !    ! /                              !
      !    !/                               !
      !    +--------------------------------+
           0                                nx
           ----------- A_cell --------------
     

Program will get vector ORIGIN from file automatically. If it is not
possible to get correct vector, program will use ORIGIN = ( 0.5, 0.5,
izmax/(2*nz)). If you want you can define ORIGIN yourself.

Use keywords:

   `DSCALEM <#dscalem>`__, `INVERM <#inverm>`__, `ROLIM <#rolim>`__,
   `DRAD <#drad>`__, `ORIGIN <#origin>`__

`Search model in electron density map <#search_model>`__ will be
performed as usual.

.. _LRF:
 


* Locked Cross Rrotation Function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Locked Cross Rotation function (LRF) means to average the Cross Rotation
function by NCS which can be determined with Self Rotation function. LRF
is especially useful when NCS forms a group.

Use keywords:

   `LOCK <#lock>`__, `NSRF <#dyad_nsrf>`__, `FILE_TSRF <#file_tsr>`__,

.. _RB:
 


* Rigid body refinement
~~~~~~~~~~~~~~~~~~~~~~~~

You can use 

.. _pure RB: 

 
 **Rigid Body Refinement** in
Patterson or Real space (keyword FUN = 'B'). This possibility is useful
in the last stage of MR. For example after fitting the model into EM
map. You must define Fobs or Fobs and phases (or Map). Also use keyword
MODEL_2 for model to refine. If you define the phases or use the map
program will produce real space refinement (more correctly, in
reciprocal space using phase information).

If keyword DOM = 'N' (default) program refines MODEL_2 as single
molecule.

If keyword DOM = 'Y' program performs multi-domain refinement. For this
you must put into PDB file additional lines before each domain.
Additional line contains word '#DOMAIN' and domain number (free format).

For example:

   ::

      HEADER    HYDROLASE (ENDORIBONUCLEASE)         
      CRYST1   64.900   78.320   38.790  90.00  90.00 ...
      #DOMAIN     1 
      ATOM      1  N   ASP A   1      45.161  12.836 ... 
      ATOM      2  CA  ASP A   1      45.220  12.435 ...   
       ... 
      ATOM    745  SG  CYS A  96      58.398   6.673 ... 
      ATOM    746  O   CYS A  96      62.238   7.178 ...  
      #DOMAIN     2 
      ATOM    747  N   PHE A  97      44.487  11.386 ...  
      ATOM    748  CA  PHE A  97      44.559  11.129 ... 
       ...
      ATOM    945  C   VAL A 196      58.398   6.673 ... 
      ATOM    946  O   VAL A 196      62.238   7.178 ...  
      #DOMAIN     1 
      ATOM    947  N   ASP A 197      44.487  11.386 ...  
      ATOM    948  CA  ASP A 197      44.559  11.129 ... 
       ...

If you structure contains several molecules which forms some point group
you can use this NCS as constraints. First of all you must define NCS
parameters in PDB file or using keywords: NCS,ANGLES, CENTRE. See `How
to define NCS <#howNCS>`__

If you have some trouble with NCS parameters (values:
theta,phi,chi,cx,cy,cz) use keyword DOM = 'I' and you can find these
actual values in output PDB file: molrep.pdb. Alternative way to create
initial PDB file with complete model is to describe only first
(reference) molecule and use keyword DOM = 'C'. Complete model you can
find these in output PDB file:molrep.pdb. Finally use keyword DOM = 'S'
for refinement with constraints.

Use keywords:

   `FUN = B <#fun>`__, `DOM <#dom>`__, `SIM <#sim>`__,
   `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `NREF <#nref>`__,
   `NCS <#ncs>`__, `ANGLES <#angles>`__, `CENTRE <#centre>`__

.. _find HA:  
 


* Find HA positions by MR solution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To define derivative use corresponding label for MTZ file or derivative
file (FILE_DER).

Use keywords:

   `FUN = D <#fun>`__, `MODEL_2 (as MR solution) <#model_2>`__

.. _heavy atom search: 
 


* Heavy atom search
~~~~~~~~~~~~~~~~~~~~

To define derivative use corresponding label for MTZ file or derivative
file (FILE_DER).

In this case you need not to use any model.

Use keywords:

   `DIFF = H <#diff>`__, `FUN = T or R <#fun>`__

| 'FUN = T' means Heavy atom search (experimental version)
| 'FUN = R' means Self RF for Heave atom structure.

.. _how: 
 


HOW TO USE MOLREP
-----------------

(you can use `Testing_program_MOLREP <#testing>`__ as tutorial)

A simple way to use MOLREP is to define files for Fobs and the model and
use default values for all parameters ( *i.e.* without using any
keywords). There is always a chance of solving the structure
automatically. If this does not work, use a common strategy of molecular
replacement.

Planning ahead
~~~~~~~~~~~~~~

Success of the molecular replacement method depends on:

-  quality of experimental data
-  scaling |F|_obs and |F|_calc
-  low resolution limit
-  high resolution limit
-  quality of the model, homology, conformation

Things to look out for:

data
   Look at your data quality. Completeness is very important. Absence of
   low resolution reflections may cause problems, especially if the
   model is some part of a whole structure. Look at anisotropy and
   twinning.
   Think carefully: can you 'safely' use the high resolution
   reflections? If not, use keyword `SIM <#sim>`__ to remove the
   potentially bad effect of this part of the data. It might be a good
   idea to use some program to check the data, for example
   `SFCHECK <sfcheck.html>`__.
model
   Look at the model regarding the shape. The automatic choice of the
   cut-off radius for RF is twice the radius of gyration. This is good
   enough if the shape is approximately spherical. If the model is very
   asymmetrical, it is better to make a choice yourself.
   Make a choice for `SIM <#sim>`__, `RESMIN <#resmin>`__. If you have
   not any idea about similarity, SIM=0.35 is a good approximation.
   If you have a dimer use it, but use `RAD <#rad>`__ corresponding to a
   monomer.
   It is very useful to shift the model to the origin of coordinates.
   Use keyword `SURF <#surf>`__ = O or Y (Y is default).
Self-RF
   Compute Self-RF. It may give you some idea about NCS or about the
   number of copies in the asymmetrical part of unit cell.
Cross-RF only
   In the DOC_file you can find the list of expected orientations of the
   model and also the rotations between them. Compare this with the
   Self-RF. This is an additional check of correctness of the expected
   orientation. But sometimes we can not find corresponding peaks in
   Self-RF for correct orientation.
Translation function
   If there are several copies of the model in the asymmetrical part of
   unit cell, use keyword `NMON <#nmon>`__ or multi-copy or dyad search.
   It is not necessary to use the option of Pseudo-translation for a
   dyad search, since this can recognize Pseudo-translation itself.

Dialogue or batch
~~~~~~~~~~~~~~~~~

You can use MOLREP `by dialogue or by command (batch)
file <#keywords>`__. Modern computing technology allows the carrying out
of most of the calculations for small and medium sized proteins in real
time, therefore, dialogue is a preferable way of running MOLREP.
However, the program automatically produces a batch command file during
dialogue. This feature might be useful for repeated calculations.

.. _pseudo-translation: 
 
Pseudo-translation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MOLREP can detect pseudo-translation, and define a pseudo-translation
vector.

If keyword `PST <#pst>`__ = Y, the program applies pseudo-translation
with a pseudo-translation vector which was defined by the program or the
user. When calculating a Translation Function, the program will use this
vector to modify structure factors. Pseudo-translation copy will be
added to the final model at the end program running.

If `FUN=R <#fun_r>`__ and `LIST=L <#list_l>`__ MOLREP computes a list of
Patterson peaks and writes these to *molrep.doc*. This may be helpful in
the detection of pseudo-translation.

Use keywords:

   `PST <#pst>`__, `VPST <#vpst>`__

Flexible model
~~~~~~~~~~~~~~

If your model is flexible, for example, consists of two domains, you can
try to solve this problem by two ways:

1. Create two files for each domain and use dyad search (DYAD = D or
DYAD = M)

2. Solve one domain, refine it and use the solution as fixed model to
solve another domain.

The use many homologous models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have several homologous models you can create a pseudo NMR file
with these models and use its together (see `NMR <#NMR>`__ model). But
these models must be superimposed before, for example, by MOLREP (see
`fitting two models <#fitting_two_models>`__).

Keep in mind
~~~~~~~~~~~~

-  Without a model file, the program only computes a Self Rotation
   Function.
-  If `FUN=R <#fun_r>`__, the program computes and writes to
   *molrep.doc* all symmetry-related peaks of the Rotation Function. If
   also keywords NSRF and FILT_TSRF are used you can fine the pairs of
   peaks of cross RF which corresponds to NCsymmetry.
-  If you want to change the space group of the structure factor file,
   use keyword `NOSG <#nosg>`__, *i.e.* new space group number, or
   keyword `SG <#sg>`__
-  The Packing Function (`PF <#PF>`__) is very important to remove wrong
   solutions which correspond to overlapping symmetry-related or
   different models. But you can remove this option (`PACK <#pack>`__ =
   N ), for example, if you want to find the model in a special
   position. Value of PF = 1 corresponds to non-overlapping, value = -1
   corresponds to completely overlapping two models.
-  When MOLREP is trying to find several models (`NMON <#nmon>`__ > 1)
   it is useful to use keyword `STICK <#stick>`__ = Y. Then for each
   additional molecule the program will choose a symmetry-related
   molecule closest to which was/were found before.
-  For the `PRF <#PRF>`__ and the `SAPTF <#SAPTF>`__, the default
   cut-off radius is *once* the radius of gyration, whereas for a
   Patterson calculation the cut-off radius would be *twice* the radius
   of gyration.

Tips:
~~~~~

1
   In the beginning use molrep without keywords or only SIM (default
   0.35)
2
   In case with pseudo-translation use also "PST N"
3
   If input model is an ensemble of models (for example from BALBES) try
   also "NMR 1" (some variant of RF) and "NMR -1" means only first model
   from ensemble.
4
   If you are searching additional part of structure in the map after
   refmac: "molrep -f refmac.mtz -m model.pdb -mx refmac.pdb" with
   "LABIN F=FWT PH=PHWT" use "DIFF M","SIM -1" (i.e. not use SIM), NP
   about 50 also you can try "PRF Y" or "PRF S" (SAPTF)
5
   In difficult case play with SIM, RESMIN and NP
6
   To check alternative space groups use "SG ALL"
7
   For "DYAD M" useful keywords are "ALL","NPTD" and "NML"
8
   You can stop program safely if you create in current directory or in
   PATH_OUT (if option "-po" is used) file: *molrep_stop_signal.xml*
   (contents does not matter)

.. _keywords: 
 


KEYWORDED INPUT
---------------

It is easy to use MOLREP interactively, but can be used in
`batch <#batch>`__. The available keywords are:



   Common:

   `FILE_T <#file_t>`__, `FUN <#fun>`__, `NMON <#nmon>`__, `NP <#np>`__,
   `NPT <#npt>`__, `RAD <#rad>`__ `PATH_SCR <#path_src>`__

   And for structure factors control:

   `RESMAX <#resmax>`__, `RESMIN <#resmin>`__, `SIM <#sim>`__

   And for model control:

   `MODEL_2 <#model_2>`__, `SURF <#surf>`__

   And for multi-copy search:

   `DYAD <#dyad>`__, `FILE_M2 <#dyad_filem2>`__,
   `FILE_T2 <#dyad_filet2>`__, `NP2 <#dyad_np2>`__,
   `NPTD <#dyad_nptd>`__, `NSRF <#dyad_nsrf>`__

   And for search in ED:

   `PHASE <#phase>`__, `PRF <#prf>`__, `INVER <#inver>`__

   And for fitting two models:

   `PRF <#prf>`__

   And for EM or electron density model:

   `DSCALEM <#dscalem>`__, `INVERM <#inverm>`__, `ROLIM <#rolim>`__,
   `DRAD <#drad>`__, `ORIGIN <#origin>`__

   And for EM or electron density instead of Fobs:

   `DSCALE <#dscale>`__, `INVER <#inver>`__, `DLIM <#dlim>`__



   Common:

   `ANISO <#aniso>`__, `BADD <#badd>`__, `LIST <#list>`__,
   `LMAX <#lmax>`__, `LMIN <#lmin>`__, `PACK <#pack>`__,

   And for standard MR:

   `DIFF <#diff>`__, `FILE_S <#file_s>`__, `NMR <#keyNMR>`__,
   `NOSG <#nosg>`__, `P2 <#p2>`__, `PST <#pst>`__, `STICK <#stick>`__,
   `VPST <#vpst>`__, `LOCK <#LOCK>`__,

   And for Self RF:

   `CHI <#chi>`__, `PST <#pst>`__, `SCALE <#scale>`__,
   `FILE_TSRF <#file_tsr>`__

   And for multi-copy search:

   `AXIS <#dyad_axis>`__, `DIFF <#diff>`__, `DIST <#dyad_dist>`__,
   `P2 <#p2>`__, `ALL <#all>`__, `STICK <#stick>`__

   And for search in ED:

   `DIFF <#diff>`__, `P2 <#p2>`__, `NPTD <#dyad_nptd>`__

   And for fitting two models:

   `NPTD <#dyad_nptd>`__

   And for search in ED:

   `NCS <#ncs>`__, `ANGLES <#angles>`__, `CENTRE <#centre>`__

   And for Pure Rigid Body Refinement:

   `DOM <#dom>`__, `NCS <#ncs>`__, `ANGLES <#angles>`__,
   `CENTRE <#centre>`__

.. _keywords_general:
 
General keywords
-----------------------------------------------------------------

.. _np: 
 
NP <np>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <auto>

<np> is the number of peaks from the rotation function to be used
(maximum: 200).

.. _npt:  
 
NPT <npt>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <15>

<npt> is the number of peaks from the translation function to be used
(maximum: 50).

.. _nmon:  
 
NMON <nmon>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <auto>

<nmon> is the number of monomers. The program will try to create a full
model, which will consist of NMON initial models plus model_2.

.. _resmin:  
 
RESMIN <resmin>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <auto>

soft resolution cut_off: `Boff <#soft_low_resolution_cut-off>`__
=2*RES_min^2

.. _compl:  
COMPL <compl>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: automatic choice

COMPL = V_model/(V_cell/Nsym), RESMIN ~ Rad_model. It corresponds to
`Boff <#soft_low_resolution_cut-off>`__:

.. _sim:  
 
SIM <sim>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: 0.35

Similarity of the model: from 0.1 to 1.0. It corresponds to
`Badd <#priori_knowledge>`__. SIM=1 means normalized F will be used.
SIM=-1 means do not use SIM (i.e. BADD=0) If SIM is used, the keyword
`BADD <#badd>`__ is ignored.

SIM (Badd)
   controls high resolution data
RESMIN,COMPL (Boff)
   controls low resolution data

The use of Boff and Badd means to change Fobs and Fmodel:

   |F|_new = |F|_input
   *exp(-Badd*s :sup:`2`)*(1-exp(-Boff*s :sup:`2`)

.. _fun:  
 
FUN < A | R | T | S | B | D >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <A>

+-------+-------------------------------------------------------------+
| **R** | calculate only Rotation Function                            |
+-------+-------------------------------------------------------------+
| **T** | calculate only Translation Function, reading list of peaks  |
|       | of RF from file (`molrep_rf.tab <#molrep_rf_tab>`__) or     |
|       | from TAB_file                                               |
+-------+-------------------------------------------------------------+
| **A** | calculate both: RF and TF                                   |
+-------+-------------------------------------------------------------+
| **S** | rotate and position the model and compute R-factor and      |
|       | Correlation Coefficient                                     |
+-------+-------------------------------------------------------------+
| **B** | pure Rigid Body refinement                                  |
+-------+-------------------------------------------------------------+
| **D** | find HA positions by MR solution                            |
+-------+-------------------------------------------------------------+

.. _file_t:  
 
FILE_T <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <*molrep_rf.tab*>

Input or output TAB_file (see also `molrep_rf.tab <#molrep_rf_tab>`__)

.. _model_2:  
 
MODEL_2 <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: no model_2

Input file with the second (fixed) model in correct position and
orientation, in PDB or BLANC format. This model will be fixed during the
search. When fitting two models to each other, the second model is the
target model.

.. _surf:  
 
SURF < N | Y | A | O | 2 | C >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <Y>

Perform model correction.

**N**

do not perform any model correction.For FUN=S (just_rotate_and_position)
program changes N to O

**O**

only shift to the origin

**A**

make the protein into a polyalanine model ( *i.e.* remove from the model:
water molecules, H atoms, atoms with alternative conformation (except
the first), atoms with occupacy = 0), make all B = 20, and shift to the
origin

**Y**

remove various atoms from the model (water molecules, H atoms, atoms
with alternative conformation (except the first), atoms atoms with
occupacy = 0), shift to the origin, compute atomic accessible surface
area and replace atomic B with B = 15.0 + SURFACE_AREA*10.0

**2**

set all B = 20 and shift to the origin

**C**

as Y but new B only for Packing function (not change original B) and
shift to the origin

.. _rad: 
 
RAD <rad>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: automatically calculated from the model, unless:

-  in case of Self-RF calculations: 30Å
-  for Rotation Function calculations: twice the radius of gyration
-  for PRF and SAPTF: radius of gyration

Cut-off radius for Patterson search or for electron density search.

.. _resmax:

 
RESMAX <resmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <auto>

High resolution limit.

.. _path_src:  
 
PATH_SCR <path>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <not used>

Path to directory for scratch files. For example: /y/people/alexei/

.. _keywords_special:  
 
 Keywords for special cases
----------------------------------------------------------------------------

.. _pst: 
 
PST < N | C | Y >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

How to deal with pseudo-translation.

===== =============================================
**N** ignore pseudo-translation altogether
**C** check only, but do not use pseudo-translation
**Y** use pseudo-translation.
===== =============================================

.. _vpst: 
 
VPST <vpst1,vpst2,vpst3>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: automatically from Patterson

Pseudo-translation vector (in fractional units), used when
`PST <#pst>`__ = Y.

.. _badd:  
 
BADD <badd>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

`BOFF <#soft_low_resolution_cut-off>`__ and `BADD <#priori_knowledge>`__
mean:

   |F|_new = |F|_input
   *exp(-Badd*s :sup:`2`)*(1-exp(-Boff*s :sup:`2`)

.. _aniso:  
 
ANISO < N | Y | C | S | K >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

===== ===========================================================
**N** do not use anisotropic correction and/or scaling
**Y** use `anisotropic correction and scaling <#aniso_scaling>`__
**C** use anisotropic correction of Fobs for RF only
**S** use anisotropic scaling for TF only
**K** use scaling without B-factor
===== ===========================================================

.. _pack: 
 
PACK < Y | N >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <Y>

===== =====================================================
**Y** use Packing Function with Translation Function
**N** do not use Packing Function with Translation Function
===== =====================================================

.. _lmin: 
 
LMIN <lmin>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <4>

Minimum L-index of spherical coefficients. The program does not use
coefficients with L=0. Possible values are 2,4,6,... L = 2 means to use
all coefficients up to Lmax.

.. _lmax: 
 
LMAX <lmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: automatic choice

Maximum L-index of spherical coefficients. Possible values are
2,4,6,8,...,98,100.

.. _prf:
 
PRF < N | Y | S | P >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

+-------+-------------------------------------------------------------+
| **N** | standard RF and Phased Translation Function is calculated   |
+-------+-------------------------------------------------------------+
| **Y** | SAPTF (Spherically averaged phased translation function),   |
|       | Phased Rotation Function (PRF) and Phased Translation       |
|       | Function will be used.                                      |
+-------+-------------------------------------------------------------+
| **S** | SAPTF (Spherically averaged phased translation function),   |
|       | Usual Rotation Function (RF) for modified map and Phased    |
|       | Translation Function will be used.                          |
+-------+-------------------------------------------------------------+
| **P** | Search the model orientation in ED map by rotating model    |
|       | around the defined points in ED map. List of points must be |
|       | in the file `FILE_T2 <#file_t>`__.                          |
+-------+-------------------------------------------------------------+

Program will use the phases of BLANC (by keyword `PHASE <#phase>`__) or
from MTZ file or from EM map.

If keyword `FUN=T <#fun_t>`__, rather than computing the Rotation
Function, the program reads rotation function results from file
`FILE_T <#file_t>`__ ( or "molrep_rf.tab"): "Sol_ peak number, polar
angles (theta,phi,chi) and shift (sx,sy,sz)"

.. _phase:
 
PHASE < name >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: none

BLANC file of phases. If input Fobs file is CIF use 'PHASE +'. It means
to use the phases from CIFile.

.. _nosg: 
 
NOSG <nosg>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

Number of new space group if you want to change the space group for the
file of structure factors. Program just changes space group name, group
number and cryst. symmetry operators, but not cell and data.

.. _sg: 
 
SG <name>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: < >

New space group name. "ALL" means to check all possible SG.

.. _diff:  
 
DIFF < N | P | F | M | H >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

+-------+-------------------------------------------------------------+
| **N** | use unmodified structure factors                            |
+-------+-------------------------------------------------------------+
| **P** | use modified stucture factors instead of Fobs for RF, as    |
|       | follows: sqrt(|Iobs-Imod2*(P2/100)|)                        |
+-------+-------------------------------------------------------------+
| **F** | use modified stucture factors instead of Fobs for RF, as    |
|       | follows: vector difference (Fobs - Fmod2*(P2/100))          |
+-------+-------------------------------------------------------------+
| **M** | remove fixed model from diff. fourie by mask (model_2).     |
+-------+-------------------------------------------------------------+
| **H** | for heavy atom search                                       |
+-------+-------------------------------------------------------------+

.. _p2:
 
P2 <p2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

Percentage of model_2 in the structure.

.. _nref: 
 
NREF <ncycle>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <5>

number of cycles of rigid body refinement.

.. _stick: 
 
STICK < N | Y >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <Y>

Choose from symmetry-related models closest to which found before (this
option does not work with pseudo-translation possibility).

.. _file_s: 
 
FILE_S <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

File with sequence for model correction by sequence alignment.

.. _keyNMR:  
 
NMR < 0 | 1 | 2 | 3 >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

+-------+-------------------------------------------------------------+
| **0** | use PDB file with NMR structures as single model            |
+-------+-------------------------------------------------------------+
| **1** | use NMR possibility only for RF                             |
+-------+-------------------------------------------------------------+
| **2** | use NMR possibility for RF and TF. Best NMR model will be   |
|       | found and used as solution.                                 |
+-------+-------------------------------------------------------------+
| **3** | use NMR possibility for RF and TF. Averaged TF will be      |
|       | used. All NMR models will be used as solution.              |
+-------+-------------------------------------------------------------+

.. _lock:
 
LOCK < Y | N >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

Locked Cross Rotation function will be performed. Use also keywords:
`FILE_TSRF <#file_tsr>`__ and `NSRF <#dyad_nsrf>`__

Without FILE_TSRF program computes Self_RF and uses NSRF peaks from it.

.. _self:
 
SELF < N | A >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

**A** means after RF to compute Self_RF and use from RF only related
peaks for TF.

If you like you can use keyword: `NSRF <#dyad_nsrf>`__

.. _score:  
 
SCORE < Y | N | C >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <Y>

===== ========================================
**N** do not stop if contrast is good
**C** Corr.Coef. instead Score and do not stop
===== ========================================

.. _keywords_special_dyad: 
 
 Keywords specific for multi-copy search
---------------------------------------------------------------------------------------------------

.. _dyad:
 
DYAD < N | Y | D | M >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

===== ====================
**Y** multi-copy search
**D** dyad search
**M** multi-monomer search
===== ====================

.. _dyad_dist:  
 
DIST <Dmin,Dmax,Dpar>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Three distances for dyad search.

======== ===============================================================
**Dmin** Default: radius of gyration. minimal distance between molecules
**Dmax** Default: 1000Å. maximal distance between molecules
**Dpar** Default: 1000Å. maximal shift along rotation axis
======== ===============================================================

.. _dyad_axis: 
 
AXIS <Chi,Delta>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0,0>

Chi
   check only rotation by Chi (in degrees). 0 means to check all
   orientations.
Delta
   delta for Chi (in degrees)

.. _dyad_nsrf: 
 
NSRF <nsrf>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

Number of peaks of Self-RF which will be used. 0 means not to use
Self-RF. A list of Self-RF peaks will be taken from file defined by
keyword `FILE_TSRF <#file_tsr>`__ which must be prepared in advance (see
`Self Rotation Function <#self_rotation_function>`__).

.. _dyad_nptd:

NPTD <nptd>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Number of peaks in the `STF <#stf>`__ (Special Translation Function) to
be used.

.. _dyad_npt:  
 
NPT <nptd>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Number of peaks in TF to be used.

.. _dyad_np2:  
 
NP2 <np2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Number of peaks in RF for second searching model to be used for dyad
search.

.. _dyad_filem2:  
 
FILE_M2 <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

file of second searching model

.. _dyad_filet2: 
 
FILE_T2 <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

file with list of peaks of RF for second searching model

.. _all: 
 
ALL < N | Y >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

if ALL = Y , program will use all Crystallographical Symmetry Operators

.. _nml:   
 
NML <nml>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

number of Nmers to check (DYAD = "M").

.. _keywords_special_nomodel:
 
 Keywords for Self Rotation Function
-----------------------------------------------------------------------------------------------------

Without a file of the model, the program computes a Self Rotation
Function.

.. _chi: 
 
CHI <chi>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <60>

Angle chi of additional fourth section of RF(theta,phi,chi).

.. _scale:
 
SCALE <scale>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <6>

Maximum value of RF is SCALE * SIGMA(RF).

.. _file_tsr:
 
FILE_TSR <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <*molrep_srf.tab*>

Input or output TAB_file with peaks of Self_RF.

Keywords for EM or electron density model:
------------------------------------------

.. _dscalem:  

**DSCALEM** <scale>

Default: <1>

scale factor of correction of density cell

.. _inverm:  

INVERM < N | Y >
~~~~~~~~~~~~~~~~~

Default: <N>

If Y , inverted phases will be used

.. _rolim: 

ROLIM <limit>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <not used>

minimal value of density which will be used

.. _drad:  

DRAD <radius>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

radius of the model (in A). If parameter DRAD is defined program will
use the density only inside the sphere with radius = DRAD and with
centre in vector ORIGIN.

.. _origin:  

ORIGIN <vector>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0,0,0>

center of the model in the cell (in fract.units)

Keywords for EM or electron density instead of Fobs:
----------------------------------------------------

.. _dscale: 

DSCALE <scale>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <1>

scale factor of correction of density cell

.. _inver: 


INVER < N | Y >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

If Y , inverted phases will be used

.. _dlim: 


DLIM <limit>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <not used>

minimal value of density which will be used

Keywords for Rigid Body Refinement:
-----------------------------------

.. _dom:  
 
DOM < N | Y | I | S | C >
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <N>

**N**

RB refinement as single body.

**Y**

Multi-domain refinement.

**I**

Give only information about molecule-domain structure. Useful for RB
refinement with constraints.

**S**

Multi-domain refinement with constraints.

**C**

only create complete model using NCS parameters. See `How to define
NCS <#howNCS>`__

Keywords for NCS parameters:
----------------------------

See `How to define NCS <#howNCS>`__

.. _ncs: 


NCS <ncs_id>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0>

NCS identifier or = "1" which means to use NCS parameters from file.

.. _angles:


ANGLES <theta,phi,chi>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0,0,0>

Polar angles of NCS which define the standard system orientation in the
cell.

.. _centre: 


CENTRE <vector>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Default: <0,0,0>

position of the NCS centre in the cell (in fract.units)

Keywords for Finding or Searching Heavy atoms:
----------------------------------------------

.. _file_der:   
 
FILE_DER <filename>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input file of derivative or use labels for MTZ file.

.. _batch:  
 


COMMAND (BATCH) FILE
--------------------

The best and easiest way to prepare a command file is to run MOLREP once
by dialogue. The program creates a command (batch) file ( *molrep.btc* )
automatically.

See some `command (batch) file examples <#batch_examples>`__.

.. _CCP4:  
 


MOLREP VERSION TO READ MTZ file
-------------------------------

Keywords for reading MTZ file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following keywords are necessary only for MTZ files.

=========== ===============================
**F**       label of F or F(+)
**SIGF**    label of sigma F or sigma F(+)
**F(-)**    label of F(-)
**SIGF(-)** label of sigma F(-)
**I**       Label of Intensity of hkl
**SIGI**    Standard deviation of the above
**I(-)**    label of Intensity of -h -k -l
**SIGI(-)** Standard deviation of the above
**PH**      Label of phases
**FOM**     Label of figure of merit
**FD**      Label of F-derivative
**SIGFD**   Label of sigma F-derivative
**DP**      Label of !F(+)!-!F(-)!
**SIGDP**   Label of DP
=========== ===============================

.. _testing:


TESTING THE MOLREP PACKAGE
--------------------------

(also you can use this as tutorial)

In directory "../molrep/molrep_check/" there are two files:

#. .../molrep/molrep_check/mr.bat - command (batch) X-ray testing
#. .../molrep/molrep_check/em.bat - command (batch) EM testing

In directory "../molrep/molrep_check/data/" there are several files for
X-ray test:

In directory "../molrep/molrep_check/em/" there are several files for EM
test:

See "../molrep/molrep_check/readme"

.. _theory:  
 


MOLECULAR REPLACEMENT METHOD - THEORY
-------------------------------------

.. _theory_mr_method:  
 
 Molecular replacement method (MR)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two major steps in the Molecular replacement method:
orientation and translation search. They are performed by Rotation and
Translation function. Both of them are correlation functions (or
overlapping functions) between observed and calculated from model
Patterson.

Rotation function ( **RF** ):

::

                 ROT(R) = I Pobs(r) * Pcalc(R,r) dr
                         rad

where

   **R**
      operator of rotation
    I
   rad
      integral inside a sphere in the centre of patterson with
      radius=rad ( *i.e.* the cut-off radius)
   P :sup:`obs`
      observed Patterson
   P :sup:`calc`
      calculated Patterson for rotated ( **R** ) model

Translation function ( **TF** ):

::

               TR(s)  = I Pobs(r) * Pcalc(s,r) dr  =
                       cell

                      = Sum ( I Pobs(r) * Pcalc_ij(s,r) dr) = Sum TRij(s)  
                        i#j                                    i#j

where

   s
      vector of translation
   I
      integral
   i,j
      cryst. symmetry operator numbers
   P :sup:`calc_ij`(s,r)
      calculated Patterson for model corresponding to ith operator and
      model corresponding to jth operator
   TR :sup:`ij`(s)
      translation function of Pattersons P :sup:`obs`(r) and
      P :sup:`calc_ij`(s,r).



   The Translation Function is the sum of translation functions for each
   pair of different cryst. symmetry operators.

The best rotation function algorithm is the Crowther Fast Rotation
Function which we use here. It utilizes FFT. MOLREP can compute the
Rotation Function for three different orientations of the model and
average them. That reduces the noise of Rotation function.

Translation function algorithm was developed by the author and performs
calculations in the reciprocal space using FFT.

There are two major differences from other translation functions.

#. Instead of summation of the translation functions for two operators
   TR :sup:`ij`, we use their multiplication which makes the resulting
   map far more contrast-rich.

#. Finally we can multiply the translation function with the Packing
   Function to remove peaks corresponding to incorrect solutions with
   bad packing.

      Packing function ( **PF** ):

      The overlap for the model in the position s

      ::

             
         O(s) = Sum (I Ro_i(r,s) * Ro_j(r,s) dr),  where i#j
                i,j                  

      where Ro_i(r,s) is the electron density of the model which
      corresponds to the ith cryst. symmetry operator.

      Packing function:

      ::

             
         P(s) = 1 - scale * O(s)

      scale is chosen to make overlap of one copy with itself equal one,
      i.e. to make

      ::

          I Ro_i(r,s) * Ro_i(r,s) dr = 1

      Finally we have P(s) = 1 for no overlap. Negative values are
      converted to 0.

      If we have a fixed model

      ::

          
         O(s) = Sum (I Ro_i(r,s) * Ro_j(r,s) dr) + Sum (I Ro_fixed(r) * Ro_i(r,s) dr)

      The algorithm of calculation of the Packing Function is similar to
      the one for the Translation Function and performed by the same
      program.

      Finally the 'advanced' Translation function is:

      ::

                     TR(s)  = [  M  TRij(s) ] * P(s)
                                i#j

      where M means multiplication of different TR :sup:`ij`.

.. _scaling_by_Patterson: 
 
 Scaling by Patterson
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For scaling we use a completely new strategy based on the Patterson
origin peak which is approximated by a Gaussian. This peak is computed
for both the observed and calculated amplitudes, and each case the
B_overall is computed. The difference

B_diff_overall = B_obs_overall - B_calc_overall

is then added to calculated B_overall so as to make the width of the
calculated Patterson origin peak equal to the observed peak. This method
makes it possible to have a good approximation for the scaling problem
even if only low resolution data is available where other methods do not
work. Scaling by Patterson is also useful for the Cross Rotation
Function where we have different cells for the model and the unknown
structure.

.. _soft_low_resolution_cut-off:  
 
Low resolution cut-off (Boff)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Low resolution cut-off introduces systematical errors in the electron
density especially near the surface of the model. This is known as the
series termination effect. Instead of using the usual low resolution
cut-off, MOLREP multiplies the modules of the structure factors by a
special coefficient:

   ::

      Fnew = Fold (1-exp(-Boff*s2)), where Boff= 2resmin2

Boff is called the "soft low resolution cut-off", which allows removal
of structure factors in this resolution range without inroducing the
series termination effect.

.. _priori_knowledge:  
 


The use of a priori knowledge of similarity and completeness of the model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For low similarity the high resolution reflections are weighted down.
For this, MOLREP uses an additional overall factor Badd:

   ::

      Fnew = Fold exp(-Badd*s2)

Value of similarity 'SIM' can be: from 0.1 to 1.0. It corresponds to
Badd: from (B_limit-Boverall) to -Boverall, where B_limit + 80.

SIM=1 means normalized F will be used.

For low completeness, *e.g.* when there are several molecules in the
a.u., the contribution of low resolution reflections is weighted down.
To manage the completeness of the model, MOLREP uses a low resolution
cut-off (Boff). Completeness of model 'COMPL' can be : from 0.2 to 1.0.
It corresponds to Boff: from 400 to 1600.

Functions of electron density searching ( SM )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We suggest a new approach to divide a phased six-dimensional search into
three steps:

#. A `spherically averaged translation function <#SAPTF>`__ is used to
   locate the position of a molcule or its fragment. It compares locally
   spherically averaged experimental electron density with that
   calculated from the model and tabulates highly probable positions
   accordingly.
#. Then for each position a local `phased rotation function <#PRF>`__ is
   used to find the orientation of the molecule.
#. The third step is the `phased translation function <#PTF>`__, used to
   check and refine the found position.

Spherically averaged phased translation function SAPTF
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SAPTF gives the expected position of a model in an electron density map
by the comparison of spherically averaged density of the model with
locally spherically averaged observed density.

   ::

      SAPTF(s) = I Robs(r,s) * Rcalc(r) dr
               rad(s)

where

     I
   rad(s)
      integral inside a sphere centred in point s of electron density
      with radius=rad ( *i.e.* the cut-off radius)
   R :sup:`obs`
      spherically averaged around point s observed electron density
   R :sup:`calc`
      spherically averaged around origin of coordinate system calculated
      electron density for model

Phased Rotation function ( PRF )
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PRF gives the orientation of model placed in some point of electron
density.

   ::

      PROT(O) = I Robs(r) * Rcalc(O,r) dr
              rad(s)

where

   O
      operator of rotation
     I
   rad(s)
      integral inside a sphere centred in point s of electron density
      with radius=rad
   R :sup:`obs`
      observed electron density
   R :sup:`calc`
      calculated electron density for rotated (O) model

Phased Translation function  **PTF**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Translation search in electron density map.

   ::

      PTR(s)  = I Robs(r) * Rcalc(s,r) dr
              cell

where

   s
      vector of translation
   I
      integral
   R :sup:`obs`
      observed electron density
   R :sup:`calc`(s,r)
      calculated electron density for model placed in the vector s

.. _fm:

Fitting two models ( **FM** )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fitting through electron density. Second model (MODEL_2) is the target
model which converted to electron density. To search the best
overlapping of electron densities of models there are two algorithms:

#. Rotation Function (Patterson) and Phased Translation Function
   (electron density).
#. All functions for electron density. Spherically Averaged Phased
   Translation Function gives expected position for model. Phased
   Rotation Function for expected position gives orientation. Phased
   Translation Function checks and refines the translation vector.

Special Translation Function (
   
.. _STF:
 
 **STF** ) for dyad search
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Multi-copy search
^^^^^^^^^^^^^^^^^

Search two copies of a model simultaneously. There are three stages to
this:

#. Rotation function. The program checks all pairs of first NP peaks of
   Rotation Function (RF). For each pair the program uses the first
   rotation to prepare model-1. Model-2 will be prepared by using the
   second rotation and one rotation from the crystallographical symmetry
   operatators.
#. Next, for the current pair (model-1 and model-2): MOLREP computes the
   Special Translation Function (STF) to find the inter-molecular vector
   of this dyad.
#. For NPT peaks of the previous Special Translation Function (STF)
   ( *i.e.* for NPT inter-molecular vectors) the program computes a
   standard Translation Function (TF) using the current dyad as model
   and calculates a Correlation Coefficient for first NPTD peaks of TF.

Special Translation Function (STF)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Imagine two models in the asymm. part of the unit cell:

   F1(h)
      structure factor of model_1 with the centre of gravity in the
      origin of the coord. system
   F2(h)
      structure factor of model_2

Let

   S1
      vector in unit cell from the origin of the coord. system to the
      centre of gravity of model_1
   S2
      vector for model_2

When F(h) is the total structure factor (for the whole crystal
structure):

   ::

      F(h) = F1(h)exp(-2pihS1) + F2(h)exp(-2pihS2)

Then the Patterson is:

   ::

      P(h) = F(h)*F'(h)

             = F1(h)*F1'(h)
              + F1'(h)*F2(h)*exp(-2pih(S2-S1))
              + F2'(h)*F2(h)
              + F1(h)*F2'(h)*exp(-2pih(S1-S2))

             = P0(0) + P1(S2-S1) + P1(S1-S2)

The Special Translation Function is a Phased TF with a Patterson
function as electron density and P1 = F1'(h)*F2(h) as structure factors
of the model. Solution of this function is the dyad vector S1-S2.

.. _aniso_scaling:
 
 Anisotropic correction and scaling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   ::

      Aniso correction:

        For Structure Factors we can estimate:
                 
           1.  isotropic B_overal:

                 F(s) ~ Scale_overall * exp (-B_overall*s^2) 

           2.  anisotropic B_overall (tensor) : 

                 F(s) ~ Scale_overall * exp(-(B11a*a*hh +2B12a*b*hk+..)

          
           Aniso correction means to make data isotropic with B_overall:


         F_new(s) = F_old(s) * exp(+(B11a*a*hh +2B12a*b*hk+..) * exp(-B_overall*s^2) 
              

      Aniso scaling:

             Fnew = Scale*Fold*exp(-(B11a*a*hh +2B12a*b*hk+..)

             Scale ans aniso B are taken by mimimization: sum(!Fobs-Fnew!)

.. _input_examples:  
 


INPUT FILE EXAMPLES
-------------------

A. Example of CIF file of amplitudes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

            data_structure_9ins
            _cell.length_a      100.000
            _cell_length_b      100.000
            _cell.length_c      100.000
            _cell.angle_alpha    90.000
            _cell.angle_beta     90.000
            _cell.angle_gamma    90.000
            _symmetry.space_group_name_H-M  'P 1 21 1'
            loop_
            _refln.index_h
            _refln.index_k
            _refln.index_l
            _refln.F_meas_au
            _refln.F_meas_sigma_au
               2  3   4    12.3   1.2
              -2 -3  -4    11.4   1.1
             . . . . . . . . . . . . .

For intensities use:

::

            _refln.intensity_meas 
            _refln.intensity_sigma 

B. Example of CIF file of amplitudes with phases:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

            data_9ins
            _cell.length_a      100.000
            _cell_length_b      100.000
            _cell.length_c      100.000
            _cell.angle_alpha    90.000
            _cell.angle_beta     90.000
            _cell.angle_gamma    90.000
            _symmetry.space_group_name_H-M  'P 1 21 1'
            loop_
            _refln.index_h
            _refln.index_k
            _refln.index_l
            _refln.F_meas_au
            _refln.F_meas_au_sigma
            _refln.phase_calc
            _refln.fom
               1   0   0    3468.4934   138.7397    0.746  1.000 
               2   0   0     618.4012    24.7360   11.948  1.000 
             . . . . . . . . . . . . . . . . . . . . . . . . . . . 

Phases are in degrees.

C. Example of PDB file of amplitudes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

          HEADER   R2SARSF   15-JAN-91
          COMPND   RIBONUCLEASE SA (E.C.3.1.4.8) COMPLEX WITH 3'-*GUANYLIC ACID 
          SOURCE   (STREPTOMYCES $AUREOFACIENS)
          AUTHOR   J.SEVCIK,E.J.DODSON,G.G.DODSON
          CRYST1  64.900   78.320   38.790  90.00  90.00  90.00 P 21 21 21    8
          CONTNT   H,K,L,S,FOBS,SIGMA(FOBS)
          FORMAT   (2(I3,2I4,2F7.0,F6.0,9X))
          COORDS   2SAR
          REMARK  1 TWO REFLECTIONS PER RECORD.
          REMARK  2 DMIN=1.85, DMAX=16.28
          CHKSUM  1 MIN H=0,MAX H=34,MIN K=0,MAX K=41,MIN L=0,MAX L=20
          CHKSUM  2 TOTAL NUMBER OF REFLECTIONS=17346
          CHKSUM  3 TOTAL NUMBER OF REFLECTION RECORDS=8673
          CHKSUM  4 SUM OF FOBS=0.235499E+07
            0   0   3     60      9    16           0   0   4    106    307    25
            0   0   5    166     23    20           0   0   6    239    657    52
            0   0   7    326      0    38           0   0   8    425    511    40
          . . . . . . . . . . . . . . . . . . . . . .

D. Example of simple formatted file of amplitudes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this case the assumption is that order of data is H,K,L,F,sig(F)

::

                      
               2  3   4    12.3   1.2
              -2 -3  -4    11.4   1.1
              . . . . . . . . . . . . .

                  or 

               2  3   4    12.3  
              -2 -3  -4    11.4  
              . . . . . . . . . 

The length of file records must not exceed 80 characters. The format of
the records is free, *e.g.* data must be separated by blancs (be careful
- some PDB files do not satisfy this rule).

.. _batch_examples:
 


COMMAND (BATCH) FILE EXAMPLES
-----------------------------

(see also `Testing_program_MOLREP <#testing>`__ as tutorial)

BATCH file example of Cross Rotation and Translation functions:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
    molrep -f fobs.dat -m mm1.crd -i <<stop
   # --------------------------------
   #
   _NP   8
   _RAD 27
   _ANISO C
   _sim   .1
   _compl .5
   stop

BATCH file example of Self Rotation function:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::


   # --------------------------------
   molrep -f fobs.dat -i <<stop
   # --------------------------------
   _RAD 27
   stop

BATCH example with MTZ file:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -f p1.mtz -m p1_pdb.pdb -i <<stop
   # --------------------------------
   _F  FO
   _SIGF  SDFO
   _NP   8
   _ANISO C
   _sim   .1
   _compl .5
   stop

.. _batch-example-with-mtz-file-1:

BATCH example with MTZ file
~~~~~~~~~~~~~~~~~~~~~~~~~~~

For searching in the electron density map for some model (standard
Rotation Function will be used):

::

   # --------------------------------
   molrep -f p1.mtz -m mod.pdb -i <<stop
   # --------------------------------
   _F  FO
   _SIGF  SDFO
   _PH    PH_FO
   _NP    8
   stop

BATCH file example of fitting two models:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -m mod1.pdb -mx mod2.pdb -i <<stop
   # --------------------------------
   _PRF Y
   stop

BATCH file example of dyad search:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -f fobs.dat -m mod1.pdb -i <<stop
   # --------------------------------
   _dyad y
   _axis 0,10
   _dist 0,300,300
   _NPT  3
   _NPTD 3
   stop

BATCH file example of dimer search:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -f fobs.dat -m mod1.pdb -i <<stop
   # --------------------------------
   _dyad y
   _axis 180,10
   _dist 0,300,1
   _NPT  3
   _NPTD 3
   stop

BATCH file example dimer search for Self-RF orientations:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -f fobs.dat -m mod1.pdb -i <<stop
   # --------------------------------
   _dyad y
   _axis 180,10
   _dist 0,300,1
   _NSRF 20
   _NPT  3
   _NPTD 3
   stop

BATCH file example of using file of sequence
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   # --------------------------------
   molrep -f mtz.mtz -m 1hpg.pdb -s new.seq -i <<stop
   # --------------------------------
   _F  FP
   _SIGF  SIGFP
   _NP   8
   _NMON 2
   _sim   .1
   _compl .5
   stop

.. _howNCS: 
 


How to define NCS
-----------------

Program supports the point group symmetry.

+-----------------------------------+-----------------------------------+
| **NCS_ID**                        | Point group description.          |
+-----------------------------------+-----------------------------------+
| **N00**                           | Point group is N. For example     |
|                                   | Point group is 7, NCS_ID is 700.  |
|                                   | Standard orientation: Nfold axis  |
|                                   | along Z.                          |
+-----------------------------------+-----------------------------------+
| **N20**                           | Point group is N2. For example    |
|                                   | Point group is 72, NCS_ID is 720. |
|                                   | Standard orientation: Nfold axis  |
|                                   | along Z, twofold axis along X.    |
+-----------------------------------+-----------------------------------+
| **N22**                           | Point group is N22. For example   |
|                                   | Point group is 422, NCS_ID is     |
|                                   | 422.                              |
|                                   | Standard orientation: Nfold axis  |
|                                   | along Z, twofold axis along X.    |
+-----------------------------------+-----------------------------------+
| **230**                           | Point group is 23.                |
|                                   | Standard orientation: twofold     |
|                                   | axis along Z, another twofold     |
|                                   | axis along X.                     |
+-----------------------------------+-----------------------------------+
| **432**                           | Point group is 432.               |
|                                   | Standard orientation: fourfold    |
|                                   | axis along Z, another fourfold    |
|                                   | axis along X.                     |
+-----------------------------------+-----------------------------------+
| **532**                           | Point group is 532.               |
|                                   | Standard orientation: fivefold    |
|                                   | axis along Z, projection closest  |
|                                   | (to Z axis) threefold axis in     |
|                                   | plan XY along X.                  |
+-----------------------------------+-----------------------------------+

Polar angles theta, phi, chi define the standard system orientation in
the cell. Theta, phi - polar coordinates of Z standard axis. Chi - angle
of rotation around theta-phi-axis (Z standard axis) which bring X axis
to standard X axis.

cx,cy,cz (fract.units) define the position of group centre in the cell.

It is possible to define NCS parameters using keywords or in input PDB
file.

**Definition by keywords**
~~~~~~~~~~~~~~~~~~~~~~~~~~

Input PDB file must contain only one molecule.Use keywords:

   `NCS <#ncs>`__, `ANGLES <#angles>`__, `CENTRE <#centre>`__

| NCS - NCS_ID
| ANGLES - theta, phi, chi
| CENTRE - cx,cy,cz

**Definition in input PDB file**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First (reference) molecule must be started with line (free format):

#MOLECULE NCS_ID theta phi chi cx cy cz

Other molecules must be started with line:

#MOLECULE Nmol theta phi chi

where:

Nmol - molecule number.

theta phi chi - Polar angles of rotation from first molecule to current
one.

For example: point group is 3.

   ::

      HEADER    HYDROLASE (ENDORIBONUCLEASE)         
      CRYST1   64.900   78.320   38.790  90.00  90.00 ...
      #MOLECULE  300    0   0  0  .5  .5  .5
      #DOMAIN     1 
      ATOM      1  N   ASP A   1      45.161  12.836 ... 
      ATOM      2  CA  ASP A   1      45.220  12.435 ...   
       ... 
      ATOM    745  SG  CYS A  96      58.398   6.673 ... 
      ATOM    746  O   CYS A  96      62.238   7.178 ...  
      #DOMAIN     2 
      ATOM    747  N   PHE A  97      44.487  11.386 ...  
      ATOM    748  CA  PHE A  97      44.559  11.129 ... 
       ...
      ATOM    945  C   VAL A 196      58.398   6.673 ... 
      ATOM    946  O   VAL A 196      62.238   7.178 ...  
      #DOMAIN     1 
      ATOM    947  N   ASP A 197      44.487  11.386 ...  
      ATOM    948  CA  ASP A 197      44.559  11.129 ... 
       ...
      #MOLECULE  2    0   0  120  
      #DOMAIN     1 
      ATOM      1  N   ASP A   1      45.161  12.836 ... 
      ATOM      2  CA  ASP A   1      45.220  12.435 ...   
       ... 
      ATOM    745  SG  CYS A  96      58.398   6.673 ... 
      ATOM    746  O   CYS A  96      62.238   7.178 ...  
      #DOMAIN     2 
      ATOM    747  N   PHE A  97      44.487  11.386 ...  
      ATOM    748  CA  PHE A  97      44.559  11.129 ... 
       ...
      ATOM    945  C   VAL A 196      58.398   6.673 ... 
      ATOM    946  O   VAL A 196      62.238   7.178 ...  
      #DOMAIN     1 
      ATOM    947  N   ASP A 197      44.487  11.386 ...  
      ATOM    948  CA  ASP A 197      44.559  11.129 ... 
       ...
      #MOLECULE  3    0   0  240  
      #DOMAIN     1 
      ATOM      1  N   ASP A   1      45.161  12.836 ... 
      ATOM      2  CA  ASP A   1      45.220  12.435 ...   
       ... 
      ATOM    745  SG  CYS A  96      58.398   6.673 ... 
      ATOM    746  O   CYS A  96      62.238   7.178 ...  
      #DOMAIN     2 
      ATOM    747  N   PHE A  97      44.487  11.386 ...  
      ATOM    748  CA  PHE A  97      44.559  11.129 ... 
       ...
      ATOM    945  C   VAL A 196      58.398   6.673 ... 
      ATOM    946  O   VAL A 196      62.238   7.178 ...  
      #DOMAIN     1 
      ATOM    947  N   ASP A 197      44.487  11.386 ...  
      ATOM    948  CA  ASP A 197      44.559  11.129 ... 
       ...

Alternative way is to use only first molrecule (with NCS parameters in
the file) and generate complete model automaticly. In pure RB refinement
use keyword DOM = 'C'. For fitting model into map (i.e. SAPTF+PRF+PTF
use keyword NCS = 1).

.. _redirection:
 


How to redirect output and scratch files
----------------------------------------

In command string you can use options: "-po" and "-ps".

For example:

::

        Usual MR with sequence and redirect output and scratch files
     molrep -f file.mtz -m model.pdb -s file_seq -po out/ -ps scr/


   
.. _convention:  
 
Convention for rotation 



     Rotation by Euleran angles Alpha, Beta, Gamma:

        euleran angles : 1. A( Z ) - alpha around axis Z
                         2. B( Y') - beta  around new axis Y
                         3. G( Z') - gamma around new axis Z

     Rotation by Polar angles Theta, Phi, Chi:

                       polar coordinates Theta, Phi of rotate axis:
    
          Theta     -  angle between  rotate axis and Z
          Phi       -  angle in plan XY between X and projection rotate axis

          Chi       -  rotation angle arount rotate axis

Convention for Orthonormal coordinate system
--------------------------------------------

::

          Orthonormal axes are defined to have:
    
          A parallel to X , Cstar parallel to Z

.. |stereoproj| image:: gif/stereoproj.gif
