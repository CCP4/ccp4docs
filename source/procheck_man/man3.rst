|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

3. Input requirements
=====================

The only input required for **PROCHECK** is the **PDB file** (`Bernstein
*et al.*, 1977 <manrefs.html#PDBREF>`__) holding the coordinates of the
structure of interest.

For **NMR** structures, each model in the ensemble should be separated
by the correct **MODEL** and **ENDMDL** records (see `Appendix B:
Brookhaven **PDB** file format <manappb.html>`__). Only the first model
will be analysed. A separate program, **PROCHECK-NMR** deals
specifically with the analysis of **NMR** structures.

--------------

|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: man2.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next section| image:: rightb.gif
   :target: man4.html
