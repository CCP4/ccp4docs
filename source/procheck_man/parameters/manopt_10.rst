|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

10. Distorted geometry plots
============================

There are **9** parameters defining the `**Distorted geometry
plots** <../examples/plot_10.html>`__, as follows:-

--------------

::

    10. Distorted geometry plots
    ----------------------------
    0.05  <- Deviation from "ideal" bond-length (A)
    10.0  <- Deviation from "ideal" bond-angle (degrees)
    0.03  <- RMS distance from plane for ring atoms
    0.02  <-  "      "      "    "   for other atoms
    N     <- Produce a COLOUR PostScript file (Y/N)?
    CREAM        <- Background colour of page
    BLUE         <- Colour of "ideal" bond-lengths and angles
    RED          <- Colour of actual bond-lengths/angles/planes
    GREEN        <- Colour for lettering showing differences from ideals

--------------

Description of options:-
------------------------

-  **Deviation from 'ideal' bond length (Å)** - This option defines
   which main-chain bonds are to be plotted as distorted. All
   bond-lengths that deviate by more than this amount from the Engh &
   Huber 'ideal' values will be plotted.
-  **Deviation from 'ideal' bond angle (degrees)** - This option defines
   which main-chain bond angles are to be plotted as distorted. All
   bond-angles that deviate by more than this amount from the Engh &
   Huber 'ideal' values will be plotted.
-  **RMS distance from plane** - These two options defines which planar
   groups are to be plotted as distorted. All planar groups that deviate
   by more than this amount from planarity will be plotted. The two
   different values apply separately to ring groups (Phe, Tyr, Trp and
   His) and to planar end-groups (Arg, Asn, Asp, Gln and Glu).
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the 'names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colours for the 'ideal' and actual bond lengths and angles
   and for the lettering showing the differences from the ideals.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_09.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_list.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_list.html
