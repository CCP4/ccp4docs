|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Listing options
===============

The following options govern the output in the **.out** file which gives
the residue-by-residue listing (see `Appendix D <../manappd.html>`__).

The **6** options are:-

--------------

::

    Listing options
    ---------------
    Y   <- Print explanatory text at head of parameters listing (Y/N)
    N   <- Print only asterisks denoting deviations, and not the values (Y/N)
    N   <- Print only highlighted residues (Y/N)
    0.0    <- Min. deviation from ideal required for parameter to be printed
    66     <- Number of lines per page on parameters listing
    Y   <- List the bad contacts (Y/N)

--------------

Description:-
-------------

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_10.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_cols.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_cols.html
