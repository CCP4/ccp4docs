#!/usr/bin/env python

““” Pandoc filter to convert all regular text to uppercase. Code, link
URLs, etc. are not affected. ““”

from pandocfilters import toJSONFilter, RawBlock, Div

def rst_link(ident): return RawBlock(‘rst’, f’.. {ident}:’)

def linkDivs(key, value, format, meta):

::

   if key == 'Div':
       [[ident, classes, kvs], contents] = value
       new_contents = contents
       return Div([ident, classes, kvs], new_contents)

if **name** == “**main**”: toJSONFilter(linkDivs)
