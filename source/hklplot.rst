HKLPLOT (CCP4: Supported Program)
=================================

NAME
----

**hklplot** - plots a precession photo from an HKL data file

SYNOPSIS
--------

| **hklplot hklin** *foo[.mtz]* **plot** *foo[.plt]*
| [`Keyworded input <#keywords>`__]

SEE ALSO
--------

Consider using **hklview** rather than **hklplot** ( **hklview** is an
X-windows based display program doing more or less the same thing).

.. _description: 
 
DESCRIPTION
---------------------------------

| This program plots a *simulated precession photograph* from an HKL
  file input, and gives the percentage of the total reflections in the
  file for each (sin (theta)/lambda)^2 range. **hklplot** can also
  generate a reciprocal lattice pattern for any cell and spacegroup
  (which can be useful for teaching).
| The output is a *plot84* *metafile,* which can be plotted with
  **xplot84driver** or **pltdev**.

.. _keywords: 
 
 KEYWORDED INPUT
-----------------------------------

The various data control lines are identified by keywords, those
available being:

   `ANOM <#anom>`__, `BINS <#bins>`__, `CELL <#cell>`__, `END <#end>`__,
   `LABIN <#labin>`__, `LIST <#list>`__, `NSPGROUP <#nspgroup>`__,
   `PROJECTION <#projection>`__, `RESOLUTION <#resolution>`__,
   `SCALE <#scale>`__, `SIGMA <#sigma>`__, `SQUARE <#square>`__,
   `SYMBOL <#symbol>`__, `TITLE <#title>`__, `ZONE <#zone>`__

.. _title: 
 
**TITLE** <title>

<title> is a title for plot.

.. _symbol: 
 
**SYMBOL** <num>

<num> is symbol number. See Plot84 writeup. Default is a diamond shape.

.. _resolution: 
 
**RESOLUTION** [ <dmin> ] <dmax>

One or two numbers. Resolution limits either as 4(sin(theta)/lambda)^2
or in Angstroems. If only one number is supplied, this is taken to be
the high resolution cut-off. Either order.

.. _square: 
 
**SQUARE**

Input F's to be squared before plotting.

.. _zone: 
 
**ZONE** <zone1> [ <zone2> ... ]

  zone definitions: *e.g.*
  ZONE h0l h1l 0kl

.. _bins: 
 
**BINS** <bins>

<bins> is the number of bins for analysing completeness.

.. _nspgroup: 
 
**NSPGROUP** <Number> | <Name>

Spacegroup Number or Name. It is possible to over-ride the SpaceGroup in
the MTZ file. Not recommended!

.. _sigma: 
 
**SIGMA** <nsig>

An <nsig>*SIGMA cutoff for reflections is applied. The default is no
cut-off applied.

.. _projection: 
 
**PROJECTION** <ih> <ik> <il> <projht> <prjlim>

  Plot the layer of reciprocal space perpendicular to <ih> <ik> <il> and
  within reciprocal distance <projht> +/-<prjlim> from (0 0 0). This is
  useful for looking for lost 3 fold axes etc..
  ZONE and PROJECTION cards are mutually exclusive.

.. _list: 
 
**LIST**


all reflections plotted are listed to log file.

.. _anom: 
 
**ANOM** POS | NEG

POS(itive) or NEG(ative). Anomalous measurements for F+(hkl) or F-(hkl)
plotted.

BEWARE - this does NOT plot a true precession image with the F(+) and
F(-) in the correct part of reciprocal space. It will plot two near
identical sections which can be overlapped to look for differences.

.. _scale: 
 
**SCALE** <scale>

By default, reflections are automatically scaled to the Largest F in the
file. But this automatic scaling can result in many very insignificant
spots... If <scale> is given all symbols are scaled up or down by
<scale>. All symbols greater than maximum are scaled to the maximum
size.

.. _labin: 
 
**LABIN** <program label>=<file label> ...

Change labels in the input file to match those expected by the program,
which needs F=... and SIGF=...

.. _cell: 
 
**CELL** <a> <b> <c> [ <alpha> <beta> <gamma> ]

Useful if you want to generate the reciprocal lattice pattern (I use it
for illustrating spacegroups).

.. _end: 
 
**END**

last card.

.. _examples: 
 
EXAMPLE
-------

::


   hklplot 
   hklin  {$SCRATCH}mary2.mtz 
   << END-mtzprec
   RESO 10 5.00
   BINS  10
   ZONE 0kl h0l hk0 1kl h1l hk1 
   TITLE w89f c6 test
   NSPGRP 16
   SCALE 0.5 
   SYMB 15
   LABI F=FP SIGF=SIGFP 
   END   
   END-mtzprec

AUTHOR
------

Eleanor Dodson November 1991

.. _see-also-1:

SEE ALSO
--------

`pltdev <pltdev.html>`__, `xplot84driver <xplot84driver.html>`__,
`hklview <hklview.html>`__ 
