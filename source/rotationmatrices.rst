Rotation Matrices (CCP4: General)
=================================

NAME
----

rotationmatrices - on Eulerian angles, polar angles and direction
cosines, and orthogonalisation codes

Contents
~~~~~~~~

-  `General Remarks <#generalremarks>`__
-  `Orthonormal Axes, and Orthogonalisation
   Codes <#orthogonalisationcodes>`__
-  `Polar Angles <#polarangles>`__
-  `Eulerian Angles <#eulerianangles>`__

.. _generalremarks: 
 


General Remarks
~~~~~~~~~~~~~~~

A rotation matrix is a matrix which moves a body as a rigid unit without
altering its internal geometry. It is defined as a 3 X 3 matrix which
when multiplied by a vector has the effect of changing the direction of
the vector but not its magnitude (adapted from: `Rotation matrix in
Wikipedia <http://en.wikipedia.org/wiki/Rotation_matrix>`__).

A molecule is a body described by a set of coordinates, each of which is
equivalent to a vector from the origin to that atom. If all the
coordinates are rotated by the same matrix, the new rotated set can be
generated by applying this equation to each coordinate of the original
model:

   ::

      [Xrot]    = [ROT] [Xorig]
      [Yrot]            [Yorig]
      [Zrot]            [Zorig]

      where [ROT] = [ R11 R12 R13 ]
                    [ R21 R22 R23 ]
                    [ R31 R32 R33 ]

Such a rotation can be fully described by only three parameters. For
example if we know the direction of the rotation axis relative to some
fixed axes, and the amount of the rotation about this axis, then the
rotation matrix is defined. This method is called that of Polar angles.
Another way of describing the rotation is to rotate the model 3 times in
succession about any 3 non-planar directions. This method is called that
of Eulerian angles. Whatever way the rotation is described, each must
generate the same rotation matrix, and any set of descriptors can be
converted to any other using the appropriate equations.

.. _orthogonalisationcodes: 
 
Orthonormal Axes, and Orthogonalisation Codes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is very complicated to write down the form of the rotation matrix
when rotations are performed relative to general axes. In practice we
always define these rotations relative to axes which are orthonormal,
*i.e.* perpendicular to each other and of equal length. The coordinates
must also be given relative to these axes, which in general are not the
crystallographic axes.

A system is orthogonal if its vectors are pairwise orthogonal ( *i.e.*
perpendicular). See `orthogonal
matrix <http://en.wikipedia.org/wiki/Orthogonal_matrix>`__ and
`orthonormal basis <http://en.wikipedia.org/wiki/Orthonormal_basis>`__.
A basis is orthonormal, or unitary, if it is orthogonal and all vectors
have unit length (from: `Math Reference - Karl
Dahlke <http://www.mathreference.com/la,orth.html>`__).

In the PDB, the default orthogonal system for a crystal has the Xortho
axis, (X :sup:`o`), along the crystallographic a-axis, the Zortho axis,
(Z :sup:`o`), along the crystallographic c*, and the Yortho axis,
(Y :sup:`o`), along the direction completing the system (c* x a).
Other orthogonalisation conventions used occasionally are given below.
It is sometimes necessary to interconvert between conventions and CCP4
programs refer to the different systems using a variable NCODE.

   ===== ====================== ===================
   NCODE orthogonal x y z along remark
   ===== ====================== ===================
   1     a, c*×a, c*           Brookhaven, default
   2     b, a*×b, a*           
   3     c, b*×c, b*           
   4     a+b, c*×(a+b), c*     
   5     a*, c×a*, c            Rollett
   6     a, b*, a×b*           
   7     a*, b, a*×b            TNT
   ===== ====================== ===================

.. _polarangles: 
 
Polar Angles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The CCP4 convention is to define the direction of the axis about which
the rotation κ (kappa) takes place using the direction cosines (l m n)
where

   ::

      ( l )    ( sinω cosϕ )
      ( m )  = ( sinω sinϕ )
      ( n )    ( cosω )

Thus ω is the angle the rotation axis makes to the Z :sup:`o` direction
( *i.e.* from the pole), and ϕ is the angle the projection of the
rotation axis onto the X :sup:`o`-Y :sup:`o`-plane ( *i.e.* the
equatorial plane) makes to the X :sup:`o`-axis.

Since direction cosines are the cosines of the angles a vector makes
with the positive axes of an orthogonal system, they obey l :sup:`2` +
m :sup:`2` + n :sup:`2` = 1.

Then:

   ::

      [ROT] = ( l2+(m2+n2)cosκ          lm(1-cosκ)-nsinκ        nl(1-cosκ)+msinκ )
              ( lm(1-cosκ)+nsinκ        m2+(l2+n2)cosκ          mn(1-cosκ)-lsinκ )
              ( nl(1-cosκ)-msinκ        mn(1-cosκ)+lsinκ        n2+(l2+m2)cosκ   )

This can also be visualised as rotation ϕ about Z, rotation ω about the
new Y, rotation κ about the new Z, rotation (-ω) about the new Y,
rotation (-ϕ) about the new Z.

Note that the rotation matrix generated from (ω,ϕ,κ) is identical to
that generated from (π-ω,π+ϕ,-κ) so it is conventional to restrict κ to
range: 0 to π.

Also note that if ω = 0 or 180, then ϕ is indeterminate.

And note that in some conventions, ψ is used instead of ω, and χ instead
of κ.

It is often convenient in crystallography to present the Polar angles in
sections of κ. For instance, if it is suspected that the crystal could
contain a multimer such as a dimer, trimer, tetramer, pentamer, *etc.*,
then there would be a large self rotation peak at κ = 180, 120, 90, 72
respectively.

.. _eulerianangles: 
 
Eulerian Angles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A rotation can be uniquely defined by rotating the rigid body 3 times in
succession about any 3 non-planar directions. These rotation angles are
called the Eulerian angles.

All CCP4 programs define these in the following way (described in early
papers by Tony Crowther): (α,β,γ) are the Eulerian angles which rotate
the coordinates of crystal 2 (the model) first through γ about the
initial Z-axis, then through β about the new position of the Y-axis,
then through α about the final Z-axis. Positive rotation is clockwise
when looking along the axis from the origin. The rotations are carried
out in the orthogonal frame, which is related to the crystallographic
frame according to the setting of the `ORTH
flag <#orthogonalisationcodes>`__. Beware of the permutations
introduced by ORTH ≠ 1. Schematically:

   ::

      [ROT] = [ROT]α * [ROT]β * [ROT]γ

              [cosα   -sinα   0]   [ cosβ   0   sinβ]   [cosγ   -sinγ   0]
            = [sinα    cosα   0] * [    0   1      0] * [sinγ    cosγ   0]
              [   0       0   1]   [-sinβ   0   cosβ]   [   0       0   1]

              [ cosα cosβ cosγ - sinα sinγ     -cosα cosβ sinγ - sinα cosγ     cosα sinβ ]
            = [ sinα cosβ cosγ + cosα sinγ     -sinα cosβ sinγ + cosα cosγ     sinα sinβ ]
              [ -sinβ cosγ                     sinβ sinγ                       cosβ      ]

Note that the rotation matrix generated from (α,β,γ) is identical to
that generated from (π+α,-β,π+γ) so it is conventional to restrict β to
range: 0 to π.

This description of the rotation is equivalent to keeping the
coordinates fixed and rotating the AXES [I,J,K] first through α, then
through β, then through γ.

   ::

      [ Io  Jo  Ko ][XO1]    = [ Io  Jo  Ko ] [ROT] [XO2]
                    [YO1]                          [YO2]
                    [ZO1]                          [ZO2]

      where [ROT] = [ R11 R12 R13 ]
                    [ R21 R22 R23 ]
                    [ R31 R32 R33 ]

`ACORN <acorn.html>`__, `AMoRe <amore.html>`__,
`MOLREP <molrep.html>`__, `PHASER <phaser.html>`__,
`ALMN <almn.html>`__, `LSQKAB <lsqkab.html>`__, `PDBSET <pdbset.html>`__
and `'dm' <dm.html>`__ use Eulerian angles in this convention to define
the rotation matrices. There are various programs for interconverting
between Euler angles, polar angles and direction cosines.

From rotmat.html:

::

    Rotation matrices to PREMULTIPLY coordinate columns or POSTMULTIPLY axis rows vectors are defined as follows:

         Using Eulerian angles ALPHA BETA GAMMA (CCP4/MERLOT) relative to orthogonal axes (I J K) and defining [R] as the product of:

           Rotation 1 (ALPHA) about K         :  [R]alpha
           Rotation 2 (BETA)  about the new J :  [R]beta
           Rotation 3 (GAMMA) about the new K :  [R]gamma

       (and abbreviating cos(ALPHA) = CA; sin(BETA) = SB etc)

          [ R11 R12 R13 ]
          [ R21 R22 R23 ]  =  [ R]alpha * [R]beta * [R]gamma 
          [ R31 R32 R33 ]
                              [CA -SA 0] [ CB 0 SB] [CG -SG 0]
                           =  [SA  CA 0]*[  0 1  0]*[SG  CG 0] 
                              [ 0   0 1] [-SB 0 CB] [ 0   0 1]

         ( CA CB CG - SA SG      -CA CB SG - SA CG      CA SB )
       = ( SA CB CG + CA SG      -SA CB SG + CA CG      SA SB )
         (         -SB CG                  SB SG           CB )

             Note that the rotation matrix generated from (ALPHA,BETA,GAMMA) is identical to that
             generated from (PI+ALPHA,-BETA,PI+GAMMA) so it is conventional to restrict BETA to range: 0 to PI 
             If you think of coordinates as vector products of the row vector of the AXES by the column vector of orthogonal coordinates

                                [ I J K] * [ X0]
                                           [ Y0]
                                           [ Z0]

             and a rotation matrix [R] as moving these to

                          [ I J K] * [R] * [ X0]
                                           [ Y0]
                                           [ Z0]

             then it is obvious that rotation [R] can be seen either
             as rotating the axes [I J K ] by ALPHA then BETA then GAMMA or
             as rotating coordinates [X0] by -GAMMA then -BETA then -ALPHA. 

SEE ALSO
--------

`ACORN <acorn.html>`__, `ALMN <almn.html>`__, `AMoRe <amore.html>`__,
`'dm' <dm.html>`__, `LSQKAB <lsqkab.html>`__, `MAPROT <maprot.html>`__,
`PDBCUR <pdbcur.html>`__, `PDBSET <pdbset.html>`__,
`POLARRFN <polarrfn.html>`__, `ROTMAT <rotmat.html>`__.

AUTHORS
-------

| Maria Turkenburg and Eleanor Dodson, University of York, England
| Prepared for CCP4 by Maria Turkenburg
