REFMAC (CCP4: Supported Program)
================================

User's manual for the program refmac version 5.*
-------------------------------------------------

DICTIONARY
==========

Dictionary entries from minimum description of the ligand
---------------------------------------------------------

When information about connectivity is available ( *i.e.* which atom is
bonded to what) and about the contents of the ligand ( *i.e.* which
element occupies which position), then a minimum description can be used
to derive a dictionary entry for the ligand. It is a good idea first to
draw the ligand on a piece of paper like:

    ::



                               
                  OH
                  |
            HO    C2    O1-P1(O3)
                /  /
               /  /
                C3   C1
                |    |
                |    |
                C4   C6
               :  / :
              :   \/   :
             O4    C5   OH
             |     |
           P4(O3)  O5
                   |
                   P5(O3)

             Figure 1

where dotted lines show that the atom is below the plane of the
paper/screen and dashed lines above the plane of the paper/screen.

If this picture is converted to the minimum description, it will have
the form as in `Text 1 <Text1.html>`__.

When making a minimum description, one should be careful with the chiral
volumes present in the ligand. For a description of chiral volumes and
their relation of them to R- and S-forms, see `chiral volume
description <../theory/chiral.html>`__.

Then `LIBCHECK <../../libcheck.html>`__ could be run like:

    ::

        libcheck << eol
        N
        _file_l <minimum_description>
        _mon MON
        eol

where <``minimum_description``> is the file containing a minimum
description and ``MON`` is the name of the ligand.

After LIBCHECK there will be a complete dictionary description and
coordinate files in mmCIF and PDB format with all hydrogens present.
