Neutron refinement with REFMAC5/REFMACAT
========================================

REFMAC5/refmacat now supports the refinement of structural models obtained from neutron crystallographic data.
A key new feature introduced in REFMAC5 for neutron data analysis is the refinement of the protium/deuterium (H/D) fraction.
This parameter represents the relative contribution of H and D to the neutron scattering, which is crucial for correctly modelling hydrogen positions in neutron crystallography.

Keywords
---------

**ATOMSF**
   It specifies the location of atomic scattering factors.
   For neutron diffraction those are tabulated in the CCP4 *atomsf_neutron.lib* library in $CLIBD (ccp4-9.0/lib/data) directory.

**SOURCE NEUTRON**
   Use of neutron scattering lengths.

**MAKE HYDR [ ALL | YES | NO ]**
   ALL - generate all hydrogens (H) in their riding positions and use them for geometry and structure factor calculations;
   YES - use H atoms that are already present in the input file;
   NO - ignore H atoms even if they are present in the input coordinate file.

**MAKE HOUT [ YES | NO ]**
   YES - write H atoms to the output file;
   NO - do not write H atoms to the output file.

**HYDROGEN REFINE [ ALL | POLAR | RPOLAR ] (Default ALL)**
   Individual refinement of H atom positions.
   ALL - all H atoms are refined;
   POLAR - only polar hydrogen atoms, i.e., H atoms attached to parent atoms that can play a role of donor in hydrogen bonds;
   RPOLAR - only rotatable polar H atoms are refined, i.e., H atoms with only one bond to parent atoms (H atom on –OH group of Tyr, Ser and Thr and –SH of Cys).

**RESTRAINT TORSION HYDROGEN INCLUDE ALL**
   Use of torsion angles restraints for all H atoms.

**REFINEMENT DFRACTION**
   It enables the refinement of the deuterium fraction parameters for H atoms.

**HYDROGEN DFRACTION [ ALL | POLAR ]**
   It specifies the H atoms for which deuterium fraction parameters are subjected to refinement.
   ALL - in perdeuterated structures where all H atoms are exchanged;
   POLAR - in instances of structures involving H/D exchange, where exclusively polar H atoms undergo exchange.

**HYDROGEN DFRACTION INIT [ 1 | REFINEABLE 1 UNREFINEABLE 0 ]**
   This keyword initialises the deuterium fraction for hydrogen atoms.
   It is used only at the first refinement run when generating hydrogen atoms (MAKE HYDR ALL) to set the initial values of the deuterium fraction parameter.
   1 - for perdeuterated structures, sets the deuterium fraction parameter to 1 for all H atoms.
   REFINEABLE 1 UNREFINEABLE 0 - In the case of H/D-exchanged structures, the deuterium fraction is set to 1 only for H atoms exchanged with D.

.. note:: In the case of perdeuterated structures, it is recommended to only initialise and not refine the deuterium fraction.

Examples
--------

Protocol for H/D exchanged structures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Example 1: Initialisation and deuterium fraction refinement**

Hydrogen atoms are generated at the start of refinement. The deuterium fraction is initialised and then it is refined.

.. code-block:: 

   #!/bin/bash

   refmacat \
   XYZIN input.pdb \    # Input structural model
   HKLIN input.mtz \    # Input reflection data
   XYZOUT output.pdb \  # Output refined structural model
   HKLOUT output.mtz \  # Output refined reflection data
   ATOMSF \
   /Applications/ccp4-9/lib/data/atomsf_neutron.lib \  # Path to neutron scattering lengths library
   << EOF
   SOURCE NEUTRON    # Specify neutron source
   MAKE HYDR ALL     # Generate all hydrogen atoms
   MAKE HOUT YES     # Output hydrogen atoms
   REFINEMENT DFRACTION    # Refine deuterium fraction
   HYDROGEN DFRACTION INIT REFINEABLE 1 UNREFINEABLE 0  # Initialise deuterium fraction for H/D-exchanged structures
   HYDROGEN DFRACTION POLAR  # Refine deuterium fraction for polar hydrogen atoms
   HYDROGEN REFINE ALL   # Refine all hydrogen atom positions
   RESTRAINT TORSION HYDROGEN INCLUDE ALL  # Apply torsion angle restraints to all hydrogen atoms
   END
   EOF

**CCP4I2 interface**

1. Click on the “Task menu” button in toolbar and select: Refinement - Refmacat/Refmac5

.. image:: ../images/task.png

2. Import your model and reflections files

.. image:: ../images/import.png

3. In **Advanced** - Diffraction experiment type, select: Neutron

.. image:: ../images/neutron_tab.png

**Example 2: Deuterium fraction refinement without initialisation**

After the initial refinement run (or during re-refinements),
we can use the hydrogen atoms already present in the input file.
There is no need to reinitialise the deuterium fraction.

.. code-block:: 

   #!/bin/bash

   refmacat \
   XYZIN input.pdb \    # Input structural model
   HKLIN input.mtz \    # Input reflection data
   XYZOUT output.pdb \  # Output refined structural model
   HKLOUT output.mtz \  # Output refined reflection data
   ATOMSF \
   /Applications/ccp4-9/lib/data/atomsf_neutron.lib \  # Path to neutron scattering lengths library
   << EOF
   SOURCE NEUTRON    # Specify neutron source
   MAKE HYDR YES    # Use hydrogen atoms already present in the input file
   MAKE HOUT YES     # Output hydrogen atoms
   REFINEMENT DFRACTION    # Refine deuterium fraction
   HYDROGEN DFRACTION POLAR  # Refine deuterium fraction for polar hydrogen atoms
   HYDROGEN REFINE ALL   # Refine all hydrogen atom positions
   RESTRAINT TORSION HYDROGEN INCLUDE ALL  # Apply torsion angle restraints to all hydrogen atoms
   END
   EOF

.. image:: ../images/neutron_tab2.png

.. note:: Validation reports are currently unavailable. In the **Output** tab, under *Validation and Analysis*, please ensure all boxes are unticked before running the refinement.

Protocol for perdeuterated structures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Example 1: Initialisation without deuterium fraction refinement**

For perdeuterated structures, where all hydrogen atoms are exchanged with deuterium, the deuterium fraction can be set to 1 without the need for refinement.

.. code-block:: 

   #!/bin/bash

   refmacat \
   XYZIN input.pdb \    # Input structural model
   HKLIN input.mtz \    # Input reflection data
   XYZOUT output.pdb \  # Output refined structural model
   HKLOUT output.mtz \  # Output refined reflection data
   ATOMSF \
   /Applications/ccp4-9/lib/data/atomsf_neutron.lib \  # Path to neutron scattering lengths library
   << EOF
   SOURCE NEUTRON    # Specify neutron source
   MAKE HYDR ALL     # Generate all hydrogen atoms
   MAKE HOUT YES     # Output hydrogen atoms
   HYDROGEN DFRACTION INIT 1  # Initialise deuterium fraction as 1 for all the H atoms
   HYDROGEN REFINE ALL   # Refine all hydrogen atom positions
   RESTRAINT TORSION HYDROGEN INCLUDE ALL  # Apply torsion angle restraints to all hydrogen atoms
   END
   EOF

.. image:: ../images/neutron_tab3.png

For subsequent refinement, initialisation is not required.
In the script above, simply use `MAKE HYDR YES` instead of `ALL`.
In the CCP4i2 interface, in "Use hydrogens during refinement" select: "only if present in the file", and untick the initialisation box.

Use of jelly-body restraints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Jelly-body (harmonic) restraints can be used during neutron refinement to help maintain the overall shape of the molecule while allowing for local adjustments.

The keyword for jelly-body restraints is:

**RIDGE DISTANCE SIGMA 0.02**
   the sigma can be adjusted

For more information, see Murshudov et al. (2011), and for more information about the **RIDGE** keywords, see
`here <http://www.ysbl.york.ac.uk/refmac/data/refmac_news.html#Ridge>`_.

In the CCP4i2 interface, you can enable jelly-body in the **Restraints** tab

.. image:: ../images/jelly_body.png

Use of X-ray reference structure restraints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To improve the *data-to-parameters* ratio in neutron refinement, structural information from homologous X-ray models can be incorporated through external restraints.
X-ray models provide more precise coordinates for non-hydrogen atoms compared to neutron models, making them a valuable source of prior structural information.
External restraints can be generated using ProSMART (R.A. Nicholls et al., 2012).
For more details, refer to the `ProSMART documentation <https://www.ccp4.ac.uk/html/prosmart.html>`_.

**Protocol**

1. Generate external restraints file using ProSMART:

   .. code-block:: 

      prosmart -p1 neutron_model.pdb -p2 X-ray_model.pdb

   It generates a **prosmart_restraints_file.txt**

2. Use external restraints keywords in REFMAC5:

   **EXTERNAL DMAX 4.2**
   Maximum restraint interatomic distance. Highly recommended value: 4.2.

   **EXTERNAL WEIGHT SCALE 5**
   External restraints weight. Increasing this weight increases the influence of external restraints during refinement.

   **EXTERNAL ALPHA 2.0** 
   It is the parameter which controls robustness to outliers. Increasing this value reduces the influence of outliers (i.e. restraints that are very different from the current interatomic distance).
   However, increasing this value too high results in too many restraints being considered outliers - this means that only the restraints that are very similar to the current interatomic distances will have much effect.
   Note that the optimal value is highly correlated with the external restraints weight parameter.
   For more details, see https://doi.org/10.48550/arXiv.1701.03077.

**Example**

.. code-block::

   #!/bin/bash

   refmacat \
   XYZIN input.pdb \    # Input structural model
   HKLIN input.mtz \    # Input reflection data
   XYZOUT output.pdb \  # Output refined structural model
   HKLOUT output.mtz \  # Output refined reflection data
   ATOMSF \
   /Applications/ccp4-9/lib/data/atomsf_neutron.lib \  # Path to neutron scattering lengths library
   << EOF
   SOURCE NEUTRON    # Specify neutron source
   MAKE HYDR YES    # Use hydrogen atoms already present in the input file
   MAKE HOUT YES     # Output hydrogen atoms
   REFINEMENT DFRACTION    # Refine deuterium fraction
   HYDROGEN DFRACTION POLAR  # Refine deuterium fraction for polar hydrogen atoms
   HYDROGEN REFINE ALL   # Refine all hydrogen atom positions
   RESTRAINT TORSION HYDROGEN INCLUDE ALL  # Apply torsion angle restraints to all hydrogen atoms
   EXTERNAL DMAX 4.2  # Maximum restraint interatomic distance
   EXTERNAL WEIGHT SCALE 5  # External restraints weight
   EXTERNAL ALPHA 2.0  # Geman-McClure parameter for robustness to outliers
   @prosmart_restraints_file.txt  # Path to the external restraints file
   END
   EOF

In the CCP4i2 interface, you can import the external restraints file in the **Restraints** tab

.. image:: ../images/external_restraints.png

The external restraints keywords can be specified in the **Advanced** tab

.. image:: ../images/external_keys.png
   :scale: 60%

Output models
-------------

The refined output model (mmCIF) includes an additional column, **_atom_site.ccp4_deuterium_fraction**.
This model contains only hydrogen atoms, with the deuterium fraction recorded in this column.
When refined, a value near 1 indicates deuterium, while a value near 0 represents hydrogen.
If the model is only initialized, the deuterium fraction is set to 1 for deuterium and 0 for hydrogen.

**Example mmCIF**

.. parsed-literal::

   # H/D exchanged structure

   **_atom_site.ccp4_deuterium_fraction**
   ATOM 32   H  H    . THR Axp A     3   ? 46.667 25.859 24.195 1 30.699  ? 3   A 1 **0.834636**
   ATOM 33   H  HA   . THR Axp A     3   ? 47.092 23.095 23.075 1 28.394  ? 3   A 1 0
   ATOM 34   H  HB   . THR Axp A     3   ? 44.998 25.195 22.335 1 27.935  ? 3   A 1 0

   # perdeuterated structure (no deuterium fraction refined)

   **_atom_site.ccp4_deuterium_fraction**
   ATOM 843  H  H    . THR Axp A     51  ? 32.077 20.672 -3.838  1 10.303 ? 51  A 1 **1**
   ATOM 844  H  HA   . THR Axp A     51  ? 31.198 17.824 -4.062  1 9.439  ? 51  A 1 **1**
   ATOM 845  H  HB   . THR Axp A     51  ? 33.399 17.202 -3.274  1 9.328  ? 51  A 1 **1**

Currently, the PDB discards the deuterium fraction column.
Therefore, we provide an alternative model for deposition (this mmCIF has the suffix: hd_expand), in which both H and D atoms are included, and the deuterium fraction is recorded in the occupancy column.

**Example mmCIF for deposition**

.. parsed-literal::

   # H/D exchanged structure

   ATOM 35   **H  H**    A THR Axp A     3   ? 46.667 25.859 24.195 **0.165364**   30.699  ? 3   A 1
   ATOM 36   **D  D**    D THR Axp A     3   ? 46.667 25.859 24.195 **0.834636**   30.699  ? 3   A 1
   ATOM 37   H  HA   . THR Axp A     3   ? 47.092 23.095 23.075 1          28.394  ? 3   A 1
   ATOM 38   H  HB   . THR Axp A     3   ? 44.998 25.195 22.335 1          27.935  ? 3   A 1

   # perdeuterated structure (no deuterium fraction refined)

   ATOM   845  **D  D**    . THR Axp A     51  ? 32.1   20.683 -3.84   **1** 11.163 ? 51  A 1
   ATOM   846  **D  DA**   . THR Axp A     51  ? 31.191 17.847 -4.038  **1** 9.319  ? 51  A 1
   ATOM   847  **D  DB**   . THR Axp A     51  ? 33.357 17.193 -3.308  **1** 8.575  ? 51  A 1

In the CCP4i2 interface, the output models are reported in the **Results** tab

.. image:: ../images/output_model.png
   :scale: 60%

Reference
---------

Catapano, L., Long, F., Yamashita, K., Nicholls, R. A., Steiner, R. A. & Murshudov, G. N. (2023). Neutron crystallographic refinement with REFMAC5 from the CCP4 suite. Acta Cryst. D79, 1056-1070.
