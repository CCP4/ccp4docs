REFMAC (CCP4: Supported Program)
================================

User's manual for the program REFMAC, version 5.*
--------------------------------------------------

Input and output files - TLS parameter files
============================================

`Input <#tlsin>`__ and `output <#tlsout>`__ TLS files are ASCII files
containing details of the TLS groups and the values of the corresponding
T, L and S tensors. For more details of TLS parameters see Martyn Winn's
page on `TLS in
REFMAC <http://www.ccp4.ac.uk/martyn/tls_research.html>`__ (external
link).

TLSIN
-----

The input TLS file (logical name TLSIN) may contain only information
about TLS groups without values of T, L and S tensors.The possible
records are:

TLS <title>
    This must be the first record for each TLS group, and indicates the
    start of a new group. <title> is a short description of the group.
RANGE <from> <to> [<selection>]
    This record describes which atoms are included in the TLS group.
    <from> and <to> consist of the chain identifier followed by the
    residue number in the format (A1,I4), for the beginning and end of a
    particular segment. Several segments can be included in a TLS group
    by including several RANGE records. The values of <from> and <to>
    should be enclosed in quotes, *e.g.* 'A 131'. The optional
    <selection> specifies an atom selection for a particular segment.
    The possibilities are "ALL" (all atoms - default), "MNC" (main chain
    atoms only), "SDC" (side chain atoms only) or a particular atom type
    up to 3 characters, *e.g.* "CB".
ORIGIN <origin_x> <origin_y> <origin_z>
    The origin used for the calculation of TLS parameters in the
    coordinate system of XYZIN. This defaults to the centroid of the
    group, and this record can normally be omitted.
T <T :sup:`11`> <T :sup:`22`> <T :sup:`33`> <T :sup:`12`> <T :sup:`13`>
<T :sup:`23`>
    Starting values for the translation tensor in Å². Normally, this
    would be omitted, and the program will start with all values zero.
L <L :sup:`11`> <L :sup:`22`> <L :sup:`33`> <L :sup:`12`> <L :sup:`13`>
<L :sup:`23`>
    Starting values for the libration tensor in degree². Normally, this
    would be omitted, and the program will start with all values zero.
S <S :sup:`22` - S :sup:`11`> <S :sup:`11` - S :sup:`33`> <S :sup:`12`>
<S :sup:`13`> <S :sup:`23`> <S :sup:`21`> <S :sup:`31`> <S :sup:`32`>
    Starting values for the screw rotation tensor in Å * degree.
    Normally, this would be omitted, and the program will start with all
    values zero.

TLSIN - Format
~~~~~~~~~~~~~~

The format is essentially the same as that used by the program RESTRAIN.
A simple example with two TLS groups representing two domains of a
single chain:

    ::

        TLS    Chain A   NAD binding domain
        RANGE  'A   1' 'A 137' ALL
        RANGE  'A 303' 'A 340' ALL

        TLS    Chain A   Catalytic domain
        RANGE  'A 138' 'A 302' ALL

TLSOUT
------

The output TLS file (logical name TLSOUT) contains refined TLS
parameters (details of TLS group, origin of TLS group, T, L, S tensors).

TLSOUT - Format
~~~~~~~~~~~~~~~

The format is essentially the same as `that for
TLSIN <#tlsin_format>`__, with the T, L and S records holding the
refined parameters, and the ORIGIN record holding the calculated origin,
if not specified in TLSIN. In addition, the output file contains a
record "REFMAC" denoting that the file has been output by REFMAC.
Example of output TLS file:

    ::

        ! Output from REFMAC

        TLS    Chain A
        RANGE  'A   1.' 'A 300.' ALL
        ORIGIN   -3.680   0.700  11.039
        T    -0.0161 -0.1041 -0.0362  0.0121  0.0497  0.0194
        L     1.9572  2.9920  2.5163  0.3999 -0.3219  0.1108
        S     0.0066 -0.0199  0.0299 -0.0444 -0.1246 -0.0684  0.0286  0.0062

        TLS    Chain B
        RANGE  'B   1.' 'B 175.' ALL
        ORIGIN    4.104  26.003  64.557
        T     0.0619  0.0104  0.0481 -0.0128  0.0051 -0.0226
        L     2.5549  4.4075  3.5760  0.5356  0.7041  0.2892
        S     0.1170 -0.1274 -0.0305  0.2005 -0.0652  0.1453 -0.1772  0.1566
