CAVENV (CCP4: Supported Program)
================================

NAME
----

**cavenv** - Visualise cavities in macromolecular structures

SYNOPSIS
--------

**cavenv xyzin** *foo_in.pdb* **mapout** *foo_out.map*

`Keyworded input <#keywords>`__

.. _description: 
 
DESCRIPTION
---------------------------------

CAVENV produces a CCP4 map from an input model structure, which is
designed to help visualise cavities in the protein. By default, the
program produces a "cavity" map in which each grid point is given a
value equal to the distance of the closest atom, minus its Van der Waals
radius, up to a maximum of <maxrad> (`maximal probe radius <#radmax>`__
to be tested). Thus the grid values are zero within the Van der Waals
envelope of the protein. Outside this envelope, the value represents
radius of the largest probe that access that grid point. In the middle
of solvent regions, the grid value is that of the largest probe
considered <maxrad>.

The program will also output an estimate of the percent protein and
percent solvent in the volume covered by the generated map. Here,
"protein" refers to the Van der Waals envelope of the atoms in the input
model, and will include crystallographic waters unless they are
specifically excluded. The "solvent" is the complement of the protein
region, and thus larger than the volume accessible to the centre of a
solvent probe. These percentages can only be compared to the value from
a Matthews coefficient calculation if the output map covers a whole
number of asymmetric units.

If one of the keywords `ENVPROTEIN <#envprotein>`__,
`ENVNCS <#envncs>`__ or `ENVSOLVENT <#envsolvent>`__ is given, then a
mask is produced instead of the cavity map.

.. _files: 
 
INPUT AND OUTPUT FILES
--------------------------------------

The following input and output files are used by the program:

Input Files:

XYZIN
   Coordinates of model of interest. The program will generate symmetry
   mates of the input coordinates automatically.

Output Files:

MAPOUT
   Output map containing cavity map, or mask.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

**TITLE** <title string>

Job title. Included in MAPOUT header.
Default is "From CAVENV".

.. _radmax: 
 
**RADMAX** <maxrad>

Maximum probe radius to be tested in Angstrom.
Default is 3.0

**RANGE** <nfirst> <nlast>


Residue range of atoms to be included from XYZIN. This applies to all
  chains.
Default range is 1 to 10000.

**EXCLUDE** <atomname>

Atoms to be excluded from XYZIN, e.g. "EXCLUDE NZ". Several EXCLUDE
cards may be given (up to a maximum number which is currently 150).

.. _cell: 
 
**CELL** <a> <b> <c> [ <alpha> <beta> <gamma> ]


Input cell parameters explicitly. If omitted, program will try to obtain
cell parameters from XYZIN.

**SYMM** [<spacegroup number> <spacegroup name> <symmetry operations> OFF]

Input spacegroup explicitly. If omitted, program will try to obtain
spacegroup from XYZIN.

The spacegroup symmetry operators, together with lattice translations up
to one unit cell in any direction, are used to generate symmetry mates
of the atoms in XYZIN that lie within the limits of the output map. If
the input coordinates are in a wildly different location from the
desired map limits, then some relevant coordinates may not be generated.

If the subkeyword OFF is given, then no symmetry or lattive translations
are applied.

**AXIS** <fast axis> <medium axis> <slow axis>

Specify the axis order for the output map, where each axis is one of
  "X", "Y" or "Z".
Default is "AXIS X Y Z".

.. _grid: 
 
**GRID** <nx> <ny> <nz>


Number of sampling divisions along whole cell edges.
Default is such as to give grid spacing of 0.5 A.

.. _xyzlim: 
 
**XYZLIM** <xmin> <xmax> <ymin> <ymax> <zmin> <zmax>


Range of unit cell to be included in output map. <xmin> <xmax> etc.
  are minimum and maximum grid numbers along each cell edge.
Default is to include entire unit cell.

.. _envprotein: 
 
ENVPROTEIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead of the default cavity map, MAPOUT will contain a protein mask
with 1's within the protein region and 0's in the solvent region. The
solvent region is defined as the volume accessible by a probe of radius
<maxrad> (see `RADMAX <#radmax>`__ keyword). Thus the solvent region
corresponds to the flat central region of the cavity map, when the same
value of <maxrad> is used.

.. _envncs: 
 
**ENVNCS** <numncs>


As ENVPROTEIN, but masks will be generated for <numncs> NCS-related
molecules. This card should be followed by 3 x <numncs> records
specifying the NCS operators. Each record consists of 3 elements of the
rotation matrix and one element of the translation. The first group of
three records will usually represent the identity:

::


   1.0 0.0 0.0 0.0
   0.0 1.0 0.0 0.0
   0.0 0.0 1.0 0.0

.. _envsolvent: 
 
ENVSOLVENT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead of the default cavity map, MAPOUT will contain a solvent mask
with 1's within the solvent region and 0's in the protein region. The
solvent region is defined as the volume accessible by a probe of radius
<maxrad> (see `RADMAX <#radmax>`__ keyword). Thus the solvent region
corresponds to the flat central region of the cavity map, when the same
value of <maxrad> is used.

END
~~~

End of the keyword list

.. _examples: 
 
EXAMPLES
---------------------------

Runnable examples using rnase data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(../examples/unix/runnable/cavenv.exam)

Simple example of producing a cavity map from the rnase model coordinates.

Other examples
~~~~~~~~~~~~~~

[job example (unix)]

::


   cavenv xyzin model.pdb mapout output.map << eof
   range 1 2000           ! include this residue range
   radmax 3.0             ! add 3.0 Angstroem to R_vdw
   TITLE probe sizes < 3 Angstroem  ! title for cavity map or envelope
   CELL 100. 100. 100. 90. 90. 90.  ! unit cell (can be artificial)
   AXIS X Y Z         ! fast, medium and slow map index
   GRID 200 200 200           ! Ngrid along A/B/C for unit cell
   XYZLIM 46 102 58 144 166 224     ! grid limits along A/B/C
   SYMMETRY 1             ! space group
   ENVNCS 6               ! make envelope for a hexamer
   1.0 0.0 0.0 0.0        !
   0.0 1.0 0.0 0.0        ! 1st ncs operation
   0.0 0.0 1.0 0.0        !
   -0.496382    0.868067   -0.008077      21.3054 !
   -0.868089   -0.496407   -0.001331      39.5577 ! 2nd ncs operation
   -0.005165    0.006350    0.999966      44.4143 !
   -1.000000   -0.000038    0.000297      44.4809 !
    0.000039   -1.000000    0.000687      92.5910 ! 3rd ncs operation
    0.000297    0.000687    1.000000      66.5566 !
   -0.498075   -0.867075    0.010130      44.8044 !
    0.867133   -0.498059    0.004165       1.1216 ! 4th ncs operation
    0.001434    0.010859    0.999940      88.5292 !
    0.498336    0.866924   -0.010155      -0.3177 !
   -0.866981    0.498328   -0.003483      91.4965 ! 5th ncs operation
    0.002041    0.010540    0.999942      21.9734 !
    0.496579   -0.867951    0.008414      23.1853 !
    0.867971    0.496611    0.002098      53.0822 ! 6th ncs operation
   -0.005999    0.006261    0.999962     110.9757 !
   eof

Bugs
----

The main memory requirement is holding the map in memory. The program is
currently dimensioned to hold a maximal map size of 3375000
(150*150*150) grid points. If this is too large for your system, then
you need to change the value of NMAP in 1 PARAMETER statement in the
source code, and recompile.

SEE ALSO
--------

`areaimol <areaimol.html>`__ - Analyse solvent accessible areas

.. _references: 
 
REFERENCES
-------------------------------

#. A. Volbeda, private communication or with reference (in french): Anne
   Volbeda, Speleologie des hydrogenases a nickel et a fer. In: "Les
   Ecoles Physique et Chimie du Vivant, numero 1 - avril 1999, Analyse
   de l'organisation tridimensionnelle des proteines", pp 47-52.

Other examples using CAVENV:

#. T. I. Doukov, T. M. Iverson, J. Seravalli, S. W. Ragsdale, C. L.
   Drennan (2002) *Science* **298** 567 - 572
   "... putative channel for CO as calculated by program CAVENV..."

AUTHORS
-------

ANNE VOLBEDA, IBS/LCCP GRENOBLE
CCP4 version - Martyn Winn
