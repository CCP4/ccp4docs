RDENT (CCP4: Deprecated Program)
================================

NAME
----

**rdent** - Create dictionary entries for Restrain from PDB file.

SYNOPSIS
--------

| **rdent XYZIN** *pdb_file* **DICT** *dictionary_entry*

.. _description: 
 
DESCRIPTION
---------------------------------

1. A dictionary entry is created for each residue in the pdb file.

2. The coordinates in the file must represent an accurate structure of
the residue, e.g. from the "learn" function in X-PLOR. The coordinates
taken from a single PDB entry are probably not good enough, unless it is
a very high resolution structure determination.

3. This only creates the distance restraint entries, with zero standard
deviations (Restrain will use default values if you leave the standard
deviations zero).

4. No inter-residue restraints are made.

5. You still have to add the dihedral, chiral centre and plane
definitions (see `Restrain <restrain.html>`__ documentation for
details).

6. To obtain a complete dictionary for use with RESTRAIN, you should
make a copy of one of the standard Restrain dictionaries (in CCP4
$CLIBD) and add the entries obtained from RDENT at any suitable point
(before the first END). Alternatively ask one of the Restrain
maintainers to replace the standard dictionary with your version; this
will allow other users to access the new dictionary entry.

.. _files: 
 
INPUT AND OUTPUT FILES
--------------------------------------

Input
~~~~~

XYZIN
   PDB file containing residues to be included into dictionary.

Output
~~~~~~

DICT
   Corresponding entries in standard Restrain dictionary format.

AUTHOR
------

Ian Tickle

SEE ALSO
--------

-  `Restrain <restrain.html>`__
