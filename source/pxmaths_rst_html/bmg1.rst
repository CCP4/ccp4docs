=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================

.. raw:: html
   :file: bmg1.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg2.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |alpha,beta,gamma| image:: ../images/alphabetagammatr.gif
   :width: 39px
   :height: 19px
.. |crystal axes| image:: ../images/crystalgeometrytr.gif
   :width: 125px
   :height: 167px
.. |gamma| image:: ../images/gammatr.gif
   :width: 12px
   :height: 16px
.. |beta| image:: ../images/betatr.gif
   :width: 12px
   :height: 20px
.. |alpha| image:: ../images/alphatr.gif
   :width: 12px
   :height: 11px
