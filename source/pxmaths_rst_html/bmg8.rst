
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg8.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg9.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg7.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |expression for electron density| image:: ../images/densitytr.gif
   :width: 288px
   :height: 53px
.. |structure factor equation 3| image:: ../images/strfac3tr.gif
   :width: 330px
   :height: 129px
.. |alpha| image:: ../images/alphatr.gif
   :width: 12px
   :height: 11px
.. |structure factor equation 4| image:: ../images/strfac4tr.gif
   :width: 282px
   :height: 145px
.. |pictorial representation of structure factor for h with anomalous| image:: ../images/Fhtr.gif
   :width: 253px
   :height: 161px
.. |structure factor equation 5| image:: ../images/strfac5tr.gif
   :width: 294px
   :height: 139px
.. |pictorial representation of structure factor for -h with anomalous| image:: ../images/F-htr.gif
   :width: 253px
   :height: 130px
.. |phase triangles| image:: ../images/phasetrianglestr.gif
   :width: 237px
   :height: 223px
.. |large capital Sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
.. |large capital Sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
