
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmgbmgfull.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
.. |next button| image:: ../images/3Dnexttr.gif
   :target: bmg3.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :target: bmg1.html
.. |top button| image:: ../images/3Dtoptr.gif
   :target: index.html
.. |image4| image:: ../images/alphabetagammatr.gif
.. |image5| image:: ../images/crystalgeometrytr.gif
.. |image6| image:: ../images/p3geometrytr.gif
.. |image7| image:: ../images/ticktr.gif
.. |image8| image:: ../images/ticktr.gif
.. |image9| image:: ../images/ticktr.gif
.. |image10| image:: ../images/ticktr.gif
.. |image11| image:: ../images/ticktr.gif
.. |image12| image:: ../images/ticktr.gif
.. |image13| image:: ../images/ticktr.gif
.. |image14| image:: ../images/ticktr.gif
.. |P31 symmetry operators - general| image:: ../images/si-matrixtr.gif
.. |P31 symmetry operators - specific| image:: ../images/p31matricestr.gif
.. |image17| image:: ../images/2sinthetaoverlambdatr.gif
.. |image18| image:: ../images/4sinsqthetaoverlambdasqtr.gif
.. |scattering factor; borrowed from Crystallography 101| image:: ../images/scatfactr.gif
.. |image20| image:: ../images/phaseexpressionstr.gif
.. |image21| image:: ../images/phitr.gif
.. |image22| image:: ../images/Sigma3tr.gif
.. |image23| image:: ../images/pitr.gif
.. |image24| image:: ../images/Sigma3tr.gif
.. |image25| image:: ../images/pitr.gif
.. |image26| image:: ../images/pitr.gif
.. |image27| image:: ../images/phitr.gif
.. |image28| image:: ../images/3foldmatrixtr.gif
.. |phase sum in vector representation| image:: ../images/phasesumtr.gif
.. |image30| image:: ../images/pitr.gif
.. |image31| image:: ../images/pitr.gif
.. |image32| image:: ../images/pitr.gif
.. |image33| image:: ../images/phitr.gif
.. |image34| image:: ../images/phitr.gif
.. |image35| image:: ../images/phitr.gif
.. |image36| image:: ../images/phitr.gif
.. |image37| image:: ../images/phitr.gif
.. |image38| image:: ../images/pitr.gif
.. |phase sum in vector representation for centric reflections| image:: ../images/phasesumcentrictr.gif
.. |image40| image:: ../images/centrictr.gif
.. |image41| image:: ../images/centric2tr.gif
.. |image42| image:: ../images/phitr.gif
.. |image43| image:: ../images/piover643tr.gif
.. |image44| image:: ../images/centricexampletr.gif
.. |image45| image:: ../images/phitr.gif
.. |image46| image:: ../images/densitytr.gif
.. |image47| image:: ../images/strfac3tr.gif
.. |image48| image:: ../images/alphatr.gif
.. |image49| image:: ../images/alphatr.gif
.. |image50| image:: ../images/strfac4tr.gif
.. |pictorial representation of structure factor for h with anomalous| image:: ../images/Fhtr.gif
.. |image52| image:: ../images/phitr.gif
.. |image53| image:: ../images/phitr.gif
.. |image54| image:: ../images/phitr.gif
.. |image55| image:: ../images/phitr.gif
.. |image56| image:: ../images/phitr.gif
.. |phase triangles| image:: ../images/phasetrianglestr.gif
.. |image58| image:: ../images/phitr.gif
.. |image59| image:: ../images/phitr.gif
.. |image60| image:: ../images/phitr.gif
.. |image61| image:: ../images/phitr.gif
.. |image62| image:: ../images/phitr.gif
.. |image63| image:: ../images/phitr.gif
.. |image64| image:: ../images/phitr.gif
.. |image65| image:: ../images/phitr.gif
.. |image66| image:: ../images/phitr.gif
.. |image67| image:: ../images/phitr.gif
.. |image68| image:: ../images/phitr.gif
.. |image69| image:: ../images/phitr.gif
.. |image70| image:: ../images/Sigma2tr.gif
.. |image71| image:: ../images/phitr.gif
.. |image72| image:: ../images/Sigma2tr.gif
.. |image73| image:: ../images/phitr.gif
.. |image74| image:: ../images/Sigma2tr.gif
.. |image75| image:: ../images/phitr.gif
.. |image76| image:: ../images/phitr.gif
.. |image77| image:: ../images/phitr.gif
.. |image78| image:: ../images/phitr.gif
.. |image79| image:: ../images/phitr.gif
.. |image80| image:: ../images/phitr.gif
.. |image81| image:: ../images/phitr.gif
.. |image82| image:: ../images/phitr.gif
.. |image83| image:: ../images/phitr.gif
.. |image84| image:: ../images/Sigma2tr.gif
.. |image85| image:: ../images/phitr.gif
.. |image86| image:: ../images/phitr.gif
.. |image87| image:: ../images/phitr.gif
.. |image88| image:: ../images/phitr.gif
.. |image89| image:: ../images/phitr.gif
.. |SIR phase circles| image:: ../images/SIRtr.gif
   :height: 300px
   :target: ../images/SIRtr.gif
.. |image91| image:: ../images/phitr.gif
.. |image92| image:: ../images/phitr.gif
.. |image93| image:: ../images/pitr.gif
.. |image94| image:: ../images/phitr.gif
.. |image95| image:: ../images/phitr.gif
.. |image96| image:: ../images/phitr.gif
.. |image97| image:: ../images/phitr.gif
.. |image98| image:: ../images/phitr.gif
.. |image99| image:: ../images/phitr.gif
.. |image100| image:: ../images/phitr.gif
.. |image101| image:: ../images/phitr.gif
.. |image102| image:: ../images/phitr.gif
.. |image103| image:: ../images/phitr.gif
.. |image104| image:: ../images/phitr.gif
.. |image105| image:: ../images/phitr.gif
.. |image106| image:: ../images/phitr.gif
.. |image107| image:: ../images/phitr.gif
.. |image108| image:: ../images/pitr.gif
.. |image109| image:: ../images/phitr.gif
.. |image110| image:: ../images/phitr.gif
.. |image111| image:: ../images/phitr.gif
.. |image112| image:: ../images/pitr.gif
.. |MIR phase circles| image:: ../images/MIRtr.gif
   :height: 300px
.. |image114| image:: ../images/phitr.gif
.. |image115| image:: ../images/phitr.gif
.. |image116| image:: ../images/phitr.gif
.. |image117| image:: ../images/phitr.gif
.. |image118| image:: ../images/phitr.gif
.. |image119| image:: ../images/phitr.gif
.. |image120| image:: ../images/phitr.gif
.. |image121| image:: ../images/phitr.gif
.. |image122| image:: ../images/phitr.gif
.. |image123| image:: ../images/phitr.gif
.. |image124| image:: ../images/phitr.gif
.. |image125| image:: ../images/phitr.gif
.. |image126| image:: ../images/phitr.gif
.. |image127| image:: ../images/phitr.gif
.. |image128| image:: ../images/phitr.gif
.. |image129| image:: ../images/phitr.gif
.. |image130| image:: ../images/phitr.gif
.. |image131| image:: ../images/phitr.gif
.. |image132| image:: ../images/phitr.gif
.. |image133| image:: ../images/phitr.gif
.. |image134| image:: ../images/phitr.gif
.. |image135| image:: ../images/phitr.gif
.. |image136| image:: ../images/phitr.gif
.. |image137| image:: ../images/phitr.gif
.. |image138| image:: ../images/phitr.gif
.. |SIRAS phase circles| image:: ../images/SIRADtr.gif
   :height: 300px
.. |image140| image:: ../images/phitr.gif
.. |image141| image:: ../images/phitr.gif
.. |image142| image:: ../images/phitr.gif
.. |image143| image:: ../images/phitr.gif
.. |image144| image:: ../images/phitr.gif
.. |image145| image:: ../images/phitr.gif
.. |image146| image:: ../images/phitr.gif
.. |image147| image:: ../images/phitr.gif
.. |image148| image:: ../images/phitr.gif
.. |image149| image:: ../images/phitr.gif
.. |image150| image:: ../images/phitr.gif
.. |image151| image:: ../images/phitr.gif
.. |image152| image:: ../images/phitr.gif
.. |image153| image:: ../images/phitr.gif
.. |image154| image:: ../images/phitr.gif
.. |image155| image:: ../images/phitr.gif
.. |image156| image:: ../images/phitr.gif
.. |image157| image:: ../images/phitr.gif
.. |image158| image:: ../images/phitr.gif
.. |image159| image:: ../images/phitr.gif
.. |image160| image:: ../images/phitr.gif
.. |image161| image:: ../images/phitr.gif
.. |image162| image:: ../images/phitr.gif
.. |image163| image:: ../images/phitr.gif
.. |image164| image:: ../images/phitr.gif
.. |image165| image:: ../images/phitr.gif
.. |image166| image:: ../images/phitr.gif
.. |image167| image:: ../images/phitr.gif
.. |image168| image:: ../images/phitr.gif
.. |image169| image:: ../images/phitr.gif
.. |image170| image:: ../images/phitr.gif
.. |image171| image:: ../images/phitr.gif
.. |image172| image:: ../images/phitr.gif
.. |image173| image:: ../images/phitr.gif
.. |image174| image:: ../images/phitr.gif
.. |image175| image:: ../images/phitr.gif
.. |image176| image:: ../images/phitr.gif
.. |image177| image:: ../images/phitr.gif
.. |image178| image:: ../images/phitr.gif
.. |image179| image:: ../images/phitr.gif
.. |image180| image:: ../images/phitr.gif
.. |image181| image:: ../images/phitr.gif
.. |image182| image:: ../images/phitr.gif
.. |image183| image:: ../images/phitr.gif
.. |image184| image:: ../images/phitr.gif
.. |image185| image:: ../images/phitr.gif
.. |image186| image:: ../images/phitr.gif
.. |image187| image:: ../images/phitr.gif
.. |image188| image:: ../images/phitr.gif
.. |image189| image:: ../images/phitr.gif
.. |image190| image:: ../images/phitr.gif
.. |image191| image:: ../images/phitr.gif
.. |image192| image:: ../images/phitr.gif
.. |image193| image:: ../images/phitr.gif
.. |image194| image:: ../images/phitr.gif
.. |image195| image:: ../images/phitr.gif
.. |image196| image:: ../images/phitr.gif
.. |image197| image:: ../images/phitr.gif
.. |image198| image:: ../images/phitr.gif
.. |image199| image:: ../images/phitr.gif
.. |image200| image:: ../images/phitr.gif
.. |image201| image:: ../images/phitr.gif
.. |image202| image:: ../images/phitr.gif
.. |image203| image:: ../images/pitr.gif
.. |image204| image:: ../images/phitr.gif
.. |image205| image:: ../images/phitr.gif
.. |image206| image:: ../images/phitr.gif
.. |image207| image:: ../images/pitr.gif
.. |image208| image:: ../images/phitr.gif
.. |image209| image:: ../images/phitr.gif
.. |image210| image:: ../images/strfac6tr.gif
.. |image211| image:: ../images/Sigma2tr.gif
.. |image212| image:: ../images/pitr.gif
.. |image213| image:: ../images/Sigma2tr.gif
.. |image214| image:: ../images/scaletr.gif
.. |image215| image:: ../images/Sigma2tr.gif
.. |image216| image:: ../images/Sigma2tr.gif

