
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg10.html



.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg9.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |structure factor expression 6| image:: ../images/strfac6tr.gif
   :width: 382px
   :height: 112px
.. |large capital Sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
.. |large capital sigma| image:: ../images/Sigma2tr.gif
   :width: 14px
   :height: 19px
.. |scaling intensities, equation| image:: ../images/scaletr.gif
   :width: 398px
   :height: 25px
.. |thumbnail for Wilson plot| image:: ../images/wilson-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/wilson-leftg.gif
.. |thumbnail for Wilson plot| image:: ../images/wilson-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/wilson-rightg.gif
.. |thumbnail for amplitude v. resolution plot| image:: ../images/amp-resln-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/amp-resln-leftg.gif
.. |thumbnail for amplitude v. resolution plot| image:: ../images/amp-resln-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/amp-resln-rightg.gif
.. |thumbnail for fall-off plot| image:: ../images/falloff-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/falloff-leftg.gif
.. |thumbnail for fall-off plot| image:: ../images/falloff-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/falloff-rightg.gif
.. |4sinsqthetaoverlambdasq| image:: ../images/4sinsqthetaoverlambdasqtr.gif
   :width: 71px
   :height: 16px
.. |thumbnail for cumulative intensity distribution plot| image:: ../images/cumI-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/cumI-leftg.gif
.. |thumbnail for cumulative intensity distribution plot| image:: ../images/cumI-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/cumI-rightg.gif
.. |capital Gamma| image:: ../images/Gammatr.gif
   :width: 9px
   :height: 13px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |thumbnail for acentric moments plot 1| image:: ../images/acent-moments1-3-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments1-3-leftg.gif
.. |thumbnail for acentric moments plot 1| image:: ../images/acent-moments1-3-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments1-3-rightg.gif
.. |thumbnail for acentric moments plot 2| image:: ../images/acent-moments2-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments2-leftg.gif
.. |thumbnail for acentric moments plot 2| image:: ../images/acent-moments2-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments2-rightg.gif
.. |thumbnail for acentric moments plot 2| image:: ../images/acent-moments3-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments3-leftg.gif
.. |thumbnail for acentric moments plot 2| image:: ../images/acent-moments3-rightgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/acent-moments3-rightg.gif
.. |thumbnail for centric moments plot 1| image:: ../images/cent-moments1-3-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/cent-moments1-3-leftg.gif
.. |thumbnail for centric moments plot 1| image:: ../images/cent-moments2-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/cent-moments2-leftg.gif
.. |thumbnail for centric moments plot 1| image:: ../images/cent-moments3-leftgsmall.gif
   :width: 255px
   :height: 204px
   :target: ../images/cent-moments3-leftg.gif
