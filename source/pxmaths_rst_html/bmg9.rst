
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg9.html



.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg10.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg8.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |SIR phase circles| image:: ../images/SIRtr.gif
   :width: 545px
   :height: 520px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |MIR phase circles| image:: ../images/MIRtr.gif
   :width: 548px
   :height: 520px
.. |SIRAS phase circles| image:: ../images/SIRADtr.gif
   :width: 545px
   :height: 516px
.. |phi| image:: ../images/pitr.gif
   :width: 8px
   :height: 16px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
