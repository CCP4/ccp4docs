
|CCP4 web logo|

Basic Maths for Protein Crystallographers

+-----------------+
| |next button|   |
+-----------------+

| 

**This document is best viewed with 12pt Times and 10pt Courier fonts.**

Crystallographic Equations and Some Consequences
================================================

#. `Crystal geometry <bmg1.html>`__
#. `Atom notation <bmg2.html>`__
#. `Lattice geometry <bmg3.html>`__
#. `Symmetry operators <bmg4.html>`__
#. `Resolution <bmg5.html>`__
#. `Form factors <bmg6.html>`__
#. `Structure factor <bmg7.html>`__

   #. `Acentric reflections <bmg7.html#acentric>`__
   #. `Centric reflections <bmg7.html#centric>`__

#. `Phasing <bmg8.html>`__

   #. `How does this help phasing? <bmg8.html#how>`__
   #. `Deviation into Patterson
      theory <bmg8.html#deviation_patterson>`__
   #. `Deviation into Difference Fourier
      theory <bmg8.html#deviation_fourier>`__

#. `Phasing diagrams <bmg9.html>`__

   #. `SIR <bmg9.html#sir>`__
   #. `MIR <bmg9.html#mir>`__
   #. `SIRAS <bmg9.html#siras>`__
   #. `MIRAS <bmg9.html#miras>`__
   #. `SAD <bmg9.html#sad>`__

#. `Intensity statistics <bmg10.html>`__

   #. `Wilson Plot <bmg10.html#wilsonplot>`__
   #. `Normalisation <bmg10.html#normalisation>`__
   #. `Cumulative intensity distribution <bmg10.html#cumdis>`__
   #. `Moments <bmg10.html#moments>`__

Authors
-------

| Eleanor Dodson, University of York, England
| Produced by Maria Turkenburg, University of York, England.

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg1.html
