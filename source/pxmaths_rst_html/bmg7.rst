
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg7.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg8.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg6.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |large capital Sigma| image:: ../images/Sigma3tr.gif
   :width: 20px
   :height: 25px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |matrix for 3fold symmetry| image:: ../images/3foldmatrixtr.gif
   :width: 79px
   :height: 51px
.. |phase sum in vector representation| image:: ../images/phasesumtr.gif
   :width: 51px
   :height: 102px
.. |phase sum in vector representation for centric reflections| image:: ../images/phasesumcentrictr.gif
   :width: 151px
   :height: 158px
.. |structure factor expressions for centric reflections| image:: ../images/centrictr.gif
   :width: 201px
   :height: 74px
.. |phases for centric reflections (phi or phi+pi)| image:: ../images/centric2tr.gif
   :width: 208px
   :height: 19px
.. |pi/6, pi/4, pi/3| image:: ../images/piover643tr.gif
   :width: 83px
   :height: 15px
.. |example of structure factor and phase calculation| image:: ../images/centricexampletr.gif
   :width: 502px
   :height: 142px
