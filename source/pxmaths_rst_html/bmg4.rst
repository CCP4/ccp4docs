
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg4.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg5.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg3.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |tick| image:: ../images/ticktr.gif
   :width: 21px
   :height: 32px
.. |symmetry matrix notation| image:: ../images/si-matrixtr.gif
   :width: 59px
   :height: 70px
.. |P31 symmetry operators| image:: ../images/p31matricestr.gif
   :width: 408px
   :height: 72px
