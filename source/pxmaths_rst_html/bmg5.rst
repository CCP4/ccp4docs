
=====================================================
Basic Maths Definitions for Protein Crystallographers
=====================================================


.. raw:: html
   :file: bmg5.html




.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg6.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg4.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |2sinthetaoverlambda| image:: ../images/2sinthetaoverlambdatr.gif
   :width: 59px
   :height: 16px
.. |4sinsqthetaoverlambdasq| image:: ../images/4sinsqthetaoverlambdasqtr.gif
   :width: 71px
   :height: 16px
