DIMPLE (CCP4: Supported Program)
================================

NAME
----

**DIMPLE** - automated refinement and ligand screening

DESCRIPTION
-----------

DIMPLE is a pipeline created primarily to process crystals that contain
a known protein and possibly a ligand bound to this protein. But in
principle can be used with any model.

In the ligand screening scenario, the goal is to present a user with a
quick answer to the question of whether or not they have a bound ligand
or drug candidate in their crystal.

The software is suitable for both automated processing (it is deployed
at a few synchrotron beamlines) as well as for manual running from the
command line.

USAGE
-----

::

   dimple [options...] input.mtz input.pdb... output_dir

All output files are stored in the specified output directory. The list
of all options can be obtained by running ``dimple -h``.

For more details see `ccp4.github.io/dimple/ <http://ccp4.github.io/dimple/>`__.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AUTHORS
-------

| Current version: Marcin Wojdyr
| Previous versions: Ronan Keegan, Graeme Winter, George Pelios
