UNIX (CCP4: Library)
====================

NAME
----

**unix.m4, vms.for, w32mvs.f** - library routines for system
interactions

DESCRIPTION
-----------

These subroutines allow Fortran programs to interact directly with the
appropriate operating system. There are three files for three types of
operating system: unix.m4 (UNIX), vms.for (VMS) and w32mvs.f (Microsoft
Visual Studio, for NT).

Each file should contain the same basic set of routines in the three
different implementations; differences between them are to cope with
platform-specific aspects of each system.

Contents
--------

-  `List of Routines <#list_of_routines>`__
-  `Descriptions of the Routines <#descriptions>`__

.. _list_of_routines: 
 


List of Routines
----------------

The following table lists the subroutines, and notes which file(s) they
can be found in.

Routine

Description

unix.m4

vms.for

w32mvs.f

`CCPOPN <#ccpopn>`__

Open a file

yes

yes

yes

`UBYTES <#ubytes>`__

Returns number of bytes per word and 'words'/'bytes' to indicate if byte
handling is available

yes

yes

yes

`UCPUTM <#ucputm>`__

Get CPU time

yes

yes

yes

`UGERR <#ugerr>`__

Get error explanation

yes

yes

yes

`UGTENV <#ugtenv>`__

Get value of env. variable

yes

yes

yes

`UGTUID <#ugtuid>`__

Get user id - it's name

yes

yes

yes

`UIDATE <#uidate>`__

Get date in 3 integer format

yes

yes

yes

`UISATT <#uisatt>`__

Is file a terminal?

yes

yes

yes

`USTIME <#ustime>`__

Get absolute time in seconds

yes

yes

yes

`UTIME <#utime>`__

Get current time

yes

yes

yes

`VAXVMS <#vaxvms>`__

Logical function returns TRUE if VAX/VMS

yes

yes

yes

`WINMVS <#winmvs>`__

Logical function returns TRUE if Windows/MVS

yes

yes

yes

`TTSEND <#ttsend>`__

Write string to terminal with various carriage control options

yes

yes

yes

`GETELAPSED <#getelapsed>`__

Print timing info for CCPERR

yes

yes

yes

`UGTARG <#ugtarg>`__

Get command-line argument

yes

yes

yes

`CCPSPW <#ccpspw>`__

Spawns a new process to run shell command

yes

yes

yes

`CEXIT <#cexit>`__

Wrap around for exit routines

yes

yes

yes

IEOR

Integer function: "XOR" wrap around for f2c compiler

yes

no

no

IOR

Integer function: "OR" wrap around f2c compiler

yes

no

no

IAND

Integer function: "AND" wrap around for f2c compiler

yes

no

no

`RTNBKS <#rtnbks>`__

Character function

yes

yes

yes

`URENAM <#urenam>`__

Rename a file

no

yes

no

`GETARG <#getarg>`__

Get command line arguments

no

yes

no

`USTENV <#ustenv>`__

Create a logical name

no

yes

no

`CCPAL1 <#ccpal1>`__

Support for CCPALC

no

yes

yes

`IARGC <#iargc>`__

number of command line arguments

no

yes

no

`GETPID <#getpid>`__

Integer function: get pid

no

yes

no

`HCIFTIME <#hciftime>`__

local date and time, and relative to GMT

yes

yes

yes

`SRTBEG <#binsrt>`__

Integer function: (VMS version of that in binsortint)

no

yes

no

`SRTRLS <#binsrt>`__

Integer function: (VMS version of that in binsortint)

no

yes

no

`SRTMRG <#binsrt>`__

Integer function: (VMS version of that in binsortint)

no

yes

no

`SRTRET <#binsrt>`__

Integer function: (VMS version of that in binsortint)

no

yes

no

GDUMMY

Dummy GL stubs iff not SGI (see routine for entry's)

yes

no

yes

CLEAR

Null routine (dummy GL stub) iff not SGI

yes

no

yes

.. _descriptions: 
 


Descriptions of the Subroutines
-------------------------------

.. _ccpopn: 
 


CCPOPN
~~~~~~

SUBROUTINE CCPOPN(IIUN,LOGNAM,KSTAT,ITYPE,LREC,IFAIL)

This subroutine is used to open a file

Arguments:

INTEGER

IIUN, KSTAT, ITYPE, LREC, IFAIL

CHARACTER*( * )

LOGNAM

Input:

IIUN

unit number. If negative no messages.

LOGNAM

logical file name /file name (up to 8 characters?)

KSTAT

file status flag
   1, 'UNKNOWN'
   2, 'SCRATCH' (see below)
   3, 'OLD'
   4, 'NEW'
   5, 'READONLY' (see below)
   6, 'PRINTER' (see below)

ITYPE

file type flag
   1, 'SEQUENTIAL' & 'FORMATTED'
   2, 'SEQUENTIAL' & 'UNFORMATTED'
   3, 'DIRECT' & 'FORMATTED'
   4, 'DIRECT' & 'UNFORMATTED'

LREC

| record length for direct file access.
| In characters for a formatted file, in words for an unformatted file.
| Not relevant for a seqUential file

IFAIL

| 0, stop on open failure
| 1, continue after open failure
| 2, continue silently after open failure

Output:

IFAIL

| Unchanged if file open successful
| -1, error in opening file

The requirement to specify that leading carriage control characters in
the output records should be obeyed (or not) can't be implemented
portably; likewise specifying readonly opening. Some compilers accept
VAXtran `carriagecontrol=' and `readonly' specifiers; if so we use
them. Others have IOINIT, which can be used to specify the carriage
control. The HPUX compiler is said not to have any means of doing this
and AIX seems to be likewise, sigh; they both seem to obey the normal
unix convention of printing the format as-is rather than obeying the
first character as carriage control. Concentrix does obey the first
column a la VMS and `traditional' Fortran; the MIPS compilers have a
compile (link?) option to do so. [Unfortunately, carriagecontrol
specification isn't even defined in Fortan90, although `ACTION="READ"'
can be used.] Other machines, dispos, etc

.. _ubytes: 
 


UBYTES
~~~~~~

SUBROUTINE UBYTES(INUM,STRING)

Return statistics about byte handling

Arguments:

INTEGER

INUM

CHARACTER*5

HANDLE

Input:

none

Output:

INUM

number of bytes per word (set to 4)

HANDLE

| 'WORDS' or 'BYTES'
| For unformatted files records are usually counted in 'BYTES', however
  both VAX and SGI swap to 'WORDS' for this file type.

| In unix.m4 *_ubytes* is set at the precompilation stage dependant on
  the compiler. For SGI and Dec-Fortran the value is 'WORDS', the
  default is 'BYTES'. In vms.for and w32mvs.f the values are coded,
  'WORDS' for the VMS and 'BYTES' for MVS.
| In all cases INUM is coded as 4.

.. _ucputm: 
 


UCPUTM
~~~~~~

SUBROUTINE UCPUTM(SEC)

Get CPU time in seconds

Arguments:

SEC

REAL

Input/Output

SEC

If sec <= 0.0, initialize timer and return current elapsed cpu time
since start of execution, otherwise return elapsed cpu since timer was
initialized. Time is in seconds.

.. _ugerr: 
 


UGERR
~~~~~

SUBROUTINE UGERR(STATUS,ERRSTR)

Get error message string for error number in STATUS (VMS). Actually for
UNIX and MVS it ignores STATUS and always uses the *last* error that
occurred.

Arguments:

INTEGER

STATUS

CHARACTER*( * )

ERRSTR

Input:

STATUS

| Error number
| (If negative print error message)

Output

ERRSTR

Error message string

.. _ugtenv: 
 


UGTENV
~~~~~~

SUBROUTINE UGTENV(NAMENV,VALENV)

Get value of env. variable

Arguments:

CHARACTER*( * )

NAMENV, VALENV

Input:

NAMENV

Logical name (trailing blanks are stripped)

Output:

VALENV

Value

.. _ugtuid: 
 


UGTUID
~~~~~~

SUBROUTINE UGTUID(USRNAM)

Get user ID

Arguments:

CHARACTER*( * )

UID

Input:

none

Output:

UID

user ID string

.. _uidate: 
 


UIDATE
~~~~~~

SUBROUTINE UIDATE(IMONTH,IDAY,IYEAR)

Get date in 3 integer format.

Arguments:

INTEGER

IMONTH, IDAY, IYEAR

Input:

none

Output:

IMONTH

month

IDAY

day

IYEAR

year

Variation in IDATE syntax. Alliant uses INTEGER*4 and order is
IDAY,IMONTH,IYEAR

.. _uisatt: 
 


UISATT
~~~~~~

SUBROUTINE UISATT(FLUN,ANSWER)

This function determines whether a program is being run online, if this
information is available.

Arguments:

INTEGER

FLUN, ANS

Input:

FLUN

Fortran Unit Number

Output:

ANS

| 1, online
| 0 otherwise

.. _ustime: 
 


USTIME
~~~~~~

SUBROUTINE USTIME(ISEC)

Get absolute time in seconds.

Arguments:

INTEGER

SEC

Input:

none

Output:

SEC

.. _utime: 
 


UTIME
~~~~~

SUBROUTINE UTIME(CTIME)

Get current time hh:mm:ss

Arguments:

CHARACTER*( * )

CTIME

Input:

None

Output:

CTIME

time as a string hh:mm:ss

.. _vaxvms: 
 


VAXVMS
~~~~~~

LOGICAL FUNCTION VAXVMS()

Operating System in use; returns .TRUE. if VAXVMS

Arguments:

none

Input:

none

Returns:

.TRUE.

for VAXVMS

.FALSE.

otherwise

UNIX and MVS routines are set to .FALSE., VMS set to .TRUE.

.. _winmvs: 
 


WINMVS
~~~~~~

LOGICAL FUNCTION WINMVS()

Windows Microsoft Visual Studio

Arguments:

none

Input:

none

Returns:

.TRUE.

for w32mvs

.FALSE.

otherwise

UNIX and VAX routines are set to .FALSE., MVS set to .TRUE.

.. _ttsend: 
 


TTSEND
~~~~~~

SUBROUTINE TTSEND (IUN, STR, ICC)

Write a string to a terminal with various carriage control options [for
LAUE]

Arguments:

INTEGER

IUN, ICC

CHARACTER*( * )

STR

Input:

IUN

Unit number for output

STR

The string to be output

ICC

| 0, no carriage control at the end of the string (for prompts), e.g.
  for routine TPROMP
| 1, normal carriage control, e.g. for routine TWRITE
| 2, no carriage control (for sending escape/control character sequences
  to ANSI/T4014 terminals, e.g. for QSCREEN graphics routines
| 3, output line at current point on screen (no leading linefeed or
  carriage return - trailing does not matter)

Output:

none

UNIX and MVS versions check if leading VMS-style carriage control
characters are obeyed for file IUN (issues of support and use are
discussed in `CCPOPN <#ccpopn>`__.

.. _getelapsed: 
 


GETELAPSED
~~~~~~~~~~

SUBROUTNE GETELAPSED

print timing information for CCPERR.

| Contains ENTRY INITFYP to initialise elapsed time. unix.m4 contains
  FPE initialisation for Sun OS.
| vms.for gets user id and use for argv[0] and splits command line into
  arguments (referenced in VMS routines `GETARG <#getarg>`__ and
  `IARGC <#iargc>`__.

.. _ugtarg: 
 


UGTARG
~~~~~~

SUBROUTINE UGTARG(I, ARG)

| get I'th command-line argument.
| UNIX and MVS use system call *getarg*, except for HPUX which calls
  *igetarg*. VMS calls vms.for routine GETARG.

Arguments:

INTEGER

I

CHARACTER*( * )

ARG

Input:

I

number of argument

Output:

ARG

argument string

.. _ccpspw: 
 


CCPSPW
~~~~~~

SUBROUTINE CCPSPW(STRING)

Spawns a new process to run shell command

Arguments:

CHARACTER*( * )

STRING

Input:

STRING

command

Output:

none

Uses system (UNIX, MVS) or LIB$SPAWN (VMS)

.. _cexit: 
 


CEXIT
~~~~~

SUBROUTINE CEXIT(ICODE)

Wrap around for EXIT routines.

Arguments:

INTEGER

ICODE

Input:

ICODE

error code

Output:

none

Machine specific routines are called to exit and flush FORTRAN buffers.
EXIT in most cases. See unix.mt code for details.

.. _rtnbks: 
 


RTNBKS
~~~~~~

CHARACTER FUNCTION RTNBKS()

Returns a Backslash for NT as unix compilers are fussy!

Arguments:

none

Input:

none

Returns:

| '\\' if MVS
| ' ' if UNIX or VMS

Result is coded in appropriate library.

.. _urenam: 
 


URENAM
~~~~~~

SUBROUTINE URENAM(NAME1,NAME2,STATUS)

| Rename file assigned to NAME1 to NAME2.
| VMS (vms.for) only.

Arguments:

CHARACTER*( * )

NAME1, NAME2

INTEGER

STATUS

Input:

NAME1

file name

NAME2

file name

Output:

STATUS

| 0, if ok
| <> if an error

.. _getarg: 
 


GETARG
~~~~~~

SUBROUTINE GETARG(INUM,LINE)

| Get INUM'th command line argument or ' ' into LINE
| VMS (vms.for) only. Replaces *getarg* system call.

Arguments:

INTEGER

INUM

CHARACTER*( * )

LINE

Input:

INUM

desired command line argument

Output:

LINE

command line argument

Gets the value from a common block (ARGS) set up in ENTRY INITFYP of
`GETELAPSED <#getelapsed>`__.

.. _ustenv: 
 


USTENV
~~~~~~

SUBROUTINE USTENV(LINE,IRESULT)

| Logical name assignment
| VMS (vms.for) only. UNIX and MVS versions use
  `library.c <library.html>`__ version which accesses *putenv* (POSIX)
  or *setenv* (non-POSIX).

Arguments:

CHARACTER*( * )

LINE

INTEGER

IRESULT

Input:

LINE

| line takes the form
| '<logical name>=<filename>'

Output:

IRESULT

0 iff successful

Under VMS there is no standard way of setting the environmental
variables. The work around stores the file name in a STRUCTURE array.

.. _ccpal1: 
 


CCPAL1
~~~~~~

SUBROUTINE CCPAL1 (ROUTNE, N, TYPE, LENGTH)

Arrange to call ROUTNE with N TYPEd array arguments of given. LENGTH
(see calling routine `CCPALC <./ccplib.html#ccpalc>`__ for fuller
discussion).

Arguments:

EXTERNAL

ROUTNE

INTEGER

N

INTEGER*( * )

LENGTH

CHARACTER*1 ( * )

TYPE

Input

ROUTNE

routine to call

N

number of arguments to ROUTNE (<=12)

TYPE

type of argument to ROUTNE
   'I': INTEGER
   'R': REAL
   'D': DOUBLE
   'C': COMPLEX

LENGTH

number of elements on each array

Output:

none

| Note: assumes 32 bit (4 byte words).
| VMS and MVS routines. UNIX uses a C routine in
  `library.c <./library.html>`__.

.. _iargc: 
 


IARGC
~~~~~

INTEGER FUNCTION IARGC(IDUMMY)

| Gets the number of command-line arguments.
| VMS only. UNIX and MVS use system call *iargc*.

Arguments:

INTEGER (dummy)

IDUMMY

Input:

IDUMMY

dummy argument

Returns:

number of command-line arguments.

Gets value from common block (ARGCOUNT) variable set up in ENTRY INITFYP
of `GETELAPSED <#getelapsed>`__. If value not set calls INITFYP.

.. _getpid: 
 


GETPID
~~~~~~

INTEGER FUNCTION GETPID(IDUMMY)

| Get process ID.
| VMS only, using VMS specific system call. UNIX and MVS use system call
  *getpid*.

Arguments:

INTEGER (dummy)

IDUMMY

Input:

IDUMMY

dummy argument

Returns:

process id

.. _hciftime: 
 


HCIFTIME
~~~~~~~~

SUBROUTINE HCIFTIME(CIFTIME)

Get local time and difference from GMT.

::

          1997-08-12T13:55:58-05:00
                     local    GMT
        

Arguments:

CHARACTER*( * ) (>25)

CIFTIME

Input:

none

Output:

CIFTIME

local time stamp and difference to GMT.

| version of the routine in SCALA.
| VMS uses f90 intrinsic Date_and_Time. MVS uses same intrinsic.
| Using f77: works on VMS Fortran V7 but not earlier versions works on
  Digital UNIX V4.0F.
| UNIX uses fortran system call *Ltime* and *gmtime* which dissect a
  UNIX time into month, day, etc., either for the local time zone or as
  GMT.

.. _binsrt: 
 


Binsortint Routines
~~~~~~~~~~~~~~~~~~~

SRTBEG, SRTRLS, SRTMRG and SRTRET in this file are the VMS versions of
library routines in binsortint.c (UNIX) and w32binsort (MVS). binsortint
is a set of routines used as an interface to binsort mainly from FORTRAN
programs.

.. _srtbeg: 
 


SRTBEG
~~~~~~

INTEGER FUNCTION SRTBEG(NKEYS,KEYB,LRECL,MEMSIZE)

integer binsort -sort initialisation. Launches VMS specific sort library
routine.

Arguments:

INTEGER

NKEYS, LRECL, MEMSIZE

INTEGER*2( * )

KEYB

Input:

NKEYS

number of keys

KEYB

for each key block have five data items

#. key type (=7 only REAL implemented)
#. sort order (ascending (0) or descending (1))
#. position within record (BYTES, 1st is position 0)
#. length (data units chars, shorts ...)
#. mask applied to data element before comparison (= 0 no mask, only
   implemented)

LRECL

(fixed) length of record (BYTES)

MEMSIZE

| size of memory used by sort (BYTES)
| if == 0, default size

Returns:

== 0

status OK

== 1

failure

.. _srtrls: 
 


SRTRLS
~~~~~~

INTEGER FUNCTION SRTRLS(RECORD)

integer binsort -pipes one record into sort routine

Arguments:

REAL( * )

RECORD

Input:

RECORD

one record of data to be sorted

Returns:

==0

status OK

.. _srtmrg: 
 


SRTMRG
~~~~~~

INTEGER FUNCTION SRTMRG()

integer binsort -finishes sequence of input records(?)

Arguments:

none

Returns

==0

status OK

.. _srtret: 
 


SRTRET
~~~~~~

INTEGER FUNCTION SRTRET(RECORD)

integer binsort -gets one record from sort routine

Arguments:

REAL( * )

RECORD

Input:

none

Output:

RECORD

one record of sorted data

Returns:

==0

status OK

==-1

sort failure

--------------
