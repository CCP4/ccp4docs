CCP4 from the command line
==========================

Before you start using the CCP4 suite from the command line, some directories need to be added to the PATH and some other environment variables need to be set. In the following instructions replace ``/path/to/`` and ``<version>`` with the appropriate text for your installation.

If you are happy to use the bash shell, you can obtain a correctly set up environment by executing the script ``/path/to/ccp4-<version>/start``.

If you want to run CCP4 programs in your current shell, run:

- ``source /path/to/ccp4-<version>/bin/ccp4.setup-sh`` (in bash/dash/zsh shells)
- ``source /path/to/ccp4-<version>/bin/ccp4.setup-csh`` (in csh/tcsh shells)

Windows users can get a correctly set up environment by running the ``CCP4Console`` program.