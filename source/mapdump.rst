MAPDUMP (CCP4: Supported Program)
=================================

NAME
----

**mapdump** - print a dump of sections of a map file

SYNOPSIS
--------

| **mapdump mapin** *foo.map*
| [input line]

.. _description: 
 
DESCRIPTION
---------------------------------

mapdump gives an ASCII dump of part or all of a binary map file,
specified on the command line with logical name **mapin**. Map header
information is listed first, see `library documentation <maplib.html>`__
for full description of this. Then map data is listed as a sequence of
sections. Each section is divided into pages, each of which contains a
maximum of 25 columns. The length of each page is the number of rows in
the map grid. Map data are given as the value held in the file
multiplied by a scale factor and taken to the nearest integer. The scale
factor may need to be adjusted to give useful output.

The program reads three numbers on standard input in free format: the
first and last section numbers of the map to be printed and the scale
factor by which to multiply the contents. The default (obtained with
input of `/') is to print the first section at unit scaling. An
immediate end-of-file on the input causes the program to print only the
map header (no sections). Any of the keywords 'RUN' 'GO' or 'END' can be
used instead of the end-of-file to start the program running.

See Also
--------

`maplib.html <maplib.html>`__: documentation on map format and library
of routines for reading/writing map files.

AUTHOR
------

Phil Evans
