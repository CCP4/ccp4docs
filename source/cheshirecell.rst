+-----------------------------------+-----------------------------------+
| .. rubric:: Cheshire cells        | ======== ======== ========        |
|    :name: cheshire-cells          | |image1|                          |
|                                   | |image2| |image3| |image4|        |
|                                   | |image5|                          |
|                                   | ======== ======== ========        |
|                                   |                                   |
|                                   | From `As long as I get            |
|                                   | somewhere... <http://ho           |
|                                   | me.wxs.nl/~ekici000/alice.htm>`__ |
+-----------------------------------+-----------------------------------+

From AMoRe documentation:

   "The Cheshire cell is the minimum volume which will allow a unique
   solution. For the first molecule it will be the cell which covers a
   volume from one possible origin to the next - you can usually see it
   by inspection of International Tables, e.g.: For P212121, the
   Cheshire cell is 0-0.5,0-0.5,0-0.5. For P21 the Cheshire cell is
   0-0.5,any y,0-0.5. If you are searching for the NMOLth molecule of a
   set, the Cheshire cell will now be the whole primitive volume. You
   have assigned the origin by choosing the position of the first
   molecule, and the other molecules will have to be positioned relative
   to that choice."

Ian Tickle wrote:

   Basically the Cheshire group is the space group of the crystal when
   its material contents are removed leaving only the symmetry elements,
   like the smile that was left when the Cheshire Cat disappeared. (In
   case you're not aware of the connection between Alice in Wonderland
   and CCP4, Charles Lutwidge Dodgson, alias Lewis Carroll, was born in
   Daresbury which is located in the English county of Cheshire).

   The higher the space group symmetry, the lower is the symmetry of the
   Cheshire group, *e.g.* for space group P1 the Cheshire group consists
   of a single point ( *i.e.* all points in the space are equivalent),
   which means that in P1 the translation function for 1 mol./a.u. is
   already solved before you start! Conversely, for primitive cubic
   space groups, the Cheshire cell has much lower symmetry, which means
   you have to search half the unit cell, even with 1 mol./a.u.

   In between these extremes there are all the other space groups. For
   further details look in my documentation for the TFFC program (you
   might even try using the program!).

From `Molecular
Replacement <http://www-structmed.cimr.cam.ac.uk/Course/MolRep/molrep.html>`__
- Protein Crystallography Course at Structural Medicine, Cambridge, UK:

   .. rubric:: Search volumes
      :name: search-volumes

   In conducting a translation search, you might think that it is
   necessary to search over the asymmetric unit. In fact, this is not
   true. The origin of a crystal is, to a certain extent, arbitrary. In
   P1, for instance, any point can be an origin, so it is not necessary
   to carry out a translation search. In higher symmetry space groups,
   the mathematical form of the symmetry operations restricts the
   possible origins, but there are still different valid choices. So all
   that needs to be searched is the unique volume relative to any
   allowed choice of origin. For an orthorhombic space group, the
   two-fold axes (rotational or screw) repeat every half unit cell in
   all three directions, so there are eight possible choices of origin.
   In contrast, the asymmetric unit for (say) P222 occupies a quarter of
   the unit cell.

   This unique volume relative to a possible choice of origin is called
   the Cheshire cell. It was probably named by a fan of Lewis Carroll,
   because the Cheshire cell is what is left when you take away the
   contents of the unit cell and leave only the symmetry operators (read
   "smile").

   Mind you, you are only limited to the Cheshire cell in searching for
   the first molecule, when there is more than one molecule in the
   asymmetric unit. Placing the first molecule fixes the origin, so
   subsequent searches for additional molecules have to cover the whole
   unit cell. (To be precise, the unique volume relative to a lattice
   point; the necessary search volume is reduced in a centered lattice.)

    Cheshire cells - table
==========================

A table of the Cheshire cells for all 230 space groups is available in
Hirshfeld (1968) Acta Cryst A24 301-311. It is reproduced below.

Z :sup:` x` is a primitive cell with x axes vanishing (or x axes not
important for symmetry). eb means "no b" (or: b axis not important).

+-----------------------+-----------------------+-----------------------+
| Cheshire group and    |                       | Space groups          |
| unit cell             |                       |                       |
+=======================+=======================+=======================+
| P_                   | 1/2a * 1/2b * 1/2c  | P_                   |
| P1                    |                       | P1                    |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 3`1       | ea * eb * ec        | P1                    |
+-----------------------+-----------------------+-----------------------+
| P2/m                  | 1/2a * 1/2b * 1/2c  | P2/m, P2 :sup:`1`/m, |
|                       |                       | C2/m, P2/c, C2/c      |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`2/m     | 1/2a * eb * 1/2c    | P2, P2 :sup:`1`, C2  |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 2`2/m     | ea * 1/2b * ec      | Pm, Pc, Cm, Cc        |
+-----------------------+-----------------------+-----------------------+
| Pmmm                  | 1/2a * 1/2b * 1/2c  | P222, P222 :sup:`1`, |
|                       |                       | P2:sub              |
|                       |                       | :`1`2 :sup:`1`2, |
|                       |                       | P2 :sup:`1`2:    |
|                       |                       | sub:`1`2 :sup:`1`, |
|                       |                       | C222 :sup:`1`, C222, |
|                       |                       | I222,                 |
|                       |                       | I2 :sup:`1`2:    |
|                       |                       | sub:`1`2 :sup:`1`, |
|                       |                       | Pmmm, Pnnn, Pccm,     |
|                       |                       | Pban, Pmma, Pnna,     |
|                       |                       | Pmna, Pcca, Pbam,     |
|                       |                       | Pccn, Pbcm, Pnnm,     |
|                       |                       | Pmmn, Pbcn, Pbca,     |
|                       |                       | Pnma, Cmcm, Cmca,     |
|                       |                       | Cmmm, Cccm, Cmma,     |
|                       |                       | Ccca, Fmmm, Immm,     |
|                       |                       | Ibam, Ibca, Imma      |
+-----------------------+-----------------------+-----------------------+
| Pnnn                  | 1/2a * 1/2b * 1/2c  | Fddd                  |
+-----------------------+-----------------------+-----------------------+
| Immm                  | 1/2a * 1/2b * 1/2c  | F222                  |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`mmm     | 1/2a * 1/2b * ec    | Pmm2, Pmc2 :sup:`1`, |
|                       |                       | Pcc2, Pma2,           |
|                       |                       | Pca2 :sup:`1`, Pnc2, |
|                       |                       | Pmn2 :sup:`1`, Pba2, |
|                       |                       | Pna2 :sup:`1`, Pnn2, |
|                       |                       | Cmm2, Cmc2 :sup:`1`, |
|                       |                       | Ccc2, Amm2, Abm2,     |
|                       |                       | Ama2, Aba2, Fmm2,     |
|                       |                       | Imm2, Iba2, Ima2      |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`ban     | 1/2a * 1/2b * ec    | Fdd2                  |
+-----------------------+-----------------------+-----------------------+
| P4 :sup:`2`22      | 1/2(a-b) * 1/2(a+b)  | P4 :sup:`1`22,     |
|                       | * 1/2c               | P4:sub              |
|                       |                       | :`1`2 :sup:`1`2, |
|                       |                       | P4 :sup:`3`22,     |
|                       |                       | P4:su               |
|                       |                       | b:`3`2 :sup:`1`2 |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`422     | 1/2(a-b) * 1/2(a+b)  | P4 :sup:`1`,         |
|                       | * ec                 | P4 :sup:`3`          |
+-----------------------+-----------------------+-----------------------+
| P4/mmm                | 1/2(a-b) * 1/2(a+b)  | P_                   |
| P4/mmm                | * 1/2c               | P4, P4/m,             |
|                       | 1/2(a-b) * 1/2(a+b)  | P4 :sup:`2`/m, P4/n, |
|                       | * 1/2c               | P4 :sup:`2`/n, I4/m, |
|                       |                       | P422,                 |
|                       |                       | P42 :sup:`1`2,     |
|                       |                       | P4 :sup:`2`22,     |
|                       |                       | P4:sub              |
|                       |                       | :`2`2 :sup:`1`2, |
|                       |                       | I422,                 |
|                       |                       | P_2m, P_2c, P_21m,    |
|                       |                       | P_21c, P_m2, P_c2,    |
|                       |                       | P_b2, P_n2, I_2m,     |
|                       |                       | P42m, P42c,           |
|                       |                       | P42 :sup:`1`m,     |
|                       |                       | P42 :sup:`1`c,     |
|                       |                       | P4m2, P4c2, P4b2,     |
|                       |                       | P4n2, I42m,           |
|                       |                       | P4/mmm, P4/mcc,       |
|                       |                       | P4/nbm, P4/nnc,       |
|                       |                       | P4/mbm, P4/mnc,       |
|                       |                       | P4/nmm, P4/ncc,       |
|                       |                       | P4 :sup:`2`/mmc,     |
|                       |                       | P4 :sup:`2`/mcm,     |
|                       |                       | P4 :sup:`2`/nbc,     |
|                       |                       | P4 :sup:`2`/nnm,     |
|                       |                       | P4 :sup:`2`/mbc,     |
|                       |                       | P4 :sup:`2`/mnm,     |
|                       |                       | P4 :sup:`2`/nmc,     |
|                       |                       | P4 :sup:`2`/ncm,     |
|                       |                       | I4/mmm, I4/mcm        |
+-----------------------+-----------------------+-----------------------+
| P4 :sup:`2`/nnm      | 1/2(a-b) * 1/2(a+b)  | I4 :sup:`1`/a,       |
|                       | * 1/2c               | I4 :sup:`1`22,     |
|                       |                       | I42d,                 |
|                       |                       | I4 :sup:`1`/amd,     |
|                       |                       | I4 :sup:`1`/acd      |
+-----------------------+-----------------------+-----------------------+
| I4/mmmI4/mmm          | 1/2(a-b) * 1/2(a+b)  | I_, I_m2, I_c2        |
|                       | * 1/2c               | I4, I4m2, I4c2        |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`4/mmm   | 1/2(a-b) * 1/2(a+b)  | P4, P4 :sup:`2`, I4, |
|                       | * ec                 | P4mm, P4bm,           |
|                       |                       | P4 :sup:`2`cm,     |
|                       |                       | P4 :sup:`2`nm,     |
|                       |                       | P4cc, P4nc,           |
|                       |                       | P4 :sup:`2`mc,     |
|                       |                       | P4 :sup:`2`bc,     |
|                       |                       | I4mm, I4cm            |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`4/nbm   | 1/2(a-b) * 1/2(a+b)  | I4 :sup:`1`,         |
|                       | * ec                 | I4 :sup:`1`md,     |
|                       |                       | I4 :sup:`1`cd      |
+-----------------------+-----------------------+-----------------------+
| R_m                   | -b * (a+b) * 1/2c   | R_, R32, R_m, R_c     |
| R3m                   |                       | R3, R32, R3m, R3c     |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`31m     | 1/3(a-b) * 1/3(a+2b) | R3, R3m, R3c          |
|                       | * ec                 |                       |
+-----------------------+-----------------------+-----------------------+
| P6 :sup:`2`22      | a * b * 1/2c        | P3 :sup:`1`21,     |
|                       |                       | P6 :sup:`1`22,     |
|                       |                       | P6 :sup:`4`22      |
+-----------------------+-----------------------+-----------------------+
|                       | 1/3 (a-b) *          | P3 :sup:`1`12      |
|                       | 1/3(a+2b) * 1/2c     |                       |
+-----------------------+-----------------------+-----------------------+
| P6 :sup:`4`22      | a * b * 1/2c        | P3 :sup:`2`21,     |
|                       |                       | P6 :sup:`5`22,     |
|                       |                       | P6 :sup:`2`22      |
+-----------------------+-----------------------+-----------------------+
|                       | 1/3(a-b) * 1/3(a+2b) | P3 :sup:`2`12      |
|                       | * 1/2c               |                       |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`622     | a * b * ec          | P6 :sup:`1`,         |
|                       |                       | P6 :sup:`5`,         |
|                       |                       | P6 :sup:`2`,         |
|                       |                       | P6 :sup:`4`          |
+-----------------------+-----------------------+-----------------------+
|                       | 1/3(a-b) * 1/3(a+2b) | P3 :sup:`1`,         |
|                       | * ec                 | P3 :sup:`2`          |
+-----------------------+-----------------------+-----------------------+
| P6/mmm                | a * b * 1/2c        | P_, P321, P_1m, P_1c, |
| P6/mmm                | a * b * 1/2c        | P_m1, P_c1,           |
|                       |                       | P3, P321, P31m, P31c, |
|                       |                       | P3m1, P3c1, P6/m,     |
|                       |                       | P6 :sup:`3`/m, P622, |
|                       |                       | P6 :sup:`3`22,     |
|                       |                       | P62m, P62c, P6/mmm,   |
|                       |                       | P6/mcc,               |
|                       |                       | P6 :sup:`3`/mcm,     |
|                       |                       | P6 :sup:`3`/mmc      |
+-----------------------+-----------------------+-----------------------+
|                       | 1/3(a-b) * 1/3(a+2b) | P312, P_, P_m2, P_c2  |
|                       | * 1/2c               | P312, P6, P6m2, P6c2  |
+-----------------------+-----------------------+-----------------------+
| Z :sup:` 1`6/mmm   | a * b * ec          | P31m, P31c, P6,       |
|                       |                       | P6 :sup:`3`, P6mm,   |
|                       |                       | P6cc,                 |
|                       |                       | P6 :sup:`3`cm,     |
|                       |                       | P6 :sup:`3`mc      |
+-----------------------+-----------------------+-----------------------+
|                       | 1/3(a-b) * 1/3(a+2b) | P3, P3m1, P3c1        |
|                       | * 1/2c               |                       |
+-----------------------+-----------------------+-----------------------+
| Ia3                   | a * b * c           | Pa3                   |
+-----------------------+-----------------------+-----------------------+
| I4 :sup:`1`32      | a * b * c           | P4 :sup:`3`32,     |
|                       |                       | P4 :sup:`1`32      |
+-----------------------+-----------------------+-----------------------+
| Pm3m                  | 1/2a * 1/2b * 1/2c  | Fm3, F432, Fm3m, Fm3c |
+-----------------------+-----------------------+-----------------------+
| Pn3m                  | 1/2a * 1/2b * 1/2c  | Fd3,                  |
|                       |                       | F4 :sup:`1`32,     |
|                       |                       | Fd3m, Fd3c            |
+-----------------------+-----------------------+-----------------------+
| Im3m                  | a * b * c           | P23, I23, Pm3, Pn3,   |
| Im3m                  | a * b * c           | Im3, P432, P4232,     |
|                       |                       | I432, P_3m, I_3m,     |
|                       |                       | P_3n                  |
|                       |                       | P23, I23, Pm3, Pn3,   |
|                       |                       | Im3, P432,            |
|                       |                       | P4 :sup:`2`32,     |
|                       |                       | I432, P43m, I43m,     |
|                       |                       | P43n, Pm3m, Pn3n,     |
|                       |                       | Pm3n, Pn3m, Im3m      |
+-----------------------+-----------------------+-----------------------+
|                       | 1/2a * 1/2b * 1/2c  | F23, F_3m, F_3c       |
|                       | 1/2a * 1/2b * 1/2c  | F23, F43m, F43c       |
+-----------------------+-----------------------+-----------------------+
| Ia3d                  | a * b * c           | P213, I213, Ia3,      |
| Ia3d                  | a * b * c           | I4132, I_3d           |
|                       |                       | P2 :sup:`1`3,      |
|                       |                       | I2 :sup:`1`3, Ia3, |
|                       |                       | I4 :sup:`1`32,     |
|                       |                       | I43d, Ia3d            |
+-----------------------+-----------------------+-----------------------+

More Alice in Wonderland - and the Cheshire Cat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------+-----------------------+-----------------------+
| |Cheshire Cat|        | `Alice's Adventures   | |Cheshire Cat         |
| From `Cheshire        | in                    | animation|            |
| Cat <http://w         | Wonderland <          | From                  |
| ww.webslingerz.com/jh | http://www.literature | `Purr-                |
| offman/chcat.html>`__ | .org/authors/carroll- | fection <http://www.p |
| - J. Hoffman          | lewis/alices-adventur | urr-fections.com/>`__ |
|                       | es-in-wonderland/>`__ |                       |
|                       |                       |                       |
|                       | `Lewis Carroll home   |                       |
|                       | page <ht              |                       |
|                       | tp://www.lewiscarroll |                       |
|                       | .org/carroll.html>`__ |                       |
+-----------------------+-----------------------+-----------------------+

.. |image1| image:: images/basetop.gif
   :width: 342px
   :height: 41px
.. |image2| image:: images/baself.gif
   :width: 179px
   :height: 127px
   :target: alis.htm
.. |image3| image:: images/cat.gif
   :width: 127px
   :height: 127px
   :target: ev.htm
.. |image4| image:: images/baseri.gif
   :width: 36px
   :height: 127px
.. |image5| image:: images/basebot.gif
   :width: 342px
   :height: 45px
.. |Cheshire Cat| image:: images/chcat1.gif
.. |Cheshire Cat animation| image:: images/animches.gif
