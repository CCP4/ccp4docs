|CCP4 web logo|

Basic Maths for Protein Crystallographers

Structure factor

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

There are many atoms in the unit cell and the reflections we see are the
sum of all their diffraction waves.

.. image:: ../images/bmg7_1.png


Acentric reflections
^^^^^^^^^^^^^^^^^^^^

Grouping symmetry-related atoms together:


.. image:: ../images/bmg7_2.png

An aside: from this expression it is easy to show that the symmetry
equivalent reflection h',k',l' is [h k l][S :sup:`i` ]. This means it is
NOT always possible to simply replace x,y,z with h,k,l in the
International Tables notations. In particular for a 3fold:



|matrix for 3fold symmetry|  


For acentric reflections the phase for each atom is randomly
distributed: 

|phase sum in vector representation|

If the atoms are positioned relative to a different **origin**, the
phase of the structure factor will change but not its magnitude.
Replacing (x :sup:`i` ,y :sup:`i` ,z :sup:`i` ) by (x :sup:`i` +Ox,
y :sup:`i` +Oy, z :sup:`i` +Oz), the structure factor contribution
becomes

e :sup:`2|pi|i{h(x :sup:`i` +Ox)+k(y :sup:`i` +Oy)+l(z :sup:`i` +Oz)}` = e :sup:`2|pi|ih·x`  e :sup:`2|pi|ih·O` 

for all atoms, and the structure factor now equals

    \|F\| e :sup:`i` |phi|  e :sup:`2` |pi| :sup:`ih·O` 

A list of alternative origins is available in `alternate_origins <alternate_origins.html>`__

The magnitude of the structure factor is also the same if the atoms are on a different **hand**, *i.e.* all x :sup:`i` ,y :sup:`i` ,z :sup:`i`  are replaced by (-x :sup:`i` ,-y :sup:`i` ,-z :sup:`i` ) and none of the atoms scatter anomalously. In this case \| F( **h** )\| e :sup:`i` |phi| ( **h** )  becomes \| F( **h** )\| e :sup:`-i` |phi| ( **h** ) .

*N.B.*: For some space groups, changing the hand of the atoms also
changes the symmetry operators, *e.g.* a 1/3 stepping screw axis will
convert to a -1/3 stepping axis ( *i.e.* the P3 :sup:`1`  symmetry
converts to P3 :sup:`2` ).

Centric reflections
^^^^^^^^^^^^^^^^^^^


For centric reflections the phase for atom pairs are related such that the contributions from two atoms of a pair always equal |phi| :sup:`c`  or |phi| :sup:`c`  + |pi| :   

|phase sum in vector representation for centric reflections|

Each atom has a symmetry partner such that their combined contribution
to the structure factor can be written as:

    |structure factor expressions for centric reflections|

The phase can then only be

    |phases for centric reflections (phi or phi+pi)|

In fact the only values |phi| :sup:`c`  can take are 0, |pi/6, pi/4,
pi/3|, *etc*.

As an example in spacegroup P2 :sup:`1` 2 :sup:`1` 2 :sup:`1` , with
symmetry-related positions x,y,z and -x+½,y+½,-z, for zone (h 0 l):

    |example of structure factor and phase calculation|



--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg8.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg6.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |large capital Sigma| image:: ../images/Sigma3tr.gif
   :width: 20px
   :height: 25px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |matrix for 3fold symmetry| image:: ../images/3foldmatrixtr.png

.. |phase sum in vector representation| image:: ../images/phasesumtr.gif
   :width: 51px
   :height: 102px
.. |phase sum in vector representation for centric reflections| image:: ../images/phasesumcentrictr.gif
   :width: 151px
   :height: 158px
.. |structure factor expressions for centric reflections| image:: ../images/centrictr.gif

.. |phases for centric reflections (phi or phi+pi)| image:: ../images/centric2tr.gif

.. |pi/6, pi/4, pi/3| image:: ../images/piover643tr.gif
   :width: 83px
   :height: 15px
.. |example of structure factor and phase calculation| image:: ../images/centricexampletr.gif

