|CCP4 web logo|

Basic Maths for Protein Crystallographers

Lattice geometry

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

.. raw:: html
   </table>

   <p>Diffraction from a crystal lattice is visible at discrete points conveniently indexed as an integer triple 
   (h,k,l), or in vector notation as
   h<strong>a<sup>*</sup></strong>+k<strong>b<sup>*</sup></strong>+l<strong>c<sup>*</sup></strong>,
   relative to the reciprocal lattice vectors
   <strong>a<sup>*</sup></strong>,<strong>b<sup>*</sup></strong>,<strong>c<sup>*</sup></strong>.
   These are defined to have the properties</p>
   <blockquote><table border="0">
   <tr>
   <td><strong>a<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>a</strong> =
   <strong>b<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>b</strong> =
   <strong>c<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>c</strong> = 1</td>
   </tr>
   <tr>
   <td><strong>a<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>b</strong> =
   <strong>a<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>c</strong> =
   <strong>b<sup>*</sup></strong><strong>&#183;</strong>&nbsp;<strong>c</strong> = 0</td>
   </tr>
   </table></blockquote>

   <p><em>i.e.</em> <strong>a<sup>*</sup></strong> is perpendicular to <strong>b</strong> and
   <strong>c</strong>, and therefore parallel
   to <strong>b&#215;c</strong>. <em>E.g.</em> in P3<sub>1</sub>:</p>


.. image:: ../images/p3geometrytr.gif
   :width: 398px
   :height: 223px
   :align: center


.. raw:: html

   <p>where <strong>c</strong> and <strong>c<sup>*</sup></strong> lie vertically in the plane of the drawing, and
   <strong>a<sup>*</sup></strong> comes perpendicularly out of the plane of the drawing.</p>



--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg4.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg2.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |P31 geometry| image:: ../images/p3geometrytr.gif
   :width: 398px
   :height: 223px
   :align: center
