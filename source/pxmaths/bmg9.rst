|CCP4 web logo|

Basic Maths for Protein Crystallographers

Phasing diagrams

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

SIR - Single Isomorphous Replacement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|SIR phase circles|

Phasing diagram for Single Isomorphous Replacement.

Two circles are drawn, one with radius \|F :sup:`P` \| centred on the
origin, and one with radius \|F :sup:`PH` \| about the end of the
vector - **F :sup:`H`**. The two points of intersection A and B of the
two circles correspond to two possible values of the protein phase. The
mean or "best" protein phase for such a circle will always equal
|phi| ( **h** ) or |phi| ( **h** )  + |pi|. The "figure of merit" is a
measure of the phase error. Here it equals cos|phi| :sup:`diff` and
indicates how separate the two crossings are. If |phi| :sup:`diff` = 90
then the FOM will equal 0.0.

If the heavy atom constellation is on the wrong hand, the resultant
phase estimates for |phi| :sup:`P` will also be on the wrong hand, but
the figure of merits will be identical.

.. image:: ../images/bmg9_1.png

Maps from different hands will be enantiomorphic.

MIR - Multiple Isomorphous Replacement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|MIR phase circles|

Phasing diagram for Multiple Isomorphous Replacement.

When two derivatives are available, both with heavy atoms positioned on
the **same origin and hand**, a third circle can be drawn, and in
favourable circumstances the three circles intersect in one clearly
defined point (point A in the diagram), thus resolving the phase
ambiguity.

To make sure the two derivatives have heavy atoms on the same origin and
same hand, it is important to use difference Fouriers to position the
second derivative, using phases for the protein determined from the
first derivative.

In this case no anomalous measurements for the derivatives are
available, and if the heavy atoms structure factor is calculated on the
other hand, a mirror image of the figure across the |phi|( **h** )=0
line will be generated, changing all phases to their complex conjugates,
including the resultant phase estimate for the protein. The resultant
map will be the enantiomorph of the first one, so both should show clear
solvent boundaries, and look "good", but one will show features like
left-handed helices.

.. image:: ../images/bmg9_2.png

In this figure there is a good crossing at ( |phi| :sup:`2` - |phi| :sup:`diff` :sup:`2`, |phi| :sup:`1`+|phi| :sup:`diff`` :sup:`1` ).

And in the corresponding one for the other hand, this would be at ( - |phi| :sup:`2`+ |phi| :sup:`diff` :sup:`2`, - |phi| :sup:`1` - |phi| :sup:`diff` :sup:`1` ).

SIRAS - Single Isomorphous Replacement with Anomalous Scattering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|SIRAS phase circles|

Phasing diagram for SIR with Anomalous Dispersion.

Referring to the SIR phase, the F :sup:`P` ( **h** ) phase will be |phi| :sup:`H` ( **h** ) + |phi| :sup:`diff` ( **h** ) or |phi| :sup:`H` ( **h** ) - |phi| :sup:`diff` ( **h** ). And for F :sup:`P` (- **h** ) the phase will be |phi| :sup:`H` (- **h** ) + |phi| :sup:`diff` (- **h** ) or |phi| :sup:`H` (- **h** ) - |phi| :sup:`diff` (- **h** ). The anomalous scattering of the heavy atoms means that |phi| :sup:`H` (- **h** ) is not the complex conjugate of |phi| :sup:`H` ( **h** ). And since \|F :sup:`PH` (+**h** )\| is not equal to \|F :sup:`PH` (- **h** ) \| the |phi| :sup:`diff` ( **h** ) is also different from |phi| :sup:`diff` (- **h** ). However the phase of F :sup:`P` (- **h** ) should equal -phase of F :sup:`P` (+ **h** ), so we can select which of the crossing is the most likely. To represent this using phasing circles we actually use a trick.

We consider F :sup:`PH` (- **h** ) as another "derivative" F' :sup:`PH` ( **h** ) where the heavy atom contribution equals F :sup:`H`` :sup:`real` ( **h** ) e :sup:`i`(|phi|( **h** )) + F" :sup:`H` :sup:`imag` ( **h** ) e :sup:`i` ( |phi| ( **h** ) - 90) with the anomalous contribution lagging 90 degrees BEHIND the real contribution.

This means that like in the MIR case, there are two centres for F :sup:`H` :sup:`i`, but of course they are always rather close. Taking F :sup:`PH` ( **h** ) as F :sup:`PH` :sup:`1` ( **h** ) and F :sup:`PH` (- **h** ) as F :sup:`PH` :sup:`2` ( **h** ), we can position the centres of the two "F :sup:`H` " vectors correctly, and in this figure the resultant |phi| :sup:`P` will equal |phi| ( **h** ) :sup:`1`+ |phi| :sup:`diff` :sup:`1`, or |phi| (- **h** ) :sup:`1` - |phi| :sup:`diff` :sup:`1` . If the heavy atoms are on the other hand, |phi| ( **h** ) :sup:`2` and |phi| (- **h** ) :sup:`2` are no longer the complex conjugates of |phi| ( **h** ) :sup:`1` and |phi| (- **h** ) :sup:`1` so although the crossings will be equally reliable at (|phi| ( **h** ) :sup:`2`+ |phi| :sup:`diff` :sup:`1`, |phi| (- **h** ) :sup:`2` -
  |phi| :sup:`diff :sup:`1``) and the figures of merit will be identical, this time the protein phase will not be the enantiomorph.
  
  Hence maps using the 2 different phases are not equivalent, and you should be able to recognise that one is "better" than the other, and indicates the correct hand.

.. image:: ../images/bmg9_3.png


Rewording this, the effect of anomalous scattering is to introduce a
phase shift, which is different for reflections forming a Friedel pair.
Thus there are circles with radii \| (F :sup:`hkl`) :sup:`PH` \| and
\| (F :sup:`-h-k-l`) :sup:`*` :sup:`PH` \| centered at - ( **F** :sup:`H` **+** **F"** :sup:`H` ) and - ( **F** :sup:`H` **-** **F"** :sup:`H` ) (points B and C in the diagram), intersecting with the circle corresponding to \|F :sup:`P` \| in only one point (point A in the diagram).

MIRAS - Multiple Isomorphous Replacement with Anomalous Scattering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No picture available yet

This can be seen as a merging of the MIR and the SIRAS cases. There
should now be no phase ambiguity, and in fact refining the anomalous
"occupancy" starting from Aocc = 0.0 should result in these occupancies
all becoming positive, indicating the hand is correct, or all negative
indicating it is wrong.

SAD - Single Anomalous Dispersion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For picture see `SIRAS case <#siras>`__

The SIRAS figure can be used to illustrate the case of SAD phasing
assuming F :sup:`H` :sup:`real` is zero. Here, as in the SIR phasing,
again there are only two measurements to consider: F :sup:`PH` ( **h** )
and F :sup:`PH` (- **h** ). The crossings will now indicate a phase of
|phi| :sup:`H` +90± |phi| :sup:`diff`, and as in the SIR case the
"best" phases will be |phi| ( **h** )+90, and the figure of merit equals
cos |phi| :sup:`diff`. If the hand of the heavy atoms is changed, the
phase will change to - |phi| :sup:`H` +90± |phi| :sup:`diff`, the
"best" phase will be - |phi| ( **h** ) +90, and the figure of merit will be
the same. In this case the map on the other hand is actually a Babinet
or negated copy of the inverse of the true map, *i.e.*
|phi| :sup:`best` :sup:`2` = - |phi| :sup:`best` :sup:`1` + 180.

.. image:: ../images/bmg9_4.png

Maps from different hands will be enantiomorphic Babinet maps, *i.e.*
all negative density in one becomes positive density in the other, since
the |phi| :sup:`best` (-) = - |phi| :sup:`best` (+) + 180.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg10.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg8.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |SIR phase circles| image:: ../images/SIRtr.gif
   :width: 545px
   :height: 520px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
.. |pi| image:: ../images/pitr.gif
   :width: 10px
   :height: 11px
.. |MIR phase circles| image:: ../images/MIRtr.gif
   :width: 548px
   :height: 520px
.. |SIRAS phase circles| image:: ../images/SIRADtr.gif
   :width: 545px
   :height: 516px
.. |phi| image:: ../images/pitr.gif
   :width: 8px
   :height: 16px
.. |phi| image:: ../images/phitr.gif
   :width: 8px
   :height: 16px
