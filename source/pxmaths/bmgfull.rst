|CCP4 web logo|

Basic Maths for Protein Crystallographers

Step title

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

| 

**This document is best viewed with 12pt Times and 10pt Courier fonts.**

Crystallographic Equations and Some Consequences
================================================

Crystal geometry
~~~~~~~~~~~~~~~~

| Crystal axes a, b, c; crystal angles |image4|, geometrically defined
  thus:

|image5|

Atom notation
~~~~~~~~~~~~~

An atom in a crystal can be referenced as (x :sup:`i` ), or as a
"coordinate triple x :sup:`i`,y :sup:`i`,z :sup:`i`", or in vector
notation as
x :sup:`i` **a** +y :sup:`i` **b** +z :sup:`i` **c** . Its
position in another unit cell is (x :sup:`i`+n :sup:`x`) **a** +
(y :sup:`i`+n :sup:`y`) **b** + (z :sup:`i`+n :sup:`z`) **c**. The
atom's position is given **relative to some origin**, which is
conveniently chosen relative to the symmetry axes.

Lattice geometry
~~~~~~~~~~~~~~~~

Diffraction from a crystal lattice is visible at discrete points
conveniently indexed as an integer triple (h,k,l), or in vector notation
as **h** **a** :sup:`*` **+k** **b** :sup:`*` **+z** c** :sup:`*`,
relative to the reciprocal lattice vectors
**a** :sup:`*`, **b** :sup:`*`,**c** :sup:`*`. These are
defined to have the properties

    +-----------------------------------------------------------------------------------------+
    | **a :sup:`*`·** **a** = **b :sup:`*`·** **b** = **c :sup:`*`·** **c** = 1   |
    +-----------------------------------------------------------------------------------------+
    | **a :sup:`*`·** **b** = **a :sup:`*`·** **c** = **b :sup:`*`·** **c** = 0   |
    +-----------------------------------------------------------------------------------------+

| *i.e.* **a** :sup:`*` is perpendicular to **b** and **c**, and
  therefore parallel to **b×c**. *E.g.* in P3 :sup:`1`:

|image6|

| 
| where **c** and **c** :sup:`*` lie vertically in the plane of the
  drawing, and **a** :sup:`*` comes perpendicularly out of the plane
  of the drawing.

Symmetry operators
~~~~~~~~~~~~~~~~~~

Since crystals are repeating lattices with straight edges, the only
symmetry elements compatible with crystallinity are:

+--------------------------------------+--------------------------------------+
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| |         | rotation axis perpendicu | | screw axis   | with translation st |
| lar to a crystal plane   |           | eps   |                              |
| +=========+========================= | +==============+==================== |
| =========================+           | ======+                              |
| | 2fold   | |image7|                 | | |image11|    | 1/2                 |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 3fold   | |image8|                 | | |image12|    | 1/3 or 2/3          |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 4fold   | |image9|                 | | |image13|    | 1/4, 2/4, or 3/4    |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 5fold   | **×**                    | | **×**        | **×**               |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 6fold   | |image10|                | | |image14|    | 1/6,2/6,3/6,4/6,or  |
|                          |           | 5/6   |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
+--------------------------------------+--------------------------------------+

Symmetry-equivalent atoms are tabulated in the International Tables like
this: (x,y,z), (-y, x-y, z+1/3), (y-x, -x, z+2/3) (this is for
spacegroup P3 :sup:`1`). But it is useful to think of symmetry
operators as 3x4 or 4x4 matrices and generate symmetry related atoms
thus. Then the symmetry-related coordinates in P3 :sup:`1` are |P31
symmetry operators - general|,    *i.e.*    |P31 symmetry operators -
specific|.

| If the [S :sup:`i`] is augmented to [S' :sup:`i`], a 4x4 matrix, by the
  addition of a fourth row, [0 0 0 1], the augmented symmetry operators
  make a closed group, *i.e.*
| for some i,j and k: [S' :sup:`i`][S' :sup:`j`]=[S' :sup:`k`]and
  [S' :sup:`i`] :sup:`-1`=[S' :sup:`k`].

Resolution
~~~~~~~~~~

| The length of the reciprocal lattice vector
  h**a :sup:`*`**+k**b :sup:`*`**+l**c :sup:`*`** is
  called d :sup:`*` and equals |image17|.
| Crystallographers often define **S** as (d :sup:`*`)² or |image18|
  Protein crystallographers talk about resolution in terms of
  1/d :sup:`*` (or d), hence the 'high resolution' limit is actually a
  small number.

Form factors
~~~~~~~~~~~~

A single atom in a lattice will diffract with an amplitude proportional
to its form factor f(i,**S** ).

+----------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| |scattering factor; borrowed from Crystallography 101|   | Scattering curve for O (oxygen): electrons v d :sup:`*`. For a description/further explanation of this graph, see `Crystallography 101 - Calculation of Atomic Scattering Factors <http://www-structure.llnl.gov/xray/comp/scatfac.htm>`__   |
+----------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

| The atom's diffracting power is further reduced at higher resolution
  by any atomic vibration ( *i.e.* temperature factor), and the combined
  scattering is often labelled g(i,**S** ). If the oscillation is
  isotropic
| g(i,**S** ) = f(i,**S** ) e :sup:`-B :sup:`i`**S**²`

The diffraction "wave" from each atom for any given reflection will have
a phase dependent on its position within the lattice. By convention the
origin is chosen relative to the symmetry operators.

An atom's contribution can be expressed in various ways, often used
concurrently in text books:

    |image20|

Structure factor
~~~~~~~~~~~~~~~~

There are many atoms in the unit cell and the reflections we see are the
sum of all their diffraction waves.

    +--------------------------+--------------------------+--------------------------+
    | F(h k l) or F( **h** ) = | +-----------------+      | g(i,**S** )             |
    | |F( **h** )|e :sup:`i | | |image22|       |      | e :sup:`2|image23|i |
    | |image21|( **h** )`    | +-----------------+      | (hx :sup:`i`+ky :sup:` |
    | =                        | | i=1,all atoms   |      | i`+lz :sup:`i`)`      |
    |                          | +-----------------+      |                          |
    +--------------------------+--------------------------+--------------------------+

Acentric reflections
^^^^^^^^^^^^^^^^^^^^

Grouping symmetry-related atoms together:

    F( **h** )
    =
    +-------------+
    | |image24|   |
    +-------------+
    | i=asymm.    |
    | unit        |
    +-------------+

    g(i,**S** )
    (
    +--------------------------------------+--------------------------------------+
    | e :sup:`2|image25|i (h k l)`    | ::                                   |
    |                                      |                                      |
    |                                      |     æxö                              |
    |                                      |     çy÷                              |
    |                                      |     èzø                              |
    +--------------------------------------+--------------------------------------+

    +
    +--------------------------------------+--------------------------------------+
    | e :sup:`2|image26|i             | ::                                   |
    | **h·**[S :sup:`i`]`                 |                                      |
    |                                      |     æxö                              |
    |                                      |     çy÷                              |
    |                                      |     èzø                              |
    +--------------------------------------+--------------------------------------+

    +....
    )
    =
    |F( **h** )| e :sup:`i|image27| :sup:`h``

An aside: from this expression it is easy to show that the symmetry
equivalent reflection h',k',l' is [h k l][S :sup:`i`]. This means it is
NOT always possible to simply replace x,y,z with h,k,l in the
International Tables notations. In particular for a 3fold:

    +-------------------------------------------------+-------------+----------------+
    | (h :sup:`2` k :sup:`2` l :sup:`2`) = (h k l)   | |image28|   | = (k -h-k l)   |
    +-------------------------------------------------+-------------+----------------+

For acentric reflections the phase for each atom is randomly
distributed: |phase sum in vector representation|

If the atoms are positioned relative to a different **origin**, the
phase of the structure factor will change but not its magnitude.
Replacing (x :sup:`i`,y :sup:`i`,z :sup:`i`) by (x :sup:`i`+Ox,
y :sup:`i`+Oy, z :sup:`i`+Oz), the structure factor contribution
becomes

    e :sup:`2|image30|i{h(x :sup:`i`+Ox)+k(y :sup:`i`+Oy)+l(z :sup:`i`+Oz)}`
    = e :sup:`2|image31|i**h·x**`
    e :sup:`2|image32|i**h·O**`

for all atoms, and the structure factor now equals

    |F| e :sup:`i|image33|`e :sup:`i**h·O**`

A list of alternative origins is available in
$CLIBD/alternate-origins.list.

The magnitude of the structure factor is also the same if the atoms are
on a different **hand**, *i.e.* all x :sup:`i`,y :sup:`i`,z :sup:`i`
are replaced by (-x :sup:`i`,-y :sup:`i`,-z :sup:`i`) and none of the
atoms scatter anomalously. In this case |F( **h** )|
e :sup:`i|image34|( **h** )` becomes |F( **h** )|
e :sup:`-i|image35|( **h** )`.

*N.B.*: For some space groups, changing the hand of the atoms also
changes the symmetry operators, *e.g.* a 1/3 stepping screw axis will
convert to a -1/3 stepping axis ( *i.e.* the P3 :sup:`1` symmetry
converts to P3 :sup:`2`).

Centric reflections
^^^^^^^^^^^^^^^^^^^

+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+
| For centric reflections the phase for atom pairs are related such that the contributions from two atoms of a pair always equal |image36| :sup:`c` or |image37| :sup:`c` + |image38|:   | |phase sum in vector representation for centric reflections|   |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------+

Each atom has a symmetry partner such that their combined contribution
to the structure factor can be written as:

    |image40|

The phase can then only be

    |image41|

In fact the only values |image42| :sup:`c` can take are 0, |image43|,
*etc*.

As an example in spacegroup P2 :sup:`1`2 :sup:`1`2 :sup:`1`, with
symmetry-related positions x,y,z and -x+½,y+½,-z, for zone (h 0 l):

    |image44|

Phasing
~~~~~~~

An X-ray experiment allows us to measure all I( **h** ) to some
resolution limit. If we knew both |F( **h** )| and |image45|( **h** )
then we could generate a map of the unit cell having peaks at and only
at each atom position using the Fourier summation

    |image46|

This (the presence of peaks) is the fundamental property of crystal
diffraction which underpins all structure solution methods.

But the phases cannot be measured directly and have to be inferred from
differences between sets of intensity measurements. The experimental
techniques to find them are loosely labelled as MIR, MIRAS, SIR, SIRAS,
MAD, SAD:

SIR
    single isomorphous replacement. Measurements are taken from a
    "native" protein and one "derivative" where some additional atoms
    have been incorporated into the lattice.
MIR
    multiple isomorphous replacement. Measurements are taken from a
    "native" protein and several derivatives.
SIRAS
    single isomorphous replacement plus anomalous differences. As above
    but the anomalous measurements for F(h) and F(-h) for the derivative
    are used.
MIRAS
    multiple isomorphous replacement plus anomalous differences.
SAD
    single anomalous dispersion. The "native" crystal contains some
    atoms which scatter anomalously, and these differences are used in a
    similar way to the SIR treatment.
MAD
    multiple anomalous dispersion.

We need to consider the structure factor equation in more detail before
discussing these.

    |image47|

In fact the scattering factor f(i,**S** ) is:

    f(i,**S** ) + f'(i) + i f"(i)

where f' and f" describe the scattering from inner electron shells,
which varies as a function of the wavelength, but is more or less
constant at all resolutions ( *i.e.* f"(i,**S** ) = f"(i)). For many
elements ( C, N, O in particular) f' and f" are very small at all
accessible wavelengths. Others, such as S and Cl have a small but
detectable component at CuK|image48| (f" ~ 0.5). In general transition
elements such as Se, Br have observable f" (f" ~ 3-4) at short
wavelengths. Metals and other heavy elements such as Hg, Pt, I *etc.*
have quite large f" and f' contributions at most accessible wavelengths
(at CuK|image49| f" :sup:`Hg` ~ 8).

It helps to re-write the F :sup:`H`( **h** ) or F :sup:`A`( **h** )
component like this:

+-------------+-----------------------------------------------------------------------+
| |image50|   | |pictorial representation of structure factor for h with anomalous|   |
+-------------+-----------------------------------------------------------------------+

which means that, although the magnitudes of F :sup:`H`( **h** ) and
F :sup:`H`(-**h** ) are equal, their phases are different, and
F :sup:`H`(-**h** ) is no longer the complex conjugate of
F :sup:`H`( **h** ).

And since F :sup:`PH`( **h** ) = F :sup:`H`( **h** ) +
F :sup:`P`( **h** ), and F :sup:`PH`(-**h** ) = F :sup:`H`(-**h** )
+ F :sup:`P`(-**h** ) it follows that neither the magnitudes of
|F :sup:`PH`( **h** )| and |F :sup:`PH`(-**h** )| are equal, nor
the phase |image52| :sup:`PH`( **h** ) equal to
-|image53| :sup:`PH`(-**h** ).

How does this help phasing?
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Answer: In no way unless we can position the heavy (or anomalous) atoms.

If they are known, the vector F :sup:`H`( **h** ) can be calculated and
from the knowledge of the three magnitudes |F :sup:`H`( **h** )|,
|F :sup:`P`( **h** )| and |F :sup:`PH`( **h** )| plus the phase of
F :sup:`H`( **h** ), it is easy to show from a phase triangle that
|image54| :sup:`P` will have to equal
|image55| :sup:`H`±|image56| :sup:`diff`.

+--------------------------------------------------------------------------------------------------------------+---------------------+
| This is often represented with "phase circles" (or `phasing diagrams <bmg9.html>`__) or "phase triangles":   | |phase triangles|   |
+--------------------------------------------------------------------------------------------------------------+---------------------+

Deviation into Patterson theory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Since there are usually only a few heavy atoms associated with many
protein atoms, they can usually be positioned using Pattersons or direct
methods. Both these techniques require only an estimate of the
**magnitude** of the F :sup:`H`( **h** ).

It maybe is worth summarising here the theory behind difference
Pattersons.

| F :sup:`PH`( **h** ) = F :sup:`H`( **h** ) + F :sup:`P`( **h** ).
| The cos rule gives: |F :sup:`PH`( **h** )|² =
  |F :sup:`H`( **h** )|² + |F :sup:`P`( **h** )|² + 2
  |F :sup:`H`( **h** )| |F :sup:`P`( **h** )|
  cos|image58| :sup:`diff`
| where |image59| :sup:`diff` is the phase between vector
  F :sup:`H`( **h** ) and vector F :sup:`P`( **h** ). From this we can
  approximate:

    |F :sup:`PH`( **h** )| = {|F :sup:`H`( **h** |² +
    |F :sup:`P`( **h** )|² + 2 |F :sup:`H`( **h** )|
    |F :sup:`P`( **h** )| cos|image60| :sup:`diff`} :sup:`½` =
    |F :sup:`P`( **h** )| {1 + 2
    |F :sup:`H`( **h** )|/ |F :sup:`P`( **h** )|
    cos|image61| :sup:`diff` +
    (|F :sup:`H`( **h** )|/ |F :sup:`P`( **h** )|)²} :sup:`½`

The binomial theorem gives (1+x) :sup:`½` ~ 1 + x/2 when x is small, so

    |F :sup:`PH`( **h** )| ~ |F :sup:`P`( **h** )| {1 +
    |F :sup:`H`( **h** )|/ |F :sup:`P`( **h** )|
    cos|image62| :sup:`diff` +
    ½(|F :sup:`H`( **h** )|/ |F :sup:`P`( **h** )|)²}
    = |F :sup:`P`( **h** )| + |F :sup:`H`( **h** )|
    cos|image63| :sup:`diff` +
    ½|F :sup:`H`( **h** )|²/ |F :sup:`P`( **h** )|

| So |F :sup:`PH`( **h** )| - |F :sup:`P`( **h** )| ~
  |F :sup:`H`( **h** )| cos|image64| :sup:`diff` + an even smaller
  term, providing |F :sup:`H`( **h** )| is small compared to
  |F :sup:`P`( **h** )|.
| and a Patterson with coefficients (|F :sup:`PH`( **h** )| -
  |F :sup:`P`( **h** )|)² is approximately equivalent to one with
  coefficients (|F :sup:`H`( **h** )| cos|image65| :sup:`diff`)² =
  ½|F :sup:`H`( **h** )|² (1 + cos 2|image66| :sup:`diff`)
  (remember: cos²(x) = (1+cos(2x))/2)
| The summation of ½|F :sup:`H`( **h** )|² will give the normal
  Patterson distribution of vectors between related atoms while the
  summation of ½|F :sup:`H`( **h** )|² cos 2|image67| :sup:`diff`
  will generate only noise.

Deviation into Difference Fourier Theory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similar equations explain why a Fourier summation gives full weight
peaks at the atomic positions which have been included in the phasing,
and peaks at about half the expected height for atoms excluded from the
phasing. Say F :sup:`PH`( **h** ) = F :sup:`P`( **h** ) +
F :sup:`H`( **h** ) where F :sup:`H` is much smaller than F :sup:`P`;
*i.e.* only a few atoms are excluded from the phasing. Then as above

    |F :sup:`PH` | ~ |F :sup:`P` | + |F :sup:`H` |
    cos(|image68| :sup:`P`-|image69| :sup:`H`) + small terms

The Fourier summation

    |image70||F :sup:`PH` | e :sup:`i|image71| :sup:`P`` =
    |image72||F :sup:`P` | e :sup:`i|image73| :sup:`P`` +
    |image74||F :sup:`H` |
    cos(|image75| :sup:`P`-|image76| :sup:`H`)
    e :sup:`i|image77| :sup:`P``

Since cos(x) = (e :sup:`ix` + e :sup:`-ix`)/2

    cos(|image78| :sup:`P`-|image79| :sup:`H`)
    e :sup:`i|image80| :sup:`P`` = ½ (e :sup:`i|image81| :sup:`H``
    + e :sup:`i(2|image82| :sup:`P`-|image83| :sup:`H`)`)

and the second term becomes

    |image84|F :sup:`H` ½(e :sup:`i|image85| :sup:`H`` +
    e :sup:`i(2|image86| :sup:`P`-|image87| :sup:`H`)`)

giving the Fourier map for the atoms contributing to F :sup:`H` at half
weight, plus noise, since the phase
2|image88| :sup:`P`-|image89| :sup:`H` is not related to these atoms
at all.

Phasing diagrams
~~~~~~~~~~~~~~~~

SIR

|SIR phase circles|

Phasing diagram for Single Isomorphous Replacement. Two circles are
drawn, one with radius |F :sup:`P` | centred on the origin, and one
with radius |F :sup:`PH` | about the end of the vector
-**F :sup:`H`**. The two points of intersection A and B of the two
circles correspond to two possible values of the protein phase. The mean
or "best" protein phase for such a circle will always equal
|image91|( **h** ) or |image92|( **h** )+|image93|. The "figure of
merit" is a measure of the phase error. Here it equals
cos|image94| :sup:`diff` and indicates how separate the two crossings
are. If |image95| :sup:`diff` = 90 then the FOM will equal 0.0.

If the heavy atom constellation is on the wrong hand, the resultant
phase estimates for |image96| :sup:`P` will also be on the wrong hand,
but the figure of merits will be identical.

The phase ambiguities are:

Crossing

Hand

| |image97|( **h** ) :sup:`1` + |image98| :sup:`diff :sup:`1``
| |image99|( **h** ) :sup:`1` - |image100| :sup:`diff :sup:`1``

| -|image101|( **h** ) :sup:`1` + |image102| :sup:`diff :sup:`1``
| -|image103|( **h** ) :sup:`1` - |image104| :sup:`diff :sup:`1``

| |image105| :sup:`best` = |image106|( **h** ) :sup:`1` or
| |image107|( **h** ) :sup:`1`+|image108|

| |image109| :sup:`best` = -|image110|( **h** ) :sup:`1` or
| -|image111|( **h** ) :sup:`1`+|image112|

Maps from different hands will be enantiomorphic.

| For better resolution, click on the picture.

MIR

|MIR phase circles|

Phasing diagram for Multiple Isomorphous Replacement. When two
derivatives are available, both with heavy atoms positioned on the
**same origin and hand**, a third circle can be drawn, and in favourable
circumstances the three circles intersect in one clearly defined point
(point A in the diagram), thus resolving the phase ambiguity .

To make sure the two derivatives have heavy atoms on the same origin and
same hand, it is important to use difference Fouriers to position the
second derivative, using phases for the protein determined from the
first derivative.

In this case no anomalous measurements for the derivatives are
available, and if the heavy atoms structure factor is calculated on the
other hand, a mirror image of the figure across the
|image114|( **h** )=0 line will be generated, changing all phases to
their complex conjugates, including the resultant phase estimate for the
protein. The resultant map will be the enantiomorph of the first one, so
both should show clear solvent boundaries, and look "good", but one will
show features like left-handed helices.

The phase ambiguities are:

Crossing

Hand

| |image115|( **h** ) :sup:`1` + |image116| :sup:`diff :sup:`1``
| |image117|( **h** ) :sup:`1` - |image118| :sup:`diff :sup:`1``

| -|image119|( **h** ) :sup:`1` + |image120| :sup:`diff :sup:`1``
| -|image121|( **h** ) :sup:`1` - |image122| :sup:`diff :sup:`1``

| |image123|( **h** ) :sup:`2` + |image124| :sup:`diff :sup:`2``
| |image125|( **h** ) :sup:`2` - |image126| :sup:`diff :sup:`2``

| -|image127|( **h** ) :sup:`2` + |image128| :sup:`diff :sup:`2``
| -|image129|( **h** ) :sup:`2` - |image130| :sup:`diff :sup:`2``

| In this figure there is a good crossing at
| (|image131| :sup:`2`-|image132| :sup:`diff :sup:`2``,
  |image133| :sup:`1`+|image134| :sup:`diff :sup:`1``).
| And in the corresponding one for the other hand,
| this would be at
| (-|image135| :sup:`2`+|image136| :sup:`diff :sup:`2``,
  -|image137| :sup:`1`-|image138| :sup:`diff :sup:`1``).

| For better resolution, click on the picture.

SIRAS

|SIRAS phase circles|

| Phasing diagram for SIR with Anomalous Dispersion. Referring to the
  SIR phase, the F :sup:`P`( **h** ) phase will be
  |image140| :sup:`H`( **h** ) + |image141| :sup:`diff`( **h** ) or
  |image142| :sup:`H`( **h** ) - |image143| :sup:`diff`( **h** ). And
  for F :sup:`P`(-**h** ) the phase will be
  |image144| :sup:`H`(-**h** ) + |image145| :sup:`diff`(-**h** ) or
  |image146| :sup:`H`(-**h** ) - |image147| :sup:`diff`(-**h** ).
  The anomalous scattering of the heavy atoms means that
  |image148| :sup:`H`(-**h** ) is not the complex conjugate of
  |image149| :sup:`H`( **h** ). And since |F :sup:`PH`(+**h** )| is
  not equal to |F :sup:`PH`(-**h** )| the
  |image150| :sup:`diff`( **h** ) is also different from
  |image151| :sup:`diff`(-**h** ). However the phase of
  F :sup:`P`(-**h** ) should equal -phase of F :sup:`P`(+**h** ), so
  we can select which of the crossing is the most likely. To represent
  this using phasing circles we actually use a trick. We consider
  F :sup:`PH`(-**h** ) as another "derivative" F' :sup:`PH`( **h** )
  where the heavy atom contribution equals
  F :sup:`H :sup:`real``( **h** )e :sup:`i(|image152|( **h** ))` +
  F" :sup:`H :sup:`imag``( **h** ) e :sup:`i(|image153|( **h** ) -
  90)` with the anomalous contribution lagging 90 degrees BEHIND the
  real contribution.
| This means that like in the MIR case, there are two centres for
  F :sup:`H :sup:`i``, but of course they are always rather close.
  Taking F :sup:`PH`( **h** ) as F :sup:`PH :sup:`1``( **h** ) and
  F :sup:`PH`(-**h** ) as F :sup:`PH :sup:`2``( **h** ), we can
  position the centres of the two "F :sup:`H`" vectors correctly, and in
  this figure the resultant |image154| :sup:`P` will equal
  |image155|( **h** ) :sup:`1`+|image156| :sup:`diff :sup:`1``,
  or |image157|(-**h** ) :sup:`1`-|image158| :sup:`diff :sup:`1``.
  If the heavy atoms are on the other hand,
  |image159|( **h** ) :sup:`2` and |image160|(-**h** ) :sup:`2` are
  no longer the complex conjugates of |image161|( **h** ) :sup:`1` and
  |image162|(-**h** ) :sup:`1` so although the crossings will be
  equally reliable at (|image163|( **h** ) :sup:`2`+
  |image164| :sup:`diff :sup:`1``, |image165|(-**h** ) :sup:`2`-
  |image166| :sup:`diff :sup:`1``) and the figures of merit will be
  identical, this time the protein phase will not be the enantiomorph.
| Hence maps using the 2 different phases are not equivalent, and you
  should be able to recognise that one is "better" than the other, and
  indicates the correct hand.

The phase ambiguities are:

Crossing

Hand

| |image167|( **h** ) :sup:`1` :sup:`+` +
  |image168| :sup:`diff` :sup:`+`
| |image169|( **h** ) :sup:`1` :sup:`+` -
  |image170| :sup:`diff` :sup:`+`

| |image171|( **h** ) :sup:`2` :sup:`+` +
  |image172| :sup:`diff` :sup:`+`
| |image173|( **h** ) :sup:`2` :sup:`+` -
  |image174| :sup:`diff` :sup:`+`

| |image175|( **h** ) :sup:`1` :sup:`-` +
  |image176| :sup:`diff` :sup:`-`
| |image177|( **h** ) :sup:`1` :sup:`-` -
  |image178| :sup:`diff` :sup:`-`

| |image179|( **h** ) :sup:`2` :sup:`-` +
  |image180| :sup:`diff` :sup:`-`
| |image181|( **h** ) :sup:`2` :sup:`-` -
  |image182| :sup:`diff` :sup:`-`

Rewording this, the effect of anomalous scattering is to introduce a
phase shift, which is different for reflections forming a Friedel pair.
Thus there are circles with radii |(F :sup:`hkl`) :sup:`PH` | and
|(F :sup:`-h-k-l`) :sup:`*` :sup:`PH` | centred at
-( **F :sup:`H`**+**F" :sup:`H`** ) and
-( **F :sup:`H`**-**F" :sup:`H`** ) (points B and C in the diagram),
intersecting with the circle corresponding to |F :sup:`P` | in only
one point (point A in the diagram).

| For better resolution, click on the picture.

MIRAS

No picture available yet

| This can be seen as a merging of the MIR and the SIRAS cases. There
  should now no phase ambiguity, and in fact refining the anomalous
  "occupancy" starting from Aocc = 0.0 should result in these
  occupancies all becoming positive, indicating the hand is correct, or
  all negative indicating it is wrong.

SAD

For picture see `SIRAS case <#siras_picture>`__

The SIRAS figure can be used to illustrate the case of SAD phasing
assuming F :sup:`H :sup:`real`` is zero. Here, as in the SIR phasing,
again there are only two measurements to consider: F :sup:`PH`( **h** )
and F :sup:`PH`(-**h** ). The crossings will now indicate a phase of
|image183| :sup:`H`+90±|image184| :sup:`diff`, and as in the SIR
case the "best" phases will be |image185|( **h** )+90, and the figure of
merit equals cos|image186| :sup:`diff`. If the hand of the heavy
atoms is changed, the phase will change to
-|image187| :sup:`H`+90±|image188| :sup:`diff`, the "best" phase
will be -|image189|( **h** )+90, and the figure of merit will be the
same. In this case the map on the other hand is actually a Babinet or
negated copy of the inverse of the true map, *i.e.*
|image190| :sup:`best :sup:`2`` = -|image191| :sup:`best :sup:`1`` +
180.

The phase ambiguities are:

Crossing

Hand

| |image192|( **h** ) :sup:`1` + 90 + |image193| :sup:`diff :sup:`1``
| |image194|( **h** ) :sup:`1` - 90 + |image195| :sup:`diff :sup:`1``

| -|image196|( **h** ) :sup:`1` + 90 +
  |image197| :sup:`diff :sup:`1``
| -|image198|( **h** ) :sup:`1` - 90 +
  |image199| :sup:`diff :sup:`1``

| |image200| :sup:`best` = |image201|( **h** ) :sup:`1` + 90 or
| |image202|( **h** ) :sup:`1` + 90 + |image203|

| |image204| :sup:`best` = -|image205|( **h** ) :sup:`1` + 90 or
| -|image206|( **h** ) :sup:`1` + 90 + |image207|

| Maps from different hands will be enantiomorphic
| Babinet maps, *i.e.* all negative density in one becomes
| positive density in the other, since the
| |image208| :sup:`best`(-) = -|image209| :sup:`best`(+) + 180.

Intensity statistics
~~~~~~~~~~~~~~~~~~~~

A good deal can be inferred from the intensity statistics and
relationships between them. Certainly they are an excellent way of
assessing the quality of our experiment.

    I( **h** ) = F( **h** ) F( **h** ) :sup:`*` =
    |F( **h** )| :sup:`2`

|image210|

| The mean value of <F( **h** ) F( **h** ) :sup:`*`> ( **S** ) =
  |image211|g(j,**S** ) :sup:`2`
| since the average of the second summation wil be zero.

If all atoms have the same temperature factor, *i.e.*

    g(j,**S** ) = f(i,**S** )e :sup:`-2|image212|Bs²`

then

    <F( **h** )F( **h** ) :sup:`*`> ( **S** ) = e :sup:`-2Bs²`
    |image213|f(j,**S** ) :sup:`2`

Our measured I( **h** ) will be on an arbitrary scale so we expect

    |image214|

and taking logs:

    log k = log{<I :sup:`obs`( **h** )>/|image215|} = -2B s²

The Wilson Plot is simply the plot of
log{<I :sup:`obs`( **h** )>/|image216|} and should be a straight line
with gradient -2B.

We can use these relationships in other ways.

Normalisation
~~~~~~~~~~~~~

Modify F( **h** ) to generate E( **h** ). The definition is that <E²> =
1, *i.e.* every reflection in a resolution range is divided by
<I( **S** )>.

Moments
~~~~~~~

Moments of I are defined as

    <I :sup:`p`> / <I> :sup:`p` , where p is some suitable power, *e.g.*
    ½, 1, 2, 3, ...

They have predictable values: *e.g.* <I²>/<I>² = 2 for acentric
reflections. If these deviate in certain ways you probably have measured
twinned data. If they fluctuate you may have made a mess of processing.

| Good Wilson plots and moments in
  /y/ccp4/projects/hpce/hpce_118_scalepack2mtz.log
| There are plots in /y/ccp4/cadd/truncate-fred.log which show twinning.

Used for maximum likelihood weighting too.

Authors
-------

| Eleanor Dodson, University of York, England
| Produced by Maria Turkenburg, University of York, England.

.. |CCP4 web logo| image:: ../images/weblogo175.gif
.. |next button| image:: ../images/3Dnexttr.gif
   :target: bmg3.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :target: bmg1.html
.. |top button| image:: ../images/3Dtoptr.gif
   :target: index.html
.. |image4| image:: ../images/alphabetagammatr.gif
.. |image5| image:: ../images/crystalgeometrytr.gif
.. |image6| image:: ../images/p3geometrytr.gif
.. |image7| image:: ../images/ticktr.gif
.. |image8| image:: ../images/ticktr.gif
.. |image9| image:: ../images/ticktr.gif
.. |image10| image:: ../images/ticktr.gif
.. |image11| image:: ../images/ticktr.gif
.. |image12| image:: ../images/ticktr.gif
.. |image13| image:: ../images/ticktr.gif
.. |image14| image:: ../images/ticktr.gif
.. |P31 symmetry operators - general| image:: ../images/si-matrixtr.gif
.. |P31 symmetry operators - specific| image:: ../images/p31matricestr.gif
.. |image17| image:: ../images/2sinthetaoverlambdatr.gif
.. |image18| image:: ../images/4sinsqthetaoverlambdasqtr.gif
.. |scattering factor; borrowed from Crystallography 101| image:: ../images/scatfactr.gif
.. |image20| image:: ../images/phaseexpressionstr.gif
.. |image21| image:: ../images/phitr.gif
.. |image22| image:: ../images/Sigma3tr.gif
.. |image23| image:: ../images/pitr.gif
.. |image24| image:: ../images/Sigma3tr.gif
.. |image25| image:: ../images/pitr.gif
.. |image26| image:: ../images/pitr.gif
.. |image27| image:: ../images/phitr.gif
.. |image28| image:: ../images/3foldmatrixtr.gif
.. |phase sum in vector representation| image:: ../images/phasesumtr.gif
.. |image30| image:: ../images/pitr.gif
.. |image31| image:: ../images/pitr.gif
.. |image32| image:: ../images/pitr.gif
.. |image33| image:: ../images/phitr.gif
.. |image34| image:: ../images/phitr.gif
.. |image35| image:: ../images/phitr.gif
.. |image36| image:: ../images/phitr.gif
.. |image37| image:: ../images/phitr.gif
.. |image38| image:: ../images/pitr.gif
.. |phase sum in vector representation for centric reflections| image:: ../images/phasesumcentrictr.gif
.. |image40| image:: ../images/centrictr.gif
.. |image41| image:: ../images/centric2tr.gif
.. |image42| image:: ../images/phitr.gif
.. |image43| image:: ../images/piover643tr.gif
.. |image44| image:: ../images/centricexampletr.gif
.. |image45| image:: ../images/phitr.gif
.. |image46| image:: ../images/densitytr.gif
.. |image47| image:: ../images/strfac3tr.gif
.. |image48| image:: ../images/alphatr.gif
.. |image49| image:: ../images/alphatr.gif
.. |image50| image:: ../images/strfac4tr.gif
.. |pictorial representation of structure factor for h with anomalous| image:: ../images/Fhtr.gif
.. |image52| image:: ../images/phitr.gif
.. |image53| image:: ../images/phitr.gif
.. |image54| image:: ../images/phitr.gif
.. |image55| image:: ../images/phitr.gif
.. |image56| image:: ../images/phitr.gif
.. |phase triangles| image:: ../images/phasetrianglestr.gif
.. |image58| image:: ../images/phitr.gif
.. |image59| image:: ../images/phitr.gif
.. |image60| image:: ../images/phitr.gif
.. |image61| image:: ../images/phitr.gif
.. |image62| image:: ../images/phitr.gif
.. |image63| image:: ../images/phitr.gif
.. |image64| image:: ../images/phitr.gif
.. |image65| image:: ../images/phitr.gif
.. |image66| image:: ../images/phitr.gif
.. |image67| image:: ../images/phitr.gif
.. |image68| image:: ../images/phitr.gif
.. |image69| image:: ../images/phitr.gif
.. |image70| image:: ../images/Sigma2tr.gif
.. |image71| image:: ../images/phitr.gif
.. |image72| image:: ../images/Sigma2tr.gif
.. |image73| image:: ../images/phitr.gif
.. |image74| image:: ../images/Sigma2tr.gif
.. |image75| image:: ../images/phitr.gif
.. |image76| image:: ../images/phitr.gif
.. |image77| image:: ../images/phitr.gif
.. |image78| image:: ../images/phitr.gif
.. |image79| image:: ../images/phitr.gif
.. |image80| image:: ../images/phitr.gif
.. |image81| image:: ../images/phitr.gif
.. |image82| image:: ../images/phitr.gif
.. |image83| image:: ../images/phitr.gif
.. |image84| image:: ../images/Sigma2tr.gif
.. |image85| image:: ../images/phitr.gif
.. |image86| image:: ../images/phitr.gif
.. |image87| image:: ../images/phitr.gif
.. |image88| image:: ../images/phitr.gif
.. |image89| image:: ../images/phitr.gif
.. |SIR phase circles| image:: ../images/SIRtr.gif
   :height: 300px
   :target: ../images/SIRtr.gif
.. |image91| image:: ../images/phitr.gif
.. |image92| image:: ../images/phitr.gif
.. |image93| image:: ../images/pitr.gif
.. |image94| image:: ../images/phitr.gif
.. |image95| image:: ../images/phitr.gif
.. |image96| image:: ../images/phitr.gif
.. |image97| image:: ../images/phitr.gif
.. |image98| image:: ../images/phitr.gif
.. |image99| image:: ../images/phitr.gif
.. |image100| image:: ../images/phitr.gif
.. |image101| image:: ../images/phitr.gif
.. |image102| image:: ../images/phitr.gif
.. |image103| image:: ../images/phitr.gif
.. |image104| image:: ../images/phitr.gif
.. |image105| image:: ../images/phitr.gif
.. |image106| image:: ../images/phitr.gif
.. |image107| image:: ../images/phitr.gif
.. |image108| image:: ../images/pitr.gif
.. |image109| image:: ../images/phitr.gif
.. |image110| image:: ../images/phitr.gif
.. |image111| image:: ../images/phitr.gif
.. |image112| image:: ../images/pitr.gif
.. |MIR phase circles| image:: ../images/MIRtr.gif
   :height: 300px
.. |image114| image:: ../images/phitr.gif
.. |image115| image:: ../images/phitr.gif
.. |image116| image:: ../images/phitr.gif
.. |image117| image:: ../images/phitr.gif
.. |image118| image:: ../images/phitr.gif
.. |image119| image:: ../images/phitr.gif
.. |image120| image:: ../images/phitr.gif
.. |image121| image:: ../images/phitr.gif
.. |image122| image:: ../images/phitr.gif
.. |image123| image:: ../images/phitr.gif
.. |image124| image:: ../images/phitr.gif
.. |image125| image:: ../images/phitr.gif
.. |image126| image:: ../images/phitr.gif
.. |image127| image:: ../images/phitr.gif
.. |image128| image:: ../images/phitr.gif
.. |image129| image:: ../images/phitr.gif
.. |image130| image:: ../images/phitr.gif
.. |image131| image:: ../images/phitr.gif
.. |image132| image:: ../images/phitr.gif
.. |image133| image:: ../images/phitr.gif
.. |image134| image:: ../images/phitr.gif
.. |image135| image:: ../images/phitr.gif
.. |image136| image:: ../images/phitr.gif
.. |image137| image:: ../images/phitr.gif
.. |image138| image:: ../images/phitr.gif
.. |SIRAS phase circles| image:: ../images/SIRADtr.gif
   :height: 300px
.. |image140| image:: ../images/phitr.gif
.. |image141| image:: ../images/phitr.gif
.. |image142| image:: ../images/phitr.gif
.. |image143| image:: ../images/phitr.gif
.. |image144| image:: ../images/phitr.gif
.. |image145| image:: ../images/phitr.gif
.. |image146| image:: ../images/phitr.gif
.. |image147| image:: ../images/phitr.gif
.. |image148| image:: ../images/phitr.gif
.. |image149| image:: ../images/phitr.gif
.. |image150| image:: ../images/phitr.gif
.. |image151| image:: ../images/phitr.gif
.. |image152| image:: ../images/phitr.gif
.. |image153| image:: ../images/phitr.gif
.. |image154| image:: ../images/phitr.gif
.. |image155| image:: ../images/phitr.gif
.. |image156| image:: ../images/phitr.gif
.. |image157| image:: ../images/phitr.gif
.. |image158| image:: ../images/phitr.gif
.. |image159| image:: ../images/phitr.gif
.. |image160| image:: ../images/phitr.gif
.. |image161| image:: ../images/phitr.gif
.. |image162| image:: ../images/phitr.gif
.. |image163| image:: ../images/phitr.gif
.. |image164| image:: ../images/phitr.gif
.. |image165| image:: ../images/phitr.gif
.. |image166| image:: ../images/phitr.gif
.. |image167| image:: ../images/phitr.gif
.. |image168| image:: ../images/phitr.gif
.. |image169| image:: ../images/phitr.gif
.. |image170| image:: ../images/phitr.gif
.. |image171| image:: ../images/phitr.gif
.. |image172| image:: ../images/phitr.gif
.. |image173| image:: ../images/phitr.gif
.. |image174| image:: ../images/phitr.gif
.. |image175| image:: ../images/phitr.gif
.. |image176| image:: ../images/phitr.gif
.. |image177| image:: ../images/phitr.gif
.. |image178| image:: ../images/phitr.gif
.. |image179| image:: ../images/phitr.gif
.. |image180| image:: ../images/phitr.gif
.. |image181| image:: ../images/phitr.gif
.. |image182| image:: ../images/phitr.gif
.. |image183| image:: ../images/phitr.gif
.. |image184| image:: ../images/phitr.gif
.. |image185| image:: ../images/phitr.gif
.. |image186| image:: ../images/phitr.gif
.. |image187| image:: ../images/phitr.gif
.. |image188| image:: ../images/phitr.gif
.. |image189| image:: ../images/phitr.gif
.. |image190| image:: ../images/phitr.gif
.. |image191| image:: ../images/phitr.gif
.. |image192| image:: ../images/phitr.gif
.. |image193| image:: ../images/phitr.gif
.. |image194| image:: ../images/phitr.gif
.. |image195| image:: ../images/phitr.gif
.. |image196| image:: ../images/phitr.gif
.. |image197| image:: ../images/phitr.gif
.. |image198| image:: ../images/phitr.gif
.. |image199| image:: ../images/phitr.gif
.. |image200| image:: ../images/phitr.gif
.. |image201| image:: ../images/phitr.gif
.. |image202| image:: ../images/phitr.gif
.. |image203| image:: ../images/pitr.gif
.. |image204| image:: ../images/phitr.gif
.. |image205| image:: ../images/phitr.gif
.. |image206| image:: ../images/phitr.gif
.. |image207| image:: ../images/pitr.gif
.. |image208| image:: ../images/phitr.gif
.. |image209| image:: ../images/phitr.gif
.. |image210| image:: ../images/strfac6tr.gif
.. |image211| image:: ../images/Sigma2tr.gif
.. |image212| image:: ../images/pitr.gif
.. |image213| image:: ../images/Sigma2tr.gif
.. |image214| image:: ../images/scaletr.gif
.. |image215| image:: ../images/Sigma2tr.gif
.. |image216| image:: ../images/Sigma2tr.gif

