TRACER (CCP4: Supported Program)
================================

NAME
----

**tracer** - Lattice TRAnsformation and CEll Reduction

SYNOPSIS
--------

| **tracer**
| [`Keyworded input <#keywords>`__]

.. _description: 
 
 DESCRIPTION
----------------------------------

The reduced cell is the unit cell with the smallest volume whose axes
are the three shortest non-coplanar translations in the lattice. TRACER
converts the cell given to the reduced cell. Reduction to a triclinic
lattice with c<a<b and angles alpha and beta non-acute allows for the
identification of the Bravais lattice of highest symmetry which it
represents; this is done by calculation of the 6 scalar products a.a,
a.b ... and comparison with matrices set up within the program. There
are only 43 independent cells to consider.

.. _keywords: 
 
 KEYWORDED INPUT
-----------------------------------

Available keywords are:

   `ACENTRED <#acentred>`__, `BCENTRED <#bcentred>`__,
   `CALCULATE <#calculate>`__, `CCENTRED <#ccentred>`__,
   `CELL <#cell>`__, `DEL <#del>`__, `END <#end>`__,
   `FCENTRED <#fcentred>`__, `MATRIX <#matrix>`__, `RCELL <#rcell>`__,
   `REDUCED <#reduced>`__, `TITLE <#title>`__, `UCENTRED <#ucentred>`__.

.. _title: 
 
 **TITLE** <title>


A title, maximum of 70 characters.

.. _del: 
 
**DEL** <del>


<del> is the absolute value of the largest permissible difference
between any two of the six reduced cell scalar products. Defaults to
1.0.

The <del> value imposes an upper limit on the equivalence of the special
relationships between the scalar products. Usually the higher the
standard deviation of cell parameters, the higher the value of <del>
that should be used. For lattice parameters with errors of 1 part in
1000, a value for <del> of 0.5-1.0 is advised; for proteins, use at
least 1.0. It is sensible to try more than one value and examine the
results.

.. _cell: 
 
**CELL** <a> <b> <c> [ <alpha> <beta> <gamma> ]


Cell parameters (angles default to 90 degrees).

.. _rcell: 
 
**RCELL** <a*> <b*> <c*> <alpha*> <beta*> <gamma*>


Reciprocal cell parameters. Don't omit the angles.

.. _end: 
 
**END**


Terminate input.

.. _keywordstransformation: 
 
**Transformation commands**


The following commands specify transformations of one cell to another.
*They are applied in the order in which they are given.* A sequence of
transformations is terminated by a REDUCED or CALCULATE command. After
this, new ones may be started. Each transformation has an optional
<tag>, which is up to 25 characters of information about it read from
the rest of the line.

.. _matrix: 
 
**MATRIX** <element> ... [ <tag> ]


Transform this cell to a new cell using the 9 supplied matrix elements
in the order (1,1), (2,1) ... (2,3), (3,3).

.. _acentred: 
 
**ACENTRED** [ <tag> ]


The initial cell is A-centred; transform it to primitive cell.

.. _bcentred: 
 
**BCENTRED** [ <tag> ]


.. _ccentred: 
 
**CCENTRED** [ <tag> ]


.. _ucentred: 
 
**UCENTRED** [ <tag> ]


.. _fcentred: 
 
**FCENTRED** [ <tag> ]


These are analogous to `ACENTRED <#acentred>`__.

.. _reduced: 
 
**REDUCED** [ <tag> ]


Get reduced cell; identify and generate from it the unit cell of highest
symmetry and its corresponding lattice parameters.

.. _calculate: 
 
**CALCULATE** [ <tag> ]


Do not transform this cell. This option may be used to identify the
transformed cell and calculate its inverse cell parameters.

.. _examples: 
 
EXAMPLE
---------------------------

::

   TITLE tracer test
   DEL 0.1 ! allowed discrepancy
   CELL 10.51 15.15 6.54 90 151.7 90 ! cell parameters
   CCENTRED ! supplied cell is C-centred
   REDUCED ! find reduced cell
   END      

AUTHOR
------

S. L. Lawton
