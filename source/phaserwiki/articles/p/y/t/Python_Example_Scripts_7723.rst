.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Python Example Scripts
   :name: python-example-scripts
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 Reading MTZ Files for Molecular                                    |
|    Replacement <#Reading_MTZ_Files_for_Molecular_Replacement>`__         |
| -  `2 Automated Molecular                                                |
|    Replacement <#Automated_Molecular_Replacement>`__                     |
| -  `3 Reading MTZ Files for Experimental                                 |
|    Phasing <#Reading_MTZ_Files_for_Experimental_Phasing>`__              |
| -  `4 Automated Experimental                                             |
|    Phasing <#Automated_Experimental_Phasing>`__                          |
| -  `5 Anisotropy Correction <#Anisotropy_Correction>`__                  |
| -  `6 Cell Content Analysis <#Cell_Content_Analysis>`__                  |
| -  `7 Translational NCS Analysis <#Translational_NCS_Analysis>`__        |
| -  `8 Normal Mode Analysis <#Normal_Mode_Analysis>`__                    |
| -  `9 Logfile Handling <#Logfile_Handling>`__                            |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

See `Python
Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__ for
introduction to running phaser as a python library.

Example scripts for the most popular modes of running Phaser.

.. rubric::  Reading MTZ Files for Molecular Replacement
   :name: reading-mtz-files-for-molecular-replacement

Example script for reading data from MTZ file beta_blip.mtz.

Note that by default reflections are sorted into resolution order upon
reading, to achieve a performance gain in the molecular replacement
routines. If reflections are not being read from an MTZ file with this
script, reflections should be pre-sorted into resolution order to
achieve the same performance gain. Sorting is turned off with the
setSORT(False) function.

::

          #beta_blip_data.py
          from phaser import *
          i = InputMR_DAT()
          HKLIN = "beta_blip.mtz"
          F = "Fobs"
          SIGF = "Sigma"
          i.setHKLI(HKLIN)
          i.setLABI_F_SIGF(F,SIGF)
          i.setMUTE(True)
          r = runMR_DAT(i)
          print r.logfile()
          if r.Success():
            hkl = r.getMiller()
            fobs = r.getF()
            sigma = r.getSIGF()
            nrefl = min(10,hkl.size())
            print "Data read from: " , HKLIN
            print "First ", nrefl , " reflections"
            print "%4s %4s %4s %10s %10s" % ("H","K","L",F,SIGF)
            for i in range(0,nrefl):
              print "%4d %4d %4d %10.4f %10.4f" % \
                (hkl[i][0],hkl[i][1],hkl[i][2],fobs[i],sigma[i])
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Automated Molecular Replacement
   :name: automated-molecular-replacement

Example script for automated structure solution of BETA-BLIP

::

          beta_blip_auto.py
          from phaser import *
          i = InputMR_DAT()
          i.setHKLI("beta_blip.mtz")
          i.setLABI_F_SIGF("Fobs","Sigma")
          i.setHIRES(6.0)
          i.setMUTE(True)
          r = runMR_DAT(i)
          if r.Success():
            i = InputMR_AUTO()
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setCELL6(r.getUnitCell())
            i.setREFL(r.getMiller(),r.getFobs(),r.getSigFobs())
            i.setROOT("beta_blip_auto")
            i.addENSE_PDB_ID("beta","beta.pdb",1.0)
            i.addENSE_PDB_ID("blip","blip.pdb",1.0)
            i.addCOMP_PROT_MW_NUM(28853,1)
            i.addCOMP_PROT_MW_NUM(17522,1)
            i.addSEAR_ENSE_NUM("beta",1)
            i.addSEAR_ENSE_NUM("blip",1)
            i.setMUTE(True)
            del(r)
            r = runMR_AUTO(i)
            if r.Success():
              if r.foundSolutions() :
                print "Phaser has found MR solutions"
                print "Top LLG = %f" % r.getTopLLG()
                print "Top PDB file = %s" % r.getTopPdbFile()
              else:
                print "Phaser has not found any MR solutions"
            else:
              print "Job exit status FAILURE"
              print r.ErrorName(), "ERROR :", r.ErrorMessage()
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Reading MTZ Files for Experimental Phasing
   :name: reading-mtz-files-for-experimental-phasing

Example script for reading SAD data from MTZ file S-insulin.mtz

::

          insulin_data.py
          from phaser import *
          i = InputEP_DAT()
          HKLIN = "S-insulin.mtz"
          xtalid = "insulin"
          waveid = "cuka"
          i.setHKLI(HKLIN)
          i.addCRYS_ANOM_LABI(xtalid,waveid,"F(+)","SIGF(+)","F(-)","SIGF(-)")
          i.setMUTE(True)
          r = runEP_DAT(i)
          if r.Success():
            hkl = r.getMiller()
            Fpos = r.getFpos(xtalid,waveid)
            Ppos = r.getPpos(xtalid,waveid)
            Fneg = r.getFneg(xtalid,waveid)
            Pneg = r.getPneg(xtalid,waveid)
            print "Data read from: " , HKLIN
            print "Spacegroup Name (Hall symbol) = %s (%s)" % \
             (r.getSpaceGroupName(), r.getSpaceGroupHall())
            print "Unitcell = " , r.getUnitCell()
            nrefl = min(10,hkl.size())
            print "First ", nrefl , " reflections with anomalous differences"
            print "%4s %4s %4s %10s %10s %10s" % ("H","K","L","F(+)","F(-)","D")
            i = 0
            r = 0
            while r < nrefl:
              if Ppos[i] and Pneg[i] :
                D = abs(Fpos[i]-Fneg[i])
                if D > 0
                  print "%4d %4d %4d %10.4f %10.4f %10.4f" % \
                    (hkl[i][0],hkl[i][1],hkl[i][2],Fpos[i],Fneg[i],D)
                  r=r+1
              i=i+1
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Automated Experimental Phasing
   :name: automated-experimental-phasing

Example script for SAD phasing for insulin

::

          #insulin_sad.py
          from phaser import *
          from cctbx import xray
          i = InputEP_DAT()
          HKLIN = "S-insulin.mtz"
          xtalid = "insulin"
          waveid = "cuka"
          i.setHKLI(HKLIN)
          i.addCRYS_ANOM_LABI(xtalid,waveid,"F(+)","SIGF(+)","F(-)","SIGF(-)")
          i.setMUTE(True)
          r = runEP_DAT(i)
          if r.Success():
            hkl = r.getMiller()
            Fpos = r.getFpos(xtalid,waveid)
            Spos = r.getSIGFpos(xtalid,waveid)
            Ppos = r.getPpos(xtalid,waveid)
            Fneg = r.getFneg(xtalid,waveid)
            Sneg = r.getSIGFneg(xtalid,waveid)
            Pneg = r.getPneg(xtalid,waveid)
            i = InputEP_AUTO()
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setCELL(r.getUnitCell())
            i.setCRYS_MILLER(hkl)
            i.addCRYS_ANOM_DATA(xtalid,waveid,Fpos,Spos,Ppos,Fneg,Sneg,Pneg)
            i.setATOM_PDB(xtalid,"S-insulin_hyss.pdb")
            i.setLLGC_CRYS_COMPLETE(xtalid,True)
            i.addLLGC_CRYS_SCAT_ELEMENT(xtalid,"S")
            i.addCOMP_PROT_FASTA_NUM("S-insulin.seq",1.)
            i.setHKLO(False)
            i.setSCRI(False)
            i.setXYZO(False)
            i.setMUTE(True)
            r = runEP_AUTO(i)
            if r.Success():
              print "SAD phasing"
              print "Data read from: " , HKLIN
              print "Data output to : " , r.getMtzFile()
              print "Spacegroup Name (Hall symbol) = %s (%s)" % \
                (r.getSpaceGroupName(), r.getSpaceGroupHall())
              print "Unitcell = " , r.getUnitCell()
              print "LogLikelihood = " , r.getLogLikelihood()
              atom = r.getAtoms(xtalid)
              print atom.size(), " refined atoms"
              print "%5s %10s %10s %10s %10s %10s" % \
                ("atom","x","y","z","occupancy","u-iso")
              for i in range(0,atom.size()):
                print "%5s %10.4f %10.4f %10.4f %10.4f %10.4f" % \
               
    (atom[i].scattering_type,atom[i].site[0],atom[i].site[1],atom[i].site[2],atom[i].occupancy,atom[i].u_iso)
              hkl = r.getMiller();
              fwt = r.getFWT()
              phwt = r.getPHWT()
              fom = r.getFOM()
              nrefl = min(10,hkl.size())
              print "First ", nrefl , " reflections"
              print "%4s %4s %4s %10s %10s %10s" % \
                ("H","K","L","FWT","PHWT","FOM")
              for i in range(0,nrefl):
                print "%4d %4d %4d %10.4f %10.4f %10.4f" % \
                  (hkl[i][0],hkl[i][1],hkl[i][2],fwt[i],phwt[i],fom[i])
            else:
              print "Job exit status FAILURE"
              print r.ErrorName(), "ERROR :", r.ErrorMessage()
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Anisotropy Correction
   :name: anisotropy-correction

Example script script for anisotropy correction of BETA-BLIP data

::

          #beta_blip_ano.py
          from phaser import *
          i = InputMR_DAT()
          HKLIn = "beta_blip.mtz"
          F = "Fobs"
          SIGF = "Sigma"
          i.setHKLI(HKLIn)
          i.setLABI_F_SIGF(F,SIGF)
          i.setMUTE(True)
          r = runMR_DAT(i)
          if r.Success():
            i = InputANO()
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setCELL6(r.getUnitCell())
            i.setREFL(r.getMiller(),r.getFobs(),r.getSigFobs())
            i.setREFL_ID(F,SIGF)
            i.setHKLI(HKLIn)
            i.setROOT("beta_blip_ano")
            i.setMUTE(True)
            del(r)
            r = runANO(i)
            if r.Success():
              print "Anisotropy Correction"
              print "Data read from: " , HKLIn
              print "Data output to : " , r.getMtzFile()
              print "Spacegroup Name (Hall symbol) = %s (%s)" % \
                (r.getSpaceGroupName(), r.getSpaceGroupHall())
              print "Unitcell = " , r.getUnitCell()
              print "Principal components = " , r.getEigenBs()
              print "Range of principal components = " , r.getAnisoDeltaB()
              print "Wilson Scale = " , r.getWilsonK()
              print "Wilson B-factor = " , r.getWilsonB()
              hkl = r.getMiller();
              f = r.getF()
              sigf = r.getSIGF()
              f_iso = r.getCorrectedF()
              sigf_iso = r.getCorrectedSIGF()
              corr = r.getCorrection()
              nrefl = min(10,hkl.size())
              print "First ", nrefl , " reflections"
              print "%4s %4s %4s %10s %10s %10s %10s %10s" % \
                ("H","K","L",F,SIGF,r.getLaboutF(),r.getLaboutSIGF(),"Corr\'n")
              for i in range(0,nrefl):
                print "%4d %4d %4d %10.4f %10.4f %10.4f %10.4f %10.4f" % \
                  (hkl[i][0],hkl[i][1],hkl[i][2],f[i],sigf[i],f_iso[i],sigf_iso[i],corr[i])
            else:
              print "Job exit status FAILURE"
              print r.ErrorName(), "ERROR :", r.ErrorMessage()
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Cell Content Analysis
   :name: cell-content-analysis

Example script for cell content analysis of BETA-BLIP

::

          #beta_blip_cca.py
          from phaser import *
          i = InputMR_DAT()
          HKLIN = "beta_blip.mtz"
          F = "Fobs"
          SIGF = "Sigma"
          i.setHKLI(HKLIN)
          i.setLABI_F_SIGF(F,SIGF)
          i.setMUTE(True)
          r = runMR_DAT(i)
          if r.Success():
            i = InputCCA()
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setCELL6(r.getUnitCell())
            i.addCOMP_PROT_MW_NUM(28853,1)
            i.addCOMP_PROT_MW_NUM(17522,1)
            i.setMUTE(True)
            del(r)
            r = runCCA(i)
            if r.Success():
              print "Cell Content Analysis"
              print "Molecular weight of assembly = " , r.getAssemblyMW()
              print "Best Z value = " , r.getBestZ()
              print "Best VM value = " , r.getBestVM()
              print "Probability of Best VM = " , r.getBestProb()
            else:
              print "Job exit status FAILURE"
              print r.ErrorName(), "ERROR :", r.ErrorMessage()
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Translational NCS Analysis
   :name: translational-ncs-analysis

Example script for translational NCS analysis of BETA-BLIP

::

          #beta_blip_ncs.py
          from phaser import *
          i = InputMR_DAT()
          HKLIN = "beta_blip.mtz"
          F = "Fobs"
          SIGF = "Sigma"
          i.setHKLI(HKLIN)
          i.setLABI_F_SIGF(F,SIGF)
          i.setMUTE(True)
          r = runMR_DAT(i)
          if r.Success():
            i = InputNCS()
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setCELL6(r.getUnitCell())
            i.setREFL(r.getMiller(),r.getF(),r.getSIGF())
            i.addCOMP_PROT_MW_NUM(28853,1)
            i.addCOMP_PROT_MW_NUM(17522,1)
            i.setMUTE(True)
            del(r)
            r = runNCS(i)
            if r.Success():
              print "Translational NCS analysis"
              print "Translational NCS present = ", r.hasTNCS()
              if r.hasTNCS():
                print "Translational NCS vecor = ", r.hasTNCS()
              print "Twinning alpha = ", r.getTwinAlpha()
            else:
              print "Job exit status FAILURE"
              print r.ErrorName(), "ERROR :", r.ErrorMessage()
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Normal Mode Analysis
   :name: normal-mode-analysis

Example script for normal mode analysis of BETA-BLIP. Note that the
space group and unit cell are not required, and so the MTZ file does not
need to be read to extract these parameters.

::

          #beta_nma.py
          from phaser import *
          i = InputNMA()
          i.setROOT("beta_nma")
          i.addENSE_PDB_ID("beta","beta.pdb",1.0)
          i.setNMAP_MODES([7,10])
          i.setNMAP_FORWARD()
          i.setMUTE(True)
          r = runNMA(i)
          if r.Success():
            print "Normal Mode Analysis"
            for i in range(0,r.getNum()):
              print "PDB file = ", r.getPdbFile(i)
              displacement = r.getDisplacements(i)
              mode = r.getModes(i)
              for j in range(0,mode.size()):
                print " Mode = " , mode[j], " Displacement = ", displacement[j]
          else:
            print "Job exit status FAILURE"
            print r.ErrorName(), "ERROR :", r.ErrorMessage()

.. rubric:: Logfile Handling
   :name: logfile-handling

Example of how to redirect phaser output to a python string for
real-time viewing of output, but not via standard output. Output to
standard out is silenced with setMUTE(True).

::

          beta_blip_logfile.py
          from phaser import *
          from cStringIO import StringIO
          i = InputMR_DAT()
          i.setHKLI("beta_blip.mtz")
          i.setLABI_F_SIGF("Fobs","Sigma")
          i.setMUTE(True)
          o = Output()
          redirect_str = StringIO()
          o.setPackagePhenix(file_object=redirect_str)
          r = runMR_DAT(i,o)

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
