.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Python Functions
   :name: python-functions
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 ATOM <#ATOM>`__                                                    |
| -  `2 BFACTOR <#BFACTOR>`__                                              |
| -  `3 BINS <#BINS>`__                                                    |
| -  `4 BOXSCALE <#BOXSCALE>`__                                            |
| -  `5 CELL <#CELL>`__                                                    |
| -  `6 CLMN <#CLMN>`__                                                    |
| -  `7 CLUSTER <#CLUSTER>`__                                              |
| -  `8 COMPOSITION <#COMPOSITION>`__                                      |
| -  `9 CRYSTAL <#CRYSTAL>`__                                              |
| -  `10 DEBUG <#DEBUG>`__                                                 |
| -  `11 EIGEN <#EIGEN>`__                                                 |
| -  `12 ELLG <#ELLG>`__                                                   |
| -  `13 ENSEMBLE <#ENSEMBLE>`__                                           |
| -  `14 FFTS <#FFTS>`__                                                   |
| -  `15 HAND <#HAND>`__                                                   |
| -  `16 HKLIN <#HKLIN>`__                                                 |
| -  `17 HKLOUT <#HKLOUT>`__                                               |
| -  `18 INTEGRATION <#INTEGRATION>`__                                     |
| -  `19 JOBS <#JOBS>`__                                                   |
| -  `20 KEYWORDS <#KEYWORDS>`__                                           |
| -  `21 LABIN <#LABIN>`__                                                 |
| -  `22 LLGCOMPLETE <#LLGCOMPLETE>`__                                     |
| -  `23 MACANO <#MACANO>`__                                               |
| -  `24 MACHL <#MACHL>`__                                                 |
| -  `25 MACMR <#MACMR>`__                                                 |
| -  `26 MACTNCS <#MACTNCS>`__                                             |
| -  `27 MACSAD <#MACSAD>`__                                               |
| -  `28 MODE <#MODE>`__                                                   |
| -  `29 MUTE <#MUTE>`__                                                   |
| -  `30 NMAMETHOD <#NMAMETHOD>`__                                         |
| -  `31 NMAPDB <#NMAPDB>`__                                               |
| -  `32 NORMALIZATION <#NORMALIZATION>`__                                 |
| -  `33 OUTLIER <#OUTLIER>`__                                             |
| -  `34 PACK <#PACK>`__                                                   |
| -  `35 PARTIAL <#PARTIAL>`__                                             |
| -  `36 PEAKS <#PEAKS>`__                                                 |
| -  `37 PERMUTATIONS <#PERMUTATIONS>`__                                   |
| -  `38 PTGROUP <#PTGROUP>`__                                             |
| -  `39 PURGE <#PURGE>`__                                                 |
| -  `40 RESCORE <#RESCORE>`__                                             |
| -  `41 RESHARPEN <#RESHARPEN>`__                                         |
| -  `42 RESOLUTION <#RESOLUTION>`__                                       |
| -  `43 ROOT <#ROOT>`__                                                   |
| -  `44 ROTATE <#ROTATE>`__                                               |
| -  `45 SAMPLING <#SAMPLING>`__                                           |
| -  `46 SCATTERING <#SCATTERING>`__                                       |
| -  `47 SEARCH <#SEARCH>`__                                               |
| -  `48 SGALTERNATIVE <#SGALTERNATIVE>`__                                 |
| -  `49 SOLPARAMETERS <#SOLPARAMETERS>`__                                 |
| -  `50 SOLUTION <#SOLUTION>`__                                           |
| -  `51 SORT <#SORT>`__                                                   |
| -  `52 SPACEGROUP <#SPACEGROUP>`__                                       |
| -  `53 TARGET <#TARGET>`__                                               |
| -  `54 TITLE <#TITLE>`__                                                 |
| -  `55 TNCS <#TNCS>`__                                                   |
| -  `56 TOPFILES <#TOPFILES>`__                                           |
| -  `57 TRANSLATE <#TRANSLATE>`__                                         |
| -  `58 VARSAD <#VARSAD>`__                                               |
| -  `59 VERBOSE <#VERBOSE>`__                                             |
| -  `60 WAVELENGTH <#WAVELENGTH>`__                                       |
| -  `61 XYZOUT <#XYZOUT>`__                                               |
| -  `62 ZSCORE <#ZSCORE>`__                                               |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

Most keywords only refer to a single parameter, and if used multiple
times, the parameter will take the last value input. Some keywords are
meaningful when entered multiple times. The order may or may not be
important. **Not all functions will be valid for versions before the
current (nightly) release of Phaser through Phenix.**

**Python variable types**

-  Miller = cctbx::miller::index<int>
-  dvect3 = scitbx::vec3<floatType>
-  dmat33 = scitbx::mat3<floatType>
-  **type**_array = scitbx::af::shared<**type**> arrays

**Icons**

-  |image0| Basic Functions

-  |image1| Advanced Functions

-  |image2| Expert Functions

-  |image3| Developer Functions

There are equivalent
`Keywords <../../../../articles/k/e/y/Keywords.html>`__ for the
stand-alone executable of Phaser

.. rubric:: |image4|ATOM
   :name: atom

 setATOM_PDB(string <XTALID>,string <FILENAME>)
    Definition of atom positions using a pdb file.
 setATOM_HA(string <XTALID>,string <FILENAME>)
    Definition of atom positions using a ha file (from RANTAN, MLPHARE
    etc.)
 addATOM(string <XTALID>,string <TYPE>,float <X>,float <Y>,float
<Z>,float <OCC>)
    Minimal definition of atom position.
 setATOM_FULL(string <XTALID>,string <TYPE>,bool <True=ORTH
False=FRAC>,dvect3 <X Y Z>,float <OCC>,bool <True=ISO False=ANO>,float
<ISOB>,bool <T=ANOU F=USTAR>,dmat6 <HH KK LL HK HL KL>,bool <True=FIXX
False=REFX>,bool <True=FIXO False=REFO>,bool <True=FIXB False=REFB>,bool
<True=SWAPB False=!SWAPB>,string <SITE_NAME>)
    Full definition of atom position. B-factor defaults to isotropic and
    Wilson B-factor if not set and setATOM_BFACTOR_WILSON is used (the
    default).
 setATOM_CHAN_BFAC_WILS(bool <True|False>)
    Reset all atomic B-factors to the Wilson B-factor
 setATOM_CHAN_SCAT(string <TYPE>)
    The scattering type of the PDB can be changed (for example, for
    SHELXD which always outputs coordinates as sulphur).

-  NOTE: Use <TYPE>=TX for Ta6Br12 cluster and <TYPE>=XX for all other
   clusters. Scattering for cluster is spherically averaged. Coordinates
   of cluster compounds other than Ta6Br12 must be entered with
   setCLUS_PDB function. Ta6Br12 coordinates are in phaser code and do
   not need to be given with setCLUS_PDB function.

.. rubric:: |image5|BFACTOR
   :name: bfactor

 setBFAC_WILS_REST(bool <True|False>)
    Toggle to use the Wilson restraint on the isotropic component of the
    atomic B-factors in SAD phasing
 setBFAC_SPHE_REST(bool <True|False>)
    Toggle to use the sphericity restraint on the anisotropic B-factors
    in SAD phasing
 setBFAC_REFI_REST(bool <True|False>)
    Toggle to use the restraint on molecular B-factors for molecular
    replacement
 setBFAC_WILS_SIGM(float <SIGMA>)
    The sigma of the Wilson restraint.
 setBFAC_SPHE_SIGM(float <SIGMA>)
    The sigma of the Sphericity restraint.
 setBFAC_REFI_SIGM(float <SIGMA>)
    The sigma of the refinement restraint.

-  Default: setBFAC_WILS_REST(True)
-  Default: setBFAC_SPHE_REST(True)
-  Default: setBFAC_REFI_REST(True)
-  Default: setBFAC_WILS_SIGM(5)
-  Default: setBFAC_SPHE_SIGM(5)
-  Default: setBFAC_REF|I_SIGM(10)

.. rubric:: |image6|BINS
   :name: bins

 setBINS_MINI(float <L>)
    L = minimum number of bins.
 setBINS_MAXI(float <H>)
    H = maximum number of bins.
 setBINS_WIDT(float <W>)
    W = width of the bins in number of reflections.
 setBINS_CUBI(dvect3 <A B C>)
    A B C are the coefficients for the binning function
    A(S*S*S)+B(S*S)+CS where S = (1/resolution). CUBIC coefficients
    restricted to monotonically increasing function: A >0, B >0, C >0
    and either (a) A=B=0 or (b) A=0 or (c) B=0.

-  Default: setBINS_MINI(6)
-  Default: setBINS_MAXI(50)
-  Default: setBINS_WIDT(500)
-  Default: setBINS_CUBI(0 1 0)

.. rubric:: |image7|BOXSCALE
   :name: boxscale

 setBOXS(float <BS>)
    Scale for box for calculating structure factors. The ensembles are
    put in a box equal to (extent of molecule)*BS.

-  Constraint: BS > 2.4
-  Default: setBOXS(4)

.. rubric:: |image8|CELL
   :name: cell

 setCELL(float <A>,float <B>,float <C>,float <ALPHA>,float <BETA>,float
<GAMMA>)
    Unit cell dimensions
 setCELL6(float_array <A B C ALPHA BETA GAMMA>)
    Unit cell dimensions

-  Constraints: A>0,B>0,C>0,ALPHA>0,BETA>0,GAMMA>0

.. rubric:: |image9|CLMN
   :name: clmn

 setCLMN_SPHE(float <SPHERE>)
    Radius for the decomposition of the Patterson in Ångstroms. If it is
    0, the radius defaults to twice the mean radius of the ENSEMBLE.
 setCLMN_LMIN(float <LMIN>)
    The minimum L values for the decomposition of the Patterson
 setCLMN_LMAX(float <LMAX>)
    The minimum L values for the decomposition of the Patterson

-  Default: setCLMN_LMIN(4)
-  Default: setCLMN_LMAX(100)
-  Default: setCLMN_SPHE(0)

.. rubric:: |image10|CLUSTER
   :name: cluster

 setCLUS_PDB(string <pdbfile>
    Sample coordinates for a cluster compound for experimental phasing.
    The sites are indicated with the type XX. Ta6Br12 clusters do not
    need to have coordinates specified as the coordinates are in the
    phaser code. To use Ta6Br12 clusters, specify atomtypes/clusters as
    TX.

.. rubric:: |image11|COMPOSITION
   :name: composition

 setCOMP_AVERAGE()
    Use average solvent content (50%)
 setCOMP_SOLVENT()
    Composition entered by solvent content
 setCOMP_ASU()
    Composition entered by components of asymmetric unit
 addCOMP_PROT_MW_NUM(float <MW>,float <NUM>)
    The number of copies NUM of protein molecular weight MW
    Constraint: MW>0
 addCOMP_PROT_STR_NUM(string <SEQ>,float <NUM>)
    The number of copies NUM of protein sequence SEQ (in single letter
    code)
 addCOMP_PROT_NRES_NUM(float <NRES>,float <NUM>)
    The number of copies NUM of the number protein residues
 addCOMP_PROT_SEQ_NUM(string <FILE>,float <NUM>)
    The number of copies NUM of protein sequence given in fasta format
    (in a file FILE)
 addCOMP_NUCL_MW_NUM(float <MW>,float <NUM>)
    The number of copies NUM of nucleic acid molecular weight MW
    Constraint: MW>0
 addCOMP_NUCL_STR_NUM(string <SEQ>,float <NUM>)
    The number of copies NUM of nucleic acid sequence SEQ (in single
    letter code)
 addCOMP_NUCL_NRES_NUM(float <NRES>,float <NUM>)
    The number of copies NUM of the number nucleic acid residues
 addCOMP_NUCL_SEQ_NUM(string <FILE>,float <NUM>)
    The number of copies NUM of nucleic acid sequence given in fasta
    format (in a file FILE)
 addCOMP_ATOM(string <TYPE>,float <NUM>)
    Add NUM copies of a atom (usually a heavy atom) to the composition
 addCOMP_NUCL_NRES(float <NRES>)
    The number nucleic acid resides
 addCOMP_NUCL_FASTA_NUM(string <FILE>,float <NUM>)
    The number of copies NUM of nucleic acid sequence given in fasta
    format (in a file FILE)
 addCOMP_ENSE_FRAC(string <MODLID>,float)
    Alternative way of defining composition. Fraction scattering is
    entered explicitly for MODLID.

-  Constraint: 0<FRAC<=1

.. rubric:: |image12|CRYSTAL
   :name: crystal

 setCRYS_SAD_LABI(string <F+>,string <SIGF+>,string <F->,string
<SIGF->)
    Column of MTZ file used for data, and/or names for output columns
 addCRYS_SAD_DATA(miller_array <MILLER>,float_array
<F+>,float_array <SIGF+>,bool_array <PRESENT+>,float_array
<F->,float_array <SIGF->,bool_array <PRESENT->)
    Arrays of data

.. rubric:: |image13|DEBUG
   :name: debug

 setDEBU(bool <True=ON False=OFF>)
    Extra verbose output for debugging

-  Default: setDEBU(False)

.. rubric:: |image14|EIGEN
   :name: eigen

 setEIGE_READ(string <FILE>)
    Read a file containing the eigenvectors and eigenvalues. The
    eigenvalues and eigenvectors of the atomic Hessian are read from the
    file generated by a previous run, rather than calculated. This
    option must be used with the job that generated the eigenfile and
    the job reading the eigenfile must have identical (or default) input
    for keyword NMAMethod.
 setEIGE_WRIT(bool <True=Do False=Don't Write>)
    Control whether or not the eigenfile is written (when not using the
    READ mode).

-  Default: setEIGE_WRIT(True)

.. rubric:: |image15|ELLG
   :name: ellg

 setELLG_USE(bool <True/False>)
    Use expected LLG to determine resolution limits and search order
 setELLG_TARG(float <TARGET>)
    Target value for expected LLG for determining resolution limits and
    search order

-  Default: setELLG_USE(True)
-  Default: setELLG_TARG(120)

.. rubric:: |image16|ENSEMBLE
   :name: ensemble

 addENSE_PDB_RMS(string <MODLID>,string <FILE>,float <RMS>)
    Add a model in pdb format FILE to the ensemble MODLID with the
    expected RMS deviation of the coordinates from the "real" structure.
    First call initiates the MODLID.
 addENSE_CARD(string <MODLID>,string <FILE>,bool)
    The name of a (non-standard) PDB file used to build an ENSEMBLE. The
    RMS deviation is parsed from the MODEL cards of the pdb file (MODEL
    RMS <rms>) that separate the superimposed models concatenated in the
    one file. This syntax enables simple automation of the use of
    ensembles.The pdb file is non-standard because of the addition of
    the RMS keyword to the MODEL card and also because the atom list for
    the different models need not be the same.
 addENSE_PDB_ID(string <MODLID>,string <FILE>,float <ID>)
    Add a model in pdb format FILE to the ensemble MODLID with the
    sequence identity ID to the "real" structure. First call initiates
    the MODLID.
 setENSE_MAP(string <MODLID>,string <HKLIN>,string <SF>,string
<PHASE>,dvect3 <EXTENT>,float <RMS>,dvect3 <CENTRE>,float <PROTMW>,float
<NUCL MW>)
    An ENSEMBLE defined from a map (via an mtz file). The molecular
    weight of the object the map represents is required for scaling. The
    effective RMS coordinate error is needed to judge how the map
    accuracy falls off with resolution.. The extent is needed to
    determine reasonable rotation steps, and the centre is needed to
    carry out a proper interpolation of the molecular transform. The
    extent and the centre are both given in Ångstroms.
 setENSE_FRAC(string <MODLID>,float <frac>)
    Fraction scattering of ensemble entered directly rather than
    calculated from composition

-  Constraint: ID=%

 addENSE_HKL(string <MODLID>,string <MTZFILE>,float <SCAT>,dvect3 <EX
EY EZ>,dmat33 <P1 P2 P3 P4 P5 P6 P7 P8 P9>,dvect3 <TX TY TZ>)
    This option can be used to read back a molecular transform computed
    in an earlier Phaser job run in the MR_ENS mode. May be useful if
    the spherical harmonic decomposition is very long. This can only be
    used when repeating the search for a component of the asymmetric
    unit with no (or the same) known fixed structure as part of the
    search
 setENSE_BINS_MIN(string <MODLID>,float <L>)
    L = minimum number of bins.
 setENSE_BINS_MAX(string <MODLID>,float <H>)
    H = maximum number of bins.
 setENSE_BINS_NUM(string <MODLID>,float <N>)
    N = number of bins. If N is given then the values of L and H are
    ignored.
 setENSE_BINS_WIDTH(string <MODLID>,float <W>)
    W = width of the bins in number of reflections.
 setENSE_BINS_CUBIC(string <MODLID>,dvect3 <A B C>)
    A B C are the coefficients for the binning function
    A(S*S*S)+B(S*S)+CS where S = (1/resolution). CUBIC coefficients
    restricted to monotonically increasing function: A >0, B >0, C >0
    and either (a) A=B=0 or (b) A=0 or (c) B=0.

-  Default: setENSE_BINS_MIN(<MODLID>,6)
-  Default: setENSE_BINS_MAX(<MODLID>,200)
-  Default: setENSE_BINS_WIDTH(<MODLID>,1000)
-  Default: setENSE_BINS_CUBIC(<MODLID>,0 1 0)

.. rubric:: |image17|FFTS
   :name: ffts

 setFFTS_MINI(float <ATOMS_MIN>)
setFFTS_MAXI(float <ATOMS_MAX>)
    The minimum and maximum number of atoms of the range between which
    direct summation and fft methods are tested to see which is faster
    for structure factor and gradient calcuation (for this unit cell and
    resolution). For a number of atoms below ATOMS_MIN direct structure
    factor calculation is always used, and for a number of atoms above
    ATOMS_MAX ffts are always used for the structure factor calculation
    and the gradient calculations. Direct summation is always used for
    the curvatures. Use FFTS MIN 0 MAX O to always use ffts.

-  Default: setFFTS_MINI(20)
-  Default: setFFTS_MAXI(80)

.. rubric:: |image18|HAND
   :name: hand

 setHAND(string ["OFF"|"ON"|"BOTH"])
    Use the given hand of heavy atoms
    Use other hand of heavy atoms
    Phase using both hands of heavy atoms

-  Default: setHAND("OFF")

.. rubric:: |image19|HKLIN
   :name: hklin

 setHKLI(string <FILENAME>)
    The mtz file containing the data

.. rubric:: |image20|HKLOUT
   :name: hklout

 setHKLO(bool <True=ON False=OFF>)
    Flag for output of an mtz file containing the phasing information

-  Default: setHKLO(True)

.. rubric:: |image21|INTEGRATION
   :name: integration

 setINTE_FIXE(bool True|False)
    Determine the number of angular steps in the integration by the
    variance of the function (False) or use a fixed number of steps
    (True)
 setINTE_STEP(float <STEP>)
    Number of steps in angular integration of function if the number of
    points is fixed

-  Default: setINTE_FIXE(False)

.. rubric:: |image22|JOBS
   :name: jobs

 setJOBS(float <NUM>)
    Number of processors to use in parallelized sections of code

-  Default: JOBS 2

.. rubric:: |image23|KEYWORDS
   :name: keywords

 setKEYW(bool <True=ON False=OFF>)
    Write Phaser script file

-  Default: setKEYW(True)

.. rubric:: |image24|LABIN
   :name: labin

 setLABI(string <F>,string <SIGF>)
    Columns in mtz file. F must be given. SIGF should be given but is
    optional.

.. rubric:: |image25|LLGCOMPLETE
   :name: llgcomplete

 setLLGC_COMP(bool <True=Do False=Don't Complete>)
    Toggle for structure completion by log-likelihood gradient maps
 addLLGC_SCAT(string)
    Atom type(s) to be used for log-likelihood gradient completion. If
    more than one element is entered for log-likelihood gradient
    completion, the atom type that gives the highest Z-score for each
    peak is selected.
 setLLGC_CLAS(float <CLASH>)
    Minimum distance between atoms in log-likelihood gradient maps and
    also the distance used for determining anisotropy of atoms. If zero,
    distance determined by resolution.
 setLLGC_SIGM(float <Z>)
    Z-score (sigma) for accepting peaks as new atoms in log-likelihood
    gradient maps
 setLLGC_NCYC(int <NCYC>)
    Maximum number of cycles of log-likelihood gradient structure
    completion. By default, NMAX is 50, but this limit should never be
    reached, because all features in the log-likelihood gradient maps
    should be assigned well before 50 cycles are finished. This keyword
    should be used to reduce the number of cycles to 1 or 2.
 setLLGC_MAPS(bool <ON|OFF>)
    Output map coefficients to mtz file
 setLLG_METH(string <METHOD> = ["IMAGINARY"|"ATOMTYPE"])
    Pick peaks from the imaginary map only or from all the completion
    atomtype maps.

-  Default: setLLGC_COMP(False)
-  Default: setLLGC_CLAS(0)
-  Default: setLLGC_SIGM(6)
-  Default: setLLGC_NCYC(50)
-  Default: setLLGC_MAPS(False)
-  Default: setLLGC_METH("ATOMTYPE")

.. rubric:: |image26|MACANO
   :name: macano

setMACA_PROT(string ["DEFAULT"|"CUSTOM"|"OFF"|"ALL"])
    Protocol for the refinement of SigmaN in the anisotropy correction
 addMACA(bool <True=REFINE False=FIX ANISO>,bool <True=REFINE False=FIX
BINS>,bool <True=REFINE False=FIX SOLK>,bool <True=REFINE False=FIX
SOLB>,int <NCYC>,string <MINIMISER="BFGS"|"NEWTON"|"DESCENT">)
    Macrocycle for the custom refinement of SigmaN in the anisotropy
    correction. Macrocycles are added in the order in which they are
    entered.

-  Default: setMACA_PROT("DEFAULT")

.. rubric:: |image27|MACHL
   :name: machl

 setMACH_PROT(string ["DEFAULT"|"CUSTOM"|"OFF"|"ALL"])
    Protocol for the refinement of Hendrickson-Lattman coefficients
 addMACH(bool <True=REFINE False=FIX COEFF>,int <NCYC>,string
<MINIMISER="BFGS"|"NEWTON"|"DESCENT">)
    Macrocycle for custom refinement of Hendrickson-Lattman
    coefficients. Macrocycles performed added in the order in which they
    are entered.

-  Default: setMACH_PROT("DEFAULT")

.. rubric:: |image28|MACMR
   :name: macmr

 setMACM_PROT(string ["DEFAULT"|"CUSTOM"|"OFF"|"ALL"])
    Protocol for refinement of molecular replacement solutions
 addMACM(bool <True=REFINE False=FIX ROT>,bool <True=REFINE False=FIX
TRA>,bool <True=REFINE False=FIX BFAC>,bool <True=REFINE False=FIX
VRMS>,int <NCYC>,string <MINIMIZER="BFGS"|"NEWTON"|"DESCENT")
    Macrocycle for custom refinement of molecular replacement solutions.
    Macrocycles are performed in the order in which they are entered.

-  Default: setMACM_PROT("DEFAULT")

.. rubric:: |image29|MACTNCS
   :name: mactncs

 setMACT_PROT(string ["DEFAULT"|"CUSTOM"|"OFF"|"ALL"])
    Protocol for refinement of pseudo-translational NCS refinement
 addMACT(bool <True=REFINE False=FIX ROT>,bool <True=REFINE False=FIX
TRA>,bool <True=REFINE False=FIX DRMS>, int <NCYC>,string
<MINIMIZER="BFGS"|"NEWTON"|"DESCENT")
    Macrocycle for custom refinement of pseudo-translational NCS
    refinement. Macrocycles are performed in the order in which they are
    entered.

-  Default: setMACT_PROT("DEFAULT")

.. rubric:: |image30|MACSAD
   :name: macsad

 setMACS_PROT(string ["DEFAULT"|"CUSTOM"|"OFF"|"ALL"])
    Protocol for refinement of SAD refinement
    *n.b. PROTOCOL ALL will crash phaser and is only useful for
    debugging - see code for details*

addMACS(REF_K=<True|False>,REF_B=<True|False>,REF_SIGMA=<True|False>,REF_XYZ=<True|False>,REF_OCC=<True|False>,REF_BFAC=<True|False>,REF_FDP=<True|False>,REF_SA=<True|False>,REF_SB=<True|False>,REF_SP=<True|False>,bool
REF_SD=<True|False>,bool REF_PK=<True|False>,bool
REF_PB=<True|False>,int <NCYC>,string
<MINIMIZER="BFGS"|"NEWTON"|"DESCENT">)
    Macrocycle for custom refinement of SAD refinement. Macrocycles are
    performed in the order in which they are entered.

-  Default: setMACS_PROT("DEFAULT")

.. rubric:: |image31|MODE
   :name: mode

Not relevant to python scripting

.. rubric:: |image32|MUTE
   :name: mute

 setMUTE(bool <True=ON False=OFF>)
    Toggle for running in "silent" mode, with no summary, logfile or
    verbose output written to "standard output". Output can be extracted
    from Results object in python, or from XML file.

-  Default: setMUTE(False)

.. rubric:: |image33|NMAMETHOD
   :name: nmamethod

 setNMAM_OSCI(string ["RTB"|"CA"|"ALL"])
    Use the rotation-translation block method to determine the modes
    Use C-alpha atoms only to determine the mode
    Use all atoms only to determine the mode (only for use on very small
    molecules, less than 250 atoms)
 setNMAM_RTB_MAXB(float)
    For the RTB analysis, number of rotation-translation blocks
 setNMAM_RTB_NRES(float)
    For the RTB analysis, number of residues in a rotation-translation
    block. By default NRES is calculated so that it is as high as it can
    be without reaching MAXBlocks.

-  Default: setNMAM_RTB()
-  Default: setNMAM_RTB_MAXB(250)

 setNMAM_RADI(float <RADIUS>)
    Elastic Network Model interaction radius (Angstroms)
 setNMAM_FORC(float <FORCE>)
    Elastic Network Model force constant

-  Default: setNMAM_RADI(5)
-  Default: setNMAM_FORC(1)

.. rubric:: |image34|NMAPDB
   :name: nmapdb

 setNMAP_PERT(string ["RMS"|"DQ"])
    Tobble to use rms deviations or dq steps to perturb structure
 addNMAP_MODE(float <MODE>)
    Add a mode along which to perturb the structure. If multiple modes
    are entered, the structure is perturbed along all the modes AND
    combinations of the modes given. There is no limit on the number of
    modes that can be entered, but the number of pdb files explodes
    combinatorially.
 setNMAP_COMB(float <NMAX>)
    Controls how many modes are present in any combination.
 setNMAP_RMS_STEP(float <RMS>)
    Increment in rms Ångstroms between pdb files to be written.
 setNMAP_RMS_CLAS(float <CLASH>)
setNMAP_RMS_STRE(float <STRETCH>)
setNMAP_RMS_MAXI(float <MAX>)
    The structure will be perturbed along each mode until either the
    C-alpha atoms clash with (come within CLASH Ångstroms of) other
    C-alpha atoms, the distances between C-alpha atoms STRETCH too far
    (note that normal modes do not preserve the geometry) or the MAXRMS
    deviation has been reached.
 setNMAP_RMS_DIRE(string["FORWARDS"|"BACKWARDS"|"TOFRO"])
    Direction of perturbation along the eigenvectors of the modes
    specified.
 addNMAP_DQ(float)
    Add a DQ factors (as used by the Elnemo server (K. Suhre & Y-H.
    Sanejouand, NAR 2004 vol 32) ) by which to perturb the atoms along
    the eigenvectors.

-  Default: addNMAP_MODE(7)
-  Default: setNMAP_COMB(3)
-  Default: setNMAP_RMS(0.3)
-  Default: setNMAP_STRE(10.0)
-  Default: setNMAP_CLAS(2.0)
-  Default: setNMAP_MAXI(0.5)
-  Default: setNMAP_RMS_TOFRO()

.. rubric:: |image35|NORMALIZATION
   :name: normalization

setNORM(float_array <B1 B2 ...> ,dmat6 <HH KK LL HK HL KL>,float
<SOLK>,float <SOLB>)
The normalization factors the correct for anisotropy in the data
setNORM_BINS(float_array <B1 B2 ...>)
setNORM_ANIS(dmat6 <HH KK LL HK HL KL>)
setNORM_SOLK(float <SOLK>)
setNORM_SOLB(float <SOLB>)
.. rubric:: |image36|OUTLIER
   :name: outlier

 setOUTL_REJE(bool <True|False>)
    Reject low probability data outliers
 setOUTL_PROB(float <PROB>)
    Cutoff for rejection of low probablity outliers

-  Default: setOUTL_REJE(True)
-  Default: setNMAM_PROB(1.0e-06)

.. rubric:: |image37|PACK
   :name: pack

 setPACK_SELE(string ["BEST"|"ALLOW"|"ALL"])
    Allow the best packing solutions provided the total number of
    clashes does not exceed ALLOWED_CLASHES
    Allow all solutions that pack with the total number of clashes less
    than ALLOWED_CLASHES
    Allow all solutions (no packing test)
 setPACK_CUTO(float <ALLOWED_CLASHES>)
    Total number of allowed clashes
 setPACK_QUIC(bool <True=ON False=OFF>)
    Packing check stops when ALLOWED_CLASHES or MAX_CLASHES is reached
 setPACK_DIST(float <DISTANCE>)
    Distance within which C-alpha atoms clash given by CLASH_DISTANCE
    Ångstroms. If the model is RNA or DNA, phosphate and carbon atoms in
    the phosphate backbone, and nitrogen atoms in the bases are taken as
    the marker atoms for clashes
 setPACK_COMP(bool <True=ON False=OFF>)
    Pack ensembles into a compact association (minimize distances
    between centres of mass for the addition of each component in a
    solution).
 setPACK_TRAC(bool <True=ON False=OFF>)
    Toggle whether or not to only use Trace atoms (C-alpha atoms in
    proteins and P in RNA/DNA) for packing analysis, or all atoms. Only
    recommended to be False for use with small fragments.

-  Default: setPACK_BEST()
-  Default: setPACK_CUTO(10)
-  Default: setPACK_QUIC(True)
-  Default: setPACK_DIST(3.0)
-  Default: setPACK_COMP(True)
-  Default: setPACK_TRAC(True)

.. rubric:: |image38|PARTIAL
   :name: partial

 setPART_PDB(string <FILENAME>)
    The partial structure for SAD refinement
 setPART_HKLI(string <FILENAME>)
    The partial density for SAD refinement
 setPART_VARI(string ["ID"|"RMS"])
setPART_DEVI(float <RMS_ID>)
    The sequence identity or rms deviation between the model and target
    structure

-  Constraint: ID=%
-  Constraint: RMS>0
-  No Default

.. rubric:: |image39|PEAKS
   :name: peaks

 setPEAK_ROTA_SELE(string ["SIGMA"|"PERCENT"|"NUMBER"|"ALL"])
setPEAK_TRAN_SELE(string ["SIGMA"|"PERCENT"|"NUMBER"|"ALL"])
    Select rotation/translation peaks by taking
    peaks over CUTOFF percent of the difference between the top peak and
    the mean value.
    peaks with a Z-score greater than CUTOFF
    the top CUTOFF peaks
    all peaks

 setPEAK_ROTA_CUTO(float <CUTOFF>)
setPEAK_TRAN_CUTO(float <CUTOFF>)
    The cutoff value for peak selection
 setPEAK_ROTA_CLUS(bool <True=Do False=Don't CLUSTER>)
setPEAK_TRAN_CLUS(bool <True=Do False=Don't CLUSTER>)
    Toggle for CLUSTER selects clustered peaks from translation function

-  Default: setPEAK_ROTA_SELE("PERCENT")
-  Default: setPEAK_TRAN_SELE("PERCENT")
-  Default: setPEAK_ROTA_CUTO(0.75)
-  Default: setPEAK_TRAN_CUTO(0.75)
-  Default: setPEAK_ROTA_CLUS(True)
-  Default: setPEAK_TRAN_CLUS(True)

.. rubric:: |image40|PERMUTATIONS
   :name: permutations

 setPERM(bool <True=ON False=OFF>)
    Toggle for whether the order of the search set is to be permuted.

-  Default: setPERM(False)

.. rubric:: |image41|PTGROUP
   :name: ptgroup

 setPTGR_COVE(float <COVERAGE>)
    Percentage coverage for two sequences to be considered in same
    pointgroup
 setPTGR_IDEN(float <IDENTITY>)
    Percentage identity for two sequences to be considered in same
    pointgroup
 setPTGR_RMSD(float <RMSD>)
    Percentage rmsd for two models to be considered in same pointgroup
 setPTGR_TOLE_ANGU(float <ANG>)
    Angular tolerance for pointgroup
 setPTGR_TOLE_SPAT(float <DIST>)
    Spatial tolerance for pointgroup

.. rubric:: |image42|PURGE
   :name: purge

 setPURG_ROTA_ENAB(bool <True=ON False=OFF>
    Toggle for whether to purge the solution list from the RF according
    to the best solution found so far.
 setPURG_TRAN_ENAB(bool <True=ON False=OFF>
    Toggle for whether to purge the solution list from the TF and after
    the refinement steps (in AUTO mode) according to the best solution
    found so far.
 setPURG_ROTA_PERC(float <PERC>)
    PERC is the percent of the difference between the top solution and
    the mean at which to purge the solutions after the RF.
 setPURG_TRAN_PERC(float <PERC>)
    PERC is the percent of the difference between the top solution and
    the mean at which to purge the solutions after the TF.
 setPURG_ROTA_NUMB(float <NUM>)
    NUM is the number of solutions to retain in purging after RF. The
    number taken is the minimum of the number found with the PERCENT
    cutoff and the NUMBER cutoff. Zero is flag to not use this criteria
 setPURG_TRAN_NUMB(float <NUM>)
    NUM is the number of solutions to retain in purging after TF. The
    number taken is the minimum of the number found with the PERCENT
    cutoff and the NUMBER cutoff. Zero is flag to not use this criteria

-  Default < Phaser-2.5.0: setPURG(True), setPURG_PERC(75)
-  Default >=Phaser-2.5.0: setPURG_ROTA_ENAB(True),
   setPURG_ROTA_PERC(75), setPURG_ROTA_NUMB(0)
-  Default >=Phaser-2.5.0: setPURG_TRAN_ENAB(True),
   setPURG_TRAN_PERC(75), setPURG_TRAN_NUMB(0)

.. rubric:: |Image:Expert.gif|RESCORE
   :name: imageexpert.gifrescore

 setRESC_ROTA(bool <True=ON False=OFF>)
    Toggle for rescoring of fast rotation function search peaks
 setRESC_TRAN(bool <True=ON False=OFF>)
    Toggle for rescoring of fast translation function search peaks. The
    default value will depend on whether it is the MR_AUTO FAST or FULL
    modes, or the MR_FTF mode, ane whether or not the expected LLG
    target value is reached.

-  Default: setRESC_ROTA(True)

.. rubric:: |image44|RESHARPEN
   :name: resharpen

 setRESH_PERC(float <PERCENT>)
    Perecentage of the B-factor in the direction of lowest fall-off (in
    anisotropic data) to add back into the structure factors F_ISO and
    FWT and FDELWT so as to sharpen the electron density maps

-  Default: setRESH_PERC(100)

.. rubric:: |image45|RESOLUTION
   :name: resolution

 setRESO_HIGH(float <HIRES>)
    The high resolution limit
 setRESO_LOW(float <LORES>)
    The low resolution limit
 setRESO_AUTO_HIGH(float <HIRES>)
    The high resolution limit for high resolution refinement in
    MR_AUTO.
 setRESO_AUTO_LOW(float <LORES>)
    The low resolution limit for high resolution refinement in MR_AUTO.
 setRESO(float <HIRES>,float <LORES>)
    Resolution range in Ångstroms. The limits can be in either order.
 setRESO_AUTO(float <HIRES>,float <LORES>)
    Resolution range in Ångstroms for high resolution refinement in
    AutoMR. The limits can be in either order.

-  Constraint: HIRES>0
-  Constraint: LORES>0
-  Default: setRESO_HI(2.5) for rotation and translation functions only

.. rubric:: |image46|ROOT
   :name: root

 setROOT(string <FILEROOT>)
    Root filename for output files (e.g. FILEROOT.log)

-  Default: setROOT("PHASER")

.. rubric:: |image47|ROTATE
   :name: rotate

 setROTA_VOLU(string ["FULL"|"AROUND"|)
    Sample all unique angles or sample all unique angles within RANGE of
    EULER
 setROTA_EULE(dvect3 <EULER>)
setROTA_RANG(float <RANGE>)
setROTA_VOLU_AROU_EULE_RANG(dvect3 <EULER>,float <RANGE>)
    Restrict the search to the region of +/- RANGE degrees around
    orientation given by EULER

-  Constraint: RANGE>0
-  Default: setROTA_VOLU_FULL()

.. rubric:: |image48|SAMPLING
   :name: sampling

 setSAMP_ROTA(float <SAMP>)
    Sampling of search given in degrees for a rotation search. Default
    sampling depends on the geometric mean radius (GMR) of the Ensemble
    and the high resolution limit (dmin) of the search.
 setSAMP_TRAN(float <SAMP>)
    Sampling of search given in Ångstroms for a translation search.
    Default sampling depends on the high resolution limit (dmin) of the
    search.

-  Constraint: SAMP>0
-  Default: SAMP = 2*atan(dmin/(4*GMR)) (MODE = MR_BRF)
-  Default: SAMP = 2*atan(dmin/(4*GMR)) (MODE = MR_FRF)
-  Default: SAMP = dmin/5; (MODE = MR_BTF)
-  Default: SAMP = dmin/3; (MODE = MR_FTF)

.. rubric:: |image49|SCATTERING
   :name: scattering

 addSCAT(string <TYPE>,float <FP>,float <FDP, string <FIXFDP>)
    Measured scattering factors for a given atom type or cluster type,
    from a fluorescence scan. FIXFDP="EDGE" (default) fixes the fdp
    value if it is away from an edge, but refines it if it is close to
    an edge, while FIXFDP="ON" or FIXFDP="OFF" does not depend on
    proximity of edge.
 setSCAT_REST(bool <ON|OFF>)
    use Fdp restraints
 setSCAT_SIGM(float <SIGMA>)
    Fdp restraint sigma used is SIGMA multiplied by initial fdp value

-  Default: setSCAT_REST(True)
-  Default: setSCAT_SIGM(0.2)

.. rubric:: |image50|SEARCH
   :name: search

 addSEAR_ENSE_NUMB(string <MODLID>,int <NUM>)
    The ENSEMBLE to be searched for in a rotation search or an automatic
    search. When the keyword is entered multiple times, each SEARCH
    keyword refers to a new component of the structure. If the component
    is present multiple times the sub-keyword NUMber can be used (rather
    than entering the same SEARCH keyword NUMber times).
 addSEAR_ENSE_OR_ENSE_NUMB(string_array <MODLIDS>,int <NUM>)
    Array of alternative ensembles for a search component. The search is
    performed for each ENSEMBLE in turn. When the keyword is entered
    multiple times, each SEARCH keyword refers to a new component of the
    structure. If the component is present multiple times the
    sub-keyword NUMber can be used (rather than entering the same SEARCH
    keyword NUMber times).
 setSEAR_ORDE_AUTO(bool [True|False])
    Search in the "best" order as estimated using estimated rms
    deviation and completeness of models.
 setSEAR_METH("FULL"|"FAST")
    Search using the "full search" or "fast search" algorithms.
 setSEAR_DEEP(bool [True|False])
    Flag to control whether or not the cuttoff for the peaks in the
    rotation function is reduced if there is no TFZ over ZSCORE_CUTOFF
    in the first search. Search method FAST only.
 setSEAR_DOWN_PERC(float <PERC>)
    Percentage to reduce rotation function cutoff if there is no TFZ
    over ZSCORE_CUTOFF in first search. Search method FAST only.
 setSEAR_BFAC(float <BFAC>)
    B-factor applied to search molecule (or atom).

-  Default: setSEARC_METH("FULL")
-  Default: setSEARC_ORDE_AUTO(True)
-  Default: setSEAR_DEEP(True)
-  Default: setSEAR_DOWN_PERC(10)
-  Default: setSEAR_BFAC(0)

.. rubric:: |image51|SGALTERNATIVE
   :name: sgalternative

 setSGAL_SELE(string ["ALL"|"HAND"|"LIST"|"NONE"])
    Tests all possible space groups in the same pointgroup as the space
    group
    Test the given space group and its enantiomorph
    Test the space groups in the entered list.
    Test no alternative space groups
 addSGAL_NAME(string <NAME>)
    Add alternative space groups to test.
 setSGAL_SORT(bool [True|False])
    Sort alternative space groups in order of frequency of occurrence

.. rubric:: |image52|SOLPARAMETERS
   :name: solparameters

 setSOLP_FSOL(float <FSOL>)
setSOLP_BSOL(float <BSOL>)
    Babinet solvent parameters for Sigma(A) curves.
 setSOLP_FIXB(bool <True|False>)
    If FIXB is False, then BSOL is calculated from FSOL using the
    formula BSOL = 99.1 + 5.79*exp(4.03*FSOL) from Glykos & Kokkinkdis
    Acta Cryst D56 p1070 (2000), otherwise BSOL is fixed at the input
    (or default) value.
 setSOLP_REST(bool <True|False>)
    Restrain the Babinet solvent parameters to the initial values during
    refinement.

-  Default: setSOLP_FSOL(0.69) setSOLP_BSOL(378) setSOLP_FIXB(True)
   setSOLP_REST(False)

.. rubric:: |image53|SOLUTION
   :name: solution

 addSOLU_SET(string <ANNOTATION>)
    Start new set of solutions
 addSOLU_6DIM_ENSE(string <MODLID>,dvect3 <EULER>,bool <True=FRAC
False=ORTH>,dvect3 <X Y Z>,float <BFAC>,bool <True=FIX False=Refine
ROT>,bool <True=FIX False=Refine TRA>,bool <True=FIX False=Refine
BFAC>,int <MULT>)
    This keyword is repeated for each known position and orientation of
    a ENSEmble ID. A B G are the Euler angles and X Y Z are the
    translation. MULT (for multiplicity) defaults to 1.
 addSOLU_ENSE_VRMS(string <MODLID>, float_array <VARIANCES>)
    Refined RMS variance terms for pdb files (or map) in ensemble
    MODLID. If given as part of a solution, these values overwrite the
    values used for input in the ENSEMBLE keyword.
 addSOLU_TRIAL_ENSE(string <MODLID>,dvect3 <EULER>, float <SCORE>)
    Rotation List for translation function
 addSOLU_ORIG_ENSE(string <MODLID>)
    Create solution for ensemble MODLID at the origin

.. rubric:: |image54|SORT
   :name: sort

 setSORT(bool <True=ON False=OFF>)
    Sort the reflections into resolution order upon reading MTZ file,
    for performance gain in molecular replacement

-  Default: setSORT(True)

.. rubric:: |image55|SPACEGROUP
   :name: spacegroup

 setSPAC_NUM(int <NUM>)
    Set the space group using the number 19 (gives standard setting).
 setSPAC_NAME(string <NAME>)
    Set the space group using the Hermann-Mauguin symbols eg P21 21 21
 setSPAC_HALL(string <HALL>)
    Set the space group using the Hall symbol

.. rubric:: |image56|TARGET
   :name: target

 setTARG_FRF(string ["LERF1"|"LERF2"|"CROWTHER"])
    Target function for fast rotation searches.
 setTARG_FTF(string ["LETF1"|"LETF2"|"CORRELATION"])
    Target function for fast translation searches.

-  Default: setTARG_FRF("LERF1")
-  Default: setTARG_FTF("LETF1")

.. rubric:: |image57|TITLE
   :name: title

 setTITL(string <TITLE>)
    Title for job

-  Default: setTITL(string "[no title given]")

.. rubric:: |image58|TNCS
   :name: tncs

 setTNCS_USE(True|False)
    Use TNCS if present: apply TNCS corrections. (Note: was
    setTNCS_IGNO(True|False) in Phaser-2.4.0)
 setTNCS_REFI_ROTA(True|False)
    Use TNCS correction method that includes a rotation refinement of
    the two TNCS related rotations (to separate the rotation function
    results) before the translation function
 setTNCS_ROTA_ANGL(dvect3(rotx roty rotz))
    Input rotational difference between molecules related by the
    pseudo-translational symmetry vector, specified as rotations in
    degrees about x, y and z axes.
 setTNCS_ROTA_RANG(float <angle>)
    Maximum deviation from initial rotation from which to look for
    rotational deviation. The best refined rotational angle will be
    selected.
 setTNCS_ROTA_SAMP(float <sampling>)
    Sampling for rotation search. By default the sampling depends on the
    resolution and the estimated molecular radius.
 setTNCS_TRAN_VECT(dvect3(x y z))
    Input pseudo-translational symmetry vector (fractional coordinates).
    By default the translation is determined from the Patterson.
 setTNCS_VARI_RMSD(float <num>)
    Input estimated rms deviation between pseudo-translational symmetry
    vector related molecules.
 setTNCS_VARI_FRAC(float <num>)
    Input estimated fraction of cell content that obeys
    pseudo-translational symmetry.
 setTNCS_LINK_REST(True|False)
    Link the occupancy of atoms related by TNCS in SAD phasing
 setTNCS_LINK_SIGM(float <sigma>)
    Sigma of link restraint of the occupancy of atoms related by TNCS in
    SAD phasing
 setTNCS_PATT_HIRE(float <hires>)
    High resolution limit for Patterson calculation for TNCS detection
 setTNCS_PATT_LORE(float <lores>)
    Low resolution limit for Patterson calculation for TNCS detection
 setTNCS_PATT_PERC(float <percent>)
    Percent of origin Patterson peak that qualifies as a TNCS vector
 setTNCS_PATT_DIST(float <distance>)
    Minium distance of Patterson peak from origin that qualifies as a
    TNCS vector
 setTNCS_NMOL(int <nmol>)
    Number of molecules/molecular assemblies related by single TNCS
    vector (usually only 2)

-  Default: setTNCS_USE(True)
-  Default: setTNCS_REFI_ROTA(False)
-  Default: setTNCS_ROTA_ANGL(dvect3(0,0,0))
-  Default: setTNCS_ROTA_GRID(True)
-  Default: setTNCS_ROTA_SAMP(0)
-  Default: setTNCS_ROTA_RANG(0)
-  Default: setTNCS_VARI_RMSD(0.5)
-  Default: setTNCS_VARI_FRAC(1)
-  Default: setTNCS_LINK_REST(ON)
-  Default: setTNCS_LINK_SIGM(0.1)
-  Default: setTNCS_PATT_HIRE(5)
-  Default: setTNCS_PATT_LORE(10)
-  Default: setTNCS_PATT_PERC(20)
-  Default: setTNCS_PATT_DIST(15)
-  Default: setTNCS_NMOL(2)

.. rubric:: |image59|TOPFILES
   :name: topfiles

 setTOPF(float <NUM>)
    Number of top pdbfiles or mtzfiles to write to output.

-  Default: setTOPF(1)

.. rubric:: |image60|TRANSLATE
   :name: translate

 setTRAN_VOLU(string ["FULL"|"REGION"|"LINE"|"AROUND"])
    Search volume for brute force translation function. Cheshire cell or
    Primitive cell volume.
 setTRAN_STAR(dvect <START>)
    Start point for REGION and LINE
 setTRAN_POIN(dvect <POINT>)
    Start point AROUND
 setTRAN_END(dvect <START>)
    End point for REGION and LINE
 setTRAN_RANG(float <RANGE>)
    Range for AROUND, in Ångstroms
 setTRAN_FRAC(bool <True=FRAC False=ORTH>)
    Start and End points delimited in Fractional or Orthogonal
    coordinates.

-  Default: setTRAN_VOLU("FULL")

.. rubric:: |image61|VARSAD
   :name: varsad

 setVARS(float_array <VARIANCES>
    SAD variance parameters SA and SB (the real and imaginary components
    of Sigma Minus), SP (Sigma Plus) and SD (Sigma Delta) by resolution
    bin, the overall scale (K) and B-factor (B) for the anomalous
    scatterer model, the overall scale (PK) and B-factor (PB) for the
    partial structure (if given), and sigma-scale (SIGMA).

.. rubric:: |image62|VERBOSE
   :name: verbose

 setVERB(bool <True=ON False=OFF>)
    Toggle to send verbose output to log file.

-  Default: setVERB(False)

.. rubric:: |image63|WAVELENGTH
   :name: wavelength

 setWAVE(float <LAMBDA>)
    The wavelengh at which the SAD dataset was collected

.. rubric:: |image64|XYZOUT
   :name: xyzout

 setXYZO(bool <True=ON False=OFF>)
    Toggle for output coordinate files.
 setXYZO_ENSE(bool <True=ON False=OFF>)
    If true, then each placed ensemble is written to its own pdb file.
    The files are named FILEROOT.#.#.pdb with the first # being the
    solution number and the second # being the number of the placed
    ensemble (representing a SOLU 6DIM entry in the .sol file).

-  Default: setXYZO(False) (Rotation functions)
-  Default: setXYZO(True) (other relevant functions)
-  Default: setXYZO_ENSE(True) (all other relevant modes)

.. rubric:: |image65|ZSCORE
   :name: zscore

 setZSCO_USE(bool <True=ON False=OFF>)
    Use the TFZ tests. Only applicable with the FAST search method.
    (Note Phaser-2.4.0 and below use setZSCO_SOLV(0) to turn off the
    TFZ tests).
 setZSCO_SOLV(floatType ZSCORE_SOLVED)
    Set the translation function Z-score that indicates a definite
    solution. Enter 0 to turn off zscore tests.
 setZSCO_HALF(floatType ZSCORE_HALF)
    Set the TFZ for amalgamating solutions in the FAST search method to
    the maximum of ZSCORE_SOLVED and half the maximum TFZ, to
    accommodate cases of partially correct solutions in very high TFZ
    cases (e.g. TFZ > 16)

-  Default: setZSCO_USE(true)
-  Default: setZSCO_SOLV(8)
-  Default: setZSCO_HALF(true)

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-site" class="portlet">

.. rubric:: site
   :name: site

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Home">

   .. raw:: html

      </div>

   `Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-News">

   .. raw:: html

      </div>

   `News <../../../../articles/n/e/w/News.html>`__
-  

   .. raw:: html

      <div id="n-Events">

   .. raw:: html

      </div>

   `Events <../../../../articles/e/v/e/Events.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-documentation" class="portlet">

.. rubric:: documentation
   :name: documentation

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__
-  

   .. raw:: html

      <div id="n-Keyword-Examples">

   .. raw:: html

      </div>

   `Keyword
   Examples <../../../../articles/m/r/_/MR_using_keyword_input_9c88.html>`__
-  

   .. raw:: html

      <div id="n-Python-Examples">

   .. raw:: html

      </div>

   `Python
   Examples <../../../../articles/p/y/t/Python_Example_Scripts_7723.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Bugs">

   .. raw:: html

      </div>

   `Bugs <../../../../articles/b/u/g/Bugs.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-View-SVN">

   .. raw:: html

      </div>

   `View SVN <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-other" class="portlet">

.. rubric:: other
   :name: other

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Contact">

   .. raw:: html

      </div>

   `Contact <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developers">

   .. raw:: html

      </div>

   `Developers <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__
-  

   .. raw:: html

      <div id="n-Help">

   .. raw:: html

      </div>

   `Help <../../../../articles/c/o/n/Help%7EContents_22de.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |image0| image:: ../../../../images/thumb/d/dd/User1.gif/24px-User1.gif
   :width: 24px
   :height: 24px
   :target: ../../../../articles/u/s/e/File%7EUser1.gif_18d9.html
.. |image1| image:: ../../../../images/thumb/7/73/User2.gif/24px-User2.gif
   :width: 24px
   :height: 24px
   :target: ../../../../articles/u/s/e/File%7EUser2.gif_0ddc.html
.. |image2| image:: ../../../../images/thumb/b/bc/Expert.gif/24px-Expert.gif
   :width: 24px
   :height: 24px
   :target: ../../../../articles/e/x/p/File%7EExpert.gif_0167.html
.. |image3| image:: ../../../../images/thumb/e/e3/Developer.gif/24px-Developer.gif
   :width: 24px
   :height: 24px
   :target: ../../../../articles/d/e/v/File%7EDeveloper.gif_7ff6.html
.. |image4| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image5| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image6| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image7| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image8| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image9| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image10| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image11| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image12| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image13| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image14| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image15| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image16| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image17| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image18| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image19| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image20| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image21| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image22| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image23| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image24| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image25| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image26| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image27| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image28| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image29| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image30| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image31| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image32| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image33| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image34| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image35| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image36| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image37| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image38| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image39| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image40| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image41| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image42| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |Image:Expert.gif| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
   :target: ../../../../articles/e/x/p/File%7EExpert.gif_0167.html
.. |image44| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image45| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image46| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image47| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image48| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image49| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image50| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image51| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image52| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image53| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image54| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image55| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image56| image:: ../../../../images/b/bc/Expert.gif
   :width: 48px
   :height: 48px
.. |image57| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image58| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image59| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image60| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image61| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
.. |image62| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image63| image:: ../../../../images/d/dd/User1.gif
   :width: 48px
   :height: 48px
.. |image64| image:: ../../../../images/7/73/User2.gif
   :width: 48px
   :height: 48px
.. |image65| image:: ../../../../images/e/e3/Developer.gif
   :width: 48px
   :height: 48px
