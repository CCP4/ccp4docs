.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Python Interface
   :name: python-interface
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div style="margin-left: 25px; float: right;">

+--------------------------------------------------------------------------+
| .. raw:: html                                                            |
|                                                                          |
|    <div id="toctitle">                                                   |
|                                                                          |
| .. rubric:: Contents                                                     |
|    :name: contents                                                       |
|                                                                          |
| .. raw:: html                                                            |
|                                                                          |
|    </div>                                                                |
|                                                                          |
| -  `1 Input-Objects, Run-Jobs, and                                       |
|    Results-Objects <#Input-Objects.2C_Run-Jobs.2C_and_Results-Objects>`_ |
| _                                                                        |
| -  `2 Input-Object set- and                                              |
|    add-Functions <#Input-Object_set-_and_add-Functions>`__               |
| -  `3 Results-Object get-Functions <#Results-Object_get-Functions>`__    |
| -  `4 Error Handling <#Error_Handling>`__                                |
| -  `5 Logfile Handling <#Logfile_Handling>`__                            |
| -  `6 Example Scripts <#Example_Scripts>`__                              |
+--------------------------------------------------------------------------+

.. raw:: html

   </div>

As an alternative to keyword input, Phaser can be called directly from a
python script. This is the way Phaser is called in Phenix and we
encourage developers of other automation pipelines to use the python
scripting too. In order to call Phaser in python you will need to have
Phaser installed from source.

.. rubric:: Input-Objects, Run-Jobs, and Results-Objects
   :name: input-objects-run-jobs-and-results-objects

Using Phaser through the python interface is similar to using Phaser
through the keyword interface. Each mode of operation of Phaser
described above is controlled by an "input-object" (similar to the
command script), has a Phaser "run-job" which runs the Phaser executable
for the corresponding mode, and produces a "result-object" (which
includes the logfile text). The user input is passed to the
"input-object" with a calls to set- or add- functions. Phaser is then
run with a call to the "run-job" function, which takes the
"input-object" for control. Results are returned from the
"result-object" with get-functions.

+----------------------------------+-----------------------+----------------------+------------------+
| Functionality                    | Input-Object          | Run-Job              | Results-Object   |
+==================================+=======================+======================+==================+
| Anisotropy Correction            | i = InputANO()        | r = runANO(i)        | ResultANO()      |
+----------------------------------+-----------------------+----------------------+------------------+
| Cell Content Analysis            | i = InputCCA()        | r = runCCA(i)        | ResultCCA()      |
+----------------------------------+-----------------------+----------------------+------------------+
| Normal Mode Analysis             | i = InputNMA()        | r = runNMA(i)        | ResultNMA()      |
+----------------------------------+-----------------------+----------------------+------------------+
| Translational NCS Analysis       | i = InputNCS()        | r = runNCS(i)        | ResultNCS()      |
+----------------------------------+-----------------------+----------------------+------------------+
| Automated MR                     | i = InputMR_AUTO()   | r = runMR_AUTO(i)   | ResultMR()       |
+----------------------------------+-----------------------+----------------------+------------------+
| Rotation Function                | i = InputMR_FRF()    | r = runMR_FRF(i)    | ResultMR_RF()   |
+----------------------------------+-----------------------+----------------------+------------------+
| Translation Function             | i = InputMR_FTF()    | r = runMR_FTF(i)    | ResultMR_TF()   |
+----------------------------------+-----------------------+----------------------+------------------+
| Refinement and Phasing           | i = InputMR_RNP()    | r = runMR_RNP(i)    | ResultMR()       |
+----------------------------------+-----------------------+----------------------+------------------+
| Log-Likelihood Gain              | i = InputMR_LLG()    | r = runMR_LLG(i)    | ResultMR()       |
+----------------------------------+-----------------------+----------------------+------------------+
| Packing                          | i = InputMR_PAK()    | r = runMR_PAK(i)    | ResultMR()       |
+----------------------------------+-----------------------+----------------------+------------------+
| Automated Experimental Phasing   | i = InputEP_AUTO()   | r = runEP_AUTO(i)   | ResultEP()       |
+----------------------------------+-----------------------+----------------------+------------------+
| SAD Experimental Phasing         | i = InputEP_SAD()    | r = runEP_SAD(i)    | ResultEP()       |
+----------------------------------+-----------------------+----------------------+------------------+

The major difference between running Phaser though the keyword interface
and running Phaser though the python scripting is that the data reading
and Phaser functionality are separated. For the Phaser "run-job"
functions, the reflection data (for Miller indices, Fobs and SigmaFobs)
are simply arrays, the space group is given as a Hall string, and the
unitcell is given as an array of 6 numbers. This is an important feature
of the Phaser python scripting as it means that the Phaser "run-job"
functions are not tied to mtz file input, but the data can be read in
python from any file format, and then the data passed to Phaser.

For the convenience of developers and users, the python scripting comes
with data-reading jiffies to read data from mtz files. (These are the
same mtz reading jiffies that are used internally by Phaser when calling
Phaser from keyword input.)

+--------------------+----------------------+---------------------+-------------------+
| Functionality      | Input-Object         | Run-Job             | Result-Object     |
+====================+======================+=====================+===================+
| Read Data for MR   | i = InputMR_DAT()   | r = runMR_DAT(i)   | ResultMR_DAT()   |
+--------------------+----------------------+---------------------+-------------------+
| Read Data for EP   | i = InputEP_DAT()   | r = runEP_DAT(i)   | ResultEP_DAT()   |
+--------------------+----------------------+---------------------+-------------------+

.. rubric:: Input-Object set- and add-Functions
   :name: input-object-set--and-add-functions

The syntax of the set- and add- functions on the "input-objects" mirror
the keyword input. Each "input-object" only has set- or add- functions
corresponding to the keywords that are relevant for that mode.
Attempting to set a value on an "input-object" that is irrelevant for
that mode will result in an error. This differs from the keyword input,
where the parser simply ignores any keywords that are not relevant to
the current mode.

Note that setting the space group by name or number does not specify the
setting. It is best to set the space group via the Hall symbol, which is
unique to the full definition of the space group.

The python interface uses standard python and cctbx/scitbx variable
types.

::

    str          string
    float        double precision floating point
    Miller       cctbx::miller::index<int> 
    dvect3       scitbx::vec3<float> 
    dmat33       scitbx::mat3<float> 
    type_array   scitbx::af::shared<type> arrays
         

+------------------------------------------------+---------------------+-----------------------+
| Functionality                                  | Keyword             | Python Set Function   |
+================================================+=====================+=======================+
| Set the root filename                          | ROOT filename       | i.setROOT(filename)   |
+------------------------------------------------+---------------------+-----------------------+
| Silence logfile output to standard output      | MUTE ON             | i.setMUTE(True)       |
+------------------------------------------------+---------------------+-----------------------+
| Add a scattering type for llg map completion   | LLGC SCATTERING S   | i.addLLGC_SCAT(S)    |
+------------------------------------------------+---------------------+-----------------------+

Table:  Examples of keyword/python equivalences

.. rubric:: Results-Object get-Functions
   :name: results-object-get-functions

Data are extracted from the "result-objects" with get-functions. The
get-functions are mostly specific to the type of "result-object"
(described in sections below), but some are common to all
"result-objects" (described in table below).

Ralf Grosse-Kunstleve's scitbx::af::shared<double> array type is heavily
used for passing of arrays into the Phaser "input-objects" and
extracting arrays from the Phaser "result-objects". This is a reference
counted array type that can be used directly in python and in C++. It is
part of the Phaser installation, when Phaser is installed from source.
The scitbx (SCIentific ToolBoX) is part of the cctbx (Computational
Crystallography ToolBoX) which is hosted by sourceforge

+-----------------------------------------------------------+-----------------------+
| Results Objects                                           | Python Get Function   |
+===========================================================+=======================+
| Exit status "success"                                     | r.Success()           |
+-----------------------------------------------------------+-----------------------+
| Exit status "failure"                                     | r.Failure()           |
+-----------------------------------------------------------+-----------------------+
| Type of Error (see error table). SYNTAX errors are not    | r.ErrorName()         |
| thrown in python as they are generated by keyword input   |                       |
+-----------------------------------------------------------+-----------------------+
| Message associated with error                             | r.ErrorMessage()      |
+-----------------------------------------------------------+-----------------------+
| Text of Summary                                           | r.summary()           |
+-----------------------------------------------------------+-----------------------+
| Text of Logfile                                           | r.logfile()           |
+-----------------------------------------------------------+-----------------------+
| Text of Verbose Logfile                                   | r.verbose()           |
+-----------------------------------------------------------+-----------------------+
| Text of Warning messages                                  | r.warnings()          |
+-----------------------------------------------------------+-----------------------+
| Text of Loggraph format tables/graphs                     | r.loggraph()          |
+-----------------------------------------------------------+-----------------------+

Table:  Functions common to all output objects

There is no documentation for the functions available from each results
object. Please see the file Outputs_bpl.cpp in the boost_python
directory of the phaser source code distribution.

.. rubric:: Error Handling
   :name: error-handling

Exit status is indicated by Success() and Failure() functions of the
"result-objects". Success indicates successful execution of Phaser, not
that it has solved the structure! For molecular replacement jobs, the
foundSolutions() function indicates that Phaser has found one or more
potential solutions, the numSolutions() function returns how many
solutions were found and the uniqueSolution() function returns True if
only one solution was found. More detailed error information in the case
of Failure is given by ErrorName() and ErrorMessage().

Advanced Information: All errors are thrown and caught internally by the
"run-jobs", and so do not generate "Runtime Errors" in the python
script. In particular "INPUT" errors are not thrown by the set- or
add-functions of the "input-objects", but are stored in the
"input-object" and passed to the "result-object" once the "run-job" is
called. Results objects are derived from std::exception, and so can be
thrown. Function what() returns ErrorName() (not the ErrorMessage()).

.. rubric:: Logfile Handling
   :name: logfile-handling

Writing of the logfile to standard output can be silenced with the
i.setMUTE(True) function. The logfile or summary text can then be
printed to standard output with the print r.logfile() or print
r.summary() functions.

Advanced Information: Setting i.setMUTE(True) prevents real time viewing
of the progress of a Phaser job. This may present an inconvenience for
users. If you want to view the logfile information but not have it go to
standard output, Logfile text can be redirected to a python string using
an alternative call to the "run-job" function that includes passing an
"output-object" (which controls the Phaser logging methods) on which the
output stream has been set to a python string. This feature of Phaser
was developed thanks to Ralf Grosse-Kunstleve.

.. rubric:: Example Scripts
   :name: example-scripts

Copy and edit to start using Phaser

-  `Python Example
   Scripts <../../../../articles/p/y/t/Python_Example_Scripts_7723.html>`__

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
