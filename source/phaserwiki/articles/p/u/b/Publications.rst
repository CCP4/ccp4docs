.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Publications
   :name: publications
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. rubric:: Citation
   :name: citation

Phaser crystallographic software `**J Appl
Cryst** <http://scripts.iucr.org/cgi-bin/paper?he5368>`__
    McCoy AJ, Grosse-Kunstleve RW, Adams PD, Winn MD, Storoni LC, Read
    RJ
    J. Appl. Cryst. (2007). 40, 658-674

.. rubric:: Reviews
   :name: reviews

Using SAD data in Phaser `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5159>`__
    Read RJ, McCoy AJ
    Acta Crystallogr D Biol Crystallogr. 2011 Apr;67(Pt 4):338-344

Experimental phasing: best practice and pitfalls `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5142>`__
    McCoy AJ, Read RJ
    Acta Cryst. (2010) D66, 458-469.

An introduction to molecular replacement `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5108>`__
    Evans P, McCoy A
    Acta Cryst.(2008) D64, 1-10. Epub 2007 Dec 5.

Solving structures of protein complexes by molecular replacement with
Phaser `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5095>`__
    McCoy AJ
    Acta Cryst. (2007). D63, 32-41

Liking Likelihood `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5064>`__
    McCoy AJ
    Acta Cryst. (2004). D60, 2169-2183

.. rubric:: Primary Citations
   :name: primary-citations

A log-likelihood-gain intensity target for crystallographic phasing that
accounts for experimental error `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?dz5382>`__
    Read RJ, McCoy AJ
    Acta Cryst. (2016). D72, 375-387

Phaser.MRage: automated molecular replacement `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ba5210>`__
    Bunkoczi G, Echols N, McCoy AJ, Oeffner RD, Adams PD, Read RJ
    Acta Cryst. (2013). D69, 2276-2286

SCEDS: protein fragments for molecular replacement in Phaser `**Acta
Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5209>`__
    McCoy AJ, Nicholls RA, Schneider TR
    Acta Cryst. (2013). D69, 2216-2225

Improved estimates of coordinate error for molecular replacement `**Acta
Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5212>`__
    Oeffner RD, Bunkocz G, McCoy AJ, Read RJ
    Acta Cryst. (2013). D69, 2209-2215

Intensity statistics in the presence of translational
non-crystallographic symmetry `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?dz5268>`__
    Read RJ, Adams PD, McCoy AJ
    Acta Cryst. (2013). D69, 176-183

Improvement of molecular replacement models with *Sculptor* `**Acta
Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5163>`__
    Bunkoczi G, Read RJ
    Acta Cryst. (2011). D67, 303-312

Likelihood-enhanced fast translation functions `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?gx5042>`__
    McCoy AJ, Grosse-Kunstleve RW, Storoni LC, Read RJ
    Acta Cryst. (2005). D61, 458-464

Simple algorithm for a maximum-likelihood SAD function `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ea5015>`__
    McCoy AJ, Storoni LC, Read RJ
    Acta Cryst. (2004). D60, 1220-1228

Likelihood-enhanced fast rotation functions `**Acta Cryst
D** <http://scripts.iucr.org/cgi-bin/paper?ad5007>`__
`**pdf** <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/ad5007.pdf>`__
    Storoni LC, McCoy AJ, Read RJ
    Acta Cryst. (2004). D60, 432-438

Pushing the boundaries of molecular replacement with maximum likelihood
`**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5014>`__
    Read RJ
    Acta Cryst. (2001). D57, 1373-1382

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Publications&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Publications>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 14:29, 25 April 2016 by `Randy
   Read <../../../../articles/r/a/n/User:Randy.html>`__. Based on work
   by `Airlie McCoy <../../../../articles/a/i/r/User:Airlie.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
