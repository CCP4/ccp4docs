.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: SVN Repository
   :name: svn-repository
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. rubric:: Code Development
   :name: code-development

Phaser code is open source. Code development is managed by
`subversion <http://subversion.apache.org>`__ (SVN)

 Pipeline Developers
    If you are developing a pipeline using Phaser, please contact us for
    svn access so that we can work with you to add features and fix
    bugs.
    Note the University of Cambridge's `Licences for
    Phaser <../../../../articles/l/i/c/Licences.html>`__

 Advanced Users
    If you want the latest version of Phaser, then you don't need svn
    access. You can download recent nightly builds of Phenix, which
    always contain the latest version of Phaser that has passed
    regression tests.
    If you would like to be kept informed in real-time of changes to the
    Phaser source code we can give you permission to view the svn
    repository online (see below).

 Source Code Developers
    Source code modifications are allowed under the University of
    Cambridge's `Licences for
    Phaser <../../../../articles/l/i/c/Licences.html>`__, provided they
    are for internal use only. Distribution would require those changes
    to be incorporated into our svn repository. Please email to
    `phaser-help <mailto:cimr-phaser@lists.cam.ac.uk>`__ for further
    advice if you would like to modify the Phaser source code.

.. rubric:: Repository
   :name: repository

The SVN repository for Phaser is located in Cambridge on the CIMR server

View the `Phaser SVN
repository <http://www-structmed.cimr.cam.ac.uk/svn-cgi-bin/viewvc.cgi/>`__
online
*Viewing the Phaser SVN respository online is password restricted in
order to manage IT security. Please email requests for the password to
`phaser-help <mailto:cimr-phaser@lists.cam.ac.uk>`__.*

SSH Access
    Requests for permission to access to SVN repository at
    phaser-svn.cimr.cam.ac.uk through ssh should emailed to
    `phaser-help <mailto:cimr-phaser@lists.cam.ac.uk>`__.

.. rubric:: Mirrors
   :name: mirrors

The Berkeley mirror at cci.lbl.gov is updated at midnight Berkeley time

/net/cci/auto_build/repositories/phaser

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
