.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Help:Redirects
   :name: helpredirects
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

←
*`Help:Contents <../../../../articles/c/o/n/Help%7EContents_22de.html>`__*

Redirects are used to forward users from one page name to another. They
can be useful if a particular article is referred to by multiple names,
or has alternative punctuation, capitalization or spellings.

.. rubric:: Creating a redirect
   :name: creating-a-redirect

You may start a new page with the name you want to direct from (see
`Help:Starting a new
page <../../../../articles/s/t/a/Help%7EStarting_a_new_page_3f5a.html>`__).
You can also use an existing page that you are making inactive as a page
by going to that page and using the "edit" tab at the top. In either
case, you will be inserting the following code at the very first text
position of the Edit window for the page:

::

    #REDIRECT [[pagename]]

where *pagename* is the name of the destination page. The word
"redirect" is not case-sensitive, but there must be no space before the
"#" symbol. Any text before the code will disable the code and prevent a
redirect. Any text or regular content code after the redirect code will
be ignored (and should be deleted from an existing page). However, to
put or keep the current page name listed in a Category, the usual tag
for that category is entered or kept on a line after the redirect code
entry.

You should use the 'preview' button below the Edit window, or Alt-P, to
check that you have entered the correct destination page name. The
preview page will not look like the resulting redirect page, it will
look like a numbered list, with the destination page in blue:

::

    1. REDIRECT  pagename

If the *pagename* as you typed it is not a valid page, it will show in
red. Until there is a valid destination page, you should not make the
redirect.

.. rubric:: Viewing a redirect
   :name: viewing-a-redirect

After making a redirect at a page, you can no longer get to that page by
using its name or by any link using that name; and they do not show up
in wiki search results, either. However, near the top of the destination
page, a notice that you have been forwarded appears, with the source
pagename as an active link to it. Click this to get back to the
redirected page, showing the large bent arrow symbol and the destination
for the redirect.

By doing this, you can do all the things that any wiki page allows. You
can go to the associated discussion page to discuss the redirect. You
can view the history of the page, including a record of the redirect.
You can edit the page if the redirect is wrong, and you can revert to an
older version to remove the redirect.

.. rubric:: Deleting a redirect
   :name: deleting-a-redirect

There's generally no need to delete redirects. They do not occupy a
significant amount of database space. If a page name is vaguely
meaningful, there's no harm (and some benefit) in having it as a
redirect to the more relevant or current page.

If you do need to delete a redirect, e.g. if the page name is offensive,
or you wish to discourage people from referring to a concept by that
name, then you simply go to the redirect page as mentioned above, and
follow the procedures at `Help:Deleting a
page <../../../../articles/d/e/l/Help%7EDeleting_a_page_b3ce.html>`__.

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Help <../../../../articles/h/e/l/Category%7EHelp_3a02.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
