HGEN (CCP4: Supported Program)
==============================

NAME
----

**hgen** - generate hydrogen atom positions for proteins

SYNOPSIS
--------

| **hgen** **xyzin** *input.brk* **xyzout** *output.brk*
| [`Keyworded input <#keywords>`__]

.. _description: 
 
DESCRIPTION
---------------------------------

Generates hydrogen atoms for a protein coordinate file with standard
geometry, using a bond length of 1.0 Å. The program operates on
Brookhaven files. There is a library specifying which atoms in a
particular residue type have H atoms attached to them embedded in the
code.

.. _keywords: 
 
 KEYWORDED INPUT
-----------------------------------

There is only one line of input which determines whether the generated
hydrogens are merged with other atoms or whether they are put in a
separate file.

HYDROGENS SEPARATE
~~~~~~~~~~~~~~~~~~

hydrogens in separate file;

HYDROGENS APPEND
~~~~~~~~~~~~~~~~

hydrogens and protein merged.

By default ( *e.g.* for immediate end-of-file) the hydrogens are
appended.

.. _files: 
 
 FILES
----------------------

XYZIN
   Input coordinates.
XYZOUT
   Output coordinates - either hydrogens alone, or merged with protein
   according to the input line.

.. _examples: 
 
EXAMPLES
---------------------------

Unix example script found in $CEXAM/unix/runnable/

`hgen.exam <../examples/unix/runnable/hgen.exam>`__

AUTHOR
------

Simon Phillips. Rewritten by Z. Dauter, modified by Victor S. Lamzin.
