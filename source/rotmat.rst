ROTMAT (CCP4: Supported Program)
================================

NAME
----

**rotmat** - interconverts CCP4/MERLOT/X-PLOR rotation angles.

SYNOPSIS
--------

| **rotmat**
| [`Keyworded input <#keywords>`__]

.. _description: 
 
DESCRIPTION
---------------------------------

This program converts the CCP4/MERLOT/X-PLOR rotation angles to any
other format, and generates their symmetry equivalents. MERLOT/CCP4
conventions are the same; X-PLOR is different. See `below <#notes>`__
for further details of this.

It also tests whether [Ri]*[Rj]**-1 satisfies some target. This is
useful for checking whether your set of solutions agree with a self
rotation, or perhaps some outside criteria.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

The various data control lines are identified by keywords, those
available being:

   `CRYSTAL <#crystal>`__, `END <#end>`__, `ERROR <#error>`__,
   `INPUT <#input>`__, `OUTPUT <#output>`__, `TARGET <#target>`__,
   `VERBOSE <#verbose>`__

CRYSTAL and INPUT are compulsory. CRYSTAL (with CELL information) must
be given before INPUT, OUTPUT or TARGET.

.. _crystal: 
 
CRYSTAL subkeyword ..
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Subkeywords:

.. _crystalnumber: 
 
NUMBER <crystal number>
   <crystal number> must be 1 or 2. In cases where two CRYSTALs are
   defined with different spacegroups and cell parameters, then crystal
   2 should correspond to the set of coordinates which are rotated by
   the INPUT solutions to overlap those in crystal 1 *i.e.* crystal 1
   represents the data and crystal 2 the model.
.. _crystalfile: 
 
FILE <crystal number>
   <crystal number> must be 1 or 2
.. _crystalorth: 
 
ORTH <ncode>
   <ncode> is the orthogonalisation code.
.. _crystalsymm: 
 
SYMM <SG number> | <SG name>
   specifies the spacegroup in International Tables style.
.. _crystalcell: 
 
CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
   Specify the cell dimensions (Å) and angles (degrees). The angles
   default to 90 degrees and rarely need to be changed - just for peak
   analysis.

.. _verbose: 
 
VERBOSE
~~~~~~~~~~~~~~~~~~~~~~~~~

Produces lots more output.

.. _input: 
 
INPUT CCP4 | MERLOT | XPLOR subkeywords ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Compulsory].

Input a rotation in a specified format. Format conversion is performed
and the results output.

The possible subsidiary keywords are: `ALPHA <#inputalpha>`__,
`THETA <#inputtheta>`__, `OMEGA <#inputomega>`__,
`PSI <#inputpsi>`__, `PHI <#inputphi>`__, `AXIS <#inputaxis>`__ and
`MATRIX <#inputmatrix>`__, followed by appropriate numbers.

.. _inputalpha: 
 
ALPHA [ BETA GAMMA ] <alpha> [ <beta> <gamma> ]
   alpha beta gamma.
.. _inputtheta: 
 
THETA1 [ THETA2 THETA3 ] <theta1> [ <theta2> <theta3> ]
   theta1 theta2 theta3
.. _inputomega: 
 
OMEGA [ PHI KAPPA ] <omega> [ <phi> <kappa> ]
   omega phi kappa
.. _inputpsi: 
 
PSI [ PHI KAPPA ] <psi> [ <phi> <kappa> ]
   psi phi kappa
.. _inputphi: 
 
PHI [ PSI KAPPA ] <phi> [ <psi> <kappa> ]
   phi kappa
.. _inputaxis: 
 
AXIS
   direction cosines - kappa - special for XPLOR.
.. _inputmatrix: 
 
MATRIX <r11> <r12> <r13> <r21> <r22> <r23> <r31> <r32> <r33>
   Specifies matrix elements. Be careful of transpose.

.. _target: 
 
TARGET
~~~~~~~~~~~~~~~~~~~~~~~

Input a target rotation. The subsidiary keywords for TARGET have the
same format as those for `INPUT <#input>`__.

If `OUTPUT <#output>`__ is specified, checks whether

   [Rj]*[Ri]**(-1)*[Rtarget]**(-1)

lies near the identity matrix, *i.e.* it checks if Kappa of this matrix
is less than the acceptable error given by the `ERROR <#error>`__
keyword for pairs of `INPUT <#input>`__s. This may be useful to see
whether two cross rotation solutions are consistent with a given self
rotation specified as a `TARGET <#target>`__.

.. _output: 
 
OUTPUT
~~~~~~~~~~~~~~~~~~~~~~~

The subsidiary keywords for OUTPUT have the same format as those for
`INPUT <#input>`__ but the MATRIX sub-keyword doesn't take arguments and
implies `VERBOSE <#verbose>`__.

If an INPUT line is followed by OUTPUT request, all symmetry equivalent
solutions are given using CCP4 conventions. For this,
`CRYSTAL <#crystal>`__ information must have been supplied. If both
appropriate sub-keywords and VERBOSE are specified, then symmetry
equivalents in other conventions are given.

If a `TARGET <#target>`__ line is followed by OUTPUT request, checks
matrix pairs to see if they fit self rotation defined by TARGET rotation
angles.

.. _error: 
 
ERROR <kappa>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specify the acceptable error limit <kappa> for the `TARGET <#target>`__
command. Default value is 10.0.

.. _end: 
 
END
~~~~~~~~~~~~~~~~~

Terminates input.

CALCULATIONS
------------

For each `INPUT <#input>`__ the full set of rotation angles for all the
other conventions is calculated. These are corrected for the appropriate
orthogonalisation. Theory:

For the self rotation, where coordinates [X'O] are mapped onto [XO]

::


            [Rncode=i] [X'Oncode=i]  = [XOncode=i]          (1)
      and   [Rncode=j] [X'Oncode=j]  = [XOncode=j]          (2)

Remember: fractional coordinates are invariant, and

::


             [XOncode=?] = [ROncode=?] [Xf] for any ncode.
    Since       [RFncode=?] [ROncode=?] ==[I]                (3)

             [Xf] = [RFncode=?] [XOncode=?].

    So       [XOncode=j] = [ROncode=j] [RFncode=i] [XOncode=i]

Now (2) can be rewritten as

::


             [Rncode=j][ROncode=j] [RFncode=i] [X'Oncode=i]
                  =       [ROncode=j] [RFncode=i] [XOncode=i]

Rearranging this:

::


    [RFncode=i]**(-1) [ROncode=j]**(-1) [Rncode=j][ROncode=j] [RFncode=i] [X'Oncode=i]
                  = [XOncode=i]

>From (3) it follows that

::


             [RFncode=?]**(-1) = [ROncode=?]
        and  [ROncode=?]**(-1) = [RFncode=?]

so (2) becomes

::


        [ROncode=i][RFncode=j][Rncode=j][ROncode=j] [RFncode=i] [X'Oncode=i]
                  = [XOncode=i]

    i.e. [ROncode=i][RFncode=j][Rncode=j][ROncode=j] [RFncode=i] == [Rncode=i]

For the cross rotation, where model coordinates [YO] are mapped onto
some [XO] we do not need to consider more than one orthogonalisation
code for the [YO]. These coordinates are always positioned in some
orthogonal P1 cell.

::


            [Rncode=i] [YO]  = [XOncode=i]           (4)
      and   [Rncode=j] [YO]  = [XOncode=j]           (5)

Eq. (5) can be rewritten as

::


          [Rncode=j] [YO]  = [ROncode=j] [RFncode=i] [XOncode=i]

and rearranging this:

::


         [RFncode=i]**(-1) [ROncode=j]**(-1) [Rncode=j] [YO]  = [XOncode=i]
    i.e.   [ROncode=i][RFncode=j][Rncode=j] == [Rncode=i]

Similar arguments can be used to get the symmetry equivalents of any [R]

`ALMN <almn.html>`__ and `POLARRFN <polarrfn.html>`__ allow you to
choose your orthogonalisation convention whereas MERLOT and X-PLOR have
fixed values.

The orthogonal axes I J K are defined thus:

::

     NCODE = 1  I parallel to a, K parallel to c*, J in a-b plane.
     NCODE = 2  I parallel to b, K parallel to a*, J in b-c plane.
     NCODE = 3  I parallel to c, K parallel to b*, J in c-a plane.
     NCODE = 5  I parallel to a*, K parallel to c, J in b-c plane.
     NCODE = 6  I parallel to a, K parallel to b*, J in c-a plane.

(Others are listed in the `ALMN documentation <almn.html>`__.)

MERLOT and X-PLOR are both fixed:

MERLOT

Monoclinic spacegroups - b rotation: NCODE = 3, *i.e.*
   I parallel to c, K parallel to b*( = b), J in a-c plane.
All others: NCODE = 5, *i.e.*
   I parallel to a*, K parallel to c, J in b-c plane.

X-PLOR

All spacegroups: NCODE = 1, *i.e.*
   I parallel to a, K parallel to c* (= b), J in a-b plane.

.. _examples: 
 
EXAMPLE
--------------------------

::

   #
   #  So  far program messages are:
   #    Input/Output MERLOT ALPHA  BETA   GAMMA
   #    Input/Output MERLOT THETA1 THETA2 THETA3
   #    Input/Output MERLOT PHI    PSI    KAPPA
   #    Input/Output MERLOT MATRIX
   #    Input/Output CCP4   ALPHA  BETA   GAMMA
   #    Input/Output CCP4   PHI    PSI    KAPPA
   #    Input/Output CCP4   MATRIX
   #    Input/Output XPLOR  THETA1 THETA2 THETA3
   #    Input/Output XPLOR  PHI    PSI    KAPPA
   #    Input/Output XPLOR  MATRIX
   #
   rotmat << eof
   CRYSTAL number 1 cell 20 30 40 90 110 90
   CRYSTAL number 1 orth 3
   CRYSTAL number 2 symm 19 cell 60 70 80 90 90 90 orth 1
   INPUT XPLOR phi psi kappa 62.9 112 140 
   INPUT XPLOR phi psi kappa 63  106.46 156.663 
   INPUT XPLOR phi psi kappa 43 8   137
   OUTPUT MERLOT phi psi kappa 
   END  
   eof
   #

.. _notes: 
 
NOTES
---------------------

.. _rotation_matrix_conventions: 
 
MERLOT/ALMN/POLARRFN/LSQKAB conventions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*N.B.*
   Here the letters I J K represent AXES, while X Y Z represent
   COORDINATES in the ORTHOGONAL axial frame.
*N.N.B.*
   Positive rotations appear ANTI-CLOCKWISE when viewed toward the
   origin from the positive direction of the rotation axis.
*N.N.N.B.*
   All matrices are listed here as the TRANSPOSE of those in the MERLOT
   documentation. MERLOT gives matrices to rotate a COLUMN vector of
   AXES.

Rotation matrices to PREMULTIPLY coordinate columns or POSTMULTIPLY axis
rows vectors are defined as follows:

#. Using Eulerian angles ALPHA BETA GAMMA (CCP4/MERLOT) relative to
   orthogonal axes (I J K) and defining [R] as the product of:

   ::

              Rotation 1 (ALPHA) about K         :  [R]alpha
              Rotation 2 (BETA)  about the new J :  [R]beta
              Rotation 3 (GAMMA) about the new K :  [R]gamma

          (and abbreviating cos(ALPHA) = CA; sin(BETA) = SB etc)

             [ R11 R12 R13 ]
             [ R21 R22 R23 ]  =  [ R]alpha * [R]beta * [R]gamma 
             [ R31 R32 R33 ]
                                 [CA -SA 0] [ CB 0 SB] [CG -SG 0]
                              =  [SA  CA 0]*[  0 1  0]*[SG  CG 0] 
                                 [ 0   0 1] [-SB 0 CB] [ 0   0 1]

            ( CA CB CG - SA SG      -CA CB SG - SA CG      CA SB )
          = ( SA CB CG + CA SG      -SA CB SG + CA CG      SA SB )
            (         -SB CG                  SB SG           CB )

      Note that the rotation matrix generated from (ALPHA,BETA,GAMMA) is
      identical to that generated from (PI+ALPHA,-BETA,PI+GAMMA) so it
      is conventional to restrict BETA to range: 0 to PI
      If you think of coordinates as vector products of the row vector
      of the AXES by the column vector of orthogonal coordinates

      ::

                                      [ I J K] * [ X0]
                                                 [ Y0]
                                                 [ Z0]

      and a rotation matrix [R] as moving these to

      ::

                                [ I J K] * [R] * [ X0]
                                                 [ Y0]
                                                 [ Z0]

      | then it is obvious that rotation [R] can be seen *either*
      | as rotating the axes [I J K ] by ALPHA then BETA then GAMMA *or*
      | as rotating coordinates [X0] by -GAMMA then -BETA then -ALPHA.

#. Using polar angles PSI/OMEGA PHI KAPPA ( *N.B.* Merlot PSI identical
   to CCP4 OMEGA):

      The definition is:

      OMEGA/PSI
         gives inclination of rotation axis to K axis;
      PHI
         gives anticlockwise rotation from I to projection of rotation
         axis onto I-J plane;
      KAPPA
         is the rotation about the rotation axis.
      | Int. Tab. Vol 2 (p 59?) gives this rotation matrix:
      | (l m n) are the direction cosines of the axis about which the
        rotation KAPPA takes place.

      ::

          (abbreviating cos(OMEGA) = CO; sin(PHI) = SP etc)
                  ( l )             ( SO*CP )
                  ( m )     =       ( SO*SP )
                  ( n )             (    CO )

          and   [ R11 R12 R13 ]
                [ R21 R22 R23 ]   =
                [ R31 R32 R33 ]

              ( ll+(mm+nn)CK     lm(1-CK)-nSK     nl(1-CK)+mSK )
              ( lm(1-CK)+nSK     mm+(ll+nn)CK     mn(1-CK)-lSK )
              ( nl(1-CK)-mSK     mn(1-CK)+lSK     nn+(ll+mm)CK )

      Note that the rotation matrix generated from (OMEGA,PHI,KAPPA) is
      identical to that generated from (PI-OMEGA,PI+PHI,-KAPPA) so it is
      conventional to restrict KAPPA to range: 0 to PI

      An alternative definition which generates the same matrix is given
      in the MERLOT documentation:

      ::

              This can be visualised as
              Rotation 1 (PHI  )  about Z
              Rotation 2 (PSI  )  about the new Y
              Rotation 3 (KAPPA)  about the new Z

              Rotation 4 (PSI-1)  about the new Y
              Rotation 5 (PHI-1)  about the new Z

#. Using Eulerian angles THETA1 THETA2 THETA3

      ::

              Rotation 1 (THETA1) about K
              Rotation 2 (THETA2) about the new I
              Rotation 3 (THETA3) about the new K

         Rotation 1   =                CT1      -ST1       0  
                                       ST1       CT1       0 
                                       0         0         1

         Rotation 2   =                1         0         0 
                                       0         CT2      -ST2
                                       0         ST2       CT2

         Rotation 3   =                CT3      -ST3       0
                                       ST3       CT3       0 
                                       0         0         1


          Matrix for axis system is 
             Rotation 3 (Rotation 2 (Rotation 1) ) =  

                CT1*CT3             -CT1*ST3             ST1*ST2 
               -ST1*CT2*ST3         -ST1*CT2*CT3           

                ST1*CT3             -ST1*ST3            -CT1*ST2
               +CT1*CT2*ST3         +CT1*CT2*CT3   

                ST2*ST3              ST2*CT3             CT2

      Note that the rotation matrix generated from
      (THETA1,THETA2,THETA3) is identical to that generated from
      (PI+THETA1,-THETA2,PI+THETA3) so it is conventional to restrict
      THETA2 to range: 0 to PI

.. _xplorconventions: 
 
X-PLOR CONVENTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These are not very clear in the documentation: the following definitions
are taken from the program and SHOULD BE CHECKED.

Extracts from subroutine rotmat.

::

    Routine computes unitary rotation matrix ROT using
    a) Eulerian angles (MODE=EULE),
    b) Lattman angles (MODE=LATT), 
    c) Spherical polar angles (MODE=SPHE) or 
    d) A rotation about the specified axis (MODE=AXIS).

In Eulerian mode, angles are
   XTHETA1 XTHETA2 XTHETA3.
In Lattman mode, angles are
   XTHETA+=XTHETA1+XTHETA3, XTHETA-=XTHETA1-XTHETA3
In Spherical or Axis mode:
   compute rotation matrix corresponding to the three spherical polar
   angles PSI=t1, PHI=t2 and KAPPA=t3.

| *N.B.* BEWARE: The angle convention is DIFFERENT to MERLOT/CCP4.
| The rotation is described by specification of the direction of an axis
  through

PHI
   (azimuthal angle between the I axis and the projection of the
   rotation axis on the I-K plane)
and
PSI
   (inclination versus J axis).

The angle KAPPA specifies the rotation around the specified axis. The
KAPPA angle is anti-clockwise when looking along the rotation axis. The
PHI angle is anti-clockwise when looking along J.

In axis mode we obtain the PSI, PHI spherical polar angles from the AXIS
vector.

Output:

::

       ROT(3,3) contains the rotation matrix.  Should be applied as 
      r''(i)=sum_j ROT(i,j)*r(j)

BUT EJD thinks the X-PLOR matrix equals CCP4/MERLOT matrix transpose.
This means that: Eulerian XT1 XT2 XT3 (X-PLOR) = -T3 -T2 -T1 (MERLOT).
Which is the same as PI-T3 T2 PI-T1.

The matrix for X-PLOR axis system in Eulerian theta is

::

          CT1*CT3              ST1*CT3             ST2*ST3 
         -ST1*CT2*ST3         +CT1*CT2*ST3           

         -CT1*ST3             -ST1*ST3             ST2*CT3
         -ST1*CT2*CT3         +CT1*CT2*CT3   

          ST1*ST2             -CT1*ST2             CT2

To derive the rotation matrix for X-PLOR spherical polar angles
PHI,PSI,and KAPPA.

::

        This can be visualised as
        Rotation 1 (PHI  )  about K
        Rotation 2 (PSI  )  about the new J
        Rotation 3 (KAPPA)  about the new K

        Rotation 4 (PSI-1)  about the new J
        Rotation 5 (PHI-1)  about the new K

       +CK                 -SF*SP*SK           -CP*SK 
       +CF*CF*SP*SP(1-CK)  +CF*SP*CP*(1-CK)    -SF*CF*SP*SP(1-CK)

       +SP*SF*SK           +CK                 +CF*SP*SK  
       +CF*SP*CP(1-CK)     +CP*CP(1-CK)        -SF*CP*SP(1-CK)

       +CP*SK              -CF*SP*SK           +CK 
       -SF*CF*SP*SP(1-CK)  -SF*CP*SP(1-CK)     +SF*SF*SP*SP(1-CK)

.. _orthogonalisationconventions: 
 
ORTHOGONALISATION CONVENTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CCP4
   allows the user to request their own orthogonalisation. Useful ones
   are

   ::

        I parallel to a, J in a-b plane, K parallel to c*(ncode = 1)
        I parallel to b, J in b-c plane, K parallel to a*(ncode = 2)
        I parallel to c, J in c-a plane, K parallel to b*(ncode = 3)

MERLOT
   always uses ncode = 5 for all spacegroups except the monoclinic (b
   unique) spacegroups where it uses ncode = 3.
X-PLOR
   always uses NCODE = 1

AUTHOR
------

Eleanor Dodson, York University, Jan 1992
