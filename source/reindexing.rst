Reindexing (CCP4: General)
==========================

NAME
----

reindexing - information about changing indexing regime

Contents
~~~~~~~~

-  `General Remarks <#generalremarks>`__
-  `A real monoclinic example <#monocliniceg>`__
-  `Lookup tables <#lookuptables>`__
-  `Changing hand <#changinghand>`__
-  `Pictures <#hklviewexamples>`__

.. _generalremarks: 
 


General Remarks
~~~~~~~~~~~~~~~

It is quite common to find that the diffraction from subsequent crystals for a protein do not apparently merge well. There are many physical reasons for this, but before throwing the data away it is sensible to consider whether another indexing regime could be used. For illustrations and examples see `HKLVIEW-examples below <#hklviewexamples>`__. For documentation on re-indexing itself, and some hints, see also `REINDEX <reindex.html>`__.

For **orthorhombic** crystal forms with different cell dimensions along each axis you can usually recognise if the next crystal is the same as the last and see how to transform it (remember to keep your axial system right-handed!).

In **P1** and **P21** there are many ways of choosing axes, but they should all generate the same crystal volume. Use `MATTHEWS_COEF <matthews_coef.html>`__ or some other method to check this - if the volumes *are not* the same, or at least related by integral factors, you have a new form. If they *are* the same it is recommended to plot some sections of the reciprocal lattice; you can often see that the patterns will match if you rotate in some way (see `HKLVIEW-examples below <#hklviewexamples>`__). A common change in **P21** or **C2** where the twofold axis will be constant, is that a*new = a*old + c*old, and c*new must be chosen carefully. One very confusing case can arise if the length of (a*+nc*) is almost equal to that of a* or nc*, but it should be possible to sort out from the diffraction pattern plots.

Confusion arises mostly when two or more axes are the same length, as in the **tetragonal**, **trigonal**, **hexagonal** or **cubic** systems. In these cases any of the following definitions of axes is equally valid and likely to be chosen by an auto-indexing procedure. The classic description of this is that these are crystals where the Laue symmetry is of a lower order than the apparent crystal lattice symmetry.

+------------------+------------+----+---------------+----+-------------+----+------------------+
| real axes:       | (a,b,c)    | or | (-a,-b,c)     | or | (b,a,-c)    | or | (-b,-a,-c)       |
+------------------+------------+----+---------------+----+-------------+----+------------------+
| reciprocal axes: | (a*,b*,c*) | or | (-a*,-b*,c*)  | or | (b*,a*,-c*) | or | (-b*,-a*,-c* )   |
+------------------+------------+----+---------------+----+-------------+----+------------------+

*N.B.* There are alternatives where other pairs of symmetry operators are used, but this is the simplest and most general set of operators. For example: in P4i (-a,-b,c) is equivalent to (-b,a,c), or in P3i (-a,-b,c) is equivalent to (-b,a+b,c).

*N.B.* In general you should not change the hand of your axial system; *i.e.* the determinant of the transformation matrix should be positive, and only such transformations are discussed here.

*The crystal symmetry may mean that some of these systems are already equivalent:*

*For instance, if* (h,k,l) *is equivalent to* (-h,-k,l), *the axial system pairs* [(a,b,c) and (-a,-b,c)] *and* [(b,a,-c) and (-b,-a,-c)] *are indistinguishable. This is the case for all tetragonal, hexagonal and cubic spacegroups.If* (h,k,l) *is equivalent to* (k,h,-l), *the axial system pairs* [(a,b,c) and (b,a,-c)] *and*[(-a,-b,c) and (-b,-a,-c)] *are indistinguishable. This is truefor* P4i2i2, P3i2, P6i22 *and some cubic spacegroups.*

*If* (h,k,l) *is equivalent to* (-k,-h,-l), *the axial system pairs* [(a,b,c) and (-b,-a,-c)] *and*[(-a,-b,c) and (b,a,-c)] *are indistinguishable. This is only truefor* P3i12 *spacegroups.*
      *See detailed descriptions below*.

.. _monocliniceg: 
 


A real monoclinic example
~~~~~~~~~~~~~~~~~~~~~~~~~

Two datasets from the same crystal with apparently the same cell
dimensions:

run1 55.76 84.62 70.96 90 112.71 90

run2 56.04 85.11 71.57 90 113.15 90

However, the Rmerge from Scala was around 40% Viewing the k = constant sections in `HKLVIEW <hklview.html>`__ showed that the diffraction patterns were rotated with respect to each other. A reindexing transformation of -h, -k, h+l was inferred. In terms of the real-space cell, the new axes are constructed as follows:

.. image:: images/monoclinic_eg.gif
   :alt: cell axes transformation
   :height: 300px

Some basic trigonometry shows the new cell dimensions to be:

run1 (reindexed) 55.76 84.62 71.33 90 113.42 90

The new dimensions are slightly closer to those of run2. The previous
similarity is shown to be a coincidence.

Note the transformation -h, -k, h+l preserves the hand, i.e. the
corresponding matrix has a determinant of +1. 

.. _lookuptables: 
 


Lookup tables
~~~~~~~~~~~~~

Here are details for the possible systems:

-  All **P4i** and related **4i** space groups:
   (h,k,l) equivalent to (-h,-k,l) so we only need to check:

   ================ ========== === ===========
   real axes:       (a,b,c)    and (b,a,-c)
   reciprocal axes: (a*,b*,c*) and (b*,a*,-c*)
   ================ ========== === ===========

   *i.e.* check if reindexing (h,k,l) to (k,h,-l) gives a better match to previous data sets.

+--------------------+-------------+-------------+----------------+
| space group number | space group | point group | crystal system |
+====================+=============+=============+================+
| 75                 | P4          | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
| 76                 | P41         | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
| 77                 | P42         | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
| 78                 | P43         | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
| 79                 | I4          | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
| 80                 | I41         | PG4         | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+


-  For all **P4i2i2** and related **4i2i2** space groups:
   (h,k,l) is equivalent to (-h,-k,l) *and* (k,h,-l) *and* (-k,-h -l) so any choice of axial system will give identical data.

+--------------------+-------------+-------------+----------------+
| space group number | spacegroup  | point group | crystal system |
+====================+=============+=============+================+
|   89               | P422        | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   90               | P4212       | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   91               | P4122       | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   92               | P41212      | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   93               | P4222       | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   94               | P42212      | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   95               | P4322       | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   96               | P43212      | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   97               | I422        | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+
|   98               | I4122       | PG422       | TETRAGONAL     |
+--------------------+-------------+-------------+----------------+


**All P3i** : (h,k,l) **not** equivalent to (-h,-k,l) or (k,h,-l) or (-k,-h,-l) so we need to check all 4 possibilities for reindexing:

+------------------+------------+-----+--------------+-----+-------------+-------------------+---------------+
| real axes:       | (a,b,c)    | and | (-a,-b,c)    | and | (b,a,-c)    | and | (-b,-a,-c)             |
+------------------+------------+-----+--------------+-----+-------------+-------------------+---------------+
| reciprocal axes: | (a*,b*,c* ) | and | (-a*,-b*,c* ) | and | (b*,a*,-c* ) | and | (-b*,-a*,-c* )           |
+------------------+------------+-----+--------------+-----+-------------+-------------------+---------------+

*i.e.* reindex (h,k,l) to (-h,-k,l) *or* (h,k,l) to (k,h,-l) *or* (h,k,l) to (-k,-h,-l).
*N.B. For trigonal space groups, symmetry equivalent reflections can be conveniently described as* (h,k,l), (k,i,l) *and* (i,h,l) *where* i=-(h+k). Replacing the 4 basic sets with a symmetry equivalent gives a bewildering range of possibilities!

+--------------------+-------------+-------------+----------------+
| space group number | space group | point group | crystal system |
+====================+=============+=============+================+
| 143                | P3          | PG3         | TRIGONAL       |
+--------------------+-------------+-------------+----------------+
| 144                | P31         | PG3         | TRIGONAL       |
+--------------------+-------------+-------------+----------------+
| 145                | P32         | PG3         | TRIGONAL       |
+--------------------+-------------+-------------+----------------+
| 146                | R3          | PG3         | TRIGONAL       |
+--------------------+-------------+-------------+----------------+

-  All **P3i12** (h,k,l) already equivalent to (-k,-h,-l) so we only need to check (k,h,-l):

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         149             P312          PG312         TRIGONAL     
         151             P3112         PG312         TRIGONAL     
         153             P3212         PG312         TRIGONAL     
==================== ============= ============= ================ 

-  All **P3i21** and **R32**:
   
   (h,k,l) already equivalent to (k,h,-l) so we only need to check:

================ ========== === ============
real axes:       (a,b,c)    and (-a,-b,c)
reciprocal axes: (a*,b*,c*) and (-a*,-b*,c*)
================ ========== === ============

*i.e.* reindex (h,k,l) to (-h,-k,l).

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         150             P321          PG321         TRIGONAL     
         152             P3121         PG321         TRIGONAL     
         154             P3221         PG321         TRIGONAL     
         155              R32          PG32          TRIGONAL     
==================== ============= ============= ================ 

-  All **P6i**:
   
   (h,k,l) already equivalent to (-h,-k,l) so we only need to check:

================ =========== === ===========
real axes:       (a,b,c)     and (b,a,-c)
reciprocal axes: (a*,b*,c*)  and (b*,a*,-c*)
================ =========== === ===========

*i.e.* reindex (h,k,l) to (k,h,-l)

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         168              P6            PG6         HEXAGONAL     
         169              P61           PG6         HEXAGONAL     
         170              P65           PG6         HEXAGONAL     
         171              P62           PG6         HEXAGONAL     
         172              P64           PG6         HEXAGONAL     
         173              P63           PG6         HEXAGONAL     
==================== ============= ============= ================ 


For **R3**:hexagonal(obverse)(may be labelled as s H3 and R32 as H32). This non-primitive spacegroup has alternate origins at (0, 0, 0), (2/3, 1/3, 1/3) and (1/3, 2/3, 2/3) with respect to the a, b and c axes which means the observed reflections for this non-primitive space group require -h +k +l = 3n so the data cannot be reindexed to (-h,-k,l) or (-k,-h,-l)

If this reindexing were imposes the space group name would become R3:hexagonal (reverse) and the alternate origins would be (0, 0, 0), (1/3, 2/3, 1/3) and (2/3, 1/3, 2/3)}

There is a good description of the R3:H alternatives here: https://www.ucl.ac.uk/~rmhajc0/rhombohexagonal.pdf


-  All **P6i2**:
   (h,k,l) already equivalent to (-h,-k,l) *and* (k,h,-l) *and* (-k,-h,-l) so we do not need to check.

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         177             P622          PG622        HEXAGONAL     
         178             P6122         PG622        HEXAGONAL     
         179             P6522         PG622        HEXAGONAL     
         180             P6222         PG622        HEXAGONAL     
         181             P6422         PG622        HEXAGONAL     
         182             P6322         PG622        HEXAGONAL     
==================== ============= ============= ================ 

-  All **P2i3** and related **2i3** space groups:
   (h,k,l) already equivalent to (-h,-k,l) so we only need to check:

================ ========== === ===========
real axes:       (a,b,c)    and (b,a,-c)
reciprocal axes: (a*,b*,c* ) and (b*,a*,-c* )
================ ========== === ===========

*i.e.* reindex (h,k,l) to (k,h,-l).

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         195              P23          PG23           CUBIC       
         196              F23          PG23           CUBIC       
         197              I23          PG23           CUBIC       
         198             P213          PG23           CUBIC       
         199             I213          PG23           CUBIC       
==================== ============= ============= ================ 


-  All **P4i32** and related **4i32** space groups:
(h,k,l) already equivalent to (-h,-k,l) *and* (k,h,-l) *and* (-k,-h,-l) so we do not need to check.

==================== ============= ============= ================ 
space group number   space group   point group   crystal system  
==================== ============= ============= ================ 
         207             P432          PG432          CUBIC       
         208             P4232         PG432          CUBIC       
         209             F432          PG432          CUBIC       
         210             F4132         PG432          CUBIC       
         211             I432          PG432          CUBIC       
         212             P4332         PG432          CUBIC       
         213             P4132         PG432          CUBIC       
         214             I4132         PG432          CUBIC       
==================== ============= ============= ================ 


.. _changinghand: 
 


Changing hand
~~~~~~~~~~~~~

[Bernhardinelli(1985)]

Test to see if the other hand is the correct one:
Change x,y,z for (cx-x, cy-y, cz-z)
Usually (cx,cy,cz) = (0,0,0).

Remember you need to change the twist on the screw-axis stairs for P3i,
P4i, or P6i!

P2 :sub:`1` - to P2 :sub:`1` ; For the half step of 2 :sub:`1` axis,
the symmetry stays the same.

P3 :sub:`1` - to P3 :sub:`2`
P3 :sub:`2` - to P3 :sub:`1`

P4 :sub:`1` to P4 :sub:`3`
(P4 :sub:`2` - to P4 :sub:`2` : Half c axis step)
P4 :sub:`3` -to P4 :sub:`1`

P6 :sub:`1` to P6 :sub:`5`
P6 :sub:`2` - to P6 :sub:`4`
(P6 :sub:`3` - to P6 :sub:`3`)
etc.

In a few non-primitive spacegroups, you can change the hand and not change the spacegroup by a cunning shift of origin:

I4 :sub:`1`
   (x,y,z) to (-x,1/2-y,-z)
I4 :sub:`1` 22
   (x,y,z) to (-x,1/2-y,1/4-z)
F4 :sub:`1` 32
   (x,y,z) to (1/4-x,1/4-y,1/4-z)

Plus some centric ones:

Fdd2
   (x,y,z) to (1/4-x,1/4-y,-z)
I4 :sub:`1` md
   (x,y,z) to (1/4-x,1/4-y,-z)
I4 :sub:`1` cd
   (x,y,z) to (1/4-x,1/4-y,-z)
I4bar2d
   (x,y,z) to (1/4-x,1/4-y,-z)

.. _hklviewexamples: 
 
PICTURES
-----------------------------------

Full size versions of the example pictures can be viewed by clicking on the iconised ones.

+-----------------------------------+-----------------------------------+
| |HKLVIEW Barnase pH6|             | A P3 :sub:`2` data set indexed    |
|                                   | h,k,l                             |
+-----------------------------------+-----------------------------------+
| |HKLVIEW Barnase pH6 indexed      | The same P3 :sub:`2` data set,    |
| -h-kl|                            | reindexed -h,-k,l                 |
+-----------------------------------+-----------------------------------+
| |HKLVIEW Barnase pH6 indexed      | The same P3 :sub:`2` data set,    |
| -k-hl|                            | reindexed -k,-h,l                 |
+-----------------------------------+-----------------------------------+
| |HKLVIEW Barnase pH6 indexed      | The same P3 :sub:`2` data set,    |
| kh-l|                             | reindexed k,h,-l                  |
+-----------------------------------+-----------------------------------+

.. _HIPIPexamples: 

+-----------------------------------+-----------------------------------+
| Monoclinic data set, HKLVIEW      |                                   |
|  |HKLVIEW Hipip h2l|              | h,2,l                             |
+-----------------------------------+-----------------------------------+
|  |HKLVIEW Hipip reindexed|        | The same monoclinic data set,     |
|                                   | reindexed -h,-k,h-l               |
+-----------------------------------+-----------------------------------+

AUTHORS
-------

Eleanor Dodson, University of York, England

Prepared for CCP4 by Maria Turkenburg, University of York, England

Some additional material from Martyn Winn, Daresbury Lab.

REFERENCES
----------

[Bernhardinelli(1985)]

G.Bernardinelli and H. D. Flack (1985). Least-squares absolute-structure refinement. Practical experience and ancillary calculations. Acta Cryst. A41, 500-511.

.. |HKLVIEW Barnase pH6| image:: images/bar_wt_ph6.gif
   :height: 300px
.. |HKLVIEW Barnase pH6 indexed -h-kl| image:: images/bar_wt_ph6_-h-kl.gif
   :height: 300px
.. |HKLVIEW Barnase pH6 indexed -k-hl| image:: images/bar_wt_ph6_-k-hl.gif
   :height: 300px
.. |HKLVIEW Barnase pH6 indexed kh-l| image:: images/bar_wt_ph6_kh-l.gif
   :height: 300px
.. |HKLVIEW Hipip h2l| image:: images/hipip-h2l.gif
   :height: 300px
.. |HKLVIEW Hipip reindexed| image:: images/hipip-h2l-reind.gif
   :height: 300px
