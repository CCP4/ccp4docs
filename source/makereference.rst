#!/bin/sh

set -e

CVS Id :math:`Id`
=================

Martyn Winn Jul. 1997
=====================

.. _section-1:

make an html list of references
===============================

.. _section-2:

**** Not intended for public release ****
=============================================

.. _section-3:

referencefile=REFERENCES.html

cat <<+ >$referencefile # header <!doctype html system “html.dtd”>

.. raw:: html

   <html>

.. raw:: html

   <head>

.. raw:: html

   <title>

CCP4 Program References

.. raw:: html

   </title>

.. raw:: html

   </head>

.. raw:: html

   <body  BGCOLOR="#FFFFFF: 
 


.. raw:: html

   <H1>

CCP4 v6.5.0 Program References

.. raw:: html

   </H1>

.. raw:: html

   <P>

Any publication arising from use of the CCP4 software suite should
include both references to the specific programs used (see below) and
the following reference to the CCP4 suite:

.. raw:: html

   </P>

.. raw:: html

   <P>

M. D. Winn et al. Acta. Cryst. D67, 235-242 (2011) “Overview of the CCP4
suite and current developments” [ doi:10.1107/S0907444910045749 ]

.. raw:: html

   </P>

.. raw:: html

   <P>

Such citations are vital to the CCP4 project, and individual program
authors, in maintaining funding and thus being able to develop CCP4
further.

.. raw:: html

   </P>

.. raw:: html

   <P>

The following list is meant as an aid to citation of individual
programs, but is not complete. It has been extracted from the individual
program documentation, so look there for more details. In addition, many
relevant articles can be found in the proceedings of the 2010 Study
Weekend, published as a special issue of Acta Cryst D.

.. raw:: html

   </P>

.. raw:: html

   <HR>

-  

Loop over all html files
========================

for name in refmac5/references.html *.html ; do

Do not look at files without “startreferencelist” string.
=========================================================

if test ``grep startreferencelist $name | wc -l`` -lt 1 ; then continue
elif test $name = “REFERENCE.html” ; then continue elif test $name =
“REFERENCE_tmp.html” ; then continue fi

echo “Examining $name …”

Write program name
==================

awk ’
/INDEX_INFO/ { numfields = split($0,array,/::/) for (count = 1; count <=
numfields; count++) { if (array[count]==“INDEX_INFO”) title =
tolower(array[count+1]) } print “

.. raw:: html

   <h2>

” title ”

.. raw:: html

   </h2>

” exit } ’ :math:`name >>`referencefile

# Write references between

.. raw:: html

   <ul>

.. raw:: html

   </ul>

Takes everything between 
=========================

and 
====

cat <<+ >>$referencefile

.. raw:: html

   <ul>

-  

| awk ’
| /startreferencelist/ { getline while (index($0,“endreferencelist”) ==
  0) { print $0
| getline } } ’ :math:`name >>`referencefile

cat <<+ >>$referencefile

.. raw:: html

   </ul>

-  

done

cat <<+ >>$referencefile

.. raw:: html

   </body>

.. raw:: html

   </html>

-  

echo ” ” echo “Writing to REFERENCES.html”

.. _section-4:
