XDL_VIEW (CCP4: Library)
========================

NAME
----

**xdl_view** -XDL_VIEW version 4.3 X-windows based tootkit

SYNOPSIS
--------

**xdl_view**

DESCRIPTION
-----------

XDL_VIEW is a set of 'view-object' routines, developed originally for
use by the SERC Daresbury Laboratory Protein Crystallography project
team for the development of the Laue software and for potential use with
a much wider range of applications. These routines are designed to be
used by applications to provide a user interface within an X-windows
environment.

XDL_VIEW is used by the CCP4 programs X-windows programs
(`HKLVIEW <hklview.html>`__, `IPDISP <ipdisp.html>`__,
`XDLDATAMAN <xdldataman.html>`__, `XDLMAPMAN <xdlmapman.html>`__,
`XLOGGRAPH <xloggraph.html>`__ and
`XPLOT84DRIVER <xplot84driver.html>`__) as well as
`MOSFLM <mosflm.html>`__ and `ROTGEN <rotgen.html>`__.

Full documentation for the XDL_VIEW routines can be found in `the
xdl_view 4.0 manual <../x-windows/xdl_view/doc/xdl_view_top.html>`__

AUTHORS
-------

::

   John W. Campbell and Dave Love < d.love@dl.ac.uk >
   CCLRC Daresbury Laboratory
