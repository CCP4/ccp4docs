OVERLAPMAP (CCP4: Supported Program)
====================================

NAME
----

**overlapmap** - calculates the average of two maps

SYNOPSIS
--------

| **overlapmap mapin1** *foo.map* **mapin2** *bar.map* [ **mapin3**
  *baz.map* ] **mapout** *quux.map*
| [`Keyworded input <#keywords>`__]

.. _description: 
 
DESCRIPTION
---------------------------------

**overlapmap has basically three modes of operation, combining two
maps** to produce an output map, correlating two maps or generating a
real-space R-factor between two maps. Here are the modes in more detail:

#. Average MAPIN1 and MAPIN2 with output to MAPOUT (`MAP
   AVERAGE <#mapaverage>`__).

#. Sum the density from MAPIN1 and MAPIN2, output to MAPOUT (`MAP
   ADD <#mapadd>`__).

#. Set density to zero in MAPOUT if present in MAPIN1 ( *i.e.* non zero
   MAPIN1 density). Otherwise grid points in MAPOUT are set to MAPIN2
   (`MAP EXCLUDE <#mapexclude>`__). This requires MAPIN1 has some grid
   points which are EXACTLY zero; you are unlikely to have this unless
   you have generated a map from coordinates.

#. Set density in MAPOUT to MAPIN2 if the grid point in MAPIN1 > 0.
   Otherwise set to zero (`MAP INCLUDE <#mapinclude>`__).

#. Correlate the density between MAPIN1 and MAPIN2 over a given volume,
   which may be either the whole map, the volume occupied by a given
   residue or atom, or section by section (there is no MAPOUT in this
   mode). There are three correlation modes (`CORRELATE
   ... <#correlate>`__), two of which require a third map array (see
   `MAPIN3 <#notesmapin3>`__). This is used to flag which grid points
   are associated with each residue or atom and are therefore to be
   included in their correlation coefficient calculation.

   Remember the correlation transcends any need to scale the 2 maps.
   Below is the definition of the correlation coefficient:

   ::


      corr. coeff.  =    (<x y> - <x><y>) / 
                    ( sqrt(<x^2> - <x>^2)  *  sqrt(<y^2> - <y>^2) )

   | where x represents the density values from one map and y the values
     from the other.
   | < > represents the mean value of the quantities inside the bracket.

#. Calculates a Real Space R factor à la Branden/Jones (`reference
   [1] <#reference1>`__).

   ::


      Real space R factor =
                Sum(RHO_obs - RHO_calc)/Sum(RHO_obs + RHO_calc)

   summed over all points within a residue. This requires that RHO_obs
   and RHO_calc are ON THE SAME SCALE. It is also sensitive to the atom
   radius you chose in `SFALL <sfall.html>`__. Eleanor Dodson believes
   it is not as useful as the correlation coefficient. MAPIN3 will be
   needed for this mode of operation calculated from SFALL in the RESMOD
   mode - see `below <#notesmapin3>`__.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

The available keywords are:

   `CHAIN <#chain>`__, `CORRELATE <#correlate>`__, `MAP <#map>`__,
   `PATTERSON <#patterson>`__, `REAL SPACE R <#realspacer>`__

.. _map: 
 
MAP [ ADD | AVERAGE | EXCLUDE | INCLUDE ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _mapadd: 
 
ADD <scale1> <scale2>
   add <scale1>*map1 + <scale2>*map2 and output new map. (Default
   <scale1> = <scale2> =1.)

.. _mapaverage: 
 
AVERAGE
   average two maps and output new map.

.. _mapexclude: 
 
EXCLUDE
   exclude points from output map if they exist in MAP1. Any grid point
   that is is not zero in MAP1 will be set to zero in MAPOUT. Grid
   points in MAPOUT will be set to the value in MAP2 if the grid point
   in MAP1 is zero.

.. _mapinclude: 
 
INCLUDE
   include points in output map from MAP2 if they exist in MAP1. The
   value of the grid point in MAPOUT will be that of MAP2 if the grid
   point in MAP1 > 0. Otherwise the point in MAPOUT will be set to zero.

.. _correlate: 
 
CORRELATE [ SECTION | RESIDUE | ATOM ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _correlatesection: 
 
SECTION
   correlation is section by section.

.. _correlateresidue: 
 
RESIDUE
   correlation is residue by residue.

.. _correlateatom: 
 
ATOM
   correlation is atom by atom.

.. _realspacer: 
 
REAL SPACE R
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

tabulated residue by residue.

.. _chain: 
 
CHAIN <chain_ID> <1st_residue_number> <last_residue_number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Repeat residue numbering by chain, as in `SFALL <sfall.html>`__.

.. _patterson: 
 
PATTERSON <sigmin> sigmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate a correlation coefficient as in GROPAT
(`[2] <#reference2>`__). <sigmin> and <sigmax> are the minimum and
maximum grid value/sigma ratio, typically 3. and 10. It is recommended
that origin removed difference Pattersons should be calculated.

.. _notes: 
 
NOTES
---------------------

.. _notesmapin3: 
 
MAPIN3 may be assigned to the modified map output
of SFALL. This map flags each grid point according to certain criteria;
the most useful is the residue flag created by MODE ATMMAP RESMOD. This
flags each point in the map space to indicate which atom of a residue it
is near (using the Atom Radii specified in SFALL), and whether the atom
involved is main chain, or side chain. This is required if you want to
use CORRELATE RESIDUE or REALSPACE Rfactor.

.. _notesmapcalculation: 
 
The program must have both maps
calculated in exactly the same way. Since any Fc map generated from
coordinates has a fixed asymmetric unit and AXIS ORDER do them first and
make sure your FFT calculation matches that.

.. _notesexclude: 
 
EXCLUDE is useful for flattening the protein
density when you want to inspect the residual density only. Used in
solvent searches and also in the `PHASED
TRANSLATION <fft.html#phtranslation>`__ function (part of the FFT
package). Generate the protein density in SFALL outputting an Fc map.

.. _notescorrelate: 
 
CORRELATE can be done section by section or by
RESIDUE or ATOM. CORRELATE RESIDUE is only possible if MAPIN3 is
generated in SFALL outputting an Fc map with mode = `mode atmmap
resmod <sfall.html#modeatmmapresmod>`__.

.. _notescorrelateatom: 
 
CORRELATE ATOM is only possible if MAPIN3
is generated in SFALL outputting an Fc map with mode = `mode atmmap
atmmod <sfall.html#modeatmmapatmmod>`__.

SEE ALSO
--------

| `fft <fft.html>`__ (CCP4),
| `sfall <sfall.html>`__ (CCP4),
| `maprot <maprot.html>`__ (CCP4)

.. _references: 
 
REFERENCES
-------------------------------

.. _reference1: 
 
 Branden C. and Jones A., Nature 343 687-689
   (1990)
.. _reference2: 
 
 Jones Y. and Stuart D, Proc. of CCP4 Study
   Weekend on Isomorphous Replacement And Anomalous Scattering, 1991
   39-48.

.. _examples: 
 
EXAMPLES
---------------------------

Correlation by residue
~~~~~~~~~~~~~~~~~~~~~~

`overlapmap.exam <../examples/unix/runnable/overlapmap.exam>`__

.. _averaging_2_maps: 
 
Averaging 2 maps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   overlapmap  MAPIN1 chmi_phase_extend.ext  MAPIN2 wang_cycle3.ext 
     MAPOUT chmi_wang_av.map << END 
   MAP AVERAGE
   END

See also $CEXAM/unix/runnable/mapcorrelation_procedures and
$CEXAM/unix/non-runnable/mapcorrelation.exam.
