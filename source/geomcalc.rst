GEOMCALC (CCP4: Supported Program)
==================================

NAME
----

**geomcalc** - molecular geometry calculations.

SYNOPSIS
--------

| **geomcalc XYZIN** *foo_in.pdb* **XYZOUT** *foo_out.pdb*
| [`Keyworded input <#keywords>`__]

.. _description: 
 
DESCRIPTION
---------------------------------

Read and store molecule, then do various geometry calculations,
according to commands.

   - fit subset of atoms to plane
   - calculate deviation of atoms from plane
   - calculate atom - atom distances
   - calculate bond angles
   - calculate torsion angles

.. _keywords: 
 
KEYWORDED INPUT
-----------------------------------

Available keywords are:

   `ANGLE <#angle>`__, `ATOM <#atom>`__, `CENTRE <#centre>`__,
   `CURRENT <#current>`__, `DISTANCE <#distance>`__, `FOLD <#fold>`__,
   `FROMPLANE <#fromplane>`__, `PLANE <#plane>`__, `READ <#read>`__,
   `SEPARATION, <#Separation>`__ `TILT <#tilt>`__,
   `TORSION <#torsion>`__, `TRANSFORM <#transform>`__,
   `WRITE <#write>`__, `XAXIS <#xaxis>`__

The syntax of an atom-name is <chain-name>|<residue_number>:<atomname>,
*i.e.* with the 3 components separated by the delimiters | and :. If
either <chain-name> or <residue_number> are omitted, they default to the
previously used value (initially blank and 1 respectively). Note that
atom-specifiers are case-sensitive.

.. _read: 
 
**READ** [<input file name>]

Read molecule from file & store. If the filename is not given, the
logical name XYZIN is used. Any previously stored molecule will be
overwritten. On entry to the program, it will always try to read XYZIN.

.. _plane: 
 
**PLANE** [<plane_number>] [+] <atom-names> | ALL

List atom names to define plane. If the first token is +, these atoms
will be added to a previously defined plane. Otherwise, a PLANE command
will replace a previously defined plane. The direction of the plane
normal is defined as approximately parallel to the vector (xyz2-xyz1) x
(xyz3-xyz1), where xyz1, xyz2, xyz3 are the coordinates of atoms 1,2,3
If plane_number is given, then this command defines this plane and makes
it the current plane. If it is not set, the plane-number defaults to 1.

If the keyword ALL is given instead of an atom list, all atoms will be
used. This may be useful for finding the principal directions of a whole
molecule.

.. _fromplane: 
 
**FROMPLANE** <atom-names> | POINT <x> <y> <z>

  Calculate perpendicular distance of listed atoms (or point) from
  previously defined current plane ( *i.e.* the last one defined, or the
  one set with the CURRENT command).

.. _Separation: 
 
**SEPARATION ** <plane_number1> <plane_number2>

| Calculate distance between the centres of two previously defined
  planes. This is usually only sensible if the two planes are
  essentially the same.

.. _atom: 
 
**ATOM** <atom-name>

Print coordinates of specified atom

.. _distance: 
 
**DISTANCE** <atom-name1> <atom-name2>

Calculate distance between named atoms

.. _angle: 
 
**ANGLE** <atom-name1> <atom-name2> <atom-name3>

Calculate angle between named atoms, i.e. between the vectors 2 -> 1 and
2 -> 3.

.. _torsion: 
 
**TORSION** <atom-name1> <atom-name2> <atom-name3> <atom-name4>

Calculate torsion angle between named atoms

.. _transform: 
 
**TRANSFORM**

Transform all stored atoms into frame of previously defined plane,
*i.e.* put origin at centre of plane, with the major axis along x and
the plane normal along z. Note that all currently defined planes will
also be transformed into the new frame.

.. _xaxis: 
 
**XAXIS** <atom-name1> <atom-name2>

Redefine transformation for the current plane to put the plane normal
along z and the vector vx = <atom-name1> -> <atom-name2> in the xz plane
( *i.e.* the projection of vx on the plane is put along x). This will
then be used for any subsequent TRANSFORM commands. It allows for a
consistent frame definition for a series of related but different
planes.

.. _centre: 
 
**CENTRE** <atom-name> | <x> <y> <z>

Redefine the centre of the current plane, either to the position of an
atom, or to the given coordinates. This will shift the origin of
subsequent TRANSFORM operations, but will mangle future FROMPLANE
commands (unless the new centre lies in the plane).

.. _write: 
 
**WRITE** [<output file name>]

Write out stored coordinates to file (normally after a TRANSFORM
command) If the filename is omitted, the logical name XYZOUT is used.

.. _current: 

**CURRENT** <plane_number>

Define current plane number: this plane must have been previously
defined with a PLANE command.

.. _fold: 
 
**FOLD** <plane_number_1> <plane_number_2>

Calculate angle between two previously defined planes.

.. _tilt: 
 
**TILT** <atom-name1> <atom-name2> | <dx> <dy> <dz>

Calculate angle between vector and the normal to the current plane. The
vector may be defined either as <dx> <dy> <dz>, or as <atom-name1> ->
<atom-name2>

AUTHOR
------

| Phil Evans, MRC LMB, September 1995
| Plane fit algorithm stolen from Judd Fermi
