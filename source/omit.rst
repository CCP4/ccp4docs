OMIT (CCP4: Supported Program)
==============================

NAME
----

**omit** - program to calculate omit-maps according to Bhat procedure

SYNOPSIS
--------

| **omit HKLIN** *foo_in.mtz* **MAPOUT** *foo_out.map*
| [Key-worded input]

.. _description: 
 
DESCRIPTION
-----------

The present program has combined the original conglomerate of separate
programs, all needed to calculate an omit-map, into one single automatic
procedure. The user is strongly advised to read the paper "CALCULATION
OF AN OMIT MAP" by T.N. BHAT, *J. Appl. Cryst.* (1988) **21**, 279-281.

**Note:** Memory requirements are governed by a parameter MAXREF set in
the program, and this may need to be increased for large problems. If
this is the case, the program will hopefully exit gracefully and let you
know. Change it as necessary (top of the program source code) and
recompile. Increasing MAXREF increases the record lengths for some
scratch files, and this can cause problems on some systems. These
scratch files are also large in total, which may also cause problems.

.. _files: 
 
INPUT AND OUTPUT FILES
----------------------

Input


HKLIN
   Input data in MTZ format. This file should contain observed and/or
   calculated structure factor amplitudes, and phases from some source.

Output
------

MAPOUT
   Output binary omit-map. By default, this is in CCP4 format, but if
   the keyword `MFFOUTPUT <#mffoutput>`__ is specified then it will be
   output in MFF format (Groningen Master Fourier File format). The
   fast, medium and slow axes are set to be Z, X and Y respectively. An
   output CCP4 map can be manipulated by the programs
   `MAPMASK <mapmask.html>`__ or `MAPROT <maprot.html>`__ if required.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

.. _labin: 
 
**LABIN** <program label>=<file label>...


(Compulsory.)

Program labels are:

FP
   Observed structure factor amplitudes, Fobs.
FC
   Calculated structure factor amplitudes, Fcalc.
PHI
   Phases, Phi.

.. _scale: 
 
**SCALE** <scale_fobs> <scale_fcalc>


(Compulsory.)

<scale_fobs> and <scale_fcalc> are scale factors for Fobs and Fcalc
respectively. For example, <scale_fobs> = 2.0 and <scale_fcalc> = -1.0
allows one to calculate a 2FO-FC omit-map.

.. _resolution: 
 
**RESOLUTION** <resmin> <resmax>


Resolution limits, either 4(sin theta/lambda)**2 or d in Angstrom
(either order). If this card isn't given, resolution range will be read
from HKLIN.

.. _truncate: 
 
**TRUNCATE**


Include this card to truncate initial map, so that the density values
fall between -1.0*RMSMAP and 20.0*RMSMAP (ITRUNC=1). Default is ITRUNC=
0 (recommended).

.. _histogram: 
 
**HISTOGRAM**


Include this card to apply HISTOGRAM MAPPING to the omit map, so that
the histogram of densities of the omit map equals that of the starting
map (IHIST =1). Default is IHIST = 0 (recommended).

.. _dlimit: 
 
**DLIMIT** <dstlmt>


Points in the electron density map within <dstlmt> of a symmetry related
grid point are set to -RMS density value of the map. Set <dstlmt> = 0.0
for no modification of input map; 3.0-3.5 Angstrom seems a good value
otherwise. [Default 3.0]

.. _grid: 
 
**GRID** <nx> <ny> <nz>


Number of sampling divisions along each unit cell edge for the output
map. The number of points along each axis should be even, and a product
of the factors 2, 3, 4 or 5 (each of which may appear any number of
times). There is also a maximum value for the number of points set in
the program. If any of <nx> <ny> and <nz> do not satisfy these
conditions, then they will be reset appropriately. If this keyword is
omitted, then the program will choose appropriate values automatically.

.. _mffoutput: 
 
**MFFOUTPUT**


Output omit map in MFF format (Groningen Master Fourier File format).
The default is to output the omit map in CCP4 format.

.. _title: 
 
**TITLE** <title>


<title> will be written to MAPOUT.

.. _end: 
 
**END**


End input and run program.

.. _function: 
 
PROGRAM FUNCTION
-----------------------------------

Internal settings of the program:

-  The neutral volume (see Bhat's article!) has been fixed to a volume
   of two (2!) gridpoints around the phased volume.
-  In contrast to what Bhat mentions (step J of his procedure), in the
   current version of the program, structure factors are obtained by
   Fourier transformation of a map in which the density values in the
   phased and neutral volumes have been set to 0.0.
-  Before starting the OMIT procedure, the electron density map may be
   truncated such that the density values are between -1.0*RMSMAP and
   20.0*RMSMAP.

.. _examples: 
 
EXAMPLES
---------------------------

Unix example script found in $CEXAM/unix/runnable/


-  `omit.exam <../examples/unix/runnable/omit.exam>`__

REFERENCES
----------

#. T.N. Bhat, "CALCULATION OF AN OMIT MAP", *J. Appl. Cryst.*, **21**,
   279-281 (1988)
#. F.M.D.Vellieux and B.W.Dijkstra, "Computation of Bhat's OMIT maps
   with different coefficients", *J. Appl. Cryst.*, **30**, 396-399
   (1997)

AUTHOR
------

Program written by Bauke Dijkstra (April 1994).

`bauke@chem.rug.nl <mailto:b.w.dijkstra@rug.nl>`__

Modified by Fred. Vellieux to include histogram mapping and the option
of skipping the map truncation step (Feb. 1996).

`vellieux@lccp.ibs.fr <mailto:vellieux@ibs.fr>`__

CCP4 modifications made by Martyn Winn (July 1997).

m.d.winn@dl.ac.uk
