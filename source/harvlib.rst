HARVLIB (CCP4: Library)
=======================

NAME
----

Harvlib - Subroutine library for writing CIF harvest files.

OVERVIEW
--------

The following subroutines are used by certain CCP4 programs when writing
out harvest files. These subroutines call functions in the CCIF library.

DESCRIPTION OF ROUTINES
-----------------------

SUBROUTINE Hatom_type_scat(AtName,a1,a2,a3,a4,b1,b2,b3,b4,c)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

AtName*( * )

Real

a1,a2,a3,a4,b1,b2,b3,b4,c

**Writes the following CIF items:**

_atom_type.symbol

Element symbol of atom specie(s) representing this atom type.

_atom_type.scat_Cromer_Mann_a1

The a1 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_a2

The a2 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_a3

The a3 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_a4

The a4 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_b1

The b1 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_b2

The b2 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_b3

The b3 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_b4

The b4 Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

_atom_type.scat_Cromer_Mann_c

The c Cromer-Mann scattering-factor coefficient used to calculate the
scattering factors for this atom type.

| 

SUBROUTINE HCell(Cell)
~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Cell(6)

**Writes the following CIF items:**

_cell.length_a

Unit-cell length a corresponding to the structure reported.

_cell.length_b

Unit cell length b corresponding to the structure reported.

_cell.length_c

Unit cell length c corresponding to the structure reported.

_cell.length_alpha

Unit cell angle alpha corresponding to the structure reported.

_cell.length_beta

Unit cell angle beta corresponding to the structure reported.

_cell.length_gamma

Unit cell angle gamma corresponding to the structure reported.

| 

SUBROUTINE Hcell_percent_solvent(Fraction)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Fraction

**Writes the following CIF items:**

_exptl_crystal.percent_solvent

Writes the fraction of cell occupied by solvent calculated from crystal
cell and contents, expressed as percent solvent.

| 

SUBROUTINE Hdata_reduction_method(Method,Nlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nlines

Character

Method(Nlines)*80

**Writes the following CIF items:**

_reflns.data_reduction_method

The method used in reducing the data. Note that this is not the computer
program used, which is described in the SOFTWARE category, but the
method itself.

| 

SUBROUTINE Hdensity_Matthews(Fraction)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Fraction

**Writes the following CIF items:**

_exptl_crystal.density_Matthews

The density of the crystal, expressed as the ratio of the volume of the
asymmetric unit to the molecular mass of a monomer of the structure, in
units of angstroms^3^ per dalton. Ref: Matthews, B. W. (1960). J. Mol.
Biol., 33, 491-497

| 

SUBROUTINE Hinitialise(Pkage,ProgName,ProgVersion,ProjectName,DataSetName,UseCWD,Private, IVALND,VALND,RowLimit)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

VALND

Integer

IVALND,RowLimit

Logical

Private,UseCWD

Character

DataSetName*( * ),ProjectName*( * ),ProgVersion*( * ),ProgName*( * ),Pkage*( * )

**Does the following:**

Checks if $HARVESTHOME/DepositFiles directory exists and permissions;
creates filename for deposit information; sets Project Name and Dataset
Name;

| 

SUBROUTINE Hmerge_reject_criterion(Rcriteria,Nlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nlines

Character

Rcriteria(Nlines)*80

**Writes the following CIF items:**

_reflns.merge_reject_criterion

Criteria used in averaging equivalent Intensities during the data
reduction stage to generate the set of reduced unique data. The
description of the rejection criteria for outliers that was used may
include the criteria for either the scaling and for the merging of a set
of observations.

| 

SUBROUTINE Hoverall_observations(Ntotal,R1,R2)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Ntotal

Real

R1,R2

**Writes the following CIF items:**

_reflns.overall_d_resn_high

The highest resolution for the interplanar spacing for the number of
reflections processed from the complete set of images collected. This is
the smallest d value. Units (Angstroms or nanometres) should be
specified.

_reflns.overall_d_res_low

The lowest resolution for the interplanar spacing for the number of
reflections processed from the complete set of images collected. This is
the largest d value. Units (Angstroms or nanometres) should be
specified.

_reflns.overall_number_observations

The total number of observations of reflections processed from the
complete set of images collected within the resolution limits
_reflns.overall_d_res_low and _reflns.overall_d_res_high. This is a
count of the number of experimental measurements of Bragg intensities
made in the experiment, not the number of merged (unique) reflections.
The value of _reflns.observed_criterion DOES NOT apply to this set of
reflections.

| 

SUBROUTINE Hparse(line,Ibeg,Iend,Ityp,Fvalue,Cvalue,Idec,N)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

line

Integer

Ibeg( * ),Idec( * ),Iend( * ),Ityp( * )

Character

Cvalue( * )*4

**Does the following:**

Free format read routine. This is really a scanner, not a parser. It
scans the line into N tokens which are separated by delimiters and
updates the information arrays for each, as below. The default
delimiters are space, tab, comma and equals; they may be changed using
PARSDL. Adjacent commas delimit `null' fields (the same as empty
strings). strings may be unquoted or single- or double-quoted if they
don't contain delimiters, but must be surrounded by delimiters to be
recognised. This allows literal quotes to be read, e.g. "ab"c" will be
recognised as the token `ab"c'. An unquoted `!' or `#' in line
introduces a trailing comment, which is ignored.

| 

SUBROUTINE Hphasing_mir_der_p2(Power,RCullis,Reflns)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Resomin(2,* ),Resoman(2,* ),Criteria( * )

Integer

NumSitesDer( * )

Character

DerID( * )*80

**Writes the following CIF items:**

_phasing_MIR_der.id

The value of _phasing_MIR_der.id must uniquely identify a record in the
PHASING_MIR_DER list. Note that this item need not be a number; it can
be any unique identifier.

_phasing_MIR_der.number_of_sites

The number of heavy atom sites in this derivative.

_phasing_MIR_der.d_res_high

The highest resolution for the interplanar spacing in the reflection
data used for this derivative. This is the smallest d value.

_phasing_MIR_der.d_res_low

The lowest resolution for the interplanar spacing in the reflection data
used for this derivative. This is the highest d value.

_phasing_MIR_der.ceflns_criteria

Criteria used to limit the reflections used in the phasing calculations.

_phasing_MIR_der.Power_centric

The mean phasing power P for centric reflections in this derivative.

_phasing_MIR_der.Power_acentric

The mean phasing power P for acentric reflections in this derivative.

_phasing_MIR_der.R_cullis_centric

Residual factor R~cullis~ for centric reflections in this derivative.

_phasing_MIR_der.R_cullis_acentric

Residual factor R~cullis,acen~ for acentric reflections in this
derivative.

_phasing_MIR_der.R_cullis_anomalous

Residual factor R~cullis,ano~ for anomalous reflections in this
derivative.

_phasing_MIR_der.Reflns_acentric

The number of acentric reflections used in phasing for this derivative.

_phasing_MIR_der.Reflns_anomalous

The number of anomalous reflections used in phasing for this derivative.

_phasing_MIR_der.Reflns_centric

The number of centric reflections used in phasing for this derivative.

| 

SUBROUTINE Hphasing_mir_der_site(DerID,NumDerSites,B,Atype,X,Y,Z,Occ,OccEsd,anom,AnomEsd)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

anom( * ),AnomEsd( * ),B( * ),Occ( * ),OccEsd( * ),X( * ),Y( * ),Z( * )

Character

Atype( * )*4

**Writes the following CIF items:**

_phasing_MIR_der_site.der_id

This data item is a pointer to _phasing_MIR_der.id in the
PHASING_MIR_DER category.

_phasing_MIR_der_site.atom_type_symbol

This data item is a pointer to _atom_type.symbol in the ATOM_TYPE
category. The scattering factors referenced via this data item should be
those used in the refinement of the heavy atom data; in some cases this
is the scattering factor to the single heavy atom, in others these are
the scattering factors for an atomic cluster.

_phasing_MIR_der_site.fract_x

The x coordinate of this heavy-atom position in this derivative
specified as a fraction of _cell.length_a.

_phasing_MIR_der_site.fract_y

The y coordinate of this heavy-atom position in this derivative
specified as a fraction of _cell.length_b.

_phasing_MIR_der_site.fract_z

The z coordinate of this heavy-atom position in this derivative
specified as a fraction of _cell.length_c.

_phasing_MIR_der_site.B_iso

Isotropic temperature factor for this heavy-atom site in this
derivative.

_phasing_MIR_der_site.Occupancy_iso

The relative real isotropic occupancy of the atom type present at this
heavy-atom site in a given derivative. This atom occupancy will probably
be on an arbitrary scale.

_phasing_MIR_der_site.Occupancy_iso_su

The standard uncertainty (e.s.d.) of
_phasing_MIR_der_site.occupancy_iso.

_phasing_MIR_der_site.Occupancy_anom

The relative anomalous occupancy of the atom type present at this
heavy-atom site in a given derivative. This atom occupancy will probably
be on an arbitrary scale.

_phasing_MIR_der_site.Occupancy_anom_su

The standard uncertainty (e.s.d.) of
_phasing_MIR_der_site.occupancy_anom.

| 

SUBROUTINE Hphasing_mir_native(R1,R2,SigmaNat,fomT,fomC,fomA,Mt,Mc,Ma)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

R1,R2,fomT,fomC,fomA,SigmaNat

Integer

Mt,Mc,Ma

**Writes the following CIF items:**

_phasing_MIR.entry_id

This data item is a pointer to _entry.id in the ENTRY category.

_phasing_MIR.d_res_high

The highest resolution in angstroms for the interplanar spacing in the
reflection data used for the native data set. This is the smallest d
value.

_phasing_MIR.d_res_low

The lowest resolution in angstroms for the interplanar spacing in the
reflection data used for the native data set. This is the largest d
value.

_phasing_MIR.FOM

The mean value of the figure of merit m for all reflections phased in
the native data set.

_phasing_MIR.FOM_centric

The mean value of the figure of merit m for the centric reflections
phased in the native data set.

_phasing_MIR.FOM_acentric

The mean value of the figure of merit m for the acentric reflections
phased in the native data set.

_phasing_MIR.reflns

The total number of reflections phased in the native data set.

_phasing_MIR.reflns_centric

The number of centric reflections phased in the native data set.

_phasing_MIR.reflns_acentric

The number of acentric reflections phased in the native data set.

_phasing_MIR.reflns_criterion

Criterion used to limit the reflections used in the phasing
calculations.

| 

SUBROUTINE Hphasing_mir_native_shell(R1,R2,KRa,fomA,KRc,fomC,KRo,fomO)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

R1,R2,KRa,fomA,KRc,fomC,KRo,fomO

Integer

KRa,KRc,KRo

**Writes the following CIF items:**

_phasing_MIR_shell.d_res_high

The highest resolution for the interplanar spacing in the reflection
data in this shell. This is the smallest d value. Note that the
resolution limits of shells in the items _phasing_MIT_Shell.d_res_high
and _phasing_MIR_shell.d_res_low are independent of the resolution
limits of shells in the items _reflns_shell.d_res_high and
_reflns_shell.d_res_low.

_phasing_MIR_shell.d_res_low

The lowest resolution for the interplanar spacing in the reflection data
in this shell. This is the largest d value. Note that the resolution
limits of shells in the items _phasing_MIT_Shell.d_res_high and
_phasing_MIR_shell.d_res_low are independent of the resolution limits
of shells in the items _reflns_shell.d_res_high and
_reflns_shell.d_res_low.

_phasing_MIR_shell.FOM

The mean value of the figure of merit m for reflections in this shell.

_phasing_MIR_shell.FOM_centric

The mean value of the figure of merit m for centric reflections in this
shell.

_phasing_MIR_shell.FOM_acentric

The mean value of the figure of merit m for acentric reflections in this
shell.

_phasing_MIR_shell.reflns

The number of reflections in this shell.

_phasing_MIR_shell.reflns_centric

The number of centric reflections in this shell.

_phasing_MIR_shell.reflns_acentric

The number of acentric reflections in this shell.

| 

SUBROUTINE Hrefine_corr_esu(Corr,FreeCorr,DPI,FreeESU,Good,GoodFree,ESUml,bESU)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Corr,FreeCorr,DPI,FreeESU,Good,GoodFree,ESUml,bESU

Integer

Lenstr

| 

**Writes the following CIF items:**

_refine.correlation_coeff_Fo_to_Fc

The correlation coefficient between the observed and calculated
structure factors for reflections included in the refinement.

_refine.correlation_coeff_Fo_to_Fc_Free

The correlation coefficient between the observed and calculated
structure factors for reflections not included in the refinement (free
reflections).

_refine.goodness_of_fit_work

NO DESCRIPTION

_refine.goodness_of_fit_FreeR

NO DESCRIPTION

_refine.overall_SU_ML

The overall standard uncertainty (e.s.d.) of the positional parameters
based on a maximum likelihood residual.

_refine.overall_SU_B

The overall standard uncertainty (e.s.d.) of the thermal parameters
based on a maximum likelihood residual.

_refine.overall_SU_R_Cruickshank_DPI

The overall standard uncertainty (e.s.d.) of the thermal parameters
based on the crystallographic R value, expressed in a formalism known as
the dispersion precision indicator (DPI).

_refine.overall_SU_R_free

The overall standard uncertainty (e.s.d.) of the thermal parameters
based on the free R value.

| 

SUBROUTINE Hrefine_details(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

NHlines

Character

Hlines(NHlines)*80

**Writes the following CIF items:**

_refine.ls_weighting_details

A description of special aspects of the weighting scheme used in
least-squares refinement. Used to describe the weighting when the value
of _refine.ls_weighting_scheme is specified as 'calc'.

_refine.ls_weighting_scheme

The weighting scheme applied in the least-squares process. The standard
code may be followed by a description of the weight (but see
_refine_ls_weighting_details for a preferred approach).

_refine.details

Description of special aspects of the refinement process.

| 

SUBROUTINE Hrefine_fnmin(Nval,Nterms,Vterms,Cterms,Wterms)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Vterms( * ),Wterms( * )

Integer

Nterms( * )

Character

Cterms(Nval)*80

**Writes the following CIF items:**

_refine_funct_minimized.type

The type of the function being minimized.

_refine_funct_minimized.number_terms

The number of observations in this term. For example, if the term is a
residual of the X-ray intensities this item would contain the number of
reflections used in the refinement.

_refine_funct_minimized.residual

The residual for this term of the function which was minimized in
refinement.

_refine_funct_minimized.weight

The weight applied to this term of the function which was minimized in
the refinement.

| 

SUBROUTINE Hrefine_fom(fom,Freefom)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

fom,Freefom

**Writes the following CIF items:**

_refine.overall_FOM_work_R_set

Average figure of merit of phases of reflections not included in the
refinement. This value is derived from the likelihood function.

_refine.overall_FOM_free_R_set

Average figure of merit of phases of reflections included in the
refinement. This value is derived from the likelihood function.

| 

SUBROUTINE Hrefine_ls_matrix_type(STRING)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

String*( * )

**Writes the following CIF items:**

_refine.ls_matrix_type

Type of matrix used to accumulate the least-squares derivatives.

| 

SUBROUTINE Hrefine_ls_overall_reso(R1,R2)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

R1,R2

**Writes the following CIF items:**

_refine.ls_d_res_high

The highest resolution in angstroms for the interplanar spacing in the
reflection data used in refinement. This is the smallest d value.

_refine.ls_d_res_low

The lowest resolution in angstroms for the interplanar spacing in the
reflection data used in refinement. This is the largest d value.

| 

SUBROUTINE Hrefine_NparNrestNconstr(NPARMR,NRESTR,NCONSTR)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

NPARMR,NRESTR,NCONSTR

**Writes the following CIF items:**

_refine.ls_number_parameters

The number of parameters refined in the least-squares process. If
possible this number should include some contribution from the
restrained parameters. The restrained parameters are distinct from the
constrained parameters (where one or more parameters are linearly
dependent on the refined value of another). Least-squares restraints
often depend on geometry or energy considerations and this makes their
direct contribution to this number, and to the goodness-of-fit
calculation, difficult to assess.

_refine.ls_number_restraints

The number of restrained parameters. These are parameters which are not
directly dependent on another refined parameter. Often restrained
parameters involve geometry or energy dependencies. See also
_atom_site.constraints and _atom_site_refinement_flags. A general
description of refinement constraints may appear in _refine.details.

_refine.ls_number_constraints

The number of constrained (non-refined or dependent) parameters in the
least-squares process. These may be due to symmetry or any other
constraint process (e.g. rigid-body refinement). See also
_atom_site.constraints and _atom_site_refinement_flags. A general
description of constraints may appear in _refine.details.

| 

SUBROUTINE Hrefine_obscriteria(Criteria)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Criteria

**Writes the following CIF items:**

_reflns.observed_criterion

The criterion used to classify a reflection as 'observed'. This
criterion is usually expressed in terms of a sigma(I) or sigma(F)
threshold.

| 

SUBROUTINE Hrefine_restraints(Rtype,Num,rmsd,sigd,criteria,Nreject)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

Rtype*( * ),criteria*( * )

Integer Num,Nreject

Real rmsd,sigd

**Writes the following CIF items:**

_refine_ls_restr.type

The type of the parameter being restrained. Explicit sets of data values
are provided for the programs PROTIN/PROLSQ (beginning with p_) and
RESTRAIN (beginning with RESTRAIN_). As computer programs will evolve,
these data values are given as examples, and not as an enumeration list.
Computer programs converting a data block to a refinement table will
expect the exact form of the data values given here to be used.

_refine_ls_restr.number

The number of parameters of this type subjected to restraint in
least-squares refinement.

_refine_ls_restr.dev_ideal

For the given parameter type, the root-mean-square deviation between the
ideal values used as restraints in the least-squares refinement and the
values obtained by refinement. For instance, bond distances may deviate
by 0.018 \%A (r.m.s.) from ideal values in current model.

_refine_ls_restr.dev_ideal_target

For the given parameter type, the target root-mean-square deviation
between the ideal values used as restraints in the least-squares
refinement and the values obtained by refinement.

_refine_ls_restr.rejects

The number of parameters of this type that deviate from ideal values by
more than the amount defined in _refine_ls_restr.criterion in the model
obtained by restrained least-squares refinement.

_refine_ls_restr.criterion

A criterion used to define a parameter value that deviates significantly
from its ideal value in the model obtained by restrained least-squares
refinement.

| 

SUBROUTINE Hrefine_rfacts(Nall,Nobs,Nmiss,Nwork,Nfree,NfreeMiss,PercentObs,PercentFree,Rall,Robs,Rwork,Rfree, Wall,Wobs,Wwork,Wfree)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nall,Nobs,Nmiss,Nwork,Nfree,NfreeMiss

Real

PercentObs,PercentFree,Rall,Robs,Rwork,Rfree,Wall,Wobs,Wwork,Wfree

**Writes the following CIF items:**

_refine.ls_number_reflns_all

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low.

_refine.ls_number_reflns_obs

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low and the observation
limit established by _reflns.observed_criterion.

_refine.ls_number_reflns_R_work

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low and the observation
limit established by _reflns.observed_criterion, and that were used as
the working (i.e. included in refinement) reflections when refinement
included calculation of a "free" R factor. Details of how reflections
were assigned to the working and test sets are given in
_reflns.R_free_details.

_refine.ls_number_reflns_R_free

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low and the observation
limit established by _reflns.observed_criterion, and that were used as
the test (i.e. excluded from refinement) reflections when refinement
included calculation of a "free" R factor. Details of how reflections
were assigned to the working and test sets are given in
_reflns.R_free_details.

_refine.ls_percent_reflns_obs

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low and the observation
limit established by _reflns.observed_criterion, expressed as a
percentage of the number of geometrically observable reflections that
satisfy the resolution limits.

_refine.ls_percent_reflns_R_free

The number of reflections that satisfy the resolution limits established
by _refine.ls_d_res_high and _refine.ls_d_res_low and the observation
limit established by _reflns.observed_criterion, and that were used as
the test (i.e. excluded from refinement) reflections when refinement
included calculation of a "free" R factor, espressed as a percentage of
the number of geometrically observable reflections that satisfy the
resolution limits.

_refine.ls_R_factor_all

Residual factor R for all reflections that satisfy the resolution limits
established by _refine.ls_d_res_high and _refine.ls_d_res_low.

_refine.ls_R_factor_obs

Residual factor R for reflections that satisfy the resolution limits
established by _refine.ls_d_res_high and _refine.ls_d_res_low and the
observation limit established by _reflns.observed_criterion.
_refine.ls_R_factor_obs should not be confused with
_refine.ls_R_factor_R_work; the former reports the results of a
refinement in which all observed reflections were used, the latter a
refinement in which a subset of the observed reflections were excluded
from refinement for the calculation of a "free" R factor. However, it
would be meaningful to quote both values if a "free" R factor were
calculated for most of the refinement, but all of the observed
reflections were used in the final rounds of refinement; such a protocol
should be explained in _refine.details.

_refine.ls_R_factor_R_work

Residual factor R for reflections that satisfy the resolution limits
established by _refine.ls_d_res_high and _refine.ls_d_res_low and the
observation limit established by _reflns.observed_criterion, and that
were used as the working (i.e., included in refinement) reflections when
refinement included calculation of a "free" R factor. Details of how
reflections were assigned to the working and test sets are given in
_reflns.R_free_details. _refine.ls_R_factor_obs should not be confused
with _refine.ls_R_factor_R_work; the former reports the results of a
refinement in which all observed reflections were used, the latter a
refinement in which a subset of the observed reflections were excluded
from refinement for the calculation of a "free" R factor. However, it
would be meaningful to quote both values if a "free" R factor were
calculated for most of the refinement, but all of the observed
reflections were used in the final rounds of refinement; such a protocol
should be explained in _refine.details.

_refine.ls_R_factor_R_free

Residual factor R for reflections that satisfy the resolution limits
established by _refine.ls_d_res_high and _refine.ls_d_res_low and the
observation limit established by _reflns.observed_criterion, and that
were used as the test (i.e., excluded from refinement) reflections when
refinement included calculation of a "free" R factor. Details of how
reflections were assigned to the working and test sets are given in
_reflns.R_free_details.

_refine.ls_wR_factor_all

Weighted residual factor wR for all reflections that satisfy the
resolution limits established by _refine.ls_d_res_high and
_refine.ls_d_res_low.

_refine.ls_wR_factor_obs

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine.ls_d_res_high and _refine.ls_d_res_low
and the obervation limit established by _reflns.observed_criterion.

_refine.ls_wR_factor_R_work

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine.ls_d_res_high and _refine.ls_d_res_low
and the observation limit established by _reflns.observed_criterion,
and that were used as the working (i.e., included in refinement)
reflections when refinement included calculation of a "free" R factor.
Details of how reflections were assigned to the working and test sets
are given in _reflns.R_free_details.

_refine.ls_wR_factor_R_free

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine.ls_d_res_high and _refine.ls_d_res_low
and the observation limit established by _reflns.observed_criterion,
and that were used as the test (i.e., excluded from refinement)
reflections when refinement included calculation of a "free" R factor.
Details of how reflections were assigned to the working and test sets
are given in _reflns.R_free_details.

| 

SUBROUTINE Hrefine_rg(RDhigh,RDlow,RGall,RGwork,RGfree)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

RGall,RGwork,RGfree,RDhigh,RDlow

**Writes the following CIF items:**

_refine_analyze.RG_d_res_high

The value of the high-resolution cutoff in angstroms used in calculation
of the Hamilton generalized R factor (RG) stored in
refine_analyze.RG_work and _refine_analyze.RG_free.

_refine_analyze.RG_d_res_low

The value of the low-resolution cutoff in angstroms used in calculation
of the Hamilton generalized R factor (RG) stored in
refine_analyze.RG_work and _refine_analyze.RG_free.

_refine_analyze.RG_all

The Hamilton generalised R-factor (see W.C. Hamilton (1965) Acta Cryst.,
18, 502-510. ) for all reflections that satisfy the resolution limits
established by _refine.ls_d_res_high and _refine.ls_d_res_low.

_refine_analyze.RG_work

The Hamilton generalized R factor for all reflections that satisfy the
resolution limits established by _refine_analyze.RG_d_res_high and
_refine_analyze.RG_d_res_low and for those reflections included in the
working set when a free R set of reflections are omitted from the
refinement.

_refine_analyze.RG_free

The Hamilton generalized R factor for all reflections that satisfy the
resolution limits established by _refine_analyze.RG_d_res_high and
_refine_analyze.RG_d_res_low for the free R set of reflections that
were excluded from the refinement.

_refine_analyze.RG_free_work_ratio

The observed ratio of RGfree to RGwork. The expected RG ratio is the
value that should be achievable at the end of a structure refinement
when only random uncorrelated errors exist in data and model provided
that the observations are properly weighted. When compared with the
observed RG ratio it may indicate that a structure has not reached
convergence or a model has been over-refined with no corresponding
improvement in the model.

| 

SUBROUTINE Hrefine_rmsobs2dict(Rtype,Clow,Chigh,Usigma)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

Rtype*( * )

Real

Clow,Chigh,Usigma

**Writes the following CIF items:**

_refine_ls_restr_type.type

This data item is a pointer to _refine_ls_restr.type in the
REFINE_LS_RESTR category.

_refine_ls_restr_type.distance_cutoff_low

The lower limit in angstroms of the distance range applied to the
current restraint type.

_refine_ls_restr_type.distance_cutoff_high

The upper limit in angstroms of the distance range applied to the
current restraint type.

_refine_ls_restr_type.U_sigma_weights

_refine_ls_restr.U_sigma_weights is a coefficient used in the
calculation of the weight for thermal parameter restraints in the
program RESTRAIN. The equation used to calculate the actual weight from
this coefficient depends upon the value of _refine_ls_restr.type -
either "r.m.s. diffs for Uiso atoms at distance *" or "r.m.s. diffs for
Uaniso atoms at distance *". A similarity restraint is applied to the
thermal parameters of any pair of atoms which are subject to an
interatomic distance restraint (i.e. 1-2 and 1-3 bonded atoms). The
thermal parameter restraints are categorized by the distance between the
two affected atoms. The expected r.m.s. differences in thermal
parameter, either Uiso or Uaniso, are listed for each shell in
_refine_ls_restr.rmsdev_dictionary.

| 

SUBROUTINE Hrefine_shell(Rlow,Rhigh,NrefAll,NrefObs,Nmiss,NrefWork,PercentObs,RfacAll,RfacObs,Nfree,Rfree,Rwork,WgtRfac, WgtRobs,WgtRwork,WgtUsed,Wexpress)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Rlow,Rhigh,PercentObs,RfacObs,RfacAll,Rfree,Rwork,WgtRfac,WgtRobs,WgtRwork,Wgtused,Wexpress

Integer

NrefAll,NrefObs,Nmiss,NrefWork,Nfree

**Writes the following CIF items:**

_refine_ls_shell.d_res_high

The highest resolution for the interplanar spacing in the reflection
data in this shell. This is the largest d value.

_refine_ls_shell.d_res_low

The lowest resolution for the interplanar spacing in the reflection data
in this shell. This is the smallest d value.

_refine_ls_shell.number_reflns_all

The number of reflections that satisfy the resolution limits established
by _refine_ls_shell.d_res_high and _refine_ls_shell.d_res_low.

_refine_ls_shell.number_reflns_obs

The number of reflections that satisfy the resolution limits established
by _refine_ls_shell.d_res_high and _refine_ls_shell.d_res_low and the
observation criterion established by _reflns.observed_criterion.

_refine_ls_shell.number_reflns_R_work

The number of reflections that satisfy the resolution limits established
by _refine_ls_shell.d_res_high and _refine_ls_shell.d_res_low and the
observation limit established by _reflns.observed_criterion, and that
were used as the working (i.e., included in refinement) reflections when
refinement included calculation of a "free" R factor. Details of how
reflections were assigned to the working and test sets are given in
_reflns.R_free_details.

_refine_ls_shell.percent_reflns_obs

The number of reflections that satisfy the resolution limits established
by _refine_ls_shell.d_res_high and _refine_ls_shell.d_res_low and the
observation criterion established by _reflns.observed_criterion,
expressed as a percentage of the number of geometrically observable
reflections that satisfy the resolution limits.

_refine_ls_shell.R_factor_all

Residual factor R for reflections that satisfy the resolution limits
established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low.

_refine_ls_shell.R_factor_obs

Residual factor R for reflections that satisfy the resolution limits
established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low and the observation criterion established by
_reflns.observed_criterion.

_refine_ls_shell.number_reflns_R_free

The number of reflections that satisfy the resolution limits established
by _refine_ls_shell.d_res_high and _refine_ls_shell.d_res_low and the
observation limit established by _reflns.observed_criterion, and that
were used as the test (i.e., excluded from refinement) reflections when
refinement included calculation of a "free" R factor. Details of how
reflections were assigned to the working and test sets are given in
_reflns.R_free_details.

_refine_ls_shell.R_factor_R_free

Residual factor R for reflections that satisfy the resolution limits
established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low and the observation limit established by
_reflns.observed_criterion, and that were used as the test (i.e.,
excluded from refinement) reflections when refinement included
calculation of a "free" R factor. Details of how reflections were
assigned to the working and test sets are given in
_reflns.R_free_details.

_refine_ls_shell.R_factor_R_work

Residual factor R for reflections that satisfy the resolution limits
established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low and the observation limit established by
_reflns.observed_criterion, and that were used as the working (i.e.,
included in refinement) reflections when refinement included calculation
of a "free" R factor. Details of how reflections were assigned to the
working and test sets are given in _reflns.R_free_details.

_refine_ls_shell.wR_factor_all

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low.

_refine_ls_shell.wR_factor_obs

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low and the observation criterion established by
_reflns.observed_criterion.

_refine_ls_shell.wR_factor_R_work

Weighted residual factor wR for reflections that satisfy the resolution
limits established by _refine_ls_shell.d_res_high and
_refine_ls_shell.d_res_low and the observation limit established by
_reflns.observed_criterion, and that were used as the working (i.e.,
included in refinement) reflections when refinement included calculation
of a "free" R factor. Details of how reflections were assigned to the
working and test sets are given in _reflns.R_free_details.

_refine_ls_shell.weight_used

The mean weighting used for the reflections in the resolution limits for
the shell. The weights used may for example include experimental sigmas
in weighting; or by weighting using average diagonal term of Xray and
geometry - same as PROLSQ where weighting equates
wmat*average_diagonal_of_geometry to average_diagonal_of_Xray terms; or
Weighting equates wgrad*RMS_gradient_of_geometry to RMS_gradient_of_Xray
terms; or The relative weighting for Xray and geom terms is governed by
sigma. Wsigma applied to Xray terms as 1/(Wsigma**2)

_refine_ls_shell.weight_exp

NO DESCRIPTION IN CIF_MM.DIC

| 

SUBROUTINE Hrefine_solvent_model(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

NHlines

Character

Hlines(NHline)*80

**Writes the following CIF items:**

_refine.solvent_model_details

Special aspects of the solvent model used in refinement.

| 

SUBROUTINE Hrefine_wght_details(NHlines,Hlines)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

NHlines

Character

Hlines(NHlines)*80

**Writes the following CIF items:**

_refine.ls_weighting_details

A description of special aspects of the weighting scheme used in
least-squares refinement. Used to describe the weighting when the value
of _refine.ls_weighting_scheme is specified as 'calc'.

| 

SUBROUTINE Hrefine_wghtScheme(WghtScheme)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

WghtScheme*( * )

**Writes the following CIF items:**

_refine.ls_weighting_scheme

The weighting scheme applied in the least-squares process. The standard
code may be followed by a description of the weight (but see
_refine_ls_weighting_details for a preferred approach).

| 

SUBROUTINE Hrefln_sys_abs(IH,IK,IL,F,SF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

F,SF

Integer

IH,IK,IL

**Writes the following CIF items:**

_refln_sys_abs.index_h

Miller index h of the reflection. The values of the Miller indices in
the REFLN_SYS_ABS category must correspond to the cell defined by cell
lengths and cell angles in the CELL category.

_refln_sys_abs.index_k

Miller index k of the reflection. The values of the Miller indices in
the REFLN_SYS_ABS category must correspond to the cell defined by cell
lengths and cell angles in the CELL category.

_refln_sys_abs.index_l

Miller index l of the reflection. The values of the Miller indices in
the REFLN_SYS_ABS category must correspond to the cell defined by cell
lengths and cell angles in the CELL category.

_refln_sys_abs.I

The measured value of the intensity in arbitrary units.

_refln_sys_abs.sigmaI

The standard uncertainty (e.s.d.) of _refln_sys_abs.I, in arbitrary
units.

_refln_sys_abs.I_over_sigmaI

The ratio of _refln_sys_abs.I to _refln_sys_abs.sigmaI. Used to
evaluate whether a reflection that should be systematically absent
according to the designated space group is in fact absent.

| 

SUBROUTINE Hreflns(WilsonB,R1,R2,Criteria,AMI,AMF,Nref)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

WilsonB,R1,R1,AMI,AMF

Integer

Nref

Character

Critera*( * )

**Writes the following CIF items:**

_reflns.B_iso_Wilson_estimate

The value of the overall isotropic temperature factor estimated from the
slope of the Wilson plot.

_reflns.entry_id

This data item is a pointer to _entry.id in the ENTRY category.

_reflns.data_set_id

The value of _reflns.data_set_id identifies the particular data set for
which harvest information is presented in the data block.

_reflns.d_resolution_high

The highest resolution for the interplanar spacings in the reflection
data. This is the smallest d value.

_reflns.d_resolution_low

The lowest resolution for the interplanar spacings in the reflection
data. This is the largest d value.

_reflns.observed_criterion

The criterion used to classify a reflection as 'observed'. This
criterion is usually expressed in terms of a sigma(I) or sigma(F)
threshold.

_reflns.mean<I_over_sigI>_obs_all

Overall Mean <I/sigma(I)> for the total ensemble of reflections observed

_reflns.mean<F_over_sigF>_obs_all

Overall Mean <F/sigma(F)> for the total ensemble of reflections observed

_reflns.number_obs

The number of reflections in the REFLN list (not the DIFFRN_REFLN list)
classified as observed (see _reflns.observed_criterion). This number
may contain Friedel equivalent reflections according to the nature of
the structure and the procedures used.

| 

SUBROUTINE Hreflns_intensity_shell(Z,ACT,ACO,CT,CO)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

Z,ACT,ACO,CT,CO

**Writes the following CIF items:**

_EBI_reflns_intensity_shell.Z

Z is defined as I/<I> for the range of 4*((Sintheta/Lamda)**2)

_EBI_reflns_intensity_shell.NZ_acentric_theory

The theoretical acentric wilson Distribution of Intensity magnitudes
given in shells of N(Z)

_EBI_reflns_intensity_shell.NZ_acentric_observed

The observed acentric wilson Distribution of Intensity magnitudes given
in shells of N(Z)

_EBI_reflns_intensity_shell.NZ_centric_theory

The theoretical centric wilson Distribution of Intensity magnitudes
given in shells of N(Z)

_EBI_reflns_intensity_shell.NZ_centric_observed

The observed centric wilson Distribution of Intensity magnitudes given
in shells of N(Z)

| 

SUBROUTINE Hreflns_overall_merge_p1(R1,R2,nmeas,nuniq,ncent,nano,fsigi,Rfac,ranom,sdIsignal,fpbias,Ntb)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

nmeas,nuniq,ncent,nano,Ntb

Real

R1,R2,fsigi,Rfac,ranom,sdIsignal,fpbias

**Writes the following CIF items:**

_diffrn_reflns.d_res_high

The highest resolution in angstroms for the interplanar spacing for all
the reflections merged. This is the smallest d value.

_diffrn_reflns.d_res_low

The lowest resolution in angstroms for the interplanar spacing for all
the reflections merged. This is the largest d value.

_diffrn_reflns.meanI_over_sigI_all

The ratio of the mean of the intensities of all reflections merged
within the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high to the mean of the standard uncertainties of
the intensities of all reflections in the same resolution limits.

_diffrn_reflns.number_measured_all

The total number of reflections measured and used in merging for the
resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.number_unique_all

The total number of reflections measured which are symmetrically unique
after merging for the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.number_centric_all

The total number of reflections measured which are symmetrically unique
after merging and classified as centric for the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.number_anomalous_all

The total number of reflections measured which are symmetrically unique
after merging and that an anomalous difference was present in the
overall set of observations, for the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.Rmerge_I_all

The value of Rmerge(I) for all reflections in the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.Rmerge_I_anomalous_all

The value of Rmerge for anomalous data in the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.meanI_over_sd_all

The ratio of the mean of the intensities of all reflections merged in
the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high, to the average standard deviation derived
from experimental SDs, after application of SDFAC SDADD SDB

_diffrn_reflns.mean_fract_bias

The overall Mean Fractional partial bias in the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high

_diffrn_reflns.num_fract_bias_in_mean

The overall number of reflections used in the mean Fractional partial
bias in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high

| 

SUBROUTINE Hreflns_overall_merge_p2(complt,Rmult,pcv,pcvo,rmeas,rmeaso,anomfrc)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

complt,Rmult,pcv,pcvo,rmeas,rmeaso,anomfrc

**Writes the following CIF items:**

_diffrn_reflns.percent_possible_all

The percentage of geometrically possible reflections represented by all
reflections measured in the resolution limits _diffrn_reflns.d_res_low
and _diffrn_reflns.d_res_high.

_diffrn_reflns.multiplicity

The mean number of redundancy for the intensities of all reflections
measured in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.PCV

The pooled standard deviation (the statistically valid measure of the
noise level) is divided by the sum of the intensities (the signal level)
in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.PCV_mean

The pooled standard deviation (the statistically valid measure of the
noise level) is divided by the sum of the intensities (the signal level)
in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.Rmeas

The multiplicity-weighted Rmerge relative to I+ or I- in the resolution
limits _diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.Rmeas_mean

The multiplicity-weighted Rmerge relative to overall mean I in the
resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.anom_diff_percent_meas

AnomFrc is the % of measured acentric reflections for which an anomalous
difference has been measured in the resolution limits
_diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

| 

SUBROUTINE Hreflns_overall_merge_p3(aihmin,aihmax,t1f,nssf,t2f,t3f,t1p,nssp,t2p,t3p)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

nssf, nssp

Real

aihmin,aihmax,t1f,t2f,t3f,t1p,t2p,t3p

**Writes the following CIF items:**

_diffrn_reflns.min_intensity

The minimum intensity for a reflection measured after scaling and
merging in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.max_intensity

The maximum intensity for a reflection measured after scaling and
merging in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.Intensity_rms_fully_recorded

The overall mean rms of intensity for all fully recorded reflections in
the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.num_fully_measured

The total number of fully recorded reflections merged in the resolution
limits _diffrn_reflns.d_res_low and _diffrn_reflns.d_res_high.

_diffrn_reflns.mean_scatter_over_sd_full

The mean (scatter/SD) for all fully recorded reflections scaled/merged
in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high by the normal probability analysis of
deviations.

_diffrn_reflns.sigma_scatter_over_sd_full

The sigma (scatter/SD) for all fully recorded reflections scaled/merged
in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high by the normal probability analysis of
deviations.

_diffrn_reflns.Intensity_rms_partially_recorded

The overall mean rms of intensity for all partially recorded reflections
in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.num_partials_measured

The total number of partially recorded reflections merged in the
resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high.

_diffrn_reflns.mean_scatter_over_sd_part

The mean (scatter/SD) for all partially recorded reflections
scaled/merged in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high by the normal probability analysis of
deviations.

_diffrn_reflns.sigma_scatter_over_sd_part

The sigma (scatter/SD) for all partially recorded reflections
scaled/merged in the resolution limits _diffrn_reflns.d_res_low and
_diffrn_reflns.d_res_high by the normal probability analysis of
deviations.

| 

SUBROUTINE Hreflns_scaling_shell(R1,R2,Nref,AI,AF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

R1,R2,AI,AF

Integer

Nref

**Writes the following CIF items:**

_EBI_tmp_reflns_scaling_shell.d_res_high

The highest resolution for the interplanar spacing in the reflection
data for the dataset in this shell. This is the smallest d value.

_EBI_tmp_reflns_scaling_shell.d_res_low

The lowest resolution for the interplanar spacing in the reflection data
for the dataset in this shell. This is the highest d value.

_EBI_tmp_reflns_scaling_shell.num_reflns_observed

The total number of reflections in this shell.

_EBI_tmp_reflns_scaling_shell.mean<I_over_sigI>_obs

Mean <I/sigma(I)> for the total number of reflections in this shell.

_EBI_tmp_reflns_scaling_shell.mean<F_over_sigF>_obs

Mean <F/sigma(F)> for the total number of reflections in this shell.

| 

SUBROUTINE Hreflns_shell_p1(R1,R2,Nmeas,Nuniq,Ncent,Nanom,fsigi,Rfac,Rcum,Ranom,sdIsignal,fpbias,Npb)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nmeas,Nuniq,Ncent,Nanom,Npb

Real

R1,R2,fsigi,Rfac,Rcum,Ranom,sdIsignal,FPbias

**Writes the following CIF items:**

_reflns_shell.d_res_high

The highest resolution in angstroms for the interplanar spacing in the
reflections in this shell. This is the smallest d value.

_reflns_shell.d_res_low

The lowest resolution in angstroms for the interplanar spacing in the
reflections in this shell. This is the largest d value.

_reflns_shell.number_measured_all

The total number of reflections measured for this resolution shell.

_reflns_shell.number_unique_all

The total number of measured reflections which are symmetrically unique
after merging for this resolution shell.

_reflns_shell.number_centric_all

The total number of centric reflections measured for this resolution
shell.

_reflns_shell.number_anomalous_all

The total number of anomalous reflections measured for this resolution
shell.

_reflns_shell.Rmerge_I_all

The value of Rmerge(I) for all reflections in a given shell.

_reflns_shell.Rmerge_I_all_cumulative

The cumulative Rmerge up to this range of shells

_reflns_shell.Rmerge_I_anomalous_all

The value of Rmerge for anomalous data in a given shell.

_reflns_shell.meanI_over_sd_all

The ratio of the mean of the intensities of all reflections in this
shell to the average standard deviation derived from experimental SDs,
after application of SDFAC SDADD SDB

_reflns_shell.meanI_over_sigI_all

The ratio of the mean of the intensities of all reflections in this
shell to the mean of the standard uncertainties of the intensities of
all reflections in the resolution shell.

_reflns_shell.mean_fract_bias

The Mean Fractional partial bias in the resolution shell.

_reflns_shell.num_fract_bias_in_mean

The number of reflections used in the mean Fractional partial bias in
the resolution shell.

| 

SUBROUTINE Hreflns_shell_p2(R1,R2,complt,ccmplt,amltpl,pcvo,pcv,rmeas,rmeaso,anomfrc)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Real

complt,ccmplt,amltpl,pcvo,pcv,rmeas,rmeaso,anomfrc,R1,R2

**Writes the following CIF items:**

_reflns_shell.percent_possible_all

The percentage of geometrically possible reflections represented by all
reflections measured for this resolution shell.

_reflns_shell.cum_percent_possible_all

The cumulative percentage of geometrically possible reflections up to
this range of shells.

_reflns_shell.multiplicity

The mean number of redundancy for the intensities of all reflections in
the resolution shell.

_reflns_shell.PCV_mean

The pooled standard deviation (the statistically valid measure of the
noise level) is divided by the sum of the intensities (the signal level)
in the resolution shell.

_reflns_shell.PCV

The pooled coefficient of variation relative to I+ or I-. The pooled
standard deviation (the statistically valid measure of the noise level)
is divided by the sum of the intensities (the signal level) in the
resolution shell.

_reflns_shell.Rmeas

The multiplicity-weighted Rmerge relative to I+ or I- in the resolution
shell.

_reflns_shell.Rmeas_mean

The multiplicity-weighted Rmerge relative to overall mean I in the
resolution shell.

_reflns_shell.anom_diff_percent_meas

AnomFrc is the % of measured acentric reflections for which an anomalous
difference has been measured in the resolution shell, after merging and
scaling of the data set. The rejection criteria and treatment of
Bijvoet-related observations (I+ & I-) is covered by the
_reflns.merge_reject_criterion definition, as are all observations of
reflections.

| 

SUBROUTINE Hsoftware(SoftwareClass,SoftwareAuthor,SoftwareEmail,SoftwareDescr)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Character

SoftwareAuthor* ( * ),SoftwareEmail* ( * ),SoftwareDescr*
( * ),SoftwareClass* ( * )

**Writes the following CIF items:**

_software.classification

The classification of the program according to its major function.

_software.contact_author

The recognized contact author of the software. This could be the
original author, modifier of the code, or maintainer, but should be the
individual most commonly associated with the code.

_software.contact_author_email

The email address of the _software.contact_author.

_software.description

Description of the software.

| 

SUBROUTINE HSymmetry(IntTabNum,SpaceGrpNam,NumEquiv,RFsymm)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

IntTabNum,NumEquiv

Character

SpaceGrpNam*10

Real

RFsymm(4,4,* )

**Writes the following CIF items:**

_Symmetry.Int_Tables_number

Space-group number from International Tables for Crystallography, Vol. A
(1987).

_Symmetry.space_group_name_H-M

Hermann-Mauguin space-group symbol. Note that the H-M symbol does not
necessarily contain complete information about the symmetry and the
space-group origin. If used always supply the FULL symbol from
International Tables for Crystallography, Vol. A (1987) and indicate the
origin and the setting if it is not implicit. If there is any doubt that
the equivalent positions can be uniquely deduced from this symbol
specify the _symmetry_equiv.pos_as_xyz or
_symmetry.space_group_name_Hall data items as well. Leave spaces
between symbols referring to different axes.

_Symmetry_equiv.id

The value of _symmetry_equiv.id must uniquely identify a record in the
SYMMETRY_EQUIV category. Note that this item need not be a number; it
can be any unique identifier.

_Symmetry_equiv.pos_as_xyz

Symmetry equivalent position in the 'xyz' representation. Except for the
space group P1, these data are repeated in a loop. The format of the
data item is as per International Tables for Crystallography, Vol. A.
(1987). All equivalent positions should be entered, including those for
lattice centring and a centre of symmetry, if present.

| 

SUBROUTINE HSymTrn(Nsym,Rsm,Symchs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nsym

Real

Rsm(4,4,* )

Character

Symchs(MaxSymmetry)*80

**Does the following:**

Translates the Symmetry matrices into INT TAB character strings for each
symmetry operation and prints the real and reciprocal space operators on
standard output.

| 

SUBROUTINE HTrnSym(Nsym,Rsm,Symchs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Arguments:**

Integer

Nsym

Real

Rsm(4,4,* )

Character

Symchs(MaxSymmetry)*80

**Does the following:**

Symmetry translation from matrix back to characters. This translates the
Symmetry matrices into INT TAB character strings.

SOURCE:
http://ndbserver.rutgers.edu/mmcif/dictionaries/html/cif_mm.dic/Index/index.html
