PDB_EXTRACT (CCP4: Supported Program)
=====================================

NAME
----

**pdb_extract** - RCSB/PDB Programs for extracting harvest information
from program log files

SYNOPSIS
--------

| **pdb_extract** [options]... [files]...
| **pdb_extract_sf** [options]... [files]...

DESCRIPTION
-----------

PDB_EXTRACT provides tools developed by the
`RSCB/PDB <http://www.rcsb.org/pdb/>`__ for extracting mmCIF data from
structure determination applications. The package has its own
documentation which can be located `here <pdb_extract-CCP4.html>`__

The `Data Harvesting Management Tool <dhm_tool.html>`__ in CCP4i
provides a graphical interface to some of the functionality of
PDB_EXTRACT.

AUTHORS
-------

Protein Data Bank (PDB) at Rutgers, The State University of New Jersey

.. _references: 
 
 REFERENCES
--------------------------------

#. .. _reference1: 
 
 Automated and accurate deposition of structures
   solved by X-ray diffraction to the Protein Data Bank
   H. Yang, V. Guranovic, S. Dutta, Z. Feng, H. M. Berman and J. D.
   Westbrook *Acta Cryst.* **D60**, 1833-1839 (2004)

SEE ALSO
--------

| `RSCB/PDB <http://www.rcsb.org/pdb/>`__
| `Data Harvesting Management Tool <dhm_tool.html>`__
| `Harvesting <harvesting.html>`__
