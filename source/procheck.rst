PROCHECK (CCP4: Supported Program)
==================================

NAME
----

**procheck** - programs to check the Stereochemical Quality of Protein
Structures.

SYNOPSIS
--------

**procheck** *foo_in.brk* [chain-ID] <resolution>

CONFIDENTIALITY AGREEMENT
-------------------------

If you have not already done so, please fill in the PROCHECK license
agreement $CPROG/procheck/confid.doc and send it to Roman Laskowski
(address is on form).

DESCRIPTION
-----------

The command procheck runs a shell script $CETC/procheck on unix
machines, which in turn run a series of programs to analyse a model
structure. The results are given as several postscript plots and data
files. Full details of PROCHECK are given in
`$CHTML/procheck_man/index.html <procheck_man/index.html>`__

EXAMPLES
--------

Simple toxd example found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`procheck.exam <../examples/unix/runnable/procheck.exam>`__

REFERENCE
---------

Roman A Laskowski, Malcolm W MacArthur, David S Moss and Janet M
Thornton, *J. App. Cryst.* **26** 283 (1993)

AUTHORS
-------

Roman A Laskowski, Malcolm W MacArthur, David K Smith, David T Jones, E
Gail Hutchinson, A Louise Morris, Dorica Naylor, David S Moss and Janet
M Thornton.

SEE ALSO
--------

Other coordinate-checking programs:

-  `act <act.html>`__
-  `contact <contact.html>`__
-  `geomcalc <geomcalc.html>`__
-  `surface <surface.html>`__
-  `volume <volume.html>`__
