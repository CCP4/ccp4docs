MAPLIB (CCP4: Library)
======================

NAME
----

**maplib** - Subroutine Library for CCP4 map file handling

Original documentation by Phil Evans and John Campbell.

CONTENTS
--------

#. `INTRODUCTION <#introduction>`__
#. `DETAILED DESCRIPTION OF THE MAP FORMAT <#description>`__
#. `MAP HANDLING SUBROUTINES <#subroutines>`__
#. `THE SUBROUTINES 'MRDHDR' and 'MRDHDS' <#mrdhdr>`__
   ('ccp4_map_read_open_header' and 'ccp4_map_read_open_header_check')
#. `THE SUBROUTINE 'MPOSN' <#mposn>`__
   ('ccp4_map_read_position_section')
#. `THE SUBROUTINE 'MRDLIN' <#mrdlin>`__
   ('ccp4_map_read_line_as_mode')
#. `THE SUBROUTINE 'MGULP' <#mgulp>`__
   ('ccp4_map_read_whole_section_as_mode')
#. `THE SUBROUTINE 'MGULPR' <#mgulpr>`__
   ('ccp4_map_read_whole_section_as_real')
#. `THE SUBROUTINE 'MRCLOS' <#mrclos>`__
   ('ccp4_map_read_close')
#. `THE SUBROUTINES 'MWRHDR' and 'MWRHDL' <#mwrhdr>`__
   ('ccp4_map_write_open_header_by_id' and
   'ccp4_map_write_open_header_by_name')
#. `THE SUBROUTINE 'MSPEW' <#mspew>`__
   ('ccp4_map_write_all_section')
#. `THE SUBROUTINE 'MWRSEC' <#mwrsec>`__
   ('ccp4_map_write_part_section')
#. `THE SUBROUTINE 'MPOSNW' <#mposnw>`__
   ('ccp4_map_write_position_section')
#. `THE SUBROUTINES 'MWCLOSE', 'MCLOSE' and `MCLOSC' <#mwclose>`__
   ('ccp4_map_write_close_auto', 'ccp4_map_write_close_user_sum' and
   'ccp4_map_write_close_user_mean')
#. `THE SUBROUTINES 'MSYPUT' and 'MSYWRT' <#msyput>`__
   ('ccp4_map_write_spgname' and 'ccp4_map_write_symm_matrix')
#. `THE SUBROUTINE 'MSYMOP' <#msymop>`__
   ('ccp4_map_read_symm_matrix')
#. `THE SUBROUTINE 'MSYCPY' <#msycpy>`__
   ('ccp4_map_copy_symmetry')
#. `THE SUBROUTINE 'MSKPUT' <#mskput>`__
   ('ccp4_map_write_skew_info')
#. `THE FUNCTION 'MSKGET' <#mskget>`__
   (No alternative name)
#. `THE SUBROUTINES `MTTCPY' and `MTTREP' <#mttcpy>`__
   ('ccp4_map_copy_title' and 'ccp4_map_write_replace_title')
#. `THE SUBROUTINES 'MRFNAM' and 'MWFNAM' <#mrfnam>`__
   ('ccp4_map_get_last_read_filename' and
   'ccp4_map_get_last_write_filename')
#. `THE SUBROUTINES 'CCP4MAPHEAD', 'CCP4MAPIN' and
   'CCP4MAPOUT' <#ccp4maphead>`__ ('ccp4_map_read_header_only',
   'ccp4_map_read_whole_map' and 'ccp4_map_write_whole_map')
#. `TYPICAL CALLING SEQUENCES <#calling_sequence>`__
#. `IMPLEMENTATION NOTES <#implementation>`__

.. _introduction: 
 
 1) INTRODUCTION
---------------------------------------

The standard map file format used by the CCP4 programs is the map/image
file format devised at the MRC LMB Cambridge, originally by David Agard.
The advantages of the format include the following:

-  The information in the header describes the relationship of the map
   to the crystal cell, and other information useful in crystallographic
   calculations (e.g. crystal symmetry)
-  The file may be written and read in different ways e.g. written
   section by section and read line by line.
-  The format is suitable for both crystallographic work and for image
   processing so that Fourier and plotting programs can be used for both
   purposes.

Note that the same format is used by the cryoEM community, and referred
to as the `MRC
format <http://www.ccpem.ac.uk/mrc_format/mrc_format.php>`__. The
formats should be compatible, but note:

-  MRC format does not use the skew transformation header records (words
   25-37)
-  CCP4 format does not use the ORIGIN header records (words 50-52)

A useful list of cryoEM software is maintained at the
`EMDataBank <http://www.emdatabank.org/emsoftware.html>`__.

.. _description: 
 
 2) DETAILED DESCRIPTION OF THE MAP FORMAT
----------------------------------------------------------------

The overall layout of the file is as follows:

#. File header (256 longwords)
#. Symmetry information
#. Map, stored as a 3-dimensional array

The header is organised as 56 words followed by space for ten 80
character text labels as follows:

::

    
    1      NC              # of Columns    (fastest changing in map)
    2      NR              # of Rows
    3      NS              # of Sections   (slowest changing in map)
    4      MODE            Data type
                             0 = envelope stored as signed bytes (from
                                 -128 lowest to 127 highest)
                             1 = Image     stored as Integer*2
                             2 = Image     stored as Reals
                             3 = Transform stored as Complex Integer*2
                             4 = Transform stored as Complex Reals
                             5 == 0    
    
                             Note: Mode 2 is the normal mode used in
                                   the CCP4 programs. Other modes than 2 and 0
                                   may NOT WORK
    
    5      NCSTART         Number of first COLUMN  in map
    6      NRSTART         Number of first ROW     in map
    7      NSSTART         Number of first SECTION in map
    8      NX              Number of intervals along X
    9      NY              Number of intervals along Y
   10      NZ              Number of intervals along Z
   11      X length        Cell Dimensions (Angstroms)
   12      Y length                     "
   13      Z length                     "
   14      Alpha           Cell Angles     (Degrees)
   15      Beta                         "
   16      Gamma                        "
   17      MAPC            Which axis corresponds to Cols.  (1,2,3 for X,Y,Z)
   18      MAPR            Which axis corresponds to Rows   (1,2,3 for X,Y,Z)
   19      MAPS            Which axis corresponds to Sects. (1,2,3 for X,Y,Z)
   20      AMIN            Minimum density value
   21      AMAX            Maximum density value
   22      AMEAN           Mean    density value    (Average)
   23      ISPG            Space group number
   24      NSYMBT          Number of bytes used for storing symmetry operators
   25      LSKFLG          Flag for skew transformation, =0 none, =1 if foll
   26-34   SKWMAT          Skew matrix S (in order S11, S12, S13, S21 etc) if
                           LSKFLG .ne. 0.
   35-37   SKWTRN          Skew translation t if LSKFLG .ne. 0.
                           Skew transformation is from standard orthogonal
                           coordinate frame (as used for atoms) to orthogonal
                           map frame, as
    
                                   Xo(map) = S * (Xo(atoms) - t)
    
   38      future use       (some of these are used by the MSUBSX routines
    .          "              in MAPBRICK, MAPCONT and FRODO)
    .          "   (all set to zero by default)
    .          "
   52          "

   53  MAP         Character string 'MAP ' to identify file type
   54  MACHST      Machine stamp indicating the machine type
               which wrote file
   55      ARMS            Rms deviation of map from mean density
   56      NLABL           Number of labels being used
   57-256  LABEL(20,10)    10  80 character text labels (ie. A4 format)

Symmetry records follow - if any - stored as text as in International
Tables, operators separated by * and grouped into 'lines' of 80
characters (i.e. symmetry operators do not cross the ends of the
80-character 'lines' and the 'lines' do not terminate in a * ).

Map data array follows.

**Note on the machine stamp:** The machine stamp (word 54) is a 32-bit
quantity containing a set of four `nibbles' (half-bytes) - only half
the space is used. Each nibble is a number specifying the representation
of (in C terms) double (d), float (f), int (i) and unsigned char (c)
types. Thus each stamp is of the form 0xdfic0000. For little endian
hardware the stamp is 0x44, 0x41, 0x00, 0x00 while the big endian stamp
is 0x11, 0x11, 0x00, 0x00.

.. _subroutines: 
 
 3) MAP HANDLING SUBROUTINES
--------------------------------------------------

Reading, writing and manipulation of CCP4 format map files is now
handled by a `C library <C_library/cmap_page.html>`__. This library has
a `Fortran-callable API <C_library/cmap_f_page.html>`__. From the point
of view of an application programmer, this API should be identical to
the original set of subroutines. This document originates from the
original Fortran library, but should be applicable to the new library.

High level subroutines are provided to handle 3-D maps. There are
routines to handle the header, to read and write sections (ie a part of
the 3D array corresponding to 1 value of the slow-moving index), and to
handle the symmetry data.

**Summary of routines:**

Nb: longer descriptive names are now also available; calls to
subroutines using the new names have the same arguments as those using
the original names.

Input:

Original name

Function

Alternative name

`MRDHDR <#mrdhdr>`__

open file for reading, read header and print

ccp4_map_read_open_header

`MPOSN <#mposn>`__

position file to a given map section

ccp4_map_read_position_section

`MGULP <#mgulp>`__

read a section of map

ccp4_map_read_whole_section_as_mode

`MGULPR <#mgulpr>`__

read a section of map and convert to real*4 if necessary

ccp4_map_read_whole_section_as_real

`MRCLOS <#mrclos>`__

close input file

ccp4_map_read_close

`CCP4MAPHEAD <#ccp4maphead>`__

read spacegroup, cell and map limits from header

ccp4_map_read_header_only

`CCP4MAPIN <#ccp4maphead>`__

read in the whole map and store in x,y,z order

ccp4_map_read_whole_map

`[MRDLIN] <#mrdlin>`__

[read a line of map]

[ccp4_map_read_line_as_mode]

Output:

Original name

Function

Alternative name

`MWRHDR / MWRHDL <#mwrhdr>`__

open output file and set up header

ccp4_map_write_open_header_by_id / ccp4_map_write_open_header_by _name

`MSPEW <#mspew>`__

write section

ccp4_map_write_all_section

`MWCLOSE / [MCLOSE] / [MCLOSC] <#mwclose>`__

write out header and close file

ccp4_map_write_close_auto / [ccp4_map_write_close_user_sum] /
[ccp4_map_write_close_user_mean]

`CCP4MAPOUT <#ccp4maphead>`__

write out the whole map in x,y,z order

ccp4_map_write_whole_map

`[MWRSEC] <#mwrsec>`__

[write part of array as section]

[ccp4_map_write_part_section]

Symmetry:

Original name

Function

Alternative name

`MSYPUT <#msyput>`__

copy symmetry data from library file to map file

ccp4_map_write_spgname

`MSYWRT <#msyput>`__

write symmetry data to mapfile

ccp4_map_write_symm_matrix

`MSYMOP <#msymop>`__

read symmetry data from map file and convert to matrices and vectors.

ccp4_map_read_symm_matrix

`MSYCPY <#msycpy>`__

copy symmetry data from input map file to output map file

ccp4_map_copy_symmetry

MSYGET

get symmetry operations from library file (nothing to do with map files,
exists in symlib.f)

No alternative name

Skew transformation:

Original name

Function

Alternative name

`MSKPUT <#mskput>`__

put skew transformation into header common block

ccp4_map_write_skew_info

`MSKGET <#mskget>`__

get skew transformation from header common block (Function call)

No alternative name

Titles:

Original name

Function

Alternative name

`MTTCPY <#mttcpy>`__

Copy all titles from previously opened input and output files

ccp4_map_copy_title

`MTTREP <#mttcpy>`__

Replace title in output file

ccp4_map_write_replace_title

In all these routines IUNIT is a logical stream number in the range 1 to
12. These stream numbers are not directly related to the Fortran stream
numbers.

.. _mrdhdr: 
 
 4) THE SUBROUTINES 'MRDHDR' and 'MRDHDS'
----------------------------------------------------------

**Alternative names:** ccp4_map_read_open_header and
ccp4_map_read_open_header_check

The subroutines 'MRDHDR' (_read_open_header) or 'MRDHDS'
(_read_open_header_check) are used to open an input map file and read
the header information. MRDHDS differs from MRDHDR only in the provision
of 2 extra arguments IFAIL & IPRINT which allow for soft failure of file
opening, and control of printing

::


   Call:  CALL MRDHDR(IUNIT,MAPNAM,TITLE,NSEC,IUVW,MXYZ,NW1,NU1,NU2,
         *NV1,NV2,CELL,LSPGRP,LMODE,RHMIN,RHMAX,RHMEAN,RHRMS)

          CALL MRDHDS(IUNIT,MAPNAM,TITLE,NSEC,IUVW,MXYZ,NW1,NU1,NU2,
         *NV1,NV2,CELL,LSPGRP,LMODE,RHMIN,RHMAX,RHMEAN,RHRMS,IFAIL,IPRINT)

Arguments:

::

    
    IUNIT (I)   Map stream number
    
   MAPNAM (I)   Logical file name (type CHARACTER) e.g. 'MAPIN'
    
    TITLE (O)   Map title (CHARACTER*80)
    
     NSEC (O)   Number of sections in the map
    
     IUVW (O)   3 word array with fast, medium, slow axes (1=X, 2=Y, 3=Z)
    
     MXYZ (O)   3 word array with sampling intervals along whole cell on X, Y,
                Z
    
      NW1 (O)   No. of first section
    
      NU1 (O)   Start of section on fast axis (grid units)
    
      NU2 (O)   End of section on fast axis
    
      NV1 (O)   Start of section on medium axis
    
      NV2 (O)   End of section on medium axis
    
     CELL (O)   6 word array for cell dimensions in Angstroms and degrees
    
   LSPGRP (O)   Space group number
    
    LMODE (O)   Map data mode =0, byte
                              =1, INTEGER*2
                              =2, REAL
                              =3, COMPLEX INTEGER*2
                              =4, COMPLEX REAL
                              =5, Treated as mode 0
                Only mode 2 and 0 should occur with CCP4 programs
    
    RHMIN (O)   Minimum density
    
    RHMAX (O)   Maximum density
    
   RHMEAN (O)   Mean density

   RHRMS  (O)   Rms deviation from the mean

The following arguments are for MRDHDS (ccp4_map_read_open_header_check)
only

::


    IFAIL (I/O)  On input:     =0, stop on error
                                =1, return on error
                 On output:    unchanged if no error
                                =-1, error 
     IPRINT (I)                 = 0; silent
                             .ne. 0; print file name, header info etc

.. _mposn: 
 
 5) THE SUBROUTINE 'MPOSN'
------------------------------------------

**Alternative name:** ccp4_map_read_position_section

This subroutine is used to set the position in the map file so that the
next section to be read is section JSEC.

::

    
   Call:  CALL MPOSN(IUNIT,JSEC)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
     JSEC (I)   Position the input map before section JSEC
    

.. _mrdlin: 
 
 6) THE SUBROUTINE 'MRDLIN'
--------------------------------------------

**Alternative name:** ccp4_map_read_line_as_mode

Read the next line from an input map file. The data are returned in the
same form as that stored in the map.

::

    
   Call:  CALL MRDLIN(IUNIT,X,IER)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
        X (O)   Array to contain the line of data read from the map
    
      IER (O)   Error flag =0, OK   non-zero, error or end of file
    

.. _mgulp: 
 
 7) THE SUBROUTINE 'MGULP'
------------------------------------------

**Alternative name:** ccp4_map_read_whole_section_as_mode

This subroutine reads the next whole map section. The data are returned
in the same form as that stored in the map (ie no conversion for
different modes is done).

::

    
   Call:  CALL MGULP(IUNIT,X,IER)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
        X (O)   Array to contain the section of data read from the map
    
      IER (O)   Error flag =0, OK   non-zero, error or end of file
    

.. _mgulpr: 
 
 8) THE SUBROUTINE 'MGULPR'
--------------------------------------------

**Alternative name:** ccp4_map_read_whole_section_as_real

This subroutine reads the next whole map section. For map modes other
than 2 the output data are converted to type=REAL. For complex maps
(mode = 3 or 4) the complex amplitude is returned.

::

    
   Call:  CALL MGULPR(IUNIT,X,IER)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
        X (O)   Array to contain  the  section  of  data  read  from  the  map
                (Returned as REAL values)
    
      IER (O)   Error flag =0, OK   non-zero, error or end of file
    

.. _mrclos: 
 
 9) THE SUBROUTINE 'MRCLOS'
--------------------------------------------

**Alternative name:** ccp4_map_read_close

MRCLOS closes a map file open for reading to re-use the stream.

::


   Call: CALL MRCLOS(IUNIT)

Arguments:

::


    IUNIT (I)   The map stream number

.. _mwrhdr: 
 
 10) THE SUBROUTINES 'MWRHDR' AND'MWRHDL'
----------------------------------------------------------

**Alternative names:** ccp4_map_write_open_header_by_id and
ccp4_map_write_open_header_by_name

These subroutines are used to open an output map file and set up the
header information. The actual header is only written to the file when
the file is closed via the routine `MCLOSE
(ccp4_map_write_close_user_sum) <#mwclose>`__. The only difference
between the two subroutines is that `MWRHDR
(ccp4_map_write_open_header_by_id) <#mwrhdr>`__ does not have a
parameter for the logical file name for which a name of 'MAPOUT' is
assumed.

::


   Call:  CALL MWRHDR(IUNIT,TITLE,NSEC,IUVW,MXYZ,NW1,NU1,NU2,
         *NV1,NV2,CELL,LSPGRP,LMODE)
    
   Call:  CALL MWRHDL(IUNIT,MAPNAM,TITLE,NSEC,IUVW,MXYZ,NW1,NU1,NU2,
         *NV1,NV2,CELL,LSPGRP,LMODE)

Arguments:

::

    
    IUNIT (I)   Map stream number
    
   MAPNAM (I)   Logical  file  name  (type  CHARACTER)  e.g.  'MAPOUT'   (This
                parameter only present for MWRHDL)
    
    TITLE (I)   Map title (CHARACTER*80)
    
     NSEC (I)   Number of sections in the map
    
     IUVW (I)   3 word array with fast, medium, slow axes (1=X, 2=Y, 3=Z)
    
     MXYZ (I)   3 word array with sampling intervals along whole cell on X, Y,
                Z
    
      NW1 (I)   No. of first section
    
      NU1 (I)   Start of section on fast axis (grid units)
    
      NU2 (I)   End of section on fast axis
    
      NV1 (I)   Start of section on medium axis
    
      NV2 (I)   End of section on medium axis
    
     CELL (I)   6 word array for cell dimensions in Angstroms and degrees
    
   LSPGRP (I)   Space group number
    
    LMODE (I)   Map data mode =0, byte for envelopes
                              =1, INTEGER*2
                              =2, REAL  (usual mode)
                              =3, COMPLEX INTEGER*2
                              =4, COMPLEX REAL
                              =5, Treated as mode 0
                 Only mode 2 and 0 should occur with CCP4 programs
    

.. _mspew: 
 
 11) THE SUBROUTINE 'MSPEW'
-------------------------------------------

**Alternative name:** ccp4_map_write_all_section

This subroutine writes the next whole map section. The routine is used
when the section occupies the complete array. The data are written
without translation for different modes.

::


   Call:  CALL MSPEW(IUNIT,X)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
        X (I)   Array holding the map section
    

.. _mwrsec: 
 
 12) THE SUBROUTINE 'MWRSEC'
---------------------------------------------

**Alternative name:** ccp4_map_write_part_section

This subroutine is used to write a map section as part of an array to
the map file.

::

    
   Call:  CALL MWRSEC(IUNIT,X,MU,MV,IU1,IU2,IV1,IV2)
    

Arguments:

::

    
    IUNIT (I)   The map stream number
    
        X (I)   The array holding the map section
    
       MU (I)   The number of points along the whole fast axis
    
       MV (I)   The number of points along the whole medium axis
    
      IU1 (I)   The start array index along the fast axis
    
      IU2 (I)   The finish array index along the fast axis
    
      IV1 (I)   The start array index along the medium axis
    
      IV2 (I)   the finish array index along the medium axis
    

The elements written for a section may be described in FORTRAN notation
as ((X(I,J),I=IU1,IU2),J=IV1,IV2).

.. _mposnw: 
 
 13) THE SUBROUTINE 'MPOSNW'
---------------------------------------------

**Alternative name:** ccp4_map_write_position_section

This subroutine is used to set the position in the map file so that the
next section to be written is section JSEC.

::

    
   Call:  CALL MPOSNW(IUNIT,JSEC)
    

Arguments:

::

    
    IUNIT (I)   Map stream number
    
     JSEC (I)   Position the output map before section JSEC
    

.. _mwclose: 
 
 14) THE SUBROUTINE 'MWCLOSE', 'MCLOSC' and `MCLOSE'
-----------------------------------------------------------------------

**Alternative names:** ccp4_map_write_close_auto,
ccp4_map_write_close_user_sum and ccp4_map_write_close_user_mean

MWCLOSE, MCLOSC & MCLOSE write the header records to the output map file
and close the file. They differ in their handling of the density limits,
mean and rms deviation. You are strongly recommended to use MWCLOSE
(ccp4_map_write_close_user_sum).

Use:-

MWCLOSE (ccp4_map_write_close_auto)
   if the sums for mean & rms are accumulated inside the maplib package
   (in MSPEW/ccp4_map_write_all_section). This works for mode=2 only.
   USE THIS FOR NORMAL PURPOSES.
MCLOSC (ccp4_map_write_close_user_sum)
   when a map file is just being copied, since the RHMEAN & RHRMS
   arguments can be taken directly from the call to
   MRDHDR/ccp4_map_read_open_header. This call is also useful when the
   mean & rms need to be weighted (as in rotation functions), so they
   need to be calculated in the main program.
MCLOSE (ccp4_map_write_close_user_mean)
   if the sums for mean & rms RHSUM & RHSUM2 are accumulated outside the
   maplib package.

::


   Calls:  CALL MWCLOSE(IUNIT)
       CALL MCLOSC(IUNIT,RHMIN,RHMAX,RHMEAN,RHRMS)
       CALL MCLOSE(IUNIT,RHMIN,RHMAX,RHSUM,RHSUM2)

Arguments:

::

    
    IUNIT (I)   The map stream number
    
    RHMIN (I)   The minimum density in the map
    
    RHMAX (I)   The maximum density in the map
    
   RHSUM  (I)   The sum of all the densities in the map (This will be  divided
                internally by the number of points in the map to give the mean
                density which is then stored)
    
   RHSUM2 (I)   The sum of squares of the density values in the map (This will
                used internally to calculate the rms deviation from the mean
                value which is then stored.)
                                      
   RHMEAN (I)   The mean density in the map. This is the same as is returned
                from reading the header
    
   RHRMS  (I)   The rms deviation from the mean density, as stored in the header

.. _msyput: 
 
 15) THE SUBROUTINES 'MSYPUT' and 'MSYWRT'
-----------------------------------------------------------

**Alternative names:** ccp4_map_write_spgname and
ccp4_map_write_symm_matrix

The subroutine MSYPUT reads the symmetry operators file (logical name
SYMOP) and finds the entry for the requested space group. The symmetry
operators are copied from the symmetry operators file to the map file on
stream IUNIT leaving space for the header records.

::

    
   Call:  CALL MSYPUT(IST,LSPGRP,IUNIT)
    

Arguments:

::

    
      IST (I)   The FORTRAN unit number for  reading  the  symmetry  operators
                file (choose a unit number which does not conflict with
            the rest of your program).
    
   LSPGRP (I)   The space group number
    
    IUNIT (I)   The map stream number of the output map file

The subroutine MSYWRT writes symmetry operators to map stream IUNIT Note
that the symmetry operators are written to the file one per line and may
have a different format to those in the SYMOP file

::


   Call: CALL  MSYWRT(IUNIT,NSYM,ROT)

Arguments:

::


          IUNIT    (I)     Map stream number 
          NSYM     (I)     Number of symmetry operators
    ROT(4,4,NSYM)  (I)     rotation/translation matrices

.. _msymop: 
 
 16) THE SUBROUTINE 'MSYMOP'
---------------------------------------------

**Alternative name:** ccp4_map_read_symm_matrix

This subroutine reads the symmetry operators from an input map file
(after calling `MRDHDR /ccp4_map_read_open_header <#mrdhdr>`__). The
operators are converted and returned as matrices.

::

    
   Call:  CALL MSYMOP(IUNIT,NSYM,ROT)
    

Arguments:

::

    
    IUNIT (I)   The map stream number
    
     NSYM (O)   The number of symmetry operators
    
      ROT (O)   The array in which the symmetry operators are returned as  4*4
                matrices. It should be dimensioned as ROT(4,4,NS) where NS  >=
                NSYM (usually NS = 96)
    

.. _msycpy: 
 
 17) THE SUBROUTINE 'MSYCPY'
---------------------------------------------

**Alternative name:** ccp4_map_copy_symmetry

This subroutine copies the symmetry operators from an input map file to
an output map file after calls to `MRDHDR
(_read_open_header) <#mrdhdr>`__ and `MWRHDR
(_write_open_header_by_id) <#mwrhdr>`__ (or `MWRHDL /
_write_open_header_by_name <#mwrhdr>`__). Note that if you have more
than one input file, this routine should be called after MRDHDR for the
file from which you want to copy the symmetry operations, and before
calling MRDHDR for any other file.

::

    
   Call:  CALL MSYCPY(IN,IOUT)
    

Arguments:

::

    
       IN (I)   The input map stream number
    
     IOUT (I)   The output map stream number
    

.. _mskput: 
 
 18) THE SUBROUTINE 'MSKPUT'
---------------------------------------------

**Alternative name:** ccp4_map_write_skew_info

This subroutine writes a skew transformation to an output map header
block (after a call to `MWRHDR (_write_open_header_by_id) <#mwrhdr>`__
or `MWRHDL (_write_open_header_by_name) <#mwrhdr>`__ ).

NB *** Use of MSKPUT & MSKGET has not been generally implemented in
the CCP4 suite ***

::

    
   Call:  CALL MSKPUT(SKWMAT,SKWTRN)
    

Arguments:

::

    
   SKWMAT (I)   3*3 Skew rotation matrix
    
   SKWTRN (I)   3 word Skew translation matrix
    

.. _mskget: 
 
 19) THE FUNCTION 'MSKGET'
-------------------------------------------

**Alternative name:** None.

This function reads a skew transformation from an input map header block
(after a call to `MRDHDR (_read_open_header) <#mrdhdr>`__ ).

NB *** Use of MSKPUT & MSKGET has not been generally implemented in
the CCP4 suite ***

::

    
   Call:  I = MSKGET(SKWMAT,SKWTRN)
    

Arguments:

::

    
   SKWMAT (O)   3*3 Skew rotation matrix
    
   SKWTRN (O)   3 word Skew translation matrix
    

Returns MSKGET =1, skew transformation present, =0, absent

.. _mttcpy: 
 
 20) THE SUBROUTINES `MTTCPY' and `MTTREP'
-------------------------------------------------------------

**Alternative names:** ccp4_map_copy_title and
ccp4_map_write_replace_title

MTTCPY (ccp4_map_copy_tile) copies all titles from a previously opened
input to the previously opened output file, adding the new TITLE to the
end or overwriting the last one if there are already 10 (the maximum
which can be stored).

::


   Call:  CALL MTTCPY(TITLE)

Arguments:

::


   TITLE (I)  CHARACTER variable holding the new title (<=80 characters)

MTTREP (ccp4_map_write_replace_title) replaces the NT-th title in the
output file (after calls to `MWRHDR /
ccp4_map_read_open_header <#mwrhdr>`__ and MTTCPY /
ccp4_map_copy_title), adding a new TITLE to the end or overwriting the
last one if there are already 10.

::

    
   Call:  CALL MTTREP(TITLE,NT)

Arguments:

::


   TITLE (I)  CHARACTER variable holding the new title

      NT (I)  INTEGER number of title to be replaced

.. _mrfnam: 
 
 21) THE SUBROUTINES 'MRFNAM' and 'MWFNAM'
-----------------------------------------------------------

**Alternative names:** ccp4_map_get_last_read_filename and
ccp4_map_get_last_write_filename

These routines may be called to return the full filename for a file open
for reading (MRFNAM / ccp4_map_get_last_read_filename) or writing
(MWFNAM / ccp4_map_get_last_write_filename).

::


   Call: CALL MRFNAM(FNAME)
         CALL MWFNAM(FNAME)

Arguments:

::


     FNAME (O) file name of last file opened by MRDHDR/MRDHDS or by
           MWRHDR/MWRHDL

.. _ccp4maphead: 
 
 22) THE SUBROUTINES 'CCP4MAPHEAD', 'CCP4MAPIN' AND 'CCP4MAPOUT'
--------------------------------------------------------------------------------------

**Alternative names:** ccp4_map_read_header_only,
ccp4_map_read_whole_map, and ccp4_map_write_whole_map.

These are Kevin Cowtan's "wrapper" routines which provide an interface
to other maplib subroutines in order to easily read and write entire
maps. CCP4MAPHEAD obtains the map limits from the map file, and
CCP4MAPIN and CCP4MAPOUT then use this information to respectively read
in and write out an entire map in one go.

CCP4MAPHEAD reads header information from a map file. It is used to get
the map limits before calling CCP4MAPIN.

::

     Call: CALL ccpmap4head(iunit,name,nspgrp,cell,nu,nv,nw,nu1,nv1,nw1,nu2,
          +                 nv2,nw2)

Arguments:

::

     iunit   (I)  Map stream number (integer)
     name    (I)  Logical file name (type character*8) e.g.'MAPIN'   
     nspgrp  (O)  Space group number (integer)
     cell    (O)  6 word array for cell dimensions in Angstroms and
                  degrees (real)
     nu      (O)  Sampling interval along whole cell on X (integer)
     nv      (O)  Sampling interval along whole cell on Y (integer)
     nw      (O)  Sampling interval along whole cell on Z (integer)
     nu1     (O)  Start of map on X axis, in grid units (integer)
     nv1     (O)  Start of map on Y axis, in grid units (integer)
     nw1     (O)  Start of map on Z axis, in grid units (integer)
     nu2     (O)  End of map on X axis (integer)
     nv2     (O)  End of map on Y axis (integer)
     nw2     (O)  End of map on Z axis (integer)

Note that this differs from
`ccp4_map_read_open_header_check <#mrdhdr>`__ [MRDHDS] (which it calls)
in that the file is not left open but is closed (by a call to
`ccp4_map_read_close <#mrclos>`__ [MRCLOS]) before the subroutine
terminates. Note also that the map limits are returned in x,y,z order
rather than in fast, medium, slow order.

CCP4MAPIN reads a whole map into an array and stores it in x,y,z order.
The map limits are required as input to dimension the array holding the
map, and can be obtained with a call to the subroutine CCP4MAPHEAD.

::

     Call: CALL ccp4mapin (iunit,name,title,map,nu1,nv1,nw1,nu2,nv2,nw2)

Arguments:

::

     iunit   (I)  Map stream number (integer)
     name    (O)  logical file name (type character) e.g. 'MAPIN'
     title   (O)  Map title (type character)
     map     (O)  Real array of dimension (nu1:nu2,nv1:nv2,nw1:nw2)
                  which stores the map which is read in
     nu1     (I)  Start of map on X axis, in grid units (integer)
     nv1     (I)  Start of map on Y axis, in grid units (integer)
     nw1     (I)  Start of map on Z axis, in grid units (integer)
     nu2     (I)  End of map on X axis (integer)
     nv2     (I)  End of map on Y axis (integer)
     nw2     (I)  End of map on Z axis (integer)

CCP4MAPIN utilises calls to the following maplib routines:

-  `ccp4_map_read_open_header_check <#mrdhdr>`__ [MRDHDS],
-  `ccp4_map_read_whole_section_as_real <#mgulpr>`__ [MGULPR], and
-  `ccp4_map_read_close <#mrclos>`__ [MRCLOS].

CCP4MAPOUT writes out a whole map in x,y,z order.

::

     Call: CALL ccp4mapout(iunit,name,title,map,nspgrp,cell,nu,nv,nw,nu1,
          +                nv1,nw1,nu2,nv2,nw2)

Arguments:

::

     iunit   (I)  Map stream number (integer)
     name    (I)  Logical file name (type character) e.g.'MAPIN'   
     title   (I)  Map title (type character)
     map     (I)  Real array of dimension (nu1:nu2,nv1:nv2,nw1:nw2)
                  which stores the map being written out
     nspgrp  (I)  Space group number (integer)
     cell    (I)  6 word array for cell dimensions in Angstroms and degrees (real)
     nu      (I)  Sampling interval along whole cell on X (integer)
     nv      (I)  Sampling interval along whole cell on Y (integer)
     nw      (I)  Sampling interval along whole cell on Z (integer)
     nu1     (I)  Start of map on X axis, in grid units (integer)
     nv1     (I)  Start of map on Y axis, in grid units (integer)
     nw1     (I)  Start of map on Z axis, in grid units (integer)
     nu2     (I)  End of map on X axis (integer)
     nv2     (I)  End of map on Y axis (integer)
     nw2     (I)  End of map on Z axis (integer)

CCP4MAPOUT utilises calls to the following maplib subroutines:

-  `ccp4_map_write_open_header_by_name <#mwrhdr>`__ [MWRHDL],
-  `ccp4_map_write_symm_matrix <#msyput>`__ [MSYWRT],
-  `ccp4_map_write_all_section <#mspew>`__ [MSPEW],
-  `ccp4_map_write_close_auto <#mwclose>`__ [MWCLOSE].

There is also a call to the symlib routine MSYMLB.

.. _calling_sequence: 
 
 23) TYPICAL CALLING SEQUENCES
---------------------------------------------------------

Typical calling sequences are as follows:

Write new file

`MWRHDR/MWRHDL <#mwrhdr>`__

ccp4_map_write_open_header_by_id / ccp4_map_write_open_header_by_name

`MSYPUT <#msyput>`__

ccp4_map_write_spgname

`MSPEW <#mspew>`__ repeated for each section

ccp4_map_write_all_section

`MWCLOSE[/MCLOSE/MCLOSC] <#mwclose>`__

ccp4_map_write_close_auto [/ccp4_map_write_close_user_sum /
ccp4_map_write_close_user_mean]

Read file

`MRDHDR <#mrdhdr>`__

ccp4_map_read_open_header

`MSYMOP <#msymop>`__

ccp4_map_read_symm_matrix

`MPOSN <#mposn>`__ to 1st section

ccp4_map_read_position_section

`MGULP <#mgulp>`__ (or `MGULPR <#mgulpr>`__) repeated for each section

ccp4_map_read_whole_section_as_mode (or
ccp4_map_read_whole_section_as_real)

Read file, write modified file

`MRDHDR <#mrdhdr>`__

ccp4_map_read_open_header

`MWRHDR/MWRHDL <#mwrhdr>`__

ccp4_map_write_open_header_by_id / ccp4_map_write_open_header_by_name

`MSYMOP <#msymop>`__

ccp4_map_read_symm_matrix

`MSYCPY <#msycpy>`__

ccp4_map_copy_symmetry

| `MGULP <#mgulp>`__ ) repeated for each section
| `MSPEW <#mspew>`__ )

| ccp4_map_read_whole_section_as_mode )
| ccp4_map_write_all_section )

`MWCLOSE[/MCLOSE/MCLOSC] <#mwclose>`__

ccp4_map_write_close_auto [/ccp4_map_write_close_user_sum /
ccp4_map_write_close_user_mean

Read and write whole map

`CCP4MAPHEAD <#ccp4maphead>`__ to get map limits

ccp4_map_read_header_only

`CCP4MAPIN <#ccp4maphead>`__

ccp4_map_read_whole_map

`CCP4MAPOUT <#ccp4maphead>`__ once map has been manipulated

ccp4_map_write_whole_map

.. _implementation: 
 
 24) IMPLEMENTATION NOTES
--------------------------------------------------

The map functions (modes other than 2 or 4) requiring non standard code
are handled via the routines CCPBYT, CCPTOI, CCPBYI, CCPGTB etc. in
CCPLIB and through the DISKIO routines. In particular, use packed
integer arrays for handling masks/envelopes. The convention for masks is
that non-zero represents `true', i.e. protein region, and 0 false, i.e.
solvent region; normally only 0 and 1 should be used for masks.
