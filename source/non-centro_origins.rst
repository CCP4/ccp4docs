.. container:: Section1

   .. rubric:: Alternative Origins for Spacegroups in Macromolecular
      Crystallography (from CCP4)
      :name: alternative-origins-for-spacegroups-in-macromolecular-crystallography-from-ccp4

   .. container:: MsoNormal

      --------------

   .. rubric:: *TRICLINIC*
      :name: triclinic

   .. rubric:: P1
      :name: p1

   The origin is arbitrary

    

   .. rubric:: *MONOCLINIC*
      :name: monoclinic

   .. rubric:: P2(1) - C2
      :name: p21---c2

   The origin is not fixed along the **B** axis

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      3             0.5000 0.0000 0.0000
      4             0.5000 0.0000 0.5000
      ============= ====== ====== ======

    

   .. rubric:: *ORTHORHOMBIC*
      :name: orthorhombic

   .. rubric:: P222(1) - P21212(1) - C2221 - C222 - F222 - I222 -
      I212121
      :name: p2221---p212121---c2221---c222---f222---i222---i212121

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      3             0.0000 0.5000 0.0000
      4             0.0000 0.5000 0.5000
      5             0.5000 0.0000 0.0000
      6             0.5000 0.0000 0.5000
      7             0.5000 0.5000 0.0000
      8             0.5000 0.5000 0.5000
      ============= ====== ====== ======

   .. rubric:: *TETRAGONAL*
      :name: tetragonal

   .. rubric:: P4(123) - I4(1)
      :name: p4123---i41

   The origin is not fixed along the **C** axis

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.5000 0.5000 0.0000
      ============= ====== ====== ======

   .. rubric:: P4(123)2(1)2 - I4(1)22
      :name: p4123212---i4122

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      3             0.5000 0.5000 0.0000
      4             0.5000 0.5000 0.5000
      ============= ====== ====== ======

   .. rubric:: *TRIGONAL*
      :name: trigonal

   .. rubric:: P3(12) - R3
      :name: p312---r3

   The origin is not fixed along the **C** axis

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.3333 0.6667 0.0000
      3             0.6667 0.3333 0.0000
      ============= ====== ====== ======

   .. rubric:: P3(12)12
      :name: p31212

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      3             0.3333 0.6667 0.0000
      4             0.3333 0.6667 0.5000
      5             0.6667 0.3333 0.0000
      6             0.6667 0.3333 0.5000
      ============= ====== ====== ======

   .. rubric:: (trigonal continuing)
      :name: trigonal-continuing

   .. rubric:: P3(12)21 - R32
      :name: p31221---r32

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      ============= ====== ====== ======

    

   .. rubric:: *HEXAGONAL*
      :name: hexagonal

   .. rubric:: P6(12345)
      :name: p612345

   The origin is not fixed along the **C** axis

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      ============= ====== ====== ======

   .. rubric:: P6(12345)22
      :name: p61234522

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.0000 0.0000 0.5000
      ============= ====== ====== ======

    

   .. rubric:: *CUBIC*
      :name: cubic

   .. rubric:: P2(1)3 - F23 - I2(1)3 - P4(123)32 - F4(1)32 - I4(1)32
      :name: p213---f23---i213---p412332---f4132---i4132

   .. container::

      ============= ====== ====== ======
      **Norigin** **Xo** **Yo** **Zo**
      1             0.0000 0.0000 0.0000
      2             0.5000 0.5000 0.5000
      ============= ====== ====== ======

    
