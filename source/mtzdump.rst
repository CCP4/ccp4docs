MTZDUMP (CCP4: Supported Program)
=================================

NAME
----

**mtzdump, mtzdmp** - dump data from an MTZ reflection data file

SYNOPSIS
--------

| **mtzdump hklin** *foo.mtz*
| [`Keyworded input <#keywords>`__]

.. _description: 
 
DESCRIPTION
---------------------------------

MTZDUMP is used to give a file dump of a standard `MTZ' reflection data
file. The file header information is printed followed by a summary of
the reflection data which gives, for each data column, the following
information:

   Column number
   Sort order (asc=ascending, desc=descending, both=all values the
   Minimum value present
   Maximum value present
   Number of missing values present (identified by MNF)
   Percentage completeness *i.e.* (measured/(measured + missing)) x 100
   Average value excluding missing values
   Average absolute value excluding missing values
   Low resolution limit of data present
   High resolution limit of data present
   Column type
   Column label

The completeness column in the statistical header information calculates
the percentage of measured reflections over the total number in the file
(where a measured reflection is anything which is not a Missing Number
Flag (MNF)). This is NOT necessarily the completeness of the data to the
maximum resolution. In order for this to be the case you must use the
`uniqueify <unique.html>`__ script to generate a complete set of indices
to the maximum resolution.

The summary is followed by a compact but tabulated listing of the
individual reflection data. The output format depends on the HKLIN type
and also the options specified. If the data is real then the format
depends on the size of the number. The largest value that can be written
is 99999999. Missing data, represented internally in the MTZ file by a
MNF, are listed as '?'. This makes the output from MTZDUMP very
difficult to read fortranically as an ASCII file. Use
`MTZ2VARIOUS <mtz2various.html>`__ to produce an ASCII file from MTZ.

'mtzdmp' (no `u') is a Unix script (in $CETC) that takes as argument a
file name (with extension defaulting to .mtz) and optional other
arguments and runs mtzdump. This is a useful abbreviation and allows,
for instance, piping the output through `more'. Note that not all
options available for mtzdump are available for mtzdmp. See
`below <#mtzdmp>`__.

.. _keywords: 
 
KEYWORDED INPUT
----------------------------------

All keyworded input is optional.

Available keywords:

   `BATCH <#batch>`__, `END <#end>`__, `GO <#end>`__,
   `HEADER <#header>`__, `LRESO <#lreso>`__,
   `NREFLECTIONS <#nreflections>`__, `RESOLUTION <#resolution>`__,
   `RUN <#end>`__, `SKIP <#skip>`__, `STARTHKL <#starthkl>`__,
   `STATS <#stats>`__, `SYMMETRY <#symmetry>`__

.. _resolution: 
 
RESO <hires> [<lowres>]
   followed by high and low resolution limits in Angstroms. This option
   restricts the list of reflections output to be within the given
   limits. If only one number input then this is treated as the high
   resolution limit. (Default: no resolution limit restrictions.)
.. _header: 
 
HEADER
   print header information for the MTZ file only. No reflections are
   read or listed, and no statistics produced.
.. _nreflections: 
 
NREF <nref>
   followed by number of reflections to be listed. If negative, then no
   summary table is printed. If -1, then ALL reflections will be dumped.
   (Default 10.)
.. _starthkl: 
 
STARTHKL <H0> <K0> <L0>
   followed by H0 K0 L0, the HKL indices of the first reflection to
   list. If H, K and L are all zero (the default), start at the first
   reflection in the file.
.. _batch: 
 
BATCH
   print orientation block information for each batch. (Default: no
   batch orientation information printed.)
.. _stats: 
 
STATS NBIN <nbin> [ RESO <hires> [<lowres>] ]
   gives the no. of resolution bins and overall resolution limits used
   in constructing the FILE STATISTICS tables. If <nbin> = 1, only
   overall summary table is given, but if <nbin> > 1, both overall and
   partial summary tables are given. (Default is one bin and all
   reflections.)
.. _lreso: 
 
LRESO
   this means that the listing will include 4sin**2/Lambda**2 for each
   reflection.
.. _symmetry: 
 
SYMMETRY
   print the symmetry information contained in the header. (Default: no
   symmetry information printed.)
.. _skip: 
 
SKIP <nskip>
   followed by the number of reflections to skip before listing
   reflections (default 0).
.. _end: 
 
END | GO | RUN
   Terminate input and start the dump (alternative to end of file).

.. _mtzdmp: 
 
mtzdmp
-----------------------

There is a shell script for running mtzdump as mtzdmp <filename>
[options] (in $CETC). This script obviates the necessity of printing
hklin on the command line as well as being able to input keyword
information on the command line as in the examples below. Type "mtzdmp
-h" for listing of options.

::

    
      mtzdmp File_Name -n Number  : number of hkl to dump (def 10) 
      mtzdmp File_Name -e         : print header information only
      mtzdmp File_Name -b         : dump BATCH headers
      mtzdmp File_Name -s         : dump SYMMETRY info
      mtzdmp File_Name -bs (-sb)  : dump both SYMM and BATCH
      mtzdmp File_Name -r Rmax (Rmin) : resolution limits
      mtzdmp File_Name -i         : Interactive
    

AUTHORS
-------

Eleanor Dodson and Sandra McLaughlin

SEE ALSO
--------

`mtz2various <mtz2various.html>`__, `unique <unique.html>`__

.. _examples: 
 
EXAMPLES
---------------------------

Following examples in directory $CEXAM/unix/runnable :

-  `mtzdump.exam <../examples/unix/runnable/mtzdump.exam>`__
