COMPAR (CCP4: Unsupported Program)
==================================

NAME
----

**compar** - comparison of coordinates.

SYNOPSIS
--------

| **compar XYZIN1** *foo_in1.pdb* **XYZIN2** *foo_in2.pdb* [ **XYZIN3**
  *foo_in3.pdb* ] **RMSTAB** *foo_out.rms*
| [Input cards]

.. _description: 
 
DESCRIPTION
---------------------------------

Mini prog to compar 2 or 3 sets of atomic coordinates.

.. _files: 
 
INPUT AND OUTPUT FILES
--------------------------------------

Input
~~~~~

2 or 3 coordinate files in PDB format assigned to XYZIN1, XYZIN2, [
XYZIN3 ].

Output
~~~~~~

A file assigned to RMSTAB which can be used for SQUID. This file lists,
for each residue, the rms difference in coordinates and in B-factor for
main chain atoms and for side chain atoms.

.. _input-1:

INPUT
-----

Input is not keyworded. The following 3 lines of input are expected:

#. <title>
#. <Nsets> - number of coordinate files for comparison (2 or 3).
#. <Del(xyz)> <DEl(b) > - coordinates are monitored if Del(xyz) or
   DEl(b) are greater than these values.

For example:

::


       Comparison before and after refinement.
       2
       3.0 20.0

EXAMPLES
--------

Unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`compar.exam <../examples/unix/runnable/compar.exam>`__
