#!/usr/bin/env python3

# Note it's only really written to do one folder at a time & puts the modified rst's in new folder.
# Run this with something like (to run over lots of files - maxdepth restricts recursion) :-
# find . -maxdepth 1 -name "*.rst" | while read fname; do python3 fixit3.py ${fname}; done
# ( Can pipe into wc -l instead to check num. files.)
# or : python3 fixit3.py filetocorrect.rst
#
# To move files around use something like (eg. line below removes the mod bit):
# for file in mod_*.rst; do   mv -i "$file" "${file#mod_}"; done
#
# nb. recommend always checking the output with a good diff. This script will try to correct missing links in the
# rst files, but some files can be quite irregular (it depends highly on the style/correctness of the
# original hand-written documentation used to gen the rst files).
# 

import os
import re
import argparse

indshft = 0  # index shift for inserting lines.
insk = []    # List of internal links to insert
synkeys = []
keys = []
    
def main():
    global indshft
    global keys
    global synkeys
    global insk
    # cmd line stuff
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('infile', type=str, help='Input file (rst)')
    args = parser.parse_args()
    # script proper
    issynopsis = False
    keyword = False
    keyzone = False
    lnkzone = False
    print("rst File to correct is :- ", args.infile)
    infile = open(args.infile, 'r')
    alins = infile.readlines()
    for ind, lin in enumerate(alins):
        synkeyed = False
        if FindinTitle("SYNOPSIS", ind, lin, alins):
            print("Found SYNOPSIS")
            issynopsis = True
        if issynopsis:
            # Add the found synopsis keys to list
            AssembleSynkeys(lin)
        # Check for end of synopsis
        if (lin.isspace() or "") and len(synkeys) > 0 and issynopsis:
            print("-----===== synopsis links read =====-----\n")
            issynopsis = False
        # Check for end of keyzone (swapped to up here so checked before flagset)
        # if ((lin.isspace() or "") and len(keys) > 0 and keyzone):
        # Find any title after keyzone true, prev. blank line.
        if (FindinTitle("", ind, lin, alins) and len(keys) > 0 and keyzone):
            print("-----===== keyword links read =====-----\n")
            keyzone = False
            lnkzone = True
        for skey in synkeys:
            if FindinTitle(skey[0], ind, lin, alins):
                if skey[0] in "KEYWORDED INPUT": # if they called it something random this won't work obv.
                    keyzone = True
                if skey[1]:
                    insertlink(ind, skey[1])
                synkeyed = True
        if keyzone:
            # Add the found keys to list
            retl = AssembleKeys(lin)
            if retl:
                print("Replace", alins[ind], "with", retl)
                alins[ind] = retl
        if lnkzone and not synkeyed:
            # Find the places to insert keyword links
            for key in keys:
                if FindinTitle(key, ind, lin, alins):
                    insertlink(ind, key)
                    break # First one only
    infile.close()
    # New file to store old vrs, switch to
    print("\nPreparing to write out corrected file (nb. recommend manual check)")
    print("\nThe links found were :-")
    print(synkeys)
    print(keys)
    print("// End Scan")
    if not os.path.isdir("new"):
        os.mkdir("new")
    nfil = "mod_" + os.path.split(args.infile)[1]
    outfile = open(os.path.join(os.path.split(args.infile)[0], "new", nfil), 'w')
    # write the links into the text and save it to new file. nb. this is the links part, some line adjusts done above.
    for inl in insk:
        alins.insert(inl[0], inl[1])
    outfile.write(''.join(alins))
    outfile.close()
    print("// End Write\n")

def FindinTitle(word, ind, line, alines):
    # Is this word in a title line (~ & - rst tyles).
    try:
        # As a side note, there are actually trailing nonbreaking spaces in some files hence the \xa0 ...ffs..
        bEndClr = ((word + " " in line.replace("\xa0", " ")) or (word + "\n" in line))
        bStartClr = (line.startswith(word) or (" " + word in line))
        # Also check next line consistent with a title (check *after* first two to avoid most excepts.)
        if bEndClr and bStartClr and (("~~~" in alines[ind+1]) or ("---" in alines[ind+1])):
            # Check not a sub-keyword
            if ((word + "]" in line) or ("[" + word in line)):
                return False
            else:
                return True
        else:
            return False
    except:
        print("Failed to find a title line because no line after word found. Don't worry, usually not a problem.")
        print("FAIL: Line num ", ind, ", Line is:-", line)

def AssembleSynkeys(line):
    global synkeys
    reout = re.findall("[\[']([a-zA-Z\s]+)|\<\#([a-zA-Z_-]+)\>", line)
    if len(reout) != 2:
        return
    ot1 = reout[0][0].strip().upper() # strip trailing space & go upper.
    try:
        synkeys.append([ot1, reout[1][1]]) # if it's not in here, not worth bothering about
    except:
        print("Failed to find a properly written link in the rst") # shouldn't get here ! (see if above)

def AssembleKeys(line):
    global keys
    foundkeys_type1 = re.findall("**([A-Z\s_-]+)**|\<\#([a-zA-Z_-]+)\>", line) # Ok, sometimes like this, but not always.
    fkeys1 = ScrapeKeys(foundkeys_type1)
    if fkeys1:
        keys +=  fkeys1
    else:
        foundkeys_type2 = re.findall("[\[']([a-zA-Z\s-]+)|\<\#([a-zA-Z_-]+)\>", line)
        fkeys2 = ScrapeKeys(foundkeys_type2)
        if fkeys2:
            keys += fkeys2
    # Hopefully there's not too much non-standard stuff around !
    if fkeys1:
        print("Removing rogue stars from keywords", fkeys1)
        nline = re.sub("**", "", line)
        return nline
    else:
        return None

def ScrapeKeys(foundkeys):
    # Scrape keys from Regex return
    rkey = []
    bEven = not bool(len(foundkeys)%2)
    if bEven and len(foundkeys) > 0:
        for ckey in zip(foundkeys[::2], foundkeys[1::2]):
            if ckey[0][0]:
                rkey += [ckey[0][0].strip()]
        return rkey
    else:
        return None

def insertlink(indx, lnk):
    # indx - where to put the line in the lines list, & lnk the unformatted link key (indshft is global)
    global indshft
    global insk
    print("Inserting link for ", lnk.strip().upper())
    insk.append([indx + indshft, formatlink(lnk)]); indshft += 1
    insk.append([indx + indshft, "\n"]); indshft += 1

def formatlink(lnk):
    # Return a link line in rst format
    return ".. _" + lnk.lower() + ":\n"

if __name__ == '__main__':
    main()
