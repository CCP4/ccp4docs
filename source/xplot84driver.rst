xplot84driver (CCP4: Supported Program)
=======================================

NAME
----

**xplot84driver** - X-windows tool, a viewer for Plot84 meta files.

SYNOPSIS
--------

**xplot84driver** *plot84_meta_file*

DESCRIPTION
-----------

xplot84driver belongs to the XCCPJiffy class of simple X11 graphic based
programs. xplot84driver reads a Plot84 Metafile, recognises pictures and
interprets them.

xplot84driver uses an X-Windows device and has an option to create a
PostScript file.

Running the program xplot84driver creates a Header window with boxes
showing:

Quit
   to exit the program
(unnamed)
   box containing current Plot84 meta file name
List of Pictures
   A list of the pictures stored within the Plot84 metafile. Select the
   picture to view with the Left Mouse Button. This generates the view
   window and QUIT in that window closes the window. PLOT creates a
   PostScript version. (If you want a file then go to the CONTROL panel
   first.)
Control Panel
   Clicking the Left Mouse button here allows one to toggle between
   Portrait and Landscape for the output Postscript file conversion and
   the option to specify a Output File for the Postscript file. The
   Print command is controlled by the XCCPJiffy*psPlotCommand resource.
   The Quit button exits the control panel.

NOTE ON COLOURS AND LINE STYLES - For Users and Programmers
-----------------------------------------------------------

Where a program allows a user to specify colours in numbers (for example
PLUTO), then the user should be aware of the mapping of colour numbers
to real colours and to line properties of a black/white PostScript Laser
printer. These are controlled via the entries in the X-resource
database.

xplot84driver works in colour/linewidth graphic mode, i.e. maps colour
numbers given by plot84 file contents to real colours for X graphics and
Postscript line attributes. Currently 10 colours are available.

X colours, Postscript line attributes and width are controlled via the
entries in X-resource database (see X manual). It is recommended to load
X-resources into the X-server database (xrdb). Once the database is
loaded, one can optimise it using xrdb to load/merge a personalised set
of X-Application Resources.

X mapping is controlled by:

::

   XCCPJiffy.plot84Picture*draw2d.background:      white
   XCCPJiffy.plot84Picture*draw2d.color1:          black
      .
      .
      .
   XCCPJiffy.plot84Picture*draw2d.col or10:         indigo
   Line width is interpreted directly.

Postscript mapping is controlled by:

::

   XCCPJiffy*psColor1:     1 setlinejoin [] 0 setdash
      .
      .
      .
   XCCPJiffy*psColor10:    1 setlinejoin [8 1] 0 setdash
   Line width is calculated as:
      value from plot84matefile * XCCPJiffy*psBasicLineWidth resource

| To check your current setting use for instance
| xrdb -query | grep XCCPJiffy | more

SEE ALSO
--------

X(1), xrdb(1)

ENVIRONMENT
-----------

DISPLAY to figure out which display to use.

AUTHOR
------

Jan Zelinka - York University
