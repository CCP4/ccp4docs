REFINDEX (CCP4: Unsupported Program)
====================================

NAME
----

**refindex** - re-index dataset maximising correlation with reference
dataset or to the conventional setting in International Tables.

SYNOPSIS
--------

**refindex  [HKLIN** or **HKLIN1** *working.mtz*]  [**HKLREF** or
**HKLIN2** *reference.mtz*]  [**HKLOUT** *output.mtz*]
.. _description: 
 


DESCRIPTION
-----------

"refindex" automatically re-indexes an MTZ dataset, either to an
existing reference dataset or, if no reference dataset is specified, to
the conventional setting given in volume A of International Tables (see
Section 9.3 of Volume A, 5th edition or later).  In the former case, the
required re-indexing operator is first found by maximising the
correlation coefficient between the two datasets against all possible
re-indexings of the working dataset. In the latter case, if no working
MTZ dataset is specified, the input cell and space group are read from
standard input, the re-indexing operator and the transformed cell and
space group symbol (if different) are printed, but no actual re-indexing
is done.

Note that in each case it is assumed that the point and space groups as
read from the MTZ file, or the space group as read from standard input,
are the correct ones (though interconversion between different settings
of the same space group, such as between C2 and I2 or between
P22 :sup:`1`2 :sup:`1` and P2 :sup:`1`2 :sup:`1`2) may be
performed when re-indexing to a conventional cell).  In no circumstances
is any attempt made to re-index into a different point group.  In each
case, if a working MTZ dataset is specified and the re-indexing operator
is not (h,k,l), the re-indexing is applied to the working dataset and
the result output to a new file. .. _input: 
 


INPUT
-----

If at least one input MTZ file is specified, then nothing is read from
standard input.  If no MTZ file is specified, the program prompts for
the cell parameters and then the space group name or number (on a
separate line) from standard input.  After printing the transformed
cell, and in the case of transformation between different settings the
transformed space group symbol, the prompt for a cell is repeated: the
program is terminated by Ctrl-D. .. _files: 
 


INPUT AND OUTPUT FILES
----------------------

The following input and output files are used by the program:

Input Files:

HKLIN or HKLIN1 (optional - omit to transform input cell & space group to conventional cell)
   Input MTZ file containing the dataset to be re-indexed. The required
   column labels are found automatically; they must either be F & SIGF,
   or a matching pair of the form F_xxx SIGF_xxx (where xxx is any
   character string).  Only one Fobs sigma(Fobs) pair may be present in
   the file.
HKLREF or HKLIN2 (optional - omit to re-index to conventional setting)
   Input MTZ file containing the dataset to be used as a reference. The
   required column labels are found automatically; they must either be F
   & SIGF, or a matching pair of the form F_xxx SIGF_xxx.  Only one Fobs
   sigma(Fobs) pair may be present in the file.

Output Files:

HKLOUT
   Output file that contains the re-indexed dataset.  If no re-indexing
   is needed then no output file is produced.

NOTES
-----

In point groups 422, 622 and 432 only one indexing is possible, so
re-indexing to a reference dataset in these point groups does nothing.

In crystal systems trigonal, tetragonal, hexagonal and cubic, the cell
is unique (though the indexing may not be), so re-indexing to a
conventional cell does nothing.

PROBLEMS
--------

At present the program will stop if phase or H-L coefficient columns are
present in the HKLIN1 file.

SEE ALSO
--------

`almn <almn.html>`__ - Identifies re-indexing operator from
cross-rotation function.

`othercell <othercell.html>`__ - Finds all possible alternative cells in
all crystal systems.

`pointless <pointless.html>`__ - Automatic re-indexing to reference
dataset.

`reindex <reindex.html>`__ - Re-indexes dataset once re-indexing
operator is known.

AUTHORS
-------

Ian Tickle, Astex Therapeutics Ltd.
